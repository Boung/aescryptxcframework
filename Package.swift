// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "WingKYCRegisterSPM",
  platforms: [
    .iOS(.v12)
  ],
  products: [
    .library(name: "WingKYCRegisterSPM", targets: [
      "WingKYCRegisterSPM",
      "WingKYCRegisterFormSDK"
    ]),
  ],
  dependencies: [
    
  ],
  targets: [
    .target(name: "WingKYCRegisterSPM", dependencies: [
      "GoogleMaps",
      "GoogleMapsCore",
      "GoogleMapsBase",
      "WingKYCRegisterFormSDK",
    ]),
    .binaryTarget(name: "GoogleMaps", path: "./GoogleMaps.xcframework"),
    .binaryTarget(name: "GoogleMapsCore", path: "./GoogleMapsCore.xcframework"),
    .binaryTarget(name: "GoogleMapsBase", path: "./GoogleMapsBase.xcframework"),
    .binaryTarget(name: "WingKYCRegisterFormSDK", path: "./WingKYCRegisterFormSDK.xcframework"),
  ]
)
