//
//  KYCRegisterSDKManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 2/11/21.
//

import UIKit
@_implementationOnly import GoogleMaps

public class KYCRegisterSDKManager {
    public init(baseURL: String, authentication: AuthenticationModel, language: LanguageEnum, googleAPIKey: String) {
        Constants.baseURL = baseURL
        Constants.authentication = authentication
        Constants.applicationLanguage = language
        Constants.googleAPIKey = googleAPIKey
        
        getInitialComponents()
        
        print("Initialize SDK")
        
        RegisterFontManager.shared.registerFont(withFileName: "KhmerOS_muol.ttf")
        GMSServices.provideAPIKey(googleAPIKey)
    }
    
    public func getRegisterAccountController(sdkHandler: @escaping (UpgradeUserAccountInformationResponse? , Bool) -> Void) -> BaseViewController {
        let vc = IdentityTypeMenuViewController()
        vc.isUpgradeAccount = false
        vc.huaweiSDKHandler = sdkHandler
        
        return vc
    }
    
    public func getUpgradeAccountController(currency: String? = nil, sdkHandler: @escaping (UpgradeUserAccountInformationResponse? , Bool) -> Void) -> BaseViewController {
        let vc = IdentityTypeMenuViewController()
        vc.isUpgradeAccount = true
        vc.huaweiSDKHandler = sdkHandler
        
        return vc
    }
    
    public func getCustomSDKView(for view: ScreenViewEnum, isUpgradeAccount: Bool) -> UIView {
        switch view {
        case .identityLivelinessInstructionView:
            return IdentityLivelinessInstructionView()
        case .identityInstructionView:
            return IdentityIntructionDetailView()
        case .identityCaptureView:
            let vc = IdentityCaptureView()
            vc.isUpgradeAccount = isUpgradeAccount
            
            return vc
        case .identityPreviewView:
            let vc = IdentityImagePreviewView()
            vc.isUpgradeAccount = isUpgradeAccount
            
            return vc
        case .identityVerifyView:
            return IdentityVerificationLoadingView()
        case .identityValidationFailed:
            return IdentityValidationFailedView()
        case .identityNoConnection:
            return IdentityNoConnectionView()
            
        case .identityInstructionIDOnly(let hasBackButton):
            let vc = IdentityIntructionDetailView(hasPassport: false)
            vc.hasBackButton = hasBackButton
            
            return vc
        case .identityCaptureViewCustom(let title):
            let vc = IdentityCaptureView()
            vc.customTitle = title
            vc.isUpgradeAccount = isUpgradeAccount
            
            return vc
        case .identityPreviewViewCustom(let title):
            let vc = IdentityImagePreviewView()
            vc.customTitle = title
            vc.isUpgradeAccount = isUpgradeAccount
            
            return vc
        }
    }
    
    public func performOCRCompletedAction(isNID: Bool, isUpgradeAccount: Bool, ocrObject: OCRObjectResponse? = nil, upgradeInformationResponse: UpgradeUserAccountInformationResponse? = nil, completion: @escaping (BaseViewController?) -> Void) {
        
        if let ocrObject = ocrObject {
            guard let idNumber = ocrObject.idNumber, idNumber.isNotEmpty else {
                guard let topVC = UIApplication.shared.getTopViewController() else { return }
                topVC.dismiss(animated: true) {
                    let isPassport = ocrObject.isPassport
                    AlertManager.shared.showSingleButtonAlert(title: "\(isPassport ? "passport_not_capturable" : "id_card_not_capturable")".localize,
                                                              message: "\(isPassport ? "passport_not_capturable_message" : "id_card_not_capturable_message")".localize,
                                                              alertButton: .init(title: "retake".localize, style: .default, handler: { }))
                }
                return
            }
        }
        
        if isUpgradeAccount {
            if isNID {
                var upgradeParam = UpgradeUserValidateParam()
                
                if let upgradeInformationResponse = upgradeInformationResponse {
                    upgradeParam = upgradeInformationResponse.toUpgradeUserValidateParam()
                }
                
                ocrObject?.appendUpgradeUserValidateParam(origin: upgradeParam, completion: { param in
                    guard let param = param as? UpgradeUserValidateParam else { return }
                    upgradeParam = param
                    
                    let vc = CustomerInformationDisplayViewController()
                    vc.isNID = true
                    vc.isUpgradeAccount = true
                    vc.upgradeUserValidateParam = upgradeParam
                    
                    completion(vc)
                })
            }else {
                let vc = OtherIdentityCustomerInformationEditViewController()
                vc.isNID = false
                vc.isUpgradeAccount = true
                if let upgradeInformationResponse = upgradeInformationResponse {
                    
                    let param = upgradeInformationResponse.toUpgradeUserValidateParam()
                    if let storedValidateParam = Constants.storedUpgradeUserValidateParam {
                        appendStoredValidate(param: param, stored: storedValidateParam)
                    }
                    
                    vc.upgradeUserValidateParam = param
                }
                
                completion(vc)
            }
        }else {
            if isNID {
                let vc = CustomerInformationDisplayViewController()
                var registerParam = RegisterUserValidateParam()
                
                if let upgradeInformationResponse = upgradeInformationResponse {
                    registerParam = upgradeInformationResponse.toUpgradeUserValidateParam()
                }
                
                ocrObject?.appendRegisterUserValidateParam(origin: registerParam, completion: { param in
                    guard let param = param as? RegisterUserValidateParam else { return }
                    registerParam = param
                    
                    vc.isNID = true
                    vc.isUpgradeAccount = false
                    vc.registerUserValidateParam = registerParam
                    
                    completion(vc)
                })
            }else {
                let vc = OtherIdentityCustomerInformationEditViewController()
                vc.isNID = false
                vc.isUpgradeAccount = false
                
                if let upgradeInformationResponse = upgradeInformationResponse {
                    
                    let param = upgradeInformationResponse.toUpgradeUserValidateParam()
                    if let storedValidateParam = Constants.storedRegisterUserValidateParam {
                        appendStoredValidate(param: param, stored: storedValidateParam)
                    }
                    
                    vc.registerUserValidateParam = param
                }
                
                completion(vc)
            }
        }
    }
    
    public func getPlanSubscriptionViewController(handler: @escaping () -> Void) -> BaseViewController {
        let vc = ActionTypeViewController()
        vc.upgradeKYCHandler = handler
        
        return vc
    }
    
    func performResubmitRegisterImage() {
        Constants.pendingUploadImageParam = UserDefaultManager.shared.getPendingRegisterImages()
        print("Constants.pendingUploadImageParam Count: \(Constants.pendingUploadImageParam.count)")
        
        if !Constants.pendingUploadImageParam.isEmpty {
            Constants.pendingUploadImageParam.forEach {
                uploadPendingImages(param: $0.toUploadImageParam())
            }
        }
    }
    
    func uploadPendingImages(param: UploadImageParam?) {
        let presenter = UploadImagePresenter(delegate: self)
        
        if let param = param {
            presenter.uploadImage(param: param)
        }
    }
    
    private func appendStoredValidate(param: RegisterUserValidateParam, stored: RegisterUserValidateParam) {
        
        param.birthAddress = stored.birthAddress
        param.birthAddressTextModel = stored.birthAddressTextModel
        param.birthAddressString = stored.birthAddressString
        param.currentAddress = stored.currentAddress
        param.currentAddressTextModel = stored.currentAddressTextModel
        param.currentAddressString = stored.currentAddressString
        
        param.idType = stored.idType
        param.idNumber = stored.idNumber
        param.expiryDate = stored.expiryDate
        param.lastNameKh = stored.lastNameKh
        param.firstNameKh = stored.firstNameKh
        param.lastName = stored.lastName
        param.firstName = stored.firstName
        param.titles = stored.titles
        param.maritalStauts = stored.maritalStauts
        param.phoneNumber = stored.phoneNumber
        param.nationality = stored.nationality
        param.gender = stored.gender
        param.dob = stored.dob
        param.sourceOfFund = stored.sourceOfFund
        param.monthlyAvgIncome = stored.monthlyAvgIncome
        param.sector = stored.sector
        param.currency = stored.currency
        param.accountType = stored.accountType
        param.occupationID = stored.occupationID
        param.bankingPurpose = stored.bankingPurpose
        param.kitNumber = stored.kitNumber
        param.debitCardInformation = stored.debitCardInformation
    }
    
    private func getInitialComponents() {
        let service = BaseService()
        let kycService = KYCService()
        let presenter = SingleAccountPresenter()
        
        service.getAccessToken(shouldShowLoading: false) { [weak self] error in
            guard let self = self else { return }
            guard error == nil else {
                self.handleError(error)
                return
            }
            
            service.getEnumeration { [weak self] error in
                guard let self = self else { return }
                guard error == nil else {
                    self.handleError(error)
                    service.getEnumeration { _ in }
                    return
                }
            }
            
            kycService.getCountry { [weak self] error in
                guard let self = self else { return }
                guard error == nil else {
                    self.handleError(error)
                    kycService.getCountry { _ in }
                    return
                }
            }
            
            kycService.getRegion(param: "KHM") { [weak self] response, error in
                guard let self = self else { return }
                guard error == nil else {
                    self.handleError(error)
                    kycService.getRegion(param: "KHM") { [weak self] response, _ in
                        guard let self = self else { return }
                        guard let response = response else {
                            self.handleError(error)
                            return
                        }
                        Constants.provinceListResponse = response
                    }
                    return
                }
                
                guard let response = response else { return }
                Constants.provinceListResponse = response
                print("\n=======> Cambodia Province Datasource")
                print((response.province ?? []).map { "\($0.regionID ?? "") \($0.regionDescription ?? "")" }.joined(separator: ",\n"))
            }
            
            presenter.getAllComponent()
        }
    }
}

extension KYCRegisterSDKManager {
    func handleError(_ error: Error?) {
        guard let error = error?.asNSError() else {
            print(error)
            return
        }
        print("🚫 Error: \(error.domain)")
        showErrorAlert(error.domain)
    }
    
    func showErrorAlert(_ message: String = "error_please_fill_all_information".localize, title: String? = "error".localize) {
        guard let topVC = UIApplication.shared.getTopViewController() else { return }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(.init(title: "done".localize, style: .destructive, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        topVC.present(alert, animated: true, completion: nil)
    }
}

extension KYCRegisterSDKManager: UploadImageDelegate {
    func response(response: UploadImageResponse?, param: UploadImageParam?, error: Error?) {
        guard error == nil else {
            uploadPendingImages(param: param)
            return
        }
        
        print("✅ Reupload successfully")
        guard let param = param else { return }
        
        Constants.pendingUploadImageParam.removeAll {
            $0.customerWingAccount == param.customerWingAccount && $0.imageType == param.imageType
        }
        UserDefaultManager.shared.savePendingRegisterImages()
    }
}
