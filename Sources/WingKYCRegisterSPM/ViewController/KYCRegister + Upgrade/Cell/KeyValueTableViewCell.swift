//
//  KeyValueTableViewCell.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/11/21.
//

import UIKit

class KeyValueTableViewCell: BaseTableViewCell {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let keyLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .white
        label.textAlignment = .left
        label.font = .boldSystemFont(ofSize: 15)
        label.numberOfLines = 1
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        label.scaleToWidth()

        return label
    }()
    private let valueLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .white
        label.textAlignment = .right
        label.font = .boldSystemFont(ofSize: 15)
        label.numberOfLines = 1
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        label.scaleToWidth()
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        
        addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 8, left: 0, bottom: 8, right: 0))
        
        keyLabel.constraintWidthGreater(constant: 100)
        valueLabel.constraintWidthGreater(constant: 100)
        stackView.addArrangedSubview(keyLabel)
        stackView.addArrangedSubview(valueLabel)
    }
    
    override func layoutSubviews() {
        stackView.layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configCell(model: KeyValueModel) {
        keyLabel.text = model.key
        valueLabel.text = model.value
    }
    
    func setMaxLines(_ key: Int, value: Int) {
        keyLabel.numberOfLines = key
        valueLabel.numberOfLines = value
    }
    
    func setTextAlighn(_ key: NSTextAlignment, value: NSTextAlignment) {
        keyLabel.textAlignment = key
        valueLabel.textAlignment = value
    }
    
    func setupKeyValueLabelColor(_ key: UIColor, value: UIColor) {
        self.keyLabel.textColor = key
        self.valueLabel.textColor = value
    }
    
    func setupFont(_ key: UIFont, value: UIFont) {
        self.keyLabel.font = key
        self.valueLabel.font = value
    }
}
