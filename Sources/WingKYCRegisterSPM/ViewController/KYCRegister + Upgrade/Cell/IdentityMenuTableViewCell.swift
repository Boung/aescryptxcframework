//
//  IdentityMenuTableViewCell.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/11/21.
//

import UIKit

class IdentityMenuTableViewCell: BaseTableViewCell {
    
    private let containerView = ContainerView(background: .primaryColor)
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 24
        
        return stackView
    }()
    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        imageView.constrainWidth(constant: 100)
        imageView.constrainHeight(constant: 100)
        
        return imageView
    }()
    private let descriptionLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 18)
        label.numberOfLines = 4
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = .clear
        
        addSubview(containerView)
        containerView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 0, right: 16))
        
        containerView.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 32, left: 16, bottom: 32, right: 16))
        
        stackView.addArrangedSubview(iconImageView)
        stackView.addArrangedSubview(descriptionLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerView.setCornerRadius(radius: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configCell(model: IdentityTypeMenuModel) {
        iconImageView.image = model.icon
        descriptionLabel.text = "\(model.title)\n\(model.subTitle)"
    }
}
