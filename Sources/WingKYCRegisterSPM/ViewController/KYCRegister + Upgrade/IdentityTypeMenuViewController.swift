//
//  IdentityTypeMenuViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/11/21.
//

import UIKit

struct IdentityTypeMenuModel {
    var icon: UIImage
    var title: String
    var subTitle: String
}

class IdentityTypeMenuViewController: BaseViewController {
    
    private let tableView = UITableView()
    private let tableCell = "IdentityMenuTableViewCell"
    private var menu: [IdentityTypeMenuModel] {
        if isUpgradeAccount {
            return [
                .init(icon: .localImage("ic_nid_passport_identity"), title: "upgrade_with_id".localize, subTitle: "(id)".localize),
                .init(icon: .localImage("ic_other_identity"), title: "upgrade_with_other_id".localize, subTitle: "(other_id)".localize)
            ]
        }
        
        return [
            .init(icon: .localImage("ic_nid_passport_identity"), title: "register_with_id".localize, subTitle: "(id)".localize),
            .init(icon: .localImage("ic_other_identity"), title: "register_with_ther_id".localize, subTitle: "(other_id)".localize)
        ]
    }
    
    var isUpgradeAccount: Bool = false
    var huaweiSDKHandler: ((UpgradeUserAccountInformationResponse?, Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = !isUpgradeAccount ? "title_register_wing_account".localize : "title_upgrade_wing_account".localize
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.register(IdentityMenuTableViewCell.self, forCellReuseIdentifier: tableCell)
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(tableView)
        tableView.fillInSuperView()
    }
}

extension IdentityTypeMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCell, for: indexPath) as! IdentityMenuTableViewCell
        cell.configCell(model: menu[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let isNID = indexPath.row == 0
        
        if isUpgradeAccount {
            let vc = UpgradeAcountInputViewController()
            vc.isNID = isNID
            vc.huaweiSDKHandler = huaweiSDKHandler

            navigationController?.pushViewController(vc, animated: true)
            return
        }else {
            if isNID {
                let upgradeResponse = UpgradeUserAccountInformationResponse()
                
                showCustomerCameraVC { [weak self] customerImage in
                    guard let self = self else { return }
                    
                    upgradeResponse.customerImage = customerImage
                    guard let handler = self.huaweiSDKHandler else { return }
                    handler(upgradeResponse, isNID)
                }
            }else {
                let upgradeResponse = UpgradeUserAccountInformationResponse()

                showCustomerCameraVC { [weak self] customerImage in
                    guard let self = self else { return }

                    upgradeResponse.customerImage = customerImage
                    self.showFrontDocumentCameraVC { [weak self] frontIDImage in
                        guard let self = self else { return }

                        upgradeResponse.frontIDImage = frontIDImage
                        self.showBackDocumentCameraVC { [weak self] backIDImage in
                            guard let self = self else { return }

                            upgradeResponse.backIDImage = backIDImage
                            guard let handler = self.huaweiSDKHandler else { return }
                            handler(upgradeResponse, isNID)
                        }
                    }
                }
            }
        }
    }
    
    private func showCustomerCameraVC(completion: @escaping (UIImage) -> Void) {
        let vc = CustomerCameraViewController()
        vc.isUpgradeAccount = isUpgradeAccount
        vc.setOnContinueButtonTap(handler: completion)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showFrontDocumentCameraVC(completion: @escaping (UIImage) -> Void) {
        let vc = FrontDocumentCameraViewController()
        vc.isUpgradeAccount = isUpgradeAccount
        vc.setOnContinueButtonTap(handler: completion)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showBackDocumentCameraVC(completion: @escaping (UIImage) -> Void) {
        let vc = BackDocumentCameraViewController()
        vc.isUpgradeAccount = isUpgradeAccount
        vc.setOnContinueButtonTap(handler: completion)
        
        navigationController?.pushViewController(vc, animated: true)
    }
}
