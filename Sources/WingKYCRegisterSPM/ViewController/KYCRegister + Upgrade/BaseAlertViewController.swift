//
//  BaseAlertViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/3/23.
//

import UIKit

class BaseAlertViewController: BaseViewController {
    
    let containerView = ContainerView(background: .white)
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.textColor = .subTitleColor
        label.font = .boldSystemFont(ofSize: 18)
        label.numberOfLines = 0
        
        return label
    }()
    let messageLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .left
        label.textColor = .subTitleColor
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    let defaultButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .secondaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(containerView)
        containerView.centerInSuperview()
        containerView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 0, left: 16, bottom: 0, right: 0))
        
        containerView.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        containerView.setCornerRadius(radius: 16)
        setupBlurBackground()
        
        defaultButton.setRoundedView()
    }
    
    func setupDetails(title: String? = nil, messsage: String? = nil, views: [UIView] = []) {
        self.titleLabel.text = title
        self.messageLabel.text = messsage
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(messageLabel)
        stackView.setCustomSpacing(24, after: messageLabel)
        views.forEach {
            stackView.addArrangedSubview($0)
        }
    }
    
    func setSingleAccountAlert(title: String? = nil, messsage: String? = nil, buttonTitle: String, buttonAction: @escaping () -> Void) {
        setupDetails(title: title, messsage: messsage)
    
        stackView.addArrangedSubview(defaultButton)
        defaultButton.setTitle(string: buttonTitle)
        defaultButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true) {
                buttonAction()
            }
        }
    }
}
