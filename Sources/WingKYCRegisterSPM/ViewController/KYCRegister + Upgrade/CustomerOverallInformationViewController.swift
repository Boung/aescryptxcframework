//
//  CustomerOverallInformationViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/11/21.
//

import UIKit

class CustomerOverrallInformationViewController: BaseViewController {
    
    private let scrollView = ScrollViewContainerView()
    private var containerView: UIView {
        return scrollView.containerView
    }
    let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "please_verify_information_below".localize
        label.textAlignment = .center
        label.textColor = .subTitleColor
        label.font = .boldSystemFont(ofSize: 18)
        
        return label
    }()
    
    private let imageContainerView = ContainerView(background: .white)
    private var imageView: CustomerImagesView?
    
    private let informationView = CustomerInformationDisplayView()
    private let additionalInformationView = AdditionalInformationDisplayView()
    private let accountTypeInformationView = AccountInformationDisplayView()
    let otherRequestInformationView = OtherRequestInformationDisplayView()
    
    let registerButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "register".localize)
        button.backgroundColor = .primaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    var isNID: Bool = false
    var isUpgradeAccount: Bool = false
    lazy var registerUserValidateParam = RegisterUserValidateParam()
    lazy var upgradeUserValidateParam = UpgradeUserValidateParam()
    var upgradeUserValidateResponse: UpgradeUserValidateResponse?
    
    private var customerPin: String = ""
    private var validateResponse: RegisterUserValidateResponse?
    
    override func viewDidLoad() {
        imageView = .init(stackView: .horizontal, isNID: isNID)
        super.viewDidLoad()
        
        navigationItem.title = !isUpgradeAccount ? "title_register_wing_account".localize : "title_upgrade_wing_account".localize
        
        setupDocumentImages()
        informationView.setupDetail(model: isUpgradeAccount ? upgradeUserValidateParam : registerUserValidateParam)
        additionalInformationView.setupDetails(model: isUpgradeAccount ? upgradeUserValidateParam : registerUserValidateParam)
        accountTypeInformationView.setupDetails(model: isUpgradeAccount ? upgradeUserValidateParam : registerUserValidateParam)
        otherRequestInformationView.setupDetails(model: isUpgradeAccount ? upgradeUserValidateParam : registerUserValidateParam)
        
        registerButton.setAction { [weak self] in
            guard let self = self else { return }
            
            if self.isUpgradeAccount {
                let presenter = UpgradeUserValidatePresenter(delegate: self)
                presenter.upgradeUserValidate(param: self.upgradeUserValidateParam)
            }else {
                let presenter = GetUUIDPresenter(delegate: self)
                presenter.getUUID()
            }
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.backgroundColor = .backgroundColor
        
        view.addSubview(scrollView)
        scrollView.fillInSuperView()
        
        containerView.addSubview(vStackView)
        vStackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        vStackView.addArrangedSubview(titleLabel)
        vStackView.addArrangedSubview(imageContainerView)
        vStackView.addArrangedSubview(informationView)
        vStackView.addArrangedSubview(additionalInformationView)
        vStackView.addArrangedSubview(accountTypeInformationView)
        vStackView.addArrangedSubview(otherRequestInformationView)
        vStackView.addArrangedSubview(registerButton)
        
        imageContainerView.addSubview(imageView!)
        imageView?.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        imageContainerView.setCornerRadius(radius: 16)
        
        registerButton.setRoundedView()
    }
}

// MARK: - Functions
extension CustomerOverrallInformationViewController {
    private func presentPinVC(description: String, placeholder: String, handler: @escaping (String) -> Void) {
        let vc = PinCodeConfirmationViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.setupController(description, placeholder: placeholder, handler: handler)
        
        present(vc, animated: true, completion: nil)
    }
    
    private func setupDocumentImages() {
        if isUpgradeAccount {
            imageView?.setupDetails(customerImage: upgradeUserValidateParam.customerImage,
                                    idFront: upgradeUserValidateParam.identityFrontImage,
                                    idBack: isNID ? nil : upgradeUserValidateParam.identityBackImage)
        }else {
            imageView?.setupDetails(customerImage: registerUserValidateParam.customerImage,
                                    idFront: registerUserValidateParam.identityFrontImage,
                                    idBack: isNID ? nil : registerUserValidateParam.identityBackImage)
        }
    }
}
// MARK: - UUID
extension CustomerOverrallInformationViewController: GetUUIDDelegate {
    func response(uuid response: GetUUIDResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        registerUserValidateParam.uuid = response.uuid
        upgradeUserValidateParam.uuid = response.uuid
        
        if !isUpgradeAccount {
            uploadImages(customerWingAccount: "")
        }
    }
}

// MARK: - Register
extension CustomerOverrallInformationViewController: RegisterUserValidateDelegate {
    func response(registerValidate response: RegisterUserValidateResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        validateResponse = response
        
        registerPinValidation()
    }
}

extension CustomerOverrallInformationViewController: RegisterUserPinValidationDelegate {
    func response(pinValidation response: RegisterUserPinValidationResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let _ = response else { return }

        self.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
         
            self.registerUserCommit()
        }
    }
    
    private func registerUserCommit() {
        self.presentPinVC(description: "input_code_for_agent".localize, placeholder: "input_code_for_agent".localize) { [weak self] wcxPin in
            guard let self = self else { return }
            guard let validateResponse = self.validateResponse else { return }
            let presenter = RegisterUserCommitPresenter(delegate: self)
            let param = RegisterUserCommitParam(sessionID: validateResponse.sessionID ?? "",
                                                WCXPin: wcxPin,
                                                customerPin: self.customerPin,
                                                uuid: self.isUpgradeAccount ? self.upgradeUserValidateParam.uuid ?? "" : self.registerUserValidateParam.uuid ?? "")
            
            presenter.registerUserCommit(param: param)
        }
    }
}

extension CustomerOverrallInformationViewController: RegisterUserCommitDelegate {
    func response(registerCommit response: RegisterUserCommitResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        
        self.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            
            self.showRegisterSuccess(response: response)
        }
    }
    
    private func showRegisterSuccess(response: RegisterUserCommitResponse) {
        let vc = ConfirmationDetailViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.setupController("register_account_successfully".localize, datasources: response.toDatasource()) { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
        self.present(vc, animated: true, completion: nil)
    }
}

// MARK: - Upgrade
extension CustomerOverrallInformationViewController: UpgradeUserValidateDelegate {
    func response(upgradeValidate response: UpgradeUserValidateResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        self.upgradeUserValidateResponse = response
        print("Confirmation Code: \(EncodingManager.shared.decode(encoded: response.confirmCode ?? "") ?? "")")
        
        uploadImages(customerWingAccount: upgradeUserValidateParam.customerWingAccount ?? "")
    }
    
    private func upgradeUserCommit() {
        presentPinVC(description: "input_code_for_customer".localize, placeholder: "input_code_for_customer".localize) { [weak self] customerPin in
            guard let self = self else { return }
            
            self.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                
                self.presentPinVC(description: "input_code_for_agent".localize, placeholder: "input_code_for_agent".localize) { [weak self] wcxPin in
                    guard let self = self else { return }
                    
                    // MARK: Commit Upgrade
                    let presenter = UpgradeUserCommitPresenter(delegate: self)
                    let param = UpgradeUserCommitParam(sessionID: self.upgradeUserValidateResponse?.sessionID ?? "",
                                                       customerPin: customerPin,
                                                       confirmCode: self.upgradeUserValidateResponse?.confirmCode ?? "",
                                                       wcxPin: wcxPin)
                    
                    presenter.upgradeUserCommit(param: param)
                }
            }
        }
    }
}

extension CustomerOverrallInformationViewController: UpgradeUserCommitDelegate {
    func response(upgradeCommit response: UpgradeUserCommitResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }

        self.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            
            self.showUpgradeSuccess(response: response)
        }
    }
    
    private func showUpgradeSuccess(response: UpgradeUserCommitResponse) {
        let vc = ConfirmationDetailViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.setupController("upgrade_account_successfully".localize, datasources: response.toDatasource()) { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
        self.present(vc, animated: true, completion: nil)
    }
}

extension CustomerOverrallInformationViewController: UploadImageDelegate {
    func response(response: UploadImageResponse?, param: UploadImageParam?, error: Error?) {
        guard error == nil else {
            let retryButton = AlertButton(title: "ព្យាយាមម្តងទៀត", style: .default) { [weak self] in
                guard let self = self else { return }
                
                self.uploadImages(customerWingAccount: param?.customerWingAccount ?? "")
            }
            let cancelButton = AlertButton(title: "បោះបង់", style: .destructive) { [weak self] in
                guard let self = self else { return }
                
                self.dismiss(animated: true)
            }
            AlertManager.shared.showDoubleButtonAlert(title: "បញ្ហា", message: "មានបញ្ហាក្នុងការបញ្ជូនរូបភាពទៅប្រព័ន្ធ", firstButton: retryButton, secondButton: cancelButton)
            return
        }
        
        print("✅ Upload Image Success")
        
        if isUpgradeAccount {
            upgradeUserCommit()
        }else {
            let presenter = RegisterUserValidatePresenter(delegate: self)
            presenter.registerUserValidate(param: self.registerUserValidateParam)
        }
    }
    
    private func uploadImages(customerWingAccount: String) {
        let presenter = UploadImagePresenter(delegate: self)
        
        if isUpgradeAccount {
            presenter.uploadImage(param: upgradeUserValidateParam.toUploadImageParam(customerWingAccount: customerWingAccount))
        }else {
            presenter.uploadImage(param: registerUserValidateParam.toUploadImageParam(customerWingAccount: customerWingAccount))
        }
    }
    
    private func registerPinValidation() {
        presentPinVC(description: "create_code_for_customer".localize, placeholder: "input_new_code_for_customer".localize) { [weak self] newPin in
            guard let self = self else { return }
            self.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                
                // MARK: Confirm New Pin
                self.presentPinVC(description: "create_code_for_customer".localize, placeholder: "confirm_new_code_for_customer".localize) { [weak self] confirmPin in
                    guard let self = self else { return }
                    
                    if newPin != confirmPin {
                        self.dismiss(animated: true) { [weak self] in
                            guard let self = self else { return }
                            
                            self.showSingleButtonAlert(title: nil, message: "pin_code_mismatched".localize, buttonTitle: "done".localize) {
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                        return
                    }
                    
                    self.customerPin = newPin
                    
                    // MARK: Validate Pin API
                    guard let phoneNumber = self.registerUserValidateParam.phoneNumber else { return }
                    let presenter = RegisterUserPinValidationPresenter(delegate: self)
                    let param = RegisterUserPinValidationParam(pin: newPin, phoneNumber: phoneNumber)
                    
                    presenter.registerUserPinValidation(param: param)
                }
            }
        }
    }
}
