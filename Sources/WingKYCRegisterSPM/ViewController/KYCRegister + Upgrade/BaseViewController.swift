//
//  BaseViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

public class BaseViewController: UIViewController {
    
    private var onBackButtonTapHandler: (() -> Void)?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        if Constants.autoFillMockData {
            autoFillMockupData()
        }
        
        generateUI()
        addNavigationBackButtonItem()
        setBackButtonAction { [weak self] in
            guard let self = self else { return }
            
            self.navigationController?.popViewController(animated: true)
        }
        
        
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.backgroundColor = .primaryColor
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = appearance
            navigationController?.navigationBar.isTranslucent = false
        }else {
            navigationController?.navigationBar.tintColor = .white
            navigationController?.navigationBar.barTintColor = .primaryColor
            navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
            navigationController?.navigationBar.isTranslucent = false
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    deinit {
        print("♻️ \(self) is deinited.")
    }
    
    func generateUI() {
        view.backgroundColor = .backgroundColor
    }
    
    func setBackButtonAction(handler: @escaping () -> Void) {
        onBackButtonTapHandler = handler
    }
    
    func autoFillMockupData() { }
    
    func handleError(_ error: Error?) {
        if let error = error?.asNSError() {
            self.showErrorAlert(error.domain)
        }else {
            self.showErrorAlert(error?.localizedDescription ?? "")
        }
    }
    
    func showSingleButtonAlert(title: String?, message: String?, buttonTitle: String, completion: @escaping () -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(.init(title: buttonTitle, style: .destructive, handler: { _ in
            completion()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(_ message: String = "error_please_fill_all_information".localize, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: "error".localize, message: message, preferredStyle: .alert)
        alert.addAction(.init(title: "done".localize, style: .destructive, handler: { _ in
            alert.dismiss(animated: true, completion: completion)
        }))
        
        guard let topVC = UIApplication.shared.getTopViewController() else {
            present(alert, animated: true, completion: nil)
            return
        }
        
        topVC.present(alert, animated: true, completion: nil)
    }
    
    func removeBackButton() {
        navigationItem.leftBarButtonItem = nil
        navigationItem.setHidesBackButton(true, animated: true)
    }
    
    func horizonStackView(_ views: [UIView], alignment: UIStackView.Alignment = .fill) -> UIView {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = alignment
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        
        views.forEach {
            stackView.addArrangedSubview($0)
        }
        
        return stackView
    }
    
    private func addNavigationBackButtonItem() {
        let backButton: BaseButton = {
            let button = BaseButton()
            button.backgroundColor = .clear
            button.setImage(.localImage("ic_back").withRenderingMode(.alwaysTemplate), for: .normal)
            button.imageView?.tintColor = .white
            button.imageView?.contentMode = .scaleAspectFit
            button.constrainWidth(constant: 21)
            button.constrainHeight(constant: 21)
            button.setAction { [weak self] in
                guard let self = self else { return }
                guard let onBackButtonTap = self.onBackButtonTapHandler else { return }

                onBackButtonTap()
            }
            
            return button
        }()
        let backButtonItem = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItem = isRootViewController() ? nil : backButtonItem
    }
    
    private func isRootViewController() -> Bool {
        guard let counter = navigationController?.viewControllers.count else { return false }
        
        return counter == 1
    }
    
    func setupBlurBackground() {
//        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.frame = view.bounds
//        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
      view.backgroundColor = .black.withAlphaComponent(0.6)
//        view.addSubview(blurEffectView)
//        view.sendSubviewToBack(blurEffectView)
    }
}
