//
//  BackDocumentPreviewViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 26/11/21.
//

import UIKit

class BackDocumentPreviewViewController: BasePreviewViewController {
    
    private let captureZoneImageView: UIImageView = {
        let imageView = UIImageView(image: .localImage("ic_card_frame").withRenderingMode(.alwaysTemplate))
        imageView.backgroundColor = .clear
        imageView.tintColor = .backgroundColor
        imageView.contentMode = .scaleToFill
        
        return imageView
    }()
    
    var onContinueButtonTap: ((UIImage) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTitle("please_identity_back_image".localize)
        
        continueButton.setAction { [weak self] in
            guard let self = self else { return }
            guard
                let handler = self.onContinueButtonTap,
                let image = self.image
            else { return }
            
            handler(image)
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
        frameContainerView.addSubview(captureZoneImageView)
        captureZoneImageView.fillInSuperView()
        captureZoneImageView.constrainWidth(constant: frameWidth)
        captureZoneImageView.constrainHeight(constant: frameWidth / 1.5)
        
        view.bringSubviewToFront(frameContainerView)
    }

    func setOnContinueButtonTap(handler: @escaping (UIImage) -> Void) {
        onContinueButtonTap = handler
    }
}
