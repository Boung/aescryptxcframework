//
//  CustomerCameraViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 25/11/21.
//

import UIKit

class CustomerCameraViewController: BaseCameraViewController {
    
    private let captureZoneImageView: UIImageView = {
        let imageView = UIImageView(image: .localImage("ic_customer_photo_frame"))
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleToFill
        
        return imageView
    }()
    private let iconStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 32
        
        return stackView
    }()
    private let noHeadView: IconTextView = {
        let view = IconTextView()
        view.setupDetails(icon: .localImage("ic_no_hat_glasses_mask"), text: "no_hat_glasses_mask".localize)
        
        return view
    }()
    private let noBlurFaceView: IconTextView = {
        let view = IconTextView()
        view.setupDetails(icon: .localImage("ic_no_blur_face"), text: "no_blur_face".localize)
        
        return view
    }()
    private let rotateCameraButton: BaseButton = {
        let button = BaseButton()
        button.setImage(.localImage("ic_switch_camera"), for: .normal)
        button.backgroundColor = .clear
        button.constrainWidth(constant: 40)
        button.setDefaultHeight()
        
        return button
    }()
    
    override var capturedImage: UIImage? {
        didSet {
            let vc = CustomerPreviewViewController()
            vc.image = capturedImage
            vc.isUpgradeAccount = isUpgradeAccount
            if let handler = self.onContinueButtonTap {
                vc.setOnContinueButtonTap(handler: handler)
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTitle("take_customer_photo".localize)
        
        setCameraLayerFrame(.init(width: frameWidth, height: frameWidth))
        rotateCameraButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.switchCamera()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Constants.storedRegisterUserValidateParam = nil
        Constants.storedUpgradeUserValidateParam = nil
    }
    
    override func generateUI() {
        super.generateUI()
        
        frameContainerView.addSubview(captureZoneImageView)
        captureZoneImageView.fillInSuperView()
        captureZoneImageView.constrainWidth(constant: frameWidth)
        captureZoneImageView.constrainHeight(constant: frameWidth)
        
        view.addSubview(iconStackView)
        iconStackView.centerXInSuperview()
        iconStackView.anchor(top: frameContainerView.bottomAnchor,
                             leading: nil,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        
        iconStackView.addArrangedSubview(noHeadView)
        iconStackView.addArrangedSubview(noBlurFaceView)
        
        view.addSubview(rotateCameraButton)
        rotateCameraButton.centerYTo(view: captureButton.centerYAnchor)
        rotateCameraButton.anchor(top: nil,
                                  leading: nil,
                                  bottom: nil,
                                  trailing: captureZoneImageView.trailingAnchor,
                                  padding: .init(top: 0, left: 0, bottom: 0, right: 16))
        
        bringCaptureButtonFront()
    }
}
