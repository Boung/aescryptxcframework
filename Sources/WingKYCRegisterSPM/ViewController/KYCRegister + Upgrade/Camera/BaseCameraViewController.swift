//
//  BaseCameraViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit
import AVFoundation

public class BaseCameraViewController: BaseViewController {
    
    private let navigationBarView = ContainerView(background: .clear)
    private let backButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setImage(.localImage("ic_back").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .white
        button.constrainWidth(constant: 21)
        button.constrainHeight(constant: 21)
        
        return button
    }()
    private let navigationTitleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "title_register_wing_account".localize
        label.textAlignment = .center
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 18)
        label.scaleToWidth()
        
        return label
    }()
    private let topBlurView = ContainerView(background: .blurBackground)
    private let bottomBlurView = ContainerView(background: .blurBackground)
    private let leftBlurView = ContainerView(background: .blurBackground)
    private let rightBlurView = ContainerView(background: .blurBackground)
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = ""
        label.textAlignment = .center
        label.textColor = .white
        label.font = .systemFont(ofSize: 18)
        label.numberOfLines = 2
        label.scaleToWidth()
        
        return label
    }()
    let frameContainerView = ContainerView(background: .clear)
    let frameWidth = UIConstants.screenWidth * 0.9
    let captureButton: BaseButton = {
        let button = BaseButton()
        button.setImage(.localImage("ic_capture"), for: .normal)
        button.backgroundColor = .clear
        button.constrainWidth(constant: 65)
        button.constrainHeight(constant: 65)
        
        return button
    }()
    
    let captureSession = AVCaptureSession()
    var backFacingCamera: AVCaptureDevice?
    var frontFacingCamera: AVCaptureDevice?
    var currentDevice: AVCaptureDevice?
    var stillImageOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var capturedImage: UIImage?
    var onContinueButtonTap: ((UIImage) -> Void)?
    var isUpgradeAccount: Bool = false
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationTitleLabel.text = isUpgradeAccount ? "title_upgrade_wing_account".localize : "title_register_wing_account".localize
        backButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.navigationController?.popViewController(animated: true)
        }
        
        captureButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.captureButton.isEnabled = false
            self.capturePhoto()
        }
        
        KYCCamearaPermissionManager.shared.requestAuthorization { (status) in
            switch status {
            case .denied:
                self.showSingleButtonAlert(title: "ការអនុញ្ញាត", message: "យើងត្រូវការការអនុញ្ញាតដើម្បីប្រើប្រាស់ កាំមេរ៉ា", buttonTitle: "ទៅកាន់ Settings") {
                    UIApplication.shared.openAppSettings { [weak self] in
                        guard let self = self else { return }
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            default:
                break
            }
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.captureButton.isEnabled = true
        captureSession.startRunning()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        captureSession.stopRunning()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func generateUI() {
        super.generateUI()
        
        configure()
        
        view.addSubview(navigationBarView)
        navigationBarView.constrainHeight(constant: UIConstants.navigationHeight + UIConstants.topSafeArea)
        navigationBarView.anchor(top: view.topAnchor,
                                 leading: view.leadingAnchor,
                                 bottom: nil,
                                 trailing: view.trailingAnchor)
        
        navigationBarView.addSubview(backButton)
        backButton.centerYTo(view: navigationBarView.centerYAnchor, constant: UIConstants.topSafeArea / 2)
        backButton.anchor(top: nil,
                          leading: navigationBarView.leadingAnchor,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: 0, left: 16, bottom: 0, right: 0))
        
        navigationBarView.addSubview(navigationTitleLabel)
        navigationTitleLabel.centerXInSuperview()
        navigationTitleLabel.centerYTo(view: backButton.centerYAnchor)
        navigationTitleLabel.anchor(top: nil,
                                    leading: backButton.trailingAnchor,
                                    bottom: nil,
                                    trailing: nil,
                                    padding: .init(top: 0, left: 8, bottom: 0, right: 0))
        
        view.addSubview(frameContainerView)
        frameContainerView.centerXInSuperview()
        frameContainerView.anchor(top: navigationBarView.bottomAnchor,
                                  leading: nil,
                                  bottom: nil,
                                  trailing: nil,
                                  padding: .init(top: 128, left: 0, bottom: 0, right: 0))
        
        view.addSubview(titleLabel)
        titleLabel.anchor(top: nil,
                          leading: view.leadingAnchor,
                          bottom: frameContainerView.topAnchor,
                          trailing: view.trailingAnchor,
                          padding: .init(top: 0, left: 64, bottom: 24, right: 64))
        
        view.addSubview(topBlurView)
        topBlurView.anchor(top: view.topAnchor,
                           leading: view.leadingAnchor,
                           bottom: frameContainerView.topAnchor,
                           trailing: view.trailingAnchor,
                           padding: .init(top: 0, left: 0, bottom: -3, right: 0))
        
        view.addSubview(bottomBlurView)
        bottomBlurView.anchor(top: frameContainerView.bottomAnchor,
                              leading: view.leadingAnchor,
                              bottom: view.bottomAnchor,
                              trailing: view.trailingAnchor,
                              padding: .init(top: -3, left: 0, bottom: 0, right: 0))
        
        view.addSubview(leftBlurView)
        leftBlurView.anchor(top: topBlurView.bottomAnchor,
                            leading: topBlurView.leadingAnchor,
                            bottom: bottomBlurView.topAnchor,
                            trailing: frameContainerView.leadingAnchor,
                            padding: .init(top: 0, left: 0, bottom: 0, right: -3))
        
        view.addSubview(rightBlurView)
        rightBlurView.anchor(top: topBlurView.bottomAnchor,
                             leading: frameContainerView.trailingAnchor,
                             bottom: bottomBlurView.topAnchor,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 0, left: -3, bottom: 0, right: 0))
        
        view.addSubview(captureButton)
        captureButton.centerXInSuperview()
        captureButton.anchor(top: nil,
                             leading: nil,
                             bottom: view.layoutMarginsGuide.bottomAnchor,
                             trailing: nil,
                             padding: .init(top: 0, left: 0, bottom: 16, right: 0))
        
        [topBlurView, bottomBlurView, leftBlurView, rightBlurView, titleLabel, captureButton].forEach {
            view.bringSubviewToFront($0)
        }
        
        view.bringSubviewToFront(frameContainerView)
        view.bringSubviewToFront(navigationBarView)
    }
}

// MARK: - Camera Configuration
extension BaseCameraViewController {
    func bringCaptureButtonFront() {
        view.bringSubviewToFront(captureButton)
    }
    
    private func configure() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .unspecified)
        
        for device in deviceDiscoverySession.devices {
            if device.position == .back {
                backFacingCamera = device
            } else if device.position == .front {
                frontFacingCamera = device
            }
        }
        
        currentDevice = backFacingCamera
        
        guard
            let currentDevice = currentDevice,
            let captureDeviceInput = try? AVCaptureDeviceInput(device: currentDevice)
        else { return }
        
        stillImageOutput = AVCapturePhotoOutput()
        captureSession.addInput(captureDeviceInput)
        captureSession.addOutput(stillImageOutput!)
        
        // Preview Live Camera
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        view.layer.addSublayer(cameraPreviewLayer!)
        cameraPreviewLayer?.videoGravity = .resizeAspectFill
        cameraPreviewLayer?.cornerRadius = 16
    }
    
    func setCameraLayerFrame(_ size: CGSize) {
//        let x = (UIConstants.screenWidth - frameWidth) / 2
//        let y: CGFloat = 128 + UIConstants.navigationHeight + UIConstants.topSafeArea
        
//        let frame = CGRect(x: x, y: y, width: size.width, height: size.height)
        let frame = view.frame
        cameraPreviewLayer?.frame = frame
        cameraPreviewLayer?.layoutIfNeeded()
    }
    
    func setTitle(_ text: String) {
        titleLabel.text = text
    }
    
    func setOnContinueButtonTap(handler: @escaping (UIImage) -> Void) {
        onContinueButtonTap = handler
    }
    
    func capturePhoto() {
        let photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        photoSettings.isHighResolutionPhotoEnabled = true
        photoSettings.flashMode = .off
        
        stillImageOutput?.isHighResolutionCaptureEnabled = true
        stillImageOutput?.capturePhoto(with: photoSettings, delegate: self)
    }
    
    func switchCamera() {
        let session = captureSession
        
        //Remove existing input
        guard let currentCameraInput: AVCaptureInput = session.inputs.first else {
            return
        }
        
        //Indicate that some changes will be made to the session
        session.beginConfiguration()
        session.removeInput(currentCameraInput)
        
        //Get new input
        var newCamera: AVCaptureDevice! = nil
        if let input = currentCameraInput as? AVCaptureDeviceInput {
            if input.device.position == .back {
                newCamera = cameraWithPosition(position: .front)
            } else {
                newCamera = cameraWithPosition(position: .back)
            }
        }
        
        //Add input to session
        var err: NSError?
        var newVideoInput: AVCaptureDeviceInput!
        do {
            newVideoInput = try AVCaptureDeviceInput(device: newCamera)
        } catch {
            err = error as NSError
            newVideoInput = nil
        }
        
        if newVideoInput != nil || err == nil {
            session.addInput(newVideoInput)
        } else {
            print("Error creating capture device input: \(err?.localizedDescription ?? "")")
        }
        
        //Commit all the configuration changes at once
        session.commitConfiguration()
    }
    
    private func cameraWithPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video , position: .unspecified)
        for device in discoverySession.devices {
            if device.position == position {
                return device
            }
        }
        
        return nil
    }
}

extension BaseCameraViewController: AVCapturePhotoCaptureDelegate {
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard error == nil else {
            return
        }
        
        // Get the image from the photo buffer
        guard let imageData = photo.fileDataRepresentation() else { return }
        
        // MARK: - Image Output
        let capturedImage = UIImage(data: imageData)
        
        self.capturedImage = capturedImage
    }
}

