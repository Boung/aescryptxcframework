//
//  CustomerImagePreviewViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 25/11/21.
//

import Foundation
import UIKit

class BasePreviewViewController: BaseViewController {
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        
        return imageView
    }()
    private let topBlurView = ContainerView(background: .backgroundColor)
    private let bottomBlurView = ContainerView(background: .backgroundColor)
    private let leftBlurView = ContainerView(background: .backgroundColor)
    private let rightBlurView = ContainerView(background: .backgroundColor)
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.textColor = .titleColor
        label.font = .systemFont(ofSize: 18)
        label.numberOfLines = 2
        label.scaleToWidth()
        
        return label
    }()
    let frameContainerView = ContainerView(background: .clear)
    private let buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    let retakeButton: LeadingIconButton = {
        let button = LeadingIconButton(iconSize: .init(width: 20, height: 20))
        button.setupDetails(.localImage("ic_camera").withRenderingMode(.alwaysTemplate), title: "retake_picture".localize)
        button.setIconColor(.primaryColor)
        button.setTextColor(.primaryColor)
        button.backgroundColor = .clear
        button.constrainHeight(constant: 40)
        
        return button
    }()
    let continueButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "continue".localize)
        button.backgroundColor = .primaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    var image: UIImage?
    let frameWidth = UIConstants.screenWidth * 0.9
    var isUpgradeAccount: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = isUpgradeAccount ? "title_upgrade_wing_account".localize : "title_register_wing_account".localize
        imageView.image = image
        
        retakeButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(frameContainerView)
        frameContainerView.centerXInSuperview()
        frameContainerView.anchor(top: view.layoutMarginsGuide.topAnchor,
                                  leading: nil,
                                  bottom: nil,
                                  trailing: nil,
                                  padding: .init(top: 128, left: 0, bottom: 0, right: 0))
        
        frameContainerView.addSubview(imageView)
        imageView.fillInSuperView()
        
        view.addSubview(titleLabel)
        titleLabel.anchor(top: nil,
                          leading: view.leadingAnchor,
                          bottom: frameContainerView.topAnchor,
                          trailing: view.trailingAnchor,
                          padding: .init(top: 0, left: 64, bottom: 24, right: 64))
        
        view.addSubview(topBlurView)
        topBlurView.anchor(top: view.topAnchor,
                           leading: view.leadingAnchor,
                           bottom: frameContainerView.topAnchor,
                           trailing: view.trailingAnchor,
                           padding: .init(top: 0, left: 0, bottom: -3, right: 0))
        
        view.addSubview(bottomBlurView)
        bottomBlurView.anchor(top: frameContainerView.bottomAnchor,
                              leading: view.leadingAnchor,
                              bottom: view.bottomAnchor,
                              trailing: view.trailingAnchor,
                              padding: .init(top: -3, left: 0, bottom: 0, right: 0))
        
        view.addSubview(leftBlurView)
        leftBlurView.anchor(top: topBlurView.bottomAnchor,
                            leading: topBlurView.leadingAnchor,
                            bottom: bottomBlurView.topAnchor,
                            trailing: frameContainerView.leadingAnchor,
                            padding: .init(top: 0, left: 0, bottom: 0, right: -3))
        
        view.addSubview(rightBlurView)
        rightBlurView.anchor(top: topBlurView.bottomAnchor,
                             leading: frameContainerView.trailingAnchor,
                             bottom: bottomBlurView.topAnchor,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 0, left: -3, bottom: 0, right: 0))
        
        view.addSubview(buttonStackView)
        buttonStackView.anchor(top: nil,
                               leading: frameContainerView.leadingAnchor,
                               bottom: view.layoutMarginsGuide.bottomAnchor,
                               trailing: frameContainerView.trailingAnchor,
                               padding: .init(top: 0, left: 0, bottom: 16, right: 0))
        
        buttonStackView.addArrangedSubview(retakeButton)
        buttonStackView.addArrangedSubview(continueButton)
        
        retakeButton.setRoundedView()
        retakeButton.setBorder(width: 1, color: .primaryColor)
        continueButton.setRoundedView()
        
        view.bringSubviewToFront(titleLabel)
        frameContainerView.setCornerRadius(radius: 16)
    }
    
    func setTitle(_ text: String) {
        titleLabel.text = text
    }
}
