//
//  FrontDocumentCameraViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 25/11/21.
//

import UIKit

class FrontDocumentCameraViewController: BaseCameraViewController {
    
    private let captureZoneImageView: UIImageView = {
        let imageView = UIImageView(image: .localImage("ic_card_frame"))
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleToFill
        
        return imageView
    }()
    private let iconStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        
        return stackView
    }()
    
    private let noVibrateCameraView: IconTextView = {
        let view = IconTextView()
        view.setupDetails(icon: .localImage("ic_no_vibrate_camera"), text: "no_vibrate_camera".localize)
        return view
    }()
    private let noBackCardView: IconTextView = {
        let view = IconTextView()
        view.setupDetails(icon: .localImage("ic_no_back_card"), text: "no_back_card".localize)
        return view
    }()
    private let noBlurInformationView: IconTextView = {
        let view = IconTextView()
        view.setupDetails(icon: .localImage("ic_no_blur_information"), text: "no_blur_information".localize)
        return view
    }()
    
    override var capturedImage: UIImage? {
        didSet {
            let vc = FrontDocumentPreviewViewController()
            vc.image = capturedImage
            vc.isUpgradeAccount = isUpgradeAccount
            if let handler = self.onContinueButtonTap {
                vc.setOnContinueButtonTap(handler: handler)
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTitle("take_identity_front_photo".localize)
        setCameraLayerFrame(.init(width: frameWidth, height: frameWidth / 1.5))
    }
    
    override func generateUI() {
        super.generateUI()
        
        frameContainerView.addSubview(captureZoneImageView)
        captureZoneImageView.fillInSuperView()
        captureZoneImageView.constrainWidth(constant: frameWidth)
        captureZoneImageView.constrainHeight(constant: frameWidth / 1.5)
        
        view.addSubview(iconStackView)
        iconStackView.anchor(top: frameContainerView.bottomAnchor,
                             leading: frameContainerView.leadingAnchor,
                             bottom: nil,
                             trailing: frameContainerView.trailingAnchor,
                             padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        
        iconStackView.addArrangedSubview(noBackCardView)
        iconStackView.addArrangedSubview(noBlurInformationView)
        iconStackView.addArrangedSubview(noVibrateCameraView)
        
        bringCaptureButtonFront() 
    }
}
