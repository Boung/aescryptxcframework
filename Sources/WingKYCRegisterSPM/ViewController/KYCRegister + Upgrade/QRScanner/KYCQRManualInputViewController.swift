//
//  KYCQRManualInputViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 5/2/22.
//

import Foundation
import UIKit

class KYCQRManualInputViewController: BaseViewController {
    
    private let containerView = ContainerView(background: .backgroundColor)
    private let closeButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setImage(.localImage("ic_close_x").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .white
        button.constrainWidth(constant: 30)
        button.constrainHeight(constant: 30)
        
        return button
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.textColor = .subTitleColor
        label.font = .boldSystemFont(ofSize: 15)
        
        return label
    }()
    private let inputTextField: BaseTextField = {
        let textField = BaseTextField()
        textField.textField.textAlignment = .center
        textField.textField.keyboardType = .numberPad
        
        return textField
    }()
    private let verifyButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .secondaryColor
        button.setTitle(string: "verify_button".localize)
        button.setDefaultHeight()
        
        return button
    }()
    
    private var verifyButtonHandler: ((String) -> Void)?
    private var inputValue: String {
        return inputTextField.textField.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        verifyButton.setAction { [weak self] in
            guard let self = self else { return }
            
            guard self.inputValue.isNotEmpty else {
                self.showErrorAlert("please_input_the_value".localize)
                return
            }
            
            self.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                
                guard let verifyButtonHandler = self.verifyButtonHandler else { return }
                verifyButtonHandler(self.inputValue)
            }
        }
        
        closeButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
//        view.backgroundColor = .blurBackground
        
        view.addSubview(containerView)
        containerView.centerInSuperview()
        containerView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 0, left: 32, bottom: 0, right: 0))
        
        containerView.addSubview(titleLabel)
        titleLabel.anchor(top: containerView.topAnchor,
                          leading: containerView.leadingAnchor,
                          bottom: nil,
                          trailing: containerView.trailingAnchor,
                          padding: .init(top: 16, left: 16, bottom: 0, right: 16))
        
        containerView.addSubview(inputTextField)
        inputTextField.anchor(top: titleLabel.bottomAnchor,
                              leading: titleLabel.leadingAnchor,
                              bottom: nil,
                              trailing: titleLabel.trailingAnchor,
                              padding: .init(top: 32, left: 0, bottom: 0, right: 0))
        
        containerView.addSubview(verifyButton)
        verifyButton.anchor(top: inputTextField.bottomAnchor,
                            leading: containerView.leadingAnchor,
                            bottom: containerView.bottomAnchor,
                            trailing: containerView.trailingAnchor,
                            padding: .init(top: 64, left: 32, bottom: 16, right: 32))
        
        view.addSubview(closeButton)
        closeButton.centerXInSuperview()
        closeButton.anchor(top: nil,
                           leading: nil,
                           bottom: containerView.topAnchor,
                           trailing: nil,
                           padding: .init(top: 0, left: 0, bottom: 16, right: 0))
        
        containerView.setCornerRadius(radius: 16)
        verifyButton.setRoundedView()
        setupBlurBackground()
    }
    
    func setupViewController(title string: String, placeholder: String, completion: @escaping (String) -> Void) {
        titleLabel.text = string
        inputTextField.setPlaceholderNoTitle(placeholder)
        verifyButtonHandler = completion
    }
}
