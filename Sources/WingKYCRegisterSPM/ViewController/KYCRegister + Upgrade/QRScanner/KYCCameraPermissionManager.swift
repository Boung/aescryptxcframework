//
//  KYCCameraPermissionManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 13/1/22.
//

import Foundation

import UIKit
import AVFoundation
import Photos

class KYCCamearaPermissionManager {
    static let shared = KYCCamearaPermissionManager()
    
    private init() {}
    
    var permission: AVAuthorizationStatus = .restricted
    
    func requestAuthorization(completionHandler: @escaping (AVAuthorizationStatus) -> Void) {
        print(#function)
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch status{
        case .authorized:
            permission = .authorized
            completionHandler(permission)
        case .denied, .restricted:
            permission = .denied
            completionHandler(permission)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                DispatchQueue.main.async {
                    if !granted {
                        self.permission = .denied
                        completionHandler(self.permission)
                    } else {
                        self.permission = .authorized
                        completionHandler(self.permission)
                    }
                }
            }
        default: break
        }
    }
    
    func requestPhotoAuthorization(completionHandler: @escaping (PHAuthorizationStatus) -> Void) {
        let status = PHPhotoLibrary.authorizationStatus()
        
        switch status {
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { _status in
                
                if _status == .authorized {
                    completionHandler(.authorized)
                }else {
                    completionHandler(.denied)
                }
            }
        default:
            completionHandler(status)
        }
    }
    
    func isAuthorized() -> Bool {
        switch permission {
        case .authorized:
            return true
        default:
            return false
        }
    }
}

