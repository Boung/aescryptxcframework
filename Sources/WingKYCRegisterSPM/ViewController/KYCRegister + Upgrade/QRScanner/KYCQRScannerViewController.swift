//
//  KYCQRScannerViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 13/1/22.
//

import UIKit

class KYCQRScannerViewController: BaseViewController {
    
    private let topBlurView: UIView = {
        let view = UIView()
        view.backgroundColor = .blurBackground
        return view
    }()
    private let leftBlurView: UIView = {
        let view = UIView()
        view.backgroundColor = .blurBackground
        return view
    }()
    private let rightBlurView: UIView = {
        let view = UIView()
        view.backgroundColor = .blurBackground
        return view
    }()
    private let bottomBlurView: UIView = {
        let view = UIView()
        view.backgroundColor = .blurBackground
        return view
    }()
    private let scannerContainerView = KYCQRScannerView()
    private let closeButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setImage(.localImage("ic_close").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .white
        button.constrainWidth(constant: 30)
        button.constrainHeight(constant: 30)
        
        return button
    }()
    private let scannerFrame: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        
        return imageView
    }()
    private let manualButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setTitleColor(.white, for: .normal)
        button.setDefaultHeight()
        button.isHidden = true
        
        return button
    }()
    
    private var onScanSuccess: ((String) -> Void)?
    private var onManualButtonTap: (() -> Void)?
    var isQR: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        generateUI()
        scannerContainerView.delegate = self
        scannerFrame.image = .localImage(isQR ? "ic_qr_scanner_frame" : "ic_barcode_scanner_frame")
        
        manualButton.setAction { [weak self] in
            guard let self = self else { return }
            guard let handler = self.onManualButtonTap else { return }
            
            handler()
        }
        
        closeButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.onCloseButtonTap()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let additionSize: CGFloat = 24
        let frame = CGRect(x: scannerFrame.frame.minX - (additionSize / 2) ,
                           y: scannerFrame.frame.minY  - (additionSize / 2),
                           width: scannerFrame.frame.size.width + 24,
                           height: scannerFrame.frame.size.height + 24)
        scannerContainerView.setRectOfInterest(frame)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = "scan".localize
        
        KYCCamearaPermissionManager.shared.requestAuthorization { (status) in
            switch status {
            case .authorized:
                self.scannerContainerView.startScanning()
            default:
                break
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        scannerContainerView.stopScanning()
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(scannerContainerView)
        scannerContainerView.anchor(top: view.topAnchor,
                                    leading: view.leadingAnchor,
                                    bottom: view.bottomAnchor,
                                    trailing: view.trailingAnchor)
        
        let scannerWidth = UIScreen.main.bounds.width * 0.7
        view.addSubview(scannerFrame)
        scannerFrame.centerInSuperview(x: 0, y: -50)
        scannerFrame.constrainWidth(constant: scannerWidth)
        scannerFrame.constrainHeight(constant: isQR ? scannerWidth : 100)
        
        view.addSubview(topBlurView)
        topBlurView.anchor(top: view.topAnchor,
                           leading: view.leadingAnchor,
                           bottom: scannerFrame.topAnchor,
                           trailing: view.trailingAnchor)
        
        view.addSubview(rightBlurView)
        rightBlurView.anchor(top: scannerFrame.topAnchor,
                             leading: scannerFrame.trailingAnchor,
                             bottom: scannerFrame.bottomAnchor,
                             trailing: view.trailingAnchor)
        
        view.addSubview(bottomBlurView)
        bottomBlurView.anchor(top: scannerFrame.bottomAnchor,
                              leading: view.leadingAnchor,
                              bottom: view.bottomAnchor,
                              trailing: view.trailingAnchor)
        
        view.addSubview(leftBlurView)
        leftBlurView.anchor(top: scannerFrame.topAnchor,
                            leading: view.leadingAnchor,
                            bottom: scannerFrame.bottomAnchor,
                            trailing: scannerFrame.leadingAnchor)
        
        view.addSubview(manualButton)
        manualButton.anchor(top: scannerFrame.bottomAnchor,
                            leading: scannerFrame.leadingAnchor,
                            bottom: nil,
                            trailing: scannerFrame.trailingAnchor,
                            padding: .init(top: 16, left: 0, bottom: 0, right: 0))
        
        view.addSubview(closeButton)
        closeButton.anchor(top: view.layoutMarginsGuide.topAnchor,
                           leading: view.leadingAnchor,
                           bottom: nil,
                           trailing: nil,
                           padding: .init(top: 16, left: 16, bottom: 0, right: 0))
        
        manualButton.setBorder(width: 1, color: .white)
    }
    
    func setupManualButton(title: String, onTapHandler: @escaping () -> Void) {
        manualButton.isHidden = false
        manualButton.setTitle(string: title)
        onManualButtonTap = onTapHandler
    }
    
    func setOnScanSuccessHandler(handler: @escaping (String) -> Void) {
        self.onScanSuccess = handler
    }
    
    @objc private func onCloseButtonTap() {
        dismiss(animated: true, completion: nil)
    }
}

extension KYCQRScannerViewController: KYCQRScannerViewDelegate {
    func qrScanningDidFail() {
        
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        guard let handler = onScanSuccess else {
            dismiss(animated: true, completion: nil)
            return
        }
        
        dismiss(animated: false) { handler(str ?? "") }
    }
    
    func qrScanningDidStop() {
        
    }
}
