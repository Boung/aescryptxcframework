//
//  KYCQRScannerView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 13/1/22.
//

import UIKit
import AVFoundation

protocol KYCQRScannerViewDelegate: AnyObject {
    func qrScanningDidFail()
    func qrScanningSucceededWithCode(_ str: String?)
    func qrScanningDidStop()
}

class KYCQRScannerView: UIView {
    
    weak var delegate: KYCQRScannerViewDelegate?
    
    var captureSession: AVCaptureSession?
    let metadataOutput = AVCaptureMetadataOutput()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialSetup()
    }
    
    //MARK: overriding the layerClass to return `AVCaptureVideoPreviewLayer`.
    override class var layerClass: AnyClass  {
        return AVCaptureVideoPreviewLayer.self
    }
    
    override var layer: AVCaptureVideoPreviewLayer {
        return super.layer as! AVCaptureVideoPreviewLayer
    }
}

extension KYCQRScannerView {
    var isRunning: Bool {
        return captureSession?.isRunning ?? false
    }
    
    func startScanning() {
        captureSession?.startRunning()
    }
    
    func stopScanning() {
#if targetEnvironment(simulator)
        DispatchQueue.global(qos: .background).async {
            self.captureSession?.stopRunning()
            self.delegate?.qrScanningDidStop()
        }
#else
        self.captureSession?.stopRunning()
        self.delegate?.qrScanningDidStop()
#endif
    }
    
    /// Does the initial setup for captureSession
    private func initialSetup() {
        clipsToBounds = true
        
        let videoCaptureDevice = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .unspecified).devices
        captureSession = AVCaptureSession()
        
        
        guard
            let captureSession = captureSession,
            let device = videoCaptureDevice.filter({ $0.position == .back }).first
        else { return }
        
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: device)
        } catch { return }
        
        if captureSession.canAddInput(videoInput) {
            captureSession.addInput(videoInput)
        } else {
            return scanningDidFail()
        }
        
        if captureSession.canAddOutput(metadataOutput) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: .main)
            metadataOutput.metadataObjectTypes = [.qr, .ean13, .code128]
        } else {
            return scanningDidFail()
        }
        
        layer.session = captureSession
        layer.videoGravity = .resizeAspectFill
        
        captureSession.startRunning()
    }
    
    func scanningDidFail() {
        delegate?.qrScanningDidFail()
        captureSession = nil
    }
    
    func setRectOfInterest(_ rect: CGRect) {
        layer.layoutIfNeeded()
        metadataOutput.rectOfInterest = layer.metadataOutputRectConverted(fromLayerRect: rect)
    }
}

extension KYCQRScannerView: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        self.stopScanning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            self.delegate?.qrScanningSucceededWithCode(stringValue)
        }
    }
}
