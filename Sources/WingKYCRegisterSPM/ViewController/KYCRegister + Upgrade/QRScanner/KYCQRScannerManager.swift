//
//  KYCQRScannerManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 13/1/22.
//

import UIKit

class KYCQRScannerManager {
    
    static let shared = KYCQRScannerManager()
    
    private init() { }
    
    func presentQRScanner(isQR: Bool = true, manualButtonTitle: String? = nil, onManualButtonTap: (() -> Void)? = nil, completion: @escaping (String) -> Void) {
        guard let topVC = UIApplication.shared.getTopViewController() else { return }

        let vc = KYCQRScannerViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.isQR = isQR
        vc.setOnScanSuccessHandler(handler: completion)
        
        if let manualButtonTitle = manualButtonTitle, let onManualButtonTap = onManualButtonTap {
            vc.setupManualButton(title: manualButtonTitle, onTapHandler: onManualButtonTap)
        }

        topVC.present(vc, animated: true, completion: nil)
    }
}
