//
//  UpgradeCustomerInformationEditViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/11/21.
//

import UIKit

class OtherIdentityCustomerInformationEditViewController: BaseViewController {
  
  private let scrollView = ScrollViewContainerView()
  private var containerView: UIView {
    return scrollView.containerView
  }
  private let vStackView: UIStackView = {
    let stackView = UIStackView()
    stackView.axis = .vertical
    stackView.alignment = .fill
    stackView.distribution = .fill
    stackView.spacing = 16
    
    return stackView
  }()
  private let imageContainerView = ContainerView(background: .white)
  private var imageView: CustomerImagesView?
  
  let informationView = OtherIdentityCustomerInformationEditView()
  let otherRequestInformationView = OtherRequestInformationEditView()
  
  let shopInformationView = ShopInformationView()
  let paymentMethodView = PaymentMethodView()
  
  private let agreementView = AgreementView()
  
  private let registerButton: BaseButton = {
    let button = BaseButton()
    button.setTitle(string: "continue".localize)
    button.setDefaultHeight()
    
    return button
  }()
  
  var isNID: Bool = false
  var isUpgradeAccount: Bool = false
  var isMapSelected: Bool = false
  var isPersonalAccount = true
  var registerUserValidateParam = RegisterUserValidateParam()
  var upgradeUserValidateParam: UpgradeUserValidateParam?
  var singleAccountValidateParam = SingleAccountRegisterValidateParam()
  
  override func viewDidLoad() {
    imageView = .init(stackView: .horizontal, isNID: isNID)
    super.viewDidLoad()
    
    navigationItem.title = !isUpgradeAccount ? "title_register_wing_account".localize : "title_upgrade_wing_account".localize
    
    shopInformationView.isHidden = true
    paymentMethodView.isHidden = true
    
    if isUpgradeAccount, let upgradeParam = upgradeUserValidateParam {
      informationView.setDefaultValues(isUpgrade: true, model: upgradeParam)
      otherRequestInformationView.setDefaultValues(isUpgrade: isUpgradeAccount, model: upgradeParam)
      imageView?.setupDetails(customerImage: upgradeUserValidateParam?.customerImage,
                              idFront: upgradeUserValidateParam?.identityFrontImage,
                              idBack: isNID ? nil : upgradeUserValidateParam?.identityBackImage)
    }else {
      informationView.setDefaultValues(isUpgrade: false, model: registerUserValidateParam)
      otherRequestInformationView.setDefaultValues(isUpgrade: false, model: registerUserValidateParam)
      imageView?.setupDetails(customerImage: registerUserValidateParam.customerImage,
                              idFront: registerUserValidateParam.identityFrontImage,
                              idBack: isNID ? nil : registerUserValidateParam.identityBackImage)
    }
    
    informationView.setViewForUpgrade(isUpgradeAccount)
    informationView.setonAccountCreationPurposeChanged { [weak self] in
      guard let self = self else { return }
      
      self.storeInputedValue()
      let isPersonalAccount = self.informationView.accountCreationSelectedIndex == 0
      self.isPersonalAccount = isPersonalAccount
      
      self.shopInformationView.isHidden = isPersonalAccount
      self.paymentMethodView.isHidden = isPersonalAccount
    }
    informationView.accountCreationPurposeView.setValueWhere(index: 0)
    
    shopInformationView.selectMapButton.setAction { [weak self] in
      guard let self = self else { return }
      
      self.storeInputedValue()
      self.openMapFullScreenViewController()
    }
    
    shopInformationView.mapView.setTapAction { [weak self] in
      guard let self = self else { return }
      
      self.storeInputedValue()
      self.openMapFullScreenViewController()
    }
    
    setBackButtonAction { [weak self] in
      guard let self = self else { return }
      
      self.storeInputedValue()
      self.navigationController?.popViewController(animated: true)
    }
    
    registerButton.setAction { [weak self] in
      guard let self = self else { return }
      guard self.agreementView.isAgreed else {
        self.showErrorAlert("error_please_agree_the_valid_information".localize)
        return
      }
      
      self.goToOverralInformationVC()
    }
    
    otherRequestInformationView.setOnScanButtonTapHandler { [weak self] in
      guard let self = self else { return }
      
      self.storeInputedValue()
    }
  }
  
  override func generateUI() {
    super.generateUI()
    
    view.backgroundColor = .backgroundColor
    
    view.addSubview(scrollView)
    scrollView.fillInSuperView()
    
    containerView.addSubview(vStackView)
    vStackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
    
    vStackView.addArrangedSubview(imageContainerView)
    vStackView.addArrangedSubview(informationView)
    vStackView.addArrangedSubview(otherRequestInformationView)
    vStackView.addArrangedSubview(shopInformationView)
    vStackView.addArrangedSubview(paymentMethodView)
    vStackView.addArrangedSubview(agreementView)
    vStackView.addArrangedSubview(registerButton)
    
    imageContainerView.addSubview(imageView!)
    imageView?.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
    
    imageContainerView.setCornerRadius(radius: 16)
    registerButton.setRoundedView()
  }
  
  override func autoFillMockupData() {
    super.autoFillMockupData()
    
    informationView.autoFillMockupData()
    shopInformationView.autoFillMockupData()
    paymentMethodView.autoFillMockupData()
  }
  
  private func goToOverralInformationVC() {
    if isPersonalAccount {
      let vc = CustomerOverrallInformationViewController()
      vc.modalPresentationStyle = .overFullScreen
      vc.modalTransitionStyle = .crossDissolve
      vc.isNID = isNID
      vc.isUpgradeAccount = isUpgradeAccount
      
      otherRequestInformationView.observeErrorFields()
      
      if isUpgradeAccount {
        guard let param = informationView.generateUpgradeParam() else { return }
        guard validateKitAndDebitCard() else { return }
        updateValidateParamWith(param: param)
        
        vc.upgradeUserValidateParam = upgradeUserValidateParam ?? .init()
        Constants.storedUpgradeUserValidateParam = upgradeUserValidateParam
      }else {
        guard let param = informationView.generateRegisterParam() else { return }
        guard validateKitAndDebitCard() else { return }
        updateValidateParamWith(param: param)
        
        vc.registerUserValidateParam = registerUserValidateParam
        Constants.storedRegisterUserValidateParam = registerUserValidateParam
      }
      
      navigationController?.pushViewController(vc, animated: true)
    }else {
      guard validateSingleAccountParam() else { return }
      
      let vc = SingleAccountCustomerOverallInformationViewController()
      vc.isNID = self.isNID
      vc.isUpgradeAccount = self.isUpgradeAccount
      vc.registerUserValidateParam = self.registerUserValidateParam
      vc.upgradeUserValidateParam = self.upgradeUserValidateParam ?? .init()
      vc.singleAccountValidateParam = self.singleAccountValidateParam
      
      self.navigationController?.pushViewController(vc, animated: true)
    }
  }
  
  private func validateKitAndDebitCard() -> Bool {
    if otherRequestInformationView.isRequestWingCard {
      guard otherRequestInformationView.kitNumber.isNotEmpty else {
        registerUserValidateParam.kitNumber = nil
        upgradeUserValidateParam?.kitNumber = nil
        return false
      }
      
      upgradeUserValidateParam?.registerWith = Constants.kitRegister
      upgradeUserValidateParam?.kitNumber = otherRequestInformationView.kitNumber
      registerUserValidateParam.registerWith = Constants.kitRegister
      registerUserValidateParam.kitNumber = otherRequestInformationView.kitNumber
    }else {
      upgradeUserValidateParam?.registerWith = Constants.phoneRegister
      upgradeUserValidateParam?.kitNumber = nil
      registerUserValidateParam.registerWith = Constants.phoneRegister
      registerUserValidateParam.kitNumber = nil
    }
    
    if otherRequestInformationView.isRequestDebitCard {
      guard let debitCardInformation = otherRequestInformationView.debitCardInformationResponse else {
        registerUserValidateParam.debitCardInformation = nil
        upgradeUserValidateParam?.debitCardInformation = nil
        return false
      }
      
      upgradeUserValidateParam?.debitCardInformation = debitCardInformation
      registerUserValidateParam.debitCardInformation = debitCardInformation
    }else {
      registerUserValidateParam.debitCardInformation = nil
      upgradeUserValidateParam?.debitCardInformation = nil
    }
    
    return true
  }
  
  private func validateSingleAccountParam() -> Bool {
    shopInformationView.observeErrorField()
    paymentMethodView.observeErrorField()
    
    guard let validateParam = informationView.generateRegisterParam() else { return false }
    guard validateKitAndDebitCard() else { return false }
    guard shopInformationView.validateField() else { return false }
    guard let paymentMethod = paymentMethodView.getPaymentMethod() else { return false }
    
    updateValidateParamWith(param: validateParam)
    Constants.storedRegisterUserValidateParam = registerUserValidateParam
    
    singleAccountValidateParam.appendRegisterValidateParam(param: registerUserValidateParam)
    singleAccountValidateParam.tillNumber = shopInformationView.tillNumber
    singleAccountValidateParam.shopName = shopInformationView.shopName
    singleAccountValidateParam.businessType = shopInformationView.shopCategroy
    singleAccountValidateParam.shopAddress = shopInformationView.shopAddress
    singleAccountValidateParam.latitude = shopInformationView.shopCoordination?.latitude ?? 0.0
    singleAccountValidateParam.longitude = shopInformationView.shopCoordination?.longitude ?? 0.0
    singleAccountValidateParam.patantImage = shopInformationView.patantImage
    singleAccountValidateParam.ownershipImage = shopInformationView.ownershipImage
    singleAccountValidateParam.ownerIDFrontImage = shopInformationView.ownerIDFrontImage
    singleAccountValidateParam.ownerIDBackImage = shopInformationView.ownerIDBackImage
    singleAccountValidateParam.shopImage = shopInformationView.shopImage
    singleAccountValidateParam.merchantName = shopInformationView.shopName
    singleAccountValidateParam.website = shopInformationView.website
    singleAccountValidateParam.companyHistory = shopInformationView.shopHistory
    singleAccountValidateParam.majorProductService = shopInformationView.shopService
    singleAccountValidateParam.loyaltyStatus = (paymentMethod.wingPoint == nil) ? false : true
    
    let addressDetail = AddressDetail()
    addressDetail.addressLine1 = shopInformationView.addressView.houseNumber
    addressDetail.addressLine2 = shopInformationView.addressView.streetNumber
    addressDetail.provinceID = shopInformationView.shopAddress?.provinceID
    addressDetail.districtID = shopInformationView.shopAddress?.districtID
    addressDetail.communeID = shopInformationView.shopAddress?.communeID
    addressDetail.villageID = shopInformationView.shopAddress?.villageID
    addressDetail.latitude = shopInformationView.shopCoordination?.latitude ?? 0.0
    addressDetail.longitude = shopInformationView.shopCoordination?.longitude ?? 0.0
    
    singleAccountValidateParam.companyDetail = MerchantDetail(addressDetail: addressDetail)
    singleAccountValidateParam.paymentMethod = paymentMethod
    
    return true
  }
  
  private func storeInputedValue() {
    if isUpgradeAccount {
      let param = informationView.storeUpgradeValidateParam()
      
      if otherRequestInformationView.isRequestWingCard {
        param.kitNumber = otherRequestInformationView.kitNumber
      }else {
        param.kitNumber = nil
      }
      
      if otherRequestInformationView.isRequestDebitCard {
        param.debitCardInformation = otherRequestInformationView.debitCardInformationResponse
      }else {
        param.debitCardInformation = nil
      }
      
      updateValidateParamWith(param: param)
      Constants.storedUpgradeUserValidateParam = param
    }else {
      let param = informationView.storeRegisterValidateParam()
      
      if otherRequestInformationView.isRequestWingCard {
        param.kitNumber = otherRequestInformationView.kitNumber
      }else {
        param.kitNumber = nil
      }
      
      if otherRequestInformationView.isRequestDebitCard {
        param.debitCardInformation = otherRequestInformationView.debitCardInformationResponse
      }else {
        param.debitCardInformation = nil
      }
      
      updateValidateParamWith(param: param)
      Constants.storedRegisterUserValidateParam = param
    }
  }
  
  private func updateValidateParamWith(param: RegisterUserValidateParam) {
    if isUpgradeAccount {
      upgradeUserValidateParam?.idType = param.idType
      upgradeUserValidateParam?.idNumber = param.idNumber
      upgradeUserValidateParam?.expiryDate = param.expiryDate
      upgradeUserValidateParam?.lastNameKh = param.lastNameKh
      upgradeUserValidateParam?.firstNameKh = param.firstNameKh
      upgradeUserValidateParam?.lastName = param.lastName
      upgradeUserValidateParam?.firstName = param.firstName
      upgradeUserValidateParam?.titles = param.titles
      upgradeUserValidateParam?.maritalStauts = param.maritalStauts
      upgradeUserValidateParam?.phoneNumber = param.phoneNumber
      upgradeUserValidateParam?.nationality = param.nationality
      upgradeUserValidateParam?.gender = param.gender
      upgradeUserValidateParam?.dob = param.dob
      
      upgradeUserValidateParam?.birthAddress = param.birthAddress
      upgradeUserValidateParam?.birthAddressTextModel = param.birthAddressTextModel
      upgradeUserValidateParam?.birthAddressString = param.birthAddressString
      
      upgradeUserValidateParam?.currentAddress = param.currentAddress
      upgradeUserValidateParam?.currentAddressTextModel = param.currentAddressTextModel
      upgradeUserValidateParam?.currentAddressString = param.currentAddressString
      
      upgradeUserValidateParam?.sourceOfFund = param.sourceOfFund
      upgradeUserValidateParam?.monthlyAvgIncome = param.monthlyAvgIncome
      upgradeUserValidateParam?.sector = param.sector
      upgradeUserValidateParam?.occupationID = param.occupationID
      
      upgradeUserValidateParam?.cashOutPackage = param.cashOutPackage
      upgradeUserValidateParam?.bankingPurpose = param.bankingPurpose
    }else {
      registerUserValidateParam.idType = param.idType
      registerUserValidateParam.idNumber = param.idNumber
      registerUserValidateParam.expiryDate = param.expiryDate
      registerUserValidateParam.lastNameKh = param.lastNameKh
      registerUserValidateParam.firstNameKh = param.firstNameKh
      registerUserValidateParam.lastName = param.lastName
      registerUserValidateParam.firstName = param.firstName
      registerUserValidateParam.titles = param.titles
      registerUserValidateParam.maritalStauts = param.maritalStauts
      registerUserValidateParam.phoneNumber = param.phoneNumber
      registerUserValidateParam.nationality = param.nationality
      registerUserValidateParam.gender = param.gender
      registerUserValidateParam.dob = param.dob
      
      registerUserValidateParam.birthAddress = param.birthAddress
      registerUserValidateParam.birthAddressTextModel = param.birthAddressTextModel
      registerUserValidateParam.birthAddressString = param.birthAddressString
      
      registerUserValidateParam.currentAddress = param.currentAddress
      registerUserValidateParam.currentAddressTextModel = param.currentAddressTextModel
      registerUserValidateParam.currentAddressString = param.currentAddressString
      
      registerUserValidateParam.sourceOfFund = param.sourceOfFund
      registerUserValidateParam.monthlyAvgIncome = param.monthlyAvgIncome
      registerUserValidateParam.sector = param.sector
      registerUserValidateParam.occupationID = param.occupationID
      
      registerUserValidateParam.currency = param.currency
      registerUserValidateParam.accountType = param.accountType
      registerUserValidateParam.cashOutPackage = param.cashOutPackage
      registerUserValidateParam.bankingPurpose = param.bankingPurpose
    }
  }
  
  private func openMapFullScreenViewController() {
    switch LocationManager.shared.currentStatus {
      case .authorizedWhenInUse, .authorizedAlways:
        gotoFullScreenVC()
      default:
        LocationManager.shared.requestLocationAuthorization { [weak self] status in
          guard let self = self else { return }
          
          if status == .authorizedAlways || status == .authorizedWhenInUse {
            self.gotoFullScreenVC()
          }else {
            let vc = EnableLocationServiceViewController()
            
            self.navigationController?.pushViewController(vc, animated: true)
          }
        }
    }
  }
  
  private func gotoFullScreenVC() {
    let vc = FullScreenMapViewController()
    vc.currentLocation = self.shopInformationView.mapView.marker?.position ?? LocationManager.shared.currentLocation
    vc.setOnAddLocationAction { [weak self] location in
      guard let self = self else { return }
      
      self.isMapSelected = true
      self.shopInformationView.mapView.isHidden = false
      self.shopInformationView.shopCoordination = location
      self.shopInformationView.selectMapButton.isHidden = true
      self.shopInformationView.mapView.addMarker(at: location)
    }
    
    self.navigationController?.pushViewController(vc, animated: true)
  }
}
