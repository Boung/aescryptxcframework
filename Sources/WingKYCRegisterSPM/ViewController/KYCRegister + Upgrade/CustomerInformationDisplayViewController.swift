//
//  CustomerInformationDisplayViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/11/21.
//

import UIKit

class CustomerInformationDisplayViewController: BaseViewController {
    
    private let scrollView = ScrollViewContainerView()
    private var containerView: UIView {
        return scrollView.containerView
    }
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let imageContainerView = ContainerView(background: .white)
    private var imageView: CustomerImagesView?
    
    let informationStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let informationView = CustomerInformationDisplayView()
    
    private let editButton: LeadingIconButton = {
        let button = LeadingIconButton(iconSize: .init(width: 15, height: 15))
        button.setupDetails(.localImage("ic_edit"), title: "edit".localize)
        button.setIconColor(.secondaryColor)
        button.setTextColor(.secondaryColor)
        
        return button
    }()
    private let continueButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "continue".localize)
        button.backgroundColor = .primaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    var isNID: Bool = false
    var isUpgradeAccount: Bool = false
    var customerWingAccount: String = ""
    var registerUserValidateParam = RegisterUserValidateParam()
    var upgradeUserValidateParam = UpgradeUserValidateParam()
    
    override func viewDidLoad() {
        imageView = .init(stackView: .vertical, isNID: isNID)
        super.viewDidLoad()
        
        navigationItem.title = !isUpgradeAccount ? "title_register_wing_account".localize : "title_upgrade_wing_account".localize
        
        removeBackButton()
        setupDocumentImages()
        checkValidInformationForRedirection()
        
        editButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.openEditInformationVC(isRedirected: false)
        }
        
        continueButton.setAction { [weak self] in
            guard let self = self else { return }
            
            var isPassport = false
            var isValidExpiry = false
            var isValidAge = false
            
            if self.isUpgradeAccount {
                isPassport = self.upgradeUserValidateParam.isPassport
                isValidExpiry = self.upgradeUserValidateParam.isValidExpiryDate
                isValidAge = self.upgradeUserValidateParam.isValidAge
            }else {
                isPassport = self.registerUserValidateParam.isPassport
                isValidExpiry = self.registerUserValidateParam.isValidExpiryDate
                isValidAge = self.registerUserValidateParam.isValidAge
            }
            
            guard isValidAge, isValidExpiry else {
                let expiryIDMessage = !isValidExpiry ? "\(isPassport ? "passport_expired" : "id_card_expired")".localize : ""
                let ageInvalidMessage = !isValidAge ? "age_not_eligible".localize : ""
                let message = "invalid_customer_information".localize + expiryIDMessage + ageInvalidMessage
                
                AlertManager.shared.showSingleButtonAlert(title: "we_got_wrong_information".localize, message: message, alertButton: .init(title: "retry".localize, style: .default, handler: {
                    self.navigationController?.popViewController(animated: true)
                }))
                return
            }
                        
            if self.isUpgradeAccount {
                if !self.upgradeUserValidateParam.isValidStepOneRequiredField {
                    self.showMissingDataErrorAlert()
                    return
                }
            }else {
                if !self.registerUserValidateParam.isValidStepOneRequiredField {
                    self.showMissingDataErrorAlert()
                    return
                }
            }
            
            if Constants.authentication.applicationID == .wingEKYC, !self.isUpgradeAccount {
                let vc = SingleAccountAdditionalInformationViewController()
                vc.isNID = self.isNID
                vc.isUpgradeAccount = self.isUpgradeAccount
                vc.registerUserValidateParam = self.registerUserValidateParam
                vc.upgradeUserValidateParam = self.upgradeUserValidateParam
                
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                let vc = CustomerAdditionalInformationViewController()
                vc.isNID = self.isNID
                vc.isUpgradeAccount = self.isUpgradeAccount
                vc.registerUserValidateParam = self.registerUserValidateParam
                vc.upgradeUserValidateParam = self.upgradeUserValidateParam
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        informationView.setupDetail(model: isUpgradeAccount ? upgradeUserValidateParam : registerUserValidateParam)
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.backgroundColor = .backgroundColor
        
        view.addSubview(scrollView)
        scrollView.fillInSuperView()
        
        containerView.addSubview(vStackView)
        vStackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        vStackView.addArrangedSubview(imageContainerView)
        vStackView.addArrangedSubview(informationStackView)
        vStackView.addArrangedSubview(continueButton)
        
        informationStackView.addArrangedSubview(informationView)
        informationStackView.addArrangedSubview(editButton)
        
        imageContainerView.addSubview(imageView!)
        imageView?.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        imageContainerView.setCornerRadius(radius: 16)
        continueButton.setRoundedView()
    }
    
    private func checkValidInformationForRedirection() {
        if isUpgradeAccount {
            if !upgradeUserValidateParam.isValidStepOneRequiredField || !upgradeUserValidateParam.isValidExpiryDate || !upgradeUserValidateParam.isValidAge {
                openEditInformationVC(isRedirected: true)
            }
        }else {
            if !registerUserValidateParam.isValidStepOneRequiredField || !registerUserValidateParam.isValidExpiryDate || !registerUserValidateParam.isValidAge {
                openEditInformationVC(isRedirected: true)
            }
        }
    }
    
    private func showMissingDataErrorAlert() {
        showSingleButtonAlert(title: "information".localize, message: "please_fill_out_your_information".localize, buttonTitle: "fill_now_button".localize) { [weak self] in
            guard let self = self else { return }
            
            self.openEditInformationVC(isRedirected: true)
        }
    }
    
    private func openEditInformationVC(isRedirected: Bool) {
        let vc = CustomerInformationEditViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.isNID = self.isNID
        vc.isRedirected = isRedirected
        vc.userValidateParam = self.isUpgradeAccount ? self.upgradeUserValidateParam : self.registerUserValidateParam
        vc.onSaveButtonHandler = { [weak self] value in
            guard let self = self else { return }
            
            if self.isUpgradeAccount {
                self.upgradeUserValidateParam.idType = value.idType
                self.upgradeUserValidateParam.idNumber = value.idNumber
                self.upgradeUserValidateParam.lastNameKh = value.lastNameKh
                self.upgradeUserValidateParam.firstNameKh = value.firstNameKh
                self.upgradeUserValidateParam.lastName = value.lastName
                self.upgradeUserValidateParam.firstName = value.firstName
                self.upgradeUserValidateParam.dob = value.dob
                self.upgradeUserValidateParam.gender = value.gender
                self.upgradeUserValidateParam.expiryDate = value.expiryDate
                self.upgradeUserValidateParam.nationality = value.nationality
                self.upgradeUserValidateParam.birthAddress = value.birthAddress
                self.upgradeUserValidateParam.birthAddressTextModel = value.birthAddressTextModel
                self.upgradeUserValidateParam.birthAddressString = value.birthAddressString
                self.upgradeUserValidateParam.currentAddress = value.currentAddress
                self.upgradeUserValidateParam.currentAddressTextModel = value.currentAddressTextModel
                self.upgradeUserValidateParam.currentAddressString = value.currentAddressString
            }else {
                self.registerUserValidateParam.idType = value.idType
                self.registerUserValidateParam.idNumber = value.idNumber
                self.registerUserValidateParam.lastNameKh = value.lastNameKh
                self.registerUserValidateParam.firstNameKh = value.firstNameKh
                self.registerUserValidateParam.lastName = value.lastName
                self.registerUserValidateParam.firstName = value.firstName
                self.registerUserValidateParam.dob = value.dob
                self.registerUserValidateParam.gender = value.gender
                self.registerUserValidateParam.expiryDate = value.expiryDate
                self.registerUserValidateParam.nationality = value.nationality
                self.registerUserValidateParam.birthAddress = value.birthAddress
                self.registerUserValidateParam.birthAddressTextModel = value.birthAddressTextModel
                self.registerUserValidateParam.birthAddressString = value.birthAddressString
                self.registerUserValidateParam.currentAddress = value.currentAddress
                self.registerUserValidateParam.currentAddressTextModel = value.currentAddressTextModel
                self.registerUserValidateParam.currentAddressString = value.currentAddressString
            }
        }
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    private func setupDocumentImages() {
        if isUpgradeAccount {
            imageView?.setupDetails(customerImage: upgradeUserValidateParam.customerImage,
                                    idFront: upgradeUserValidateParam.identityFrontImage,
                                    idBack: isNID ? nil : upgradeUserValidateParam.identityBackImage)
        }else {
            imageView?.setupDetails(customerImage: registerUserValidateParam.customerImage,
                                    idFront: registerUserValidateParam.identityFrontImage,
                                    idBack: isNID ? nil : registerUserValidateParam.identityBackImage)
        }
    }
}
