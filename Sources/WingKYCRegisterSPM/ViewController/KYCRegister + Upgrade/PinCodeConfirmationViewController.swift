//
//  PinCodeConfirmationViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/11/21.
//

import UIKit

class PinCodeConfirmationViewController: BaseViewController {

    private let containerView = ContainerView(background: .primaryColor)
    private let headerContainerView = ContainerView(background: .white)
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "confirmation".localize
        label.textAlignment = .center
        
        return label
    }()
    private let closeButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setImage(.localImage("ic_close").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .systemRed
        button.constrainWidth(constant: 30)
        button.constrainHeight(constant: 30)
        
        return button
    }()
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let descriptionLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .white
        label.textAlignment = .center
        
        return label
    }()
    private let pinTextField: BaseTextField = {
        let textField = BaseTextField()
        textField.backgroundColor = .white
        textField.textField.textColor = .subTitleColor
        textField.textField.textAlignment = .center
        textField.textField.keyboardType = .numberPad
        textField.textField.isSecureTextEntry = true
        textField.constrainHeight(constant: 40)
        textField.maxInputLength = 4
        textField.setPlaceholderNoTitle("")
        
        return textField
    }()
    private let confirmButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "confirm".localize)
        button.backgroundColor = .secondaryColor
        button.setDefaultHeight()
        return button
    }()
    
    private var descriptionString: String = ""
    private var placeholder: String = ""
    private var onConfirmButtonTap: ((String) -> Void)?
    private var pinCode: String {
        return pinTextField.textField.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionLabel.text = descriptionString
        pinTextField.textField.attributedPlaceholder = .init(string: placeholder, attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.subTitleColor
        ])
        
        confirmButton.setAction { [weak self] in
            guard let self = self else { return }
            guard self.pinCode.isNotEmpty else {
                self.pinTextField.becomeFirstResponder()
                self.showErrorAlert("please_input_pin_code".localize);
                return
            }
            
            self.pinTextField.resignFirstResponder()
            
            guard let handler = self.onConfirmButtonTap else { return }
            handler(self.pinCode)
        }
        
        closeButton.setAction { [weak self] in
            guard let self = self else { return }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
//        view.backgroundColor = .blurBackground
        
        view.addSubview(containerView)
        containerView.centerInSuperview()
        containerView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 0, left: 32, bottom: 0, right: 0))
        
        containerView.addSubview(headerContainerView)
        headerContainerView.constrainHeight(constant: 50)
        headerContainerView.anchor(top: containerView.topAnchor,
                                   leading: containerView.leadingAnchor,
                                   bottom: nil,
                                   trailing: containerView.trailingAnchor)
        
        headerContainerView.addSubview(closeButton)
        closeButton.centerYInSuperview()
        closeButton.anchor(top: nil,
                           leading: nil,
                           bottom: nil,
                           trailing: headerContainerView.trailingAnchor,
                           padding: .init(top: 0, left: 0, bottom: 0, right: 16))
        
        headerContainerView.addSubview(titleLabel)
        titleLabel.centerInSuperview()
        titleLabel.anchor(top: nil,
                          leading: nil,
                          bottom: nil,
                          trailing: closeButton.leadingAnchor,
                          padding: .init(top: 0, left: 0, bottom: 0, right: 8))
        
        containerView.addSubview(vStackView)
        vStackView.anchor(top: headerContainerView.bottomAnchor,
                          leading: containerView.leadingAnchor,
                          bottom: containerView.bottomAnchor,
                          trailing: containerView.trailingAnchor,
                          padding: .init(top: 16, left: 32, bottom: 16, right: 32))
        
        vStackView.addArrangedSubview(descriptionLabel)
        vStackView.addArrangedSubview(pinTextField)
        vStackView.addArrangedSubview(confirmButton)
        
        pinTextField.setRoundedView()
        pinTextField.setBorder(width: 1, color: .titleColor)
        confirmButton.setRoundedView()
        
        vStackView.setCustomSpacing(32, after: pinTextField)
        setupBlurBackground()
    }
    
    func setupController(_ description: String, placeholder: String, handler: @escaping (String) -> Void) {
        self.descriptionString = description
        self.onConfirmButtonTap = handler
        self.placeholder = placeholder
        
    }
}
