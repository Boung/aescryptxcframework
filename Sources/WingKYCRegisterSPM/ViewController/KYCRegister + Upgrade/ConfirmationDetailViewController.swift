//
//  ConfirmationDetailViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/11/21.
//

import UIKit

class ConfirmationDetailViewController: BaseViewController {
    
    private let containerView = ContainerView(background: .primaryColor)
    private let closeButton:  BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setImage(.localImage("ic_close").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .errorTextField
        button.constrainHeight(constant: 30)
        button.constrainWidth(constant: 30)
        
        return button
    }()
    private let headerContainerView = ContainerView(background: .white)
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .subTitleColor
        label.textAlignment = .center
        
        return label
    }()
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        return tableView
    }()
    private let doneButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "done".localize)
        button.backgroundColor = .secondaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    private var titleString: String = ""
    private var datasources: [KeyValueModel] = []
    private var onDoneButtonTap: (() -> Void)?
    private let tableCell = "KeyValueTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = titleString
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(KeyValueTableViewCell.self, forCellReuseIdentifier: tableCell)
        
        doneButton.setAction { [weak self] in
            guard let self = self else { return }
            guard let handler = self.onDoneButtonTap else { return }
            handler()
        }
        
        closeButton.setAction { [weak self] in
            guard let self = self else { return }
            guard let handler = self.onDoneButtonTap else { return }
            handler()
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
//        view.backgroundColor = .blurBackground
        
        view.addSubview(containerView)
        containerView.centerInSuperview()
        containerView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 0, left: 32, bottom: 0, right: 0))
        
        containerView.addSubview(headerContainerView)
        headerContainerView.constrainHeight(constant: 50)
        headerContainerView.anchor(top: containerView.topAnchor,
                                   leading: containerView.leadingAnchor,
                                   bottom: nil,
                                   trailing: containerView.trailingAnchor)
        
        headerContainerView.addSubview(closeButton)
        closeButton.centerYInSuperview()
        closeButton.anchor(top: nil,
                           leading: nil,
                           bottom: nil,
                           trailing: headerContainerView.trailingAnchor,
                           padding: .init(top: 0, left: 0, bottom: 0, right: 16))
        
        headerContainerView.addSubview(titleLabel)
        titleLabel.centerYInSuperview()
        titleLabel.centerXInSuperview()
        titleLabel.anchor(top: nil,
                          leading: nil,
                          bottom: nil,
                          trailing: closeButton.leadingAnchor,
                          padding: .init(top: 0, left: 0, bottom: 0, right: 16))
        
        
        
        containerView.addSubview(vStackView)
        vStackView.anchor(top: headerContainerView.bottomAnchor,
                          leading: containerView.leadingAnchor,
                          bottom: containerView.bottomAnchor,
                          trailing: containerView.trailingAnchor,
                          padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        tableView.constrainHeight(constant: UIConstants.screenHeight * 0.25)
        vStackView.addArrangedSubview(tableView)
        vStackView.addArrangedSubview(doneButton)
        
        containerView.setCornerRadius(radius: 16)
        doneButton.setRoundedView()
        setupBlurBackground()
    }
    
    func setupController(_ title: String, datasources: [KeyValueModel] , handler: @escaping () -> Void) {
        self.titleString = title
        self.datasources = datasources
        self.onDoneButtonTap = handler
    }
}

extension ConfirmationDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCell, for: indexPath) as! KeyValueTableViewCell
        cell.configCell(model: datasources[indexPath.row])
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
