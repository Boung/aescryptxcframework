//
//  IdentityIntructionDetailView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 24/12/21.
//

import UIKit

class IdentityIntructionDetailView: UIView {

    private let navigationBarView = ContainerView(background: .clear)
    private let backButton: BaseButton = {
        let button = BaseButton()
        button.setImage(.localImage("ic_back").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .white
        button.constrainWidth(constant: 21)
        button.constrainHeight(constant: 21)
        button.tag = 100
        return button
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "personal_id_detection".localize
        label.textAlignment = .center
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 18)
        label.numberOfLines = 2
        label.scaleToWidth()
        
        return label
    }()
    private let captureFrameView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_identity_card_thumbnail")
        
        return imageView
    }()
    private let scrollView = ScrollViewContainerView(withScreenWidth: false)
    private var containerView: UIView {
        return scrollView.containerView
    }
    private let descriptionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    
    
    
    private let buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fillProportionally
        stackView.spacing = 16
        
        return stackView
    }()
    private let buttonTitleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "please_choose_the_certificate_type".localize
        label.textAlignment = .center
        label.textColor = .white
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 2
        label.scaleToWidth()
        
        return label
    }()
    private let nationalIDButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "national_id".localize)
        button.backgroundColor = .secondaryColor
        button.setTitleColor(.white, for: .normal)
        button.setDefaultHeight()
        button.constrainWidth(constant: 200)
        button.tag = 201
        
        return button
    }()
    private let passportButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "passport".localize)
        button.backgroundColor = .secondaryColor
        button.setTitleColor(.white, for: .normal)
        button.setDefaultHeight()
        button.constrainWidth(constant: 200)
        button.tag = 202
        
        return button
    }()
    
    private let frameWidth = UIConstants.screenWidth * 0.9
    var hasBackButton: Bool = false {
        didSet {
            backButton.isHidden = !hasBackButton
        }
    }
    
    init(hasPassport: Bool = true) {
        super.init(frame: .zero)
        
        backgroundColor = .primaryColor
        
        addSubview(navigationBarView)
        navigationBarView.constrainHeight(constant: UIConstants.navigationHeight + UIConstants.topSafeArea)
        navigationBarView.anchor(top: topAnchor,
                                 leading: leadingAnchor,
                                 bottom: nil,
                                 trailing: trailingAnchor)
        
        navigationBarView.addSubview(backButton)
        backButton.centerYTo(view: navigationBarView.centerYAnchor, constant: UIConstants.topSafeArea / 2)
        backButton.anchor(top: nil,
                          leading: leadingAnchor,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: 0, left: 16, bottom: 0, right: 0))
        
        addSubview(captureFrameView)
        captureFrameView.centerXInSuperview()
        captureFrameView.anchor(top: navigationBarView.bottomAnchor,
                                leading: nil,
                                bottom: nil,
                                trailing: nil,
                                padding: .init(top: 128, left: 0, bottom: 0, right: 0))
        
        captureFrameView.constrainWidth(constant: frameWidth)
        captureFrameView.constrainHeight(constant: frameWidth / 1.5)
        
        addSubview(titleLabel)
        titleLabel.anchor(top: nil,
                          leading: leadingAnchor,
                          bottom: captureFrameView.topAnchor,
                          trailing: trailingAnchor,
                          padding: .init(top: 0, left: 16, bottom: 24, right: 16))
        
        addSubview(buttonStackView)
        buttonStackView.centerXInSuperview()
        buttonStackView.greaterOrEqualTo(leading: leadingAnchor, constant: 16)
        buttonStackView.anchor(top: nil,
                               leading: nil,
                               bottom: bottomAnchor,
                               trailing: nil,
                               padding: .init(top: 0, left: 0, bottom: UIConstants.bottomSafeArea + 16, right: 0))
        buttonStackView.addArrangedSubview(buttonTitleLabel)
        buttonStackView.addArrangedSubview(nationalIDButton)
        buttonStackView.addArrangedSubview(passportButton)
        
        nationalIDButton.setRoundedView()
        passportButton.setRoundedView()
        
        addSubview(scrollView)
        scrollView.anchor(top: captureFrameView.bottomAnchor,
                          leading: leadingAnchor,
                          bottom: buttonStackView.topAnchor,
                          trailing: trailingAnchor,
                          padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        containerView.constrainWidth(constant: UIConstants.screenWidth - 32)
        containerView.addSubview(descriptionStackView)
        descriptionStackView.fillInSuperView()
        getDescriptionLabels().forEach {
            descriptionStackView.addArrangedSubview($0)
        }
        
        passportButton.isHidden = !hasPassport
        buttonTitleLabel.isHidden = !hasPassport
        if !hasPassport {
            nationalIDButton.setTitle("continue".localize, for: .normal)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func getDescriptionLabels() -> [BaseLabel] {
        let description: [String] = [
            "instruction_description_1".localize,
            "instruction_description_2".localize,
        ]
        
        return description.map {
            let label = BaseLabel()
            label.text = $0
            label.textAlignment = .left
            label.textColor = .white
            label.numberOfLines = 0
            
            return label
        }
    }
}
