//
//  IdentityLivelinessInstructionView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 17/10/22.
//

import UIKit

class IdentityLivelinessInstructionView: UIView {
    
    private let navigationBarView = ContainerView(background: .clear)
    private let backButton: BaseButton = {
        let button = BaseButton()
        button.setImage(.localImage("ic_back").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .white
        button.constrainWidth(constant: 21)
        button.constrainHeight(constant: 21)
        button.tag = 100
        return button
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "personal_id_detection".localize
        label.textAlignment = .center
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 18)
        label.numberOfLines = 2
        label.scaleToWidth()
        
        return label
    }()
    private let livelinessImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_cert_simple")
        imageView.contentMode = .scaleAspectFit
        imageView.constrainWidth(constant: 150)
        imageView.constrainHeight(constant: 150)
        
        return imageView
    }()
    private let noHeadView: IconTextView = {
        let view = IconTextView()
        view.setupDetails(icon: .localImage("ic_no_hat_glasses_mask"), text: "no_hat_glasses_mask".localize)
        
        return view
    }()
    private let noBlurFaceView: IconTextView = {
        let view = IconTextView()
        view.setupDetails(icon: .localImage("ic_no_blur_face"), text: "no_blur_face".localize)
        
        return view
    }()
    private let noFaceOutsideFrameView: IconTextView = {
        let view = IconTextView()
        view.setupDetails(icon: .localImage("ic_no_face_outside_frame"), text: "no_face_outside_frame".localize)
        
        return view
    }()
    private let noVibrationView: IconTextView = {
        let view = IconTextView()
        view.setupDetails(icon: .localImage("ic_no_vibrate_camera"), text: "no_vibrate_camera".localize)
        
        return view
    }()
    private let continueButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .secondaryColor
        button.setTitle(string: "continue".localize)
        button.setDefaultHeight()
        button.tag = 101
        
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .primaryColor
        
        addSubview(navigationBarView)
        navigationBarView.constrainHeight(constant: UIConstants.navigationHeight + UIConstants.topSafeArea)
        navigationBarView.anchor(top: topAnchor,
                                 leading: leadingAnchor,
                                 bottom: nil,
                                 trailing: trailingAnchor)
        
        navigationBarView.addSubview(backButton)
        backButton.centerYTo(view: navigationBarView.centerYAnchor, constant: UIConstants.topSafeArea / 2)
        backButton.anchor(top: nil,
                          leading: leadingAnchor,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: 0, left: 16, bottom: 0, right: 0))
        
        addSubview(titleLabel)
        titleLabel.anchor(top: navigationBarView.bottomAnchor,
                          leading: leadingAnchor,
                          bottom: nil,
                          trailing: trailingAnchor,
                          padding: .init(top: 48, left: 16, bottom: 0, right: 16))
        
        addSubview(livelinessImageView)
        livelinessImageView.centerXInSuperview()
        livelinessImageView.anchor(top: titleLabel.bottomAnchor,
                                   leading: nil,
                                   bottom: nil,
                                   trailing: nil,
                                   padding: .init(top: 16, left: 0, bottom: 0, right: 0))
        
        let topStackView: UIView = UIView().horizonStackView([noHeadView, noBlurFaceView])
        addSubview(topStackView)
        topStackView.anchor(top: livelinessImageView.bottomAnchor,
                            leading: titleLabel.leadingAnchor,
                            bottom: nil,
                            trailing: titleLabel.trailingAnchor,
                            padding: .init(top: 16, left: 0, bottom: 0, right: 0))
        
        let buttomStackView: UIView = UIView().horizonStackView([noFaceOutsideFrameView, noVibrationView])
        addSubview(buttomStackView)
        buttomStackView.anchor(top: topStackView.bottomAnchor,
                            leading: topStackView.leadingAnchor,
                            bottom: nil,
                            trailing: topStackView.trailingAnchor,
                            padding: .init(top: 16, left: 0, bottom: 0, right: 0))
        
        addSubview(continueButton)
        continueButton.centerXInSuperview()
        continueButton.anchor(top: nil,
                              leading: buttomStackView.leadingAnchor,
                              bottom: bottomAnchor,
                              trailing: buttomStackView.trailingAnchor,
                              padding: .init(top: 0, left: 0, bottom: UIConstants.bottomSafeArea + 16, right: 0))
        
        continueButton.setRoundedView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
