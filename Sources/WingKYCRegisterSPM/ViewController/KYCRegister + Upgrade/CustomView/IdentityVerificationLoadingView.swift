//
//  IdentityVerificationLoadingView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 29/12/21.
//

import UIKit

class IdentityVerificationLoadingView: UIView {

    private let navigationBarView = ContainerView(background: .primaryColor)
    
    private var indicatorView = CustomLoadingSpinnerView()
    private let descriptionLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "verifying_your_identity_please_wait".localize
        label.textAlignment = .center
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 18)
        label.numberOfLines = 0
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .primaryColor
        
        addSubview(navigationBarView)
        navigationBarView.constrainHeight(constant: UIConstants.navigationHeight + UIConstants.topSafeArea)
        navigationBarView.anchor(top: topAnchor,
                                 leading: leadingAnchor,
                                 bottom: nil,
                                 trailing: trailingAnchor)
        
        let loadingSize: CGFloat = 100
        addSubview(indicatorView)
        indicatorView.constrainWidth(constant: loadingSize)
        indicatorView.constrainHeight(constant: loadingSize)
        indicatorView.centerXInSuperview()
        indicatorView.anchor(top: navigationBarView.bottomAnchor,
                             leading: nil,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 128, left: 0, bottom: 0, right: 0))
        
        addSubview(descriptionLabel)
        descriptionLabel.centerXInSuperview()
        descriptionLabel.lessThenOrEqualTo(bottom: bottomAnchor, constant: UIConstants.bottomSafeArea + 16)
        descriptionLabel.anchor(top: indicatorView.bottomAnchor,
                                leading: leadingAnchor,
                                bottom: nil,
                                trailing: nil,
                                padding: .init(top: 64, left: 16, bottom: 0, right: 0))
        
        indicatorView.startAnimation()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
