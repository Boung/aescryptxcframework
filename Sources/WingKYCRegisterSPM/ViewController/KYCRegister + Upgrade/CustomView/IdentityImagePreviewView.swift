//
//  IdentityImagePreviewView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 28/12/21.
//

import UIKit

class IdentityImagePreviewView: UIView {

    private let navigationBarView = ContainerView(background: .primaryColor)
    private let backButton: BaseButton = {
        let button = BaseButton()
        button.setImage(.localImage("ic_back").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .white
        button.constrainWidth(constant: 21)
        button.constrainHeight(constant: 21)
        button.tag = 100
        return button
    }()
    private let navigationTitleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "title_register_wing_account".localize
        label.textAlignment = .center
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 18)
        label.scaleToWidth()
        
        return label
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "can_you_see_your_identity_below".localize
        label.textAlignment = .center
        label.textColor = .subTitleColor
        label.font = .boldSystemFont(ofSize: 18)
        label.numberOfLines = 2
        label.scaleToWidth(to: 0.8)
        
        return label
    }()
    private let captureFrameView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.tag = 401
        
        return imageView
    }()
    private let buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let yesButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "yes_button".localize)
        button.backgroundColor = .secondaryColor
        button.setTitleColor(.white, for: .normal)
        button.setDefaultHeight()
        button.tag = 402
        
        return button
    }()
    private let tryAgainButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "try_again_button".localize)
        button.backgroundColor = .clear
        button.setTitleColor(.secondaryColor, for: .normal)
        button.setDefaultHeight()
        button.tag = 403
        
        return button
    }()

    private let frameWidth = UIConstants.screenWidth * 0.9
    var isUpgradeAccount: Bool = false {
        didSet {
            navigationTitleLabel.text = isUpgradeAccount ? "title_upgrade_wing_account".localize : customTitle
        }
    }
    var customTitle: String = "title_register_wing_account".localize
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(navigationBarView)
        navigationBarView.constrainHeight(constant: UIConstants.navigationHeight + UIConstants.topSafeArea)
        navigationBarView.anchor(top: topAnchor,
                                 leading: leadingAnchor,
                                 bottom: nil,
                                 trailing: trailingAnchor)
        
        navigationBarView.addSubview(backButton)
        backButton.centerYTo(view: navigationBarView.centerYAnchor, constant: UIConstants.topSafeArea / 2)
        backButton.anchor(top: nil,
                          leading: leadingAnchor,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: 0, left: 16, bottom: 0, right: 0))
        
        navigationBarView.addSubview(navigationTitleLabel)
        navigationTitleLabel.centerXInSuperview()
        navigationTitleLabel.centerYTo(view: backButton.centerYAnchor)
        navigationTitleLabel.anchor(top: nil,
                                    leading: backButton.trailingAnchor,
                                    bottom: nil,
                                    trailing: nil,
                                    padding: .init(top: 0, left: 8, bottom: 0, right: 0))
        
        addSubview(captureFrameView)
        captureFrameView.centerXInSuperview()
        captureFrameView.constrainWidth(constant: frameWidth)
        captureFrameView.constrainHeight(constant: frameWidth / 1.5)
        captureFrameView.anchor(top: navigationBarView.bottomAnchor,
                                 leading: nil,
                                 bottom: nil,
                                 trailing: nil,
                                 padding: .init(top: 128, left: 0, bottom: 0, right: 0))
        
        addSubview(titleLabel)
        titleLabel.anchor(top: nil,
                          leading: leadingAnchor,
                          bottom: captureFrameView.topAnchor,
                          trailing: trailingAnchor,
                          padding: .init(top: 0, left: 16, bottom: 24, right: 16))

        addSubview(buttonStackView)
        buttonStackView.centerXInSuperview()
        buttonStackView.anchor(top: nil,
                               leading: leadingAnchor,
                               bottom: bottomAnchor,
                               trailing: nil,
                               padding: .init(top: 0, left: 16, bottom: UIConstants.bottomSafeArea + 16, right: 0))
        
        buttonStackView.addArrangedSubview(yesButton)
        buttonStackView.addArrangedSubview(tryAgainButton)
        
        yesButton.setRoundedView()
        tryAgainButton.setRoundedView()
        tryAgainButton.setBorder(width: 1, color: .secondaryColor)
        
        captureFrameView.setCornerRadius(radius: 16)
        bringSubviewToFront(navigationBarView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
