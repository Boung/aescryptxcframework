//
//  WingDebitCardTypeView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 27/1/22.
//

import Foundation
import UIKit

class WingDebitCardTypeView: UIView {
    
    private let containerView = ContainerView(background: .secondaryColor.withAlphaComponent(0.5))
    let closeButton: BaseButton = {
        let button = BaseButton()
        button.setImage(.localImage("ic_close").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .white
        button.constrainWidth(constant: 30)
        button.constrainHeight(constant: 30)
        button.backgroundColor = .clear
        
        return button
    }()
    private let cardImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        
        return imageView
    }()
    private let cardTypeLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 18)
        
        return label
    }()
    
    private var closeButtonHandler: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(containerView)
        containerView.fillInSuperView()
        
        containerView.addSubview(closeButton)
        closeButton.anchor(top: containerView.topAnchor,
                           leading: nil,
                           bottom: nil,
                           trailing: containerView.trailingAnchor,
                           padding: .init(top: 16, left: 0, bottom: 0, right: 16))
        
        containerView.addSubview(cardImageView)
        cardImageView.centerXInSuperview()
        cardImageView.anchor(top: closeButton.bottomAnchor,
                             leading: nil,
                             bottom: nil,
                             trailing: closeButton.trailingAnchor,
                             padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        
        containerView.addSubview(cardTypeLabel)
        cardTypeLabel.centerXInSuperview()
        cardTypeLabel.anchor(top: cardImageView.bottomAnchor,
                              leading: cardImageView.leadingAnchor,
                              bottom: containerView.bottomAnchor,
                              trailing: cardImageView.trailingAnchor,
                              padding: .init(top: 16, left: 0, bottom: 16, right: 0))
        
        containerView.setCornerRadius(radius: 16)
        cardImageView.setCornerRadius(radius: 16)
        
        closeButton.setAction { [weak self] in
            guard
                let self = self,
                let handler = self.closeButtonHandler
            else { return }
            
            handler()
        }
    }
    
    override func layoutSubviews() {
        cardImageView.constrainHeight(constant: (frame.width * 9) / 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setDetails(model: DebitCardInformationResponse) {
        cardImageView.loadImageFromURL(urlString: "\(Constants.baseURL)\(model.imageUrl ?? "")", placeholder: .localImage("ic_card_placeholder"))
        cardTypeLabel.text = model.label
    }
    
    func setCloseAction(handler: @escaping () -> Void) {
        closeButtonHandler = handler
    }
}
