//
//  OtherRequestInformationDisplayView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 27/1/22.
//

import Foundation
import UIKit

class OtherRequestInformationDisplayView: UIView {
    
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("other_request_information".localize)
        
        return view
    }()
    private let requestCardView = KeyValueLabelView()
    private let kitNumberView = KeyValueLabelView()
    private let requestDebitCardView = KeyValueLabelView()
    private let debitCardTypeView = KeyValueLabelView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(wrapper)
        wrapper.fillInSuperView()
        wrapper.addCustomView(view: vStackView)
    
        vStackView.addArrangedSubview(requestCardView)
        vStackView.addArrangedSubview(kitNumberView)
        vStackView.addArrangedSubview(requestDebitCardView)
        vStackView.addArrangedSubview(debitCardTypeView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetails(model: RegisterUserValidateParam) {
        let kitNumber = model.kitNumber ?? ""
        let isRequestWingCard: Bool = model.kitNumber != nil || kitNumber.isNotEmpty
        requestCardView.setupDetails("request_card".localize, value: isRequestWingCard ? "yes".localize : "no".localize)
        kitNumberView.setupDetails("kit_number".localize, value: kitNumber)
        
        let isRequestDebitCard: Bool = model.debitCardInformation != nil
        requestDebitCardView.setupDetails("request_debit_card".localize, value: isRequestDebitCard ? "yes".localize : "no".localize)
        debitCardTypeView.setupDetails("debit_card_type".localize, value: model.debitCardInformation?.label ?? "")
    }
}
