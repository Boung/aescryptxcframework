//
//  CustomerInformationDisplayView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/11/21.
//

import UIKit

class CustomerInformationDisplayView: UIView {
    
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("customer_information".localize)
        
        return view
    }()
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let identityTypeView = KeyValueLabelView()
    private let identityNumberView = KeyValueLabelView()
    private let expireDateView = KeyValueLabelView()
    private let khmerLastNameView = KeyValueLabelView()
    private let khmerFirstNameView = KeyValueLabelView()
    private let englishLastNameView = KeyValueLabelView()
    private let englishFirstNameView = KeyValueLabelView()
    private let nationalityView = KeyValueLabelView()
    private let genderView = KeyValueLabelView()
    private let dateOfBirthView = KeyValueLabelView()
    
    private let placeOfBirthView = CustomerPlaceOfBirthDisplayView()
    private let addressView = CustomerAddressDisplayView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        addSubview(wrapper)
        wrapper.fillInSuperView()
        
        wrapper.addCustomView(view: vStackView)
    
        vStackView.addArrangedSubview(identityTypeView)
        vStackView.addArrangedSubview(horizonStackView([identityNumberView, expireDateView]))
        vStackView.addArrangedSubview(horizonStackView([khmerLastNameView, khmerFirstNameView]))
        vStackView.addArrangedSubview(horizonStackView([englishLastNameView, englishFirstNameView]))
        vStackView.addArrangedSubview(nationalityView)
        vStackView.addArrangedSubview(horizonStackView([genderView, dateOfBirthView]))
        vStackView.addArrangedSubview(placeOfBirthView)
        vStackView.addArrangedSubview(addressView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetail(model: RegisterUserValidateParam) {
        identityTypeView.setupDetails("identity_type".localize, value: model.idType?.value ?? "")
        identityNumberView.setupDetails("identity_number".localize, value: model.idNumber ?? "", hideOnEmpty: false)
        expireDateView.setupDetails("expire_date".localize, value: model.expiryDate ?? "", hideOnEmpty: false)
        khmerLastNameView.setupDetails("last_name_kh".localize, value: model.lastNameKh ?? "", hideOnEmpty: false)
        khmerFirstNameView.setupDetails("first_name_kh".localize, value: model.firstNameKh ?? "", hideOnEmpty: false)
        englishLastNameView.setupDetails("last_name_en".localize, value: model.lastName ?? "", hideOnEmpty: false)
        englishFirstNameView.setupDetails("first_name_en".localize, value: model.firstName ?? "", hideOnEmpty: false)
        nationalityView.setupDetails("nationality".localize, value: model.nationality?.value ?? "")
        genderView.setupDetails("gender".localize, value: model.gender?.value ?? "", hideOnEmpty: false)
        dateOfBirthView.setupDetails("date_of_birth".localize, value: model.dob ?? "", hideOnEmpty: false)
        
        placeOfBirthView.setupDetail(model: model)
        addressView.setupDetail(model: model)
    }
}
