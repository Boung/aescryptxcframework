//
//  AdditionalInformationDisplayView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/11/21.
//

import Foundation
import UIKit

class AdditionalInformationDisplayView: UIView {
    
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("other_information".localize)
        
        return view
    }()
    private let phoneNumberView = KeyValueLabelView()
    private let maritalStatusView = KeyValueLabelView()
    private let titlesView = KeyValueLabelView()
    private let sourceOfFundView = KeyValueLabelView()
    private let montlyIncomeView = KeyValueLabelView()
    private let sectorView = KeyValueLabelView()
    private let occupationView = KeyValueLabelView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(wrapper)
        wrapper.fillInSuperView()
    
        wrapper.addCustomView(view: vStackView)
        
        vStackView.addArrangedSubview(phoneNumberView)
        vStackView.addArrangedSubview(horizonStackView([maritalStatusView, titlesView]))
        vStackView.addArrangedSubview(sourceOfFundView)
        vStackView.addArrangedSubview(montlyIncomeView)
        vStackView.addArrangedSubview(horizonStackView([sectorView, occupationView]))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetails(model: RegisterUserValidateParam) {
        phoneNumberView.setupDetails("phone_number".localize, value: model.phoneNumber ?? "")
        maritalStatusView.setupDetails("marital_status".localize, value: model.maritalStauts?.value ?? "")
        titlesView.setupDetails("titles_display".localize, value: model.titles?.value ?? "")
        sourceOfFundView.setupDetails("source_of_fund".localize, value: model.sourceOfFund?.value ?? "")
        montlyIncomeView.setupDetails("monthly_income".localize, value: model.monthlyAvgIncome?.value ?? "")
        sectorView.setupDetails("sector".localize, value: model.sector?.value ?? "")
        occupationView.setupDetails("occupation".localize, value: model.occupationID?.value ?? "")
    }
}
