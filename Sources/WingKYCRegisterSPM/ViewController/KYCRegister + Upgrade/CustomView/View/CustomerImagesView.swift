//
//  CustomerImagesView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//

import UIKit

class CustomerImagesView: UIView {
    
    private let scrollView = ScrollViewContainerView(withScreenWidth: false)
    private var containerView: UIView {
        return scrollView.containerView
    }
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 8
        
        return stackView
    }()
    private let customerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .clear
        
        return imageView
    }()
    private let identityFrontImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .clear
        
        return imageView
    }()
    private let identityBackImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .clear
        
        return imageView
    }()
    
    var isNID: Bool = false
    var customerImage: UIImage?
    var idFrontImage: UIImage?
    var idBackImage: UIImage?
    
    init(stackView axis: NSLayoutConstraint.Axis, isNID: Bool) {
        super.init(frame: .zero)
        
        self.stackView.axis = axis
        self.isNID = isNID
    }
    
    override func layoutSubviews() {
        generateUI()
        
        customerImageView.setCornerRadius(radius: 16)
        identityFrontImageView.setCornerRadius(radius: 16)
        identityBackImageView.setCornerRadius(radius: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func generateUI() {
        let width = frame.width
        let height = width * 9 / 16
        
        switch stackView.axis {
        case .vertical:
            stackView.alignment = .center
            stackView.distribution = .fillProportionally
            
            addSubview(stackView)
            stackView.fillInSuperView()
        case .horizontal:
            stackView.alignment = .fill
            stackView.distribution = .fill
            
            constrainHeight(constant: height)
            
            addSubview(scrollView)
            scrollView.fillInSuperView()
    
            containerView.addSubview(stackView)
            stackView.anchor(top: containerView.topAnchor,
                             leading: containerView.leadingAnchor,
                             bottom: containerView.bottomAnchor,
                             trailing: containerView.trailingAnchor)
        @unknown default:
            break
        }

        customerImageView.constrainWidth(constant: height)
        customerImageView.constrainHeight(constant: height)

        identityFrontImageView.constrainWidth(constant: width)
        identityFrontImageView.constrainHeight(constant: height)
        
        identityBackImageView.constrainWidth(constant: width)
        identityBackImageView.constrainHeight(constant: height)
        
        stackView.addArrangedSubview(customerImageView)
        stackView.addArrangedSubview(identityFrontImageView)
        if !isNID {
            stackView.addArrangedSubview(identityBackImageView)
        }
    }
    
    func setupDetails(customerImage: UIImage?, idFront: UIImage?, idBack: UIImage?) {
        customerImageView.image = customerImage
        identityFrontImageView.image = idFront
        identityBackImageView.image = idBack
        
        self.customerImage = customerImage
        self.idFrontImage = idFront
        self.idBackImage = idBack
        
        customerImageView.isHidden = customerImage == nil
        identityFrontImageView.isHidden = idFront == nil
        identityBackImageView.isHidden = idBack == nil
    }
}
