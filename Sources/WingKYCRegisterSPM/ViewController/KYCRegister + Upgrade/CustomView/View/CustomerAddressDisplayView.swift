//
//  CustomerAddressDisplayView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//

import UIKit

class CustomerAddressDisplayView: UIView {
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "address".localize
        
        return label
    }()
    private let containerView = ContainerView(background: .containerBackgroundColor)
    private let innerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let houseNumberView = KeyValueLabelView()
    private let streetNumberView = KeyValueLabelView()
    private let villageView = KeyValueLabelView()
    private let communeView = KeyValueLabelView()
    private let districtView = KeyValueLabelView()
    private let provinceView = KeyValueLabelView()
    private let countryView = KeyValueLabelView()
    
    private let addressView = KeyValueLabelView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(vStackView)
        vStackView.fillInSuperView(padding: .init(top: 16, left: 0, bottom: 0, right: 0))
        
        vStackView.addArrangedSubview(titleLabel)
        vStackView.addArrangedSubview(containerView)
        
        containerView.addSubview(innerStackView)
        innerStackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
//        innerStackView.addArrangedSubview(horizonStackView([houseNumberView, streetNumberView]))
        innerStackView.addArrangedSubview(countryView)
        innerStackView.addArrangedSubview(horizonStackView([provinceView, districtView]))
        innerStackView.addArrangedSubview(horizonStackView([communeView, villageView]))
        
        innerStackView.setCustomSpacing(0, after: countryView)
        innerStackView.addArrangedSubview(addressView)
        
        containerView.setCornerRadius(radius: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetail(model: RegisterUserValidateParam) {
        addressView.isHidden = true
        
        if let address = model.currentAddress {
            countryView.setupDetails("country".localize, value: address.countryID?.value ?? "", hideOnEmpty: false)
            provinceView.setupDetails("province".localize, value: address.provinceID?.value ?? "", hideOnEmpty: false)
            districtView.setupDetails("district".localize, value: address.districtID?.value ?? "", hideOnEmpty: false)
            communeView.setupDetails("commune".localize, value: address.communeID?.value ?? "", hideOnEmpty: false)
            villageView.setupDetails("village".localize, value: address.villageID?.value ?? "", hideOnEmpty: false)
        }else if let addressText = model.currentAddressTextModel {
            countryView.setupDetails("country".localize, value: addressText.country)
            provinceView.setupDetails("province".localize, value: addressText.province ?? "", hideOnEmpty: false)
            districtView.setupDetails("district".localize, value: addressText.district ?? "", hideOnEmpty: false)
            communeView.setupDetails("commune".localize, value: addressText.commune ?? "", hideOnEmpty: false)
            villageView.setupDetails("village".localize, value: addressText.village ?? "", hideOnEmpty: false)
        }else if let addressString = model.currentAddressString {
            let splitedAddress = addressString.components(separatedBy: ", ")
            let counter = splitedAddress.count
                
            if counter >= 5 {
                countryView.setupDetails("country".localize, value: Constants.countryListResponse.first { $0.isoCode == splitedAddress[0] }?.toLocalizedCountry() ?? "")
                provinceView.setupDetails("province".localize, value: splitedAddress[1])
                districtView.setupDetails("district".localize, value: splitedAddress[2])
                communeView.setupDetails("commune".localize, value: splitedAddress[3])
                villageView.setupDetails("village".localize, value: splitedAddress[4])
            }
        }else {
            countryView.setupDetails("country".localize, value: "", hideOnEmpty: false)
            provinceView.setupDetails("province".localize, value: "", hideOnEmpty: false)
            districtView.setupDetails("district".localize, value: "", hideOnEmpty: false)
            communeView.setupDetails("commune".localize, value: "", hideOnEmpty: false)
            villageView.setupDetails("village".localize, value: "", hideOnEmpty: false)
        }
    }
}
