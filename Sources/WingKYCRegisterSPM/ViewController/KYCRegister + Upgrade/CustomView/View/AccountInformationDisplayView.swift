//
//  AccountInformationDisplayView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/11/21.
//

import Foundation
import UIKit

class AccountInformationDisplayView: UIView {
    
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("account_information".localize)
        
        return view
    }()
    private let currencyView = KeyValueLabelView()
    private let accountTypeView = KeyValueLabelView()
    private let cashOutPackageView = KeyValueLabelView()
    private let purposeOfBankingView = KeyValueLabelView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
     
        addSubview(wrapper)
        wrapper.fillInSuperView()
        wrapper.addCustomView(view: vStackView)
        
        vStackView.addArrangedSubview(currencyView)
        vStackView.addArrangedSubview(accountTypeView)
        vStackView.addArrangedSubview(cashOutPackageView)
        vStackView.addArrangedSubview(purposeOfBankingView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetails(model: RegisterUserValidateParam) {
        currencyView.setupDetails("currency".localize, value: model.currency?.value ?? "")
        accountTypeView.setupDetails("account_type".localize, value: model.accountType?.value ?? "")
        cashOutPackageView.setupDetails("cashout_package".localize, value: model.cashOutPackage?.value ?? "")
        purposeOfBankingView.setupDetails("purpose_of_banking".localize, value: model.bankingPurpose?.value ?? "")
        
        
    }
}
