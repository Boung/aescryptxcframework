//
//  CustomerPlaceOfBirthDisplayView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//

import UIKit

class CustomerPlaceOfBirthDisplayView: UIView {

    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "place_of_birth".localize
        
        return label
    }()
    private let containerView = ContainerView(background: .containerBackgroundColor)
    private let innerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let communeView = KeyValueLabelView()
    private let districtView = KeyValueLabelView()
    private let provinceView = KeyValueLabelView()
    
    private let addressView = KeyValueLabelView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(vStackView)
        vStackView.fillInSuperView(padding: .init(top: 16, left: 0, bottom: 0, right: 0))
        
        vStackView.addArrangedSubview(titleLabel)
        vStackView.addArrangedSubview(containerView)
        
        containerView.addSubview(innerStackView)
        innerStackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        innerStackView.addArrangedSubview(provinceView)
        innerStackView.addArrangedSubview(horizonStackView([districtView, communeView]))
        
        innerStackView.setCustomSpacing(0, after: provinceView)
        innerStackView.addArrangedSubview(addressView)
        
        containerView.setCornerRadius(radius: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetail(model: RegisterUserValidateParam) {
        addressView.isHidden = true
        
        if let address = model.birthAddress {
            provinceView.setupDetails("province".localize, value: address.provinceID?.value ?? "", hideOnEmpty: false)
            districtView.setupDetails("district".localize, value: address.districtID?.value ?? "", hideOnEmpty: false)
            communeView.setupDetails("commune".localize, value: address.communeID?.value ?? "", hideOnEmpty: false)
        }else if let addressText = model.birthAddressTextModel {
            provinceView.setupDetails("province".localize, value: addressText.province ?? "", hideOnEmpty: false)
            districtView.setupDetails("district".localize, value: addressText.district ?? "", hideOnEmpty: false)
            communeView.setupDetails("commune".localize, value: addressText.commune ?? "", hideOnEmpty: false)
        }else if let addressString = model.birthAddressString {
            let splitedAddress = addressString.components(separatedBy: ", ")
            if splitedAddress.count >= 3 {
                provinceView.setupDetails("province".localize, value: splitedAddress[0])
                districtView.setupDetails("district".localize, value: splitedAddress[1])
                communeView.setupDetails("commune".localize, value: splitedAddress[2])
            }else {
                addressView.setupDetails("address".localize, value: addressString)
                addressView.isHidden = false
            }
        }else {
            provinceView.setupDetails("province".localize, value: "", hideOnEmpty: false)
            districtView.setupDetails("district".localize, value: "", hideOnEmpty: false)
            communeView.setupDetails("commune".localize, value: "", hideOnEmpty: false)
        }
    }
}
