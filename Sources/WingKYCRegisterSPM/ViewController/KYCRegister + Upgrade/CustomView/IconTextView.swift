//
//  IconTextView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 25/11/21.
//

import UIKit

class IconTextView: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 8

        return stackView
    }()
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        imageView.constrainWidth(constant: 50)
        imageView.constrainHeight(constant: 50)
        
        return imageView
    }()
    let label: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.textColor = .white
        label.font = .systemFont(ofSize: 13)
        label.numberOfLines = 2
        label.scaleToWidth(to: 0.8)
        label.constrainWidth(constant: 82)
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(label)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetails(icon: UIImage, text: String) {
        imageView.image = icon
        label.text = text
    }
}
