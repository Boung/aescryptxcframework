//
//  IdentityCaptureView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 28/12/21.
//

import UIKit

class IdentityCaptureView: UIView {
    
    private let navigationBarView = ContainerView(background: .primaryColor)
    private let backButton: BaseButton = {
        let button = BaseButton()
        button.setImage(.localImage("ic_back").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .white
        button.constrainWidth(constant: 21)
        button.constrainHeight(constant: 21)
        button.tag = 100
        return button
    }()
    private let navigationTitleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 18)
        label.scaleToWidth()
        
        return label
    }()
    private let topBlurView = ContainerView(background: .primaryColor)
    private let bottomBlurView = ContainerView(background: .primaryColor)
    private let leftBlurView = ContainerView(background: .primaryColor)
    private let rightBlurView = ContainerView(background: .primaryColor)
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "align_your_identity_inside_frame".localize
        label.textAlignment = .center
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 18)
        label.numberOfLines = 2
        label.scaleToWidth()
        
        return label
    }()
    private let cameraCaptureView: UIView = {
        let imageView = UIView()
        imageView.tag = 301
        
        return imageView
    }()
    private let captureFrameView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_sdk_card_frame")
        
        return imageView
    }()
    private let scrollView = ScrollViewContainerView(withScreenWidth: false)
    private var containerView: UIView {
        return scrollView.containerView
    }
    private let descriptionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    
    let captureButton: BaseButton = {
        let button = BaseButton()
        button.setImage(.localImage("ic_capture"), for: .normal)
        button.backgroundColor = .clear
        button.constrainWidth(constant: 65)
        button.constrainHeight(constant: 65)
        button.tag = 302
        
        return button
    }()
    
    private let frameWidth = UIConstants.screenWidth * 0.9
    var isUpgradeAccount: Bool = false {
        didSet {
            navigationTitleLabel.text = isUpgradeAccount ? "title_upgrade_wing_account".localize : customTitle
        }
    }
    var customTitle: String = "title_register_wing_account".localize
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        
        addSubview(navigationBarView)
        navigationBarView.constrainHeight(constant: UIConstants.navigationHeight + UIConstants.topSafeArea)
        navigationBarView.anchor(top: topAnchor,
                                 leading: leadingAnchor,
                                 bottom: nil,
                                 trailing: trailingAnchor)
        
        navigationBarView.addSubview(backButton)
        backButton.centerYTo(view: navigationBarView.centerYAnchor, constant: UIConstants.topSafeArea / 2)
        backButton.anchor(top: nil,
                          leading: leadingAnchor,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: 0, left: 16, bottom: 0, right: 0))
        
        navigationBarView.addSubview(navigationTitleLabel)
        navigationTitleLabel.centerXInSuperview()
        navigationTitleLabel.centerYTo(view: backButton.centerYAnchor)
        navigationTitleLabel.anchor(top: nil,
                                    leading: backButton.trailingAnchor,
                                    bottom: nil,
                                    trailing: nil,
                                    padding: .init(top: 0, left: 8, bottom: 0, right: 0))
        
        addSubview(captureFrameView)
        captureFrameView.centerXInSuperview()
        captureFrameView.constrainWidth(constant: frameWidth)
        captureFrameView.constrainHeight(constant: frameWidth / 1.5)
        captureFrameView.anchor(top: navigationBarView.bottomAnchor,
                                 leading: nil,
                                 bottom: nil,
                                 trailing: nil,
                                 padding: .init(top: 128, left: 0, bottom: 0, right: 0))
        
        captureFrameView.addSubview(cameraCaptureView)
        cameraCaptureView.fillInSuperView(padding: .init(top: 10, left: 10, bottom: 10, right: 10))
        
        addSubview(topBlurView)
        topBlurView.anchor(top: navigationBarView.bottomAnchor,
                           leading: leadingAnchor,
                           bottom: cameraCaptureView.topAnchor,
                           trailing: trailingAnchor)
        
        addSubview(bottomBlurView)
        bottomBlurView.anchor(top: cameraCaptureView.bottomAnchor,
                              leading: leadingAnchor,
                              bottom: bottomAnchor,
                              trailing: trailingAnchor)
        
        addSubview(leftBlurView)
        leftBlurView.anchor(top: topBlurView.bottomAnchor,
                            leading: topBlurView.leadingAnchor,
                            bottom: bottomBlurView.topAnchor,
                            trailing: cameraCaptureView.leadingAnchor)
        
        addSubview(rightBlurView)
        rightBlurView.anchor(top: topBlurView.bottomAnchor,
                             leading: cameraCaptureView.trailingAnchor,
                             bottom: bottomBlurView.topAnchor,
                             trailing: trailingAnchor)
        
        addSubview(titleLabel)
        titleLabel.anchor(top: nil,
                          leading: leadingAnchor,
                          bottom: captureFrameView.topAnchor,
                          trailing: trailingAnchor,
                          padding: .init(top: 0, left: 64, bottom: 24, right: 64))
        
        addSubview(captureButton)
        captureButton.centerXInSuperview()
        captureButton.anchor(top: nil,
                             leading: nil,
                             bottom: bottomAnchor,
                             trailing: nil,
                             padding: .init(top: 0, left: 0, bottom: UIConstants.bottomSafeArea + 16, right: 0))
        
        addSubview(scrollView)
        scrollView.anchor(top: captureFrameView.bottomAnchor,
                          leading: leadingAnchor,
                          bottom: captureButton.topAnchor,
                          trailing: trailingAnchor,
                          padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        containerView.constrainWidth(constant: UIConstants.screenWidth - 32)
        containerView.addSubview(descriptionStackView)
        descriptionStackView.fillInSuperView()
        getDescriptionLabels().forEach {
            descriptionStackView.addArrangedSubview($0)
        }
        
        bringSubviewToFront(navigationBarView)
        bringSubviewToFront(captureFrameView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func getDescriptionLabels() -> [BaseLabel] {
        let description: [String] = [
            "camera_description_1".localize,
            "camera_description_2".localize,
            "camera_description_3".localize,
        ]
        
        return description.map {
            let label = BaseLabel()
            label.text = $0
            label.textAlignment = .left
            label.textColor = .white
            label.numberOfLines = 0
            
            return label
        }
    }
}
