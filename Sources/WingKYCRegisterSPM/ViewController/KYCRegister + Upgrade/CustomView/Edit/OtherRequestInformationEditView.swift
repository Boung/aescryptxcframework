//
//  OtherRequestInformationEditView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 27/1/22.
//

import Foundation
import UIKit

class OtherRequestInformationEditView: UIView {
    
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("other_request_information".localize)
        
        return view
    }()
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let requestCardRadioButton: AvailabilityRadioButton = {
        let view = AvailabilityRadioButton()
        view.setTitle("request_card".localize + "*")
        
        return view
    }()
    private let kitNumberTextFieldContainer = ContainerView(background: .clear)
    private let kitNumberTextField = BaseTextField()
    private let kitNumberScanButtonContainer = ContainerView(background: .clear)
    private let kitNumberScanButton: LeadingIconButton = {
        let button = LeadingIconButton(iconSize: .init(width: 50, height: 50))
        button.backgroundColor = .backgroundColor
        button.setupDetails(.localImage("ic_barcode"), title: "scan_kit_card_barcode".localize)
        button.label.textAlignment = .center
        button.label.numberOfLines = 2
        button.constrainWidth(constant: UIConstants.screenWidth * 0.6)
        
        return button
    }()
    private let requestDebitCardRadioButton: AvailabilityRadioButton = {
        let view = AvailabilityRadioButton()
        view.setTitle("request_debit_card".localize + "*")
        
        return view
    }()
    private let debitCardScanButtonContainer = ContainerView(background: .clear)
    private let debitCardScanButton: LeadingIconButton = {
        let button = LeadingIconButton(iconSize: .init(width: 50, height: 50))
        button.backgroundColor = .backgroundColor
        button.setupDetails(.localImage("ic_qr"), title: "scan_debit_card_qr".localize)
        button.label.textAlignment = .center
        button.label.numberOfLines = 2
        button.constrainWidth(constant: UIConstants.screenWidth * 0.6)
        
        return button
    }()
    private let debitCardView = WingDebitCardTypeView()
    
    var isRequestWingCard: Bool = true {
        didSet {
            kitNumberScanButtonContainer.isHidden = !isRequestWingCard
        }
    }
    var isRequestDebitCard: Bool = true {
        didSet {
            debitCardScanButtonContainer.isHidden = !isRequestDebitCard
        }
    }
    var kitNumber: String {
        return kitNumberTextField.textField.text ?? ""
    }
    var debitCardInformationResponse: DebitCardInformationResponse?
    
    private var kitInputValue: String = ""
    private var onValueChangesObserver: (() -> Void)?
    private var onScanButtonTapHandler: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTextFields()
        
        addSubview(wrapper)
        wrapper.fillInSuperView()
        
        wrapper.addCustomView(view: vStackView)
        
        vStackView.addArrangedSubview(requestCardRadioButton)
        vStackView.addArrangedSubview(kitNumberScanButtonContainer)
        vStackView.addArrangedSubview(kitNumberTextFieldContainer)
        
        kitNumberScanButtonContainer.addSubview(kitNumberScanButton)
        kitNumberScanButton.centerInSuperview()
        kitNumberScanButton.greaterOrEqualTo(leading: kitNumberScanButtonContainer.leadingAnchor)
        kitNumberScanButton.anchor(top: kitNumberScanButtonContainer.topAnchor, leading: nil, bottom: nil, trailing: nil)
        
        kitNumberTextFieldContainer.addSubview(kitNumberTextField)
        kitNumberTextField.centerInSuperview()
        kitNumberTextField.constrainWidth(constant: UIConstants.screenWidth * 0.6)
        kitNumberTextField.anchor(top: kitNumberTextFieldContainer.topAnchor,
                                  leading: nil,
                                  bottom: nil,
                                  trailing: nil)
        
        vStackView.addArrangedSubview(requestDebitCardRadioButton)
        vStackView.addArrangedSubview(debitCardScanButtonContainer)
        
        debitCardScanButtonContainer.addSubview(debitCardScanButton)
        debitCardScanButton.centerInSuperview()
        debitCardScanButton.greaterOrEqualTo(leading: debitCardScanButtonContainer.leadingAnchor)
        debitCardScanButton.anchor(top: debitCardScanButtonContainer.topAnchor, leading: nil, bottom: nil, trailing: nil)
        vStackView.addArrangedSubview(debitCardView)
        
        vStackView.setCustomSpacing(24, after: kitNumberTextField)
        kitNumberScanButton.setCornerRadius(radius: 8)
        debitCardScanButton.setCornerRadius(radius: 8)
        
        requestCardRadioButton.setOnValueChanged { [weak self] isYes in
            guard let self = self else { return }
            
            self.kitNumberTextField.setText("")
            self.kitNumberTextFieldContainer.isHidden = true
            self.isRequestWingCard = isYes
            
            if !isYes {
                self.triggerValueChangeObserver()
            }
        }
        
        requestDebitCardRadioButton.setOnValueChanged { [weak self] isYes in
            guard let self = self else { return }
            
            self.debitCardInformationResponse = nil
            self.debitCardView.isHidden = true
            self.isRequestDebitCard = isYes
            
            if !isYes {
                self.triggerValueChangeObserver()
            }
        }
        
        kitNumberScanButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.triggerOnScanButtonTap()
            KYCQRScannerManager.shared.presentQRScanner(isQR: false, manualButtonTitle: "manual_input_kit_number".localize) {
                let vc = KYCQRManualInputViewController()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.setupViewController(title: "manual_input_kit_number".localize, placeholder: "kit_number".localize) { [weak self] kitNumber in
                    guard let self = self else { return }
                    
                    self.kitInputValue = kitNumber
                    self.validateKitNumber(kitNumber: kitNumber)
                }
                
                guard let topVC = UIApplication.shared.getTopViewController() else { return }
                topVC.present(vc, animated: true, completion: nil)
            } completion: { [weak self] kitNumber in
                guard let self = self else { return }
                
                self.kitInputValue = kitNumber
                self.validateKitNumber(kitNumber: kitNumber)
            }
        }
        
        debitCardScanButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.triggerOnScanButtonTap()
            KYCQRScannerManager.shared.presentQRScanner { string in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    
                    let presenter = GetDebitCardInformationPresenter(delegate: self)
                    presenter.getDebitCardInformation(param: string)
                }
            }
        }
        
        debitCardView.setCloseAction { [weak self] in
            guard let self = self else { return }
            
            self.isRequestDebitCard = false
            self.debitCardView.isHidden = true
            self.requestDebitCardRadioButton.isYes = false
            self.debitCardInformationResponse = nil
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setOnValueChangeObserverAction(handler: @escaping () -> Void) {
        self.onValueChangesObserver = handler
    }
    
    func setOnScanButtonTapHandler(handler: @escaping () -> Void) {
        self.onScanButtonTapHandler = handler
    }
    
    func setDefaultValues(isUpgrade: Bool, model: RegisterUserValidateParam) {
        if let kitNumber = model.kitNumber, kitNumber.isNotEmpty {
            requestCardRadioButton.setDefaultSelection(value: true)
            if isUpgrade {
                requestCardRadioButton.toggleEnable(false)
            }
            
            self.kitNumberTextFieldContainer.isHidden = false
            self.kitNumberScanButtonContainer.isHidden = true
            self.kitNumberTextField.setText(kitNumber)
        }
        
        if let debitCardInformationResponse = model.debitCardInformation {
            requestDebitCardRadioButton.setDefaultSelection(value: true)
            debitCardView.setDetails(model: debitCardInformationResponse)
            debitCardView.isHidden = false
            debitCardScanButtonContainer.isHidden = true
            
            self.debitCardInformationResponse = debitCardInformationResponse
            if isUpgrade {
                requestDebitCardRadioButton.toggleEnable(false)
                debitCardView.closeButton.isHidden = true
            }
        }
    }
    
    func observeErrorFields() {
        kitNumberScanButton.backgroundColor = kitNumber.isEmpty ? .errorTextField : .backgroundColor
        debitCardScanButton.backgroundColor = debitCardInformationResponse == nil ? .errorTextField : .backgroundColor
    }
    
    private func setupTextFields() {
        kitNumberTextFieldContainer.isHidden = true
        debitCardView.isHidden = true
        kitNumberTextField.setPlaceholderNoTitle("kit_number".localize)
        kitNumberTextField.textField.textAlignment = .center
        kitNumberTextField.toggleEnable(false)
    }
    
    private func validateKitNumber(kitNumber: String) {
        let presenter = ValidateKitNumberPresenter(delegate: self)
        presenter.validateKitNumber(param: kitNumber)
    }
    
    private func triggerValueChangeObserver() {
        guard let onValueChangesObserver = onValueChangesObserver else { return }
        onValueChangesObserver()
    }
    
    private func triggerOnScanButtonTap() {
        guard let onScanButtonTapHandler = onScanButtonTapHandler else { return }
        onScanButtonTapHandler()
    }
}

extension OtherRequestInformationEditView: GetDebitCardInformationDelegate {
    func response(response: DebitCardInformationResponse?, error: Error?) {
        guard error == nil else {
            showErrorAlert(error)
            return
        }
        
        guard let response = response else { return }
        debitCardView.setDetails(model: response)
        debitCardInformationResponse = response
        
        debitCardView.isHidden = false
        debitCardScanButtonContainer.isHidden = true
        triggerValueChangeObserver()
    }
}

extension OtherRequestInformationEditView: ValidateKitNumberDelegate {
    func response(validateKitNumber response: ValidateKitNumberResponse?, error: Error?) {
        guard error == nil else {
            showErrorAlert(error)
            return
        }
        
        guard let response = response else { return }
        
        if response.errorCode == "200" {
            guard let topVC = UIApplication.shared.getTopViewController() else { return }
            topVC.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                
                self.kitNumberTextFieldContainer.isHidden = false
                self.kitNumberScanButtonContainer.isHidden = true
                self.kitNumberTextField.setText(self.kitInputValue)
                self.triggerValueChangeObserver()
            }
        }
    }
}
