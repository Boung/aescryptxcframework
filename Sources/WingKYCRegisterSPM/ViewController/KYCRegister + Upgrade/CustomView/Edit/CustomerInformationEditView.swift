//
//  CustomerInformationEditView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//

import UIKit

class CustomerInformationEditView: UIView {
 
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("customer_information".localize)
        
        return view
    }()
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
//    private let titleLabel: BaseLabel = {
//        let label = BaseLabel()
//        label.textAlignment = .center
//        label.numberOfLines = 1
//
//        let attributeString = NSAttributedString.init(string: "customer_information".localize, attributes: [
//            .font: UIFont.boldSystemFont(ofSize: 18),
//            .underlineStyle: NSUnderlineStyle.thick.rawValue,
//            .underlineColor: UIColor.subTitleColor,
//            .foregroundColor: UIColor.subTitleColor,
//        ])
//        label.attributedText = attributeString
//
//        return label
//    }()
    let identityTypeTextField = BaseDropdownTextField()
    let identityNumberTextField = BaseTextField()
    let khmerLastNameTextField = BaseTextField()
    let khmerFirstNameTextField = BaseTextField()
    let englishLastNameTextField = BaseTextField()
    let englishFirstNameTextField = BaseTextField()
    let nationalityTextField = BaseDropdownTextField()
    let genderTextField = BaseDropdownTextField()
    let dateOfBirthTextField = BaseDatePickerTextField()
    let expireDateTextField = BaseDatePickerTextField()
    
    var identityType: KeyValueModel? {
        return identityTypeTextField.selectedDatasource
    }
    var identityNumber: String {
        return identityNumberTextField.textField.text ?? ""
    }
    var khmerLastName: String {
        return khmerLastNameTextField.textField.text ?? ""
    }
    var khmerFirstName: String {
        return khmerFirstNameTextField.textField.text ?? ""
    }
    var englishLastName: String {
        return englishLastNameTextField.textField.text ?? ""
    }
    var englishFirstName: String {
        return englishFirstNameTextField.textField.text ?? ""
    }
    var nationality: KeyValueModel? {
        return nationalityTextField.selectedDatasource
    }
    var gender: KeyValueModel? {
        return genderTextField.selectedDatasource
    }
    var dateOfBirth: String {
        return dateOfBirthTextField.textField.text ?? ""
    }
    var expiredDate: String {
        return expireDateTextField.textField.text ?? ""
    }
    
    private var onNationalityChanged: ((KeyValueModel) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTextField()
        
        addSubview(wrapper)
        wrapper.fillInSuperView()
        
        wrapper.addCustomView(view: vStackView)
    
        vStackView.addArrangedSubview(identityTypeTextField)
        vStackView.addArrangedSubview(horizonStackView([identityNumberTextField, expireDateTextField]))
        vStackView.addArrangedSubview(horizonStackView([khmerLastNameTextField, khmerFirstNameTextField]))
        vStackView.addArrangedSubview(horizonStackView([englishLastNameTextField, englishFirstNameTextField]))
        vStackView.addArrangedSubview(nationalityTextField)
        vStackView.addArrangedSubview(horizonStackView([genderTextField, dateOfBirthTextField]))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
    
    func setOnNationalityChanged(handler: @escaping (KeyValueModel) -> Void) {
        self.onNationalityChanged = handler
    }
    
    func autoFillMockupData() {
        identityTypeTextField.setValueWhere(index: 0)
        identityNumberTextField.setText("ABC")
        khmerLastNameTextField.setText("ABC")
        khmerFirstNameTextField.setText("ABC")
        englishLastNameTextField.setText("ABC")
        englishFirstNameTextField.setText("ABC")
        nationalityTextField.setValueWhere(index: 0)
        genderTextField.setValueWhere(index: 0)
        dateOfBirthTextField.setDateWith(string: "01012000", format: "ddMMyyyy")
        expireDateTextField.setDateWith(string: "01012001", format: "ddMMyyyy")
    }
    
    private func setupTextField() {
        identityTypeTextField.setPlaceholder("identity_type".localize, isRequired: true)
        identityNumberTextField.setPlaceholder("identity_number".localize, isRequired: true)
        khmerLastNameTextField.setPlaceholder("last_name_kh".localize)
        khmerFirstNameTextField.setPlaceholder("first_name_kh".localize)
        englishLastNameTextField.setPlaceholder("last_name_en".localize, isRequired: true)
        englishFirstNameTextField.setPlaceholder("first_name_en".localize, isRequired: true)
        nationalityTextField.setPlaceholder("nationality".localize, isRequired: true)
        genderTextField.setPlaceholder("gender".localize, isRequired: true)
        dateOfBirthTextField.setPlaceholder("date_of_birth".localize, isRequired: true)
        expireDateTextField.setPlaceholder("expire_date".localize, isRequired: true)
        
        guard let enumeration = Constants.enumerationResponse else { return }
        
        if let idType = enumeration.idType {
            identityTypeTextField.setDatasource(idType.map { .init(key: $0.idType ?? "", value: $0.idTypeDescription?.toLocalized() ?? "")})
        }
        
        genderTextField.setDatasource(Constants.genderDatasource)
        
        let nationality = Constants.countryListResponse
        nationalityTextField.setDatasource(nationality.map { .init(key: $0.isoCode ?? "", value: $0.toLocalizedNationality()) })
        nationalityTextField.setOnValueSelected { [weak self] selected in
            guard let self = self else { return }
            
            guard let handler = self.onNationalityChanged else { return }
            handler(selected)
        }
    }
    
    func setDefaultValues(model: RegisterUserValidateParam) {
        identityTypeTextField.setValueWhere(key: model.isPassport ? Constants.PASSPORT_KEY : Constants.NID_KEY)
        identityTypeTextField.toggleEnable(false)
        
        identityNumberTextField.setText(model.idNumber ?? "")
        khmerLastNameTextField.setText(model.lastNameKh ?? "")
        khmerFirstNameTextField.setText(model.firstNameKh ?? "")
        englishLastNameTextField.setText(model.lastName ?? "")
        englishFirstNameTextField.setText(model.firstName ?? "")
        genderTextField.setValueWhere(key: model.gender?.key ?? "")
        dateOfBirthTextField.setDateWith(string: model.dob ?? "", format: "dd/MM/yyyy")
        expireDateTextField.setDateWith(string: model.expiryDate ?? "", format: "dd/MM/yyyy")
        nationalityTextField.setValueWhere(key: model.nationality?.key ?? "")
        
        identityNumberTextField.toggleEnable(model.idNumber == nil || model.idNumber?.isEmpty ??  true)
        khmerLastNameTextField.toggleEnable(model.lastNameKh == nil || model.lastNameKh?.isEmpty ?? true)
        khmerFirstNameTextField.toggleEnable(model.firstNameKh == nil || model.firstNameKh?.isEmpty ?? true)
        englishLastNameTextField.toggleEnable(model.lastName == nil || model.lastName?.isEmpty ?? true)
        englishFirstNameTextField.toggleEnable(model.firstName == nil || model.firstName?.isEmpty ?? true)
        genderTextField.toggleEnable(model.gender == nil || (model.gender?.key ?? "").isEmpty)
        if let date = model.dob, date.isNotEmpty, let _ = date.toDate(from: "dd/MM/yyyy") {
            dateOfBirthTextField.toggleEnable(false)
        }else {
            dateOfBirthTextField.toggleEnable(true)
        }
        
        if let date = model.expiryDate, date.isNotEmpty, let _ = date.toDate(from: "dd/MM/yyyy") {
            expireDateTextField.toggleEnable(false)
        }else {
            expireDateTextField.toggleEnable(true)
        }
        nationalityTextField.toggleEnable(model.nationality == nil || (model.nationality?.key ?? "").isEmpty)
    }
}
