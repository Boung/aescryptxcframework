//
//  OtherIdentityCustomerAddressEditView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/11/21.
//

import UIKit

class OtherIdentityCustomerAddressEditView: CustomerAddressEditView {
    
    let countryTextField = BaseDropdownTextField()
    
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    
    private let inputStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    let provinceInputTextField = BaseTextField()
    let districtInputTextField = BaseTextField()
    let communeInputTextField = BaseTextField()
    let villageInputTextField = BaseTextField()
    
    var isCambodia: Bool = true
    var country: KeyValueModel? {
        return countryTextField.selectedDatasource
    }
    
    var provinceText: String {
        return provinceInputTextField.textField.text ?? ""
    }
    var districtText: String {
        return districtInputTextField.textField.text ?? ""
    }
    var communeText: String {
        return communeInputTextField.textField.text ?? ""
    }
    var villageText: String {
        return villageInputTextField.textField.text ?? ""
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        stackView.arrangedSubviews.forEach {
            $0.removeFromSuperview()
        }
        
//        stackView.addArrangedSubview(horizonStackView([houseNumberTextField, streetNumberTextField]))
        stackView.addArrangedSubview(countryTextField)
        stackView.addArrangedSubview(vStackView)
        stackView.addArrangedSubview(inputStackView)
        
        stackView.setCustomSpacing(0, after: vStackView)
        
        vStackView.addArrangedSubview(horizonStackView([provinceTextField, districtTextField]))
        vStackView.addArrangedSubview(horizonStackView([communeTextField, villageTextField]))
        
        inputStackView.addArrangedSubview(horizonStackView([provinceInputTextField, districtInputTextField]))
        inputStackView.addArrangedSubview(horizonStackView([communeInputTextField, villageInputTextField]))
        
        countryTextField.textFieldBackgroundColor = .white
        provinceInputTextField.textFieldBackgroundColor = .white
        districtInputTextField.textFieldBackgroundColor = .white
        communeInputTextField.textFieldBackgroundColor = .white
        villageInputTextField.textFieldBackgroundColor = .white
        provinceTextField.toggleEnable(false)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
    
    func observeCountry(model: KeyValueModel) {
        vStackView.isHidden = model.key != "KHM"
        inputStackView.isHidden = model.key == "KHM"
        isCambodia = model.key == "KHM"
        
        let dropdownTextFields = [provinceTextField, districtTextField, communeTextField, villageTextField]
        let inputTextFields = [provinceInputTextField, districtInputTextField, communeInputTextField, villageInputTextField]
        
        dropdownTextFields.forEach { $0.selectedDatasource = nil }
        inputTextFields.forEach { $0.textField.text = "" }
    }
    
    override func getAddress() -> Address? {
        observeErrorField()
        
        guard let country = country else {
            showErrorAlert()
            return nil
        }
        
        let address = Address()
        address.countryID = country
        
        if country.key == "KHM" {
            guard let province = province else {
                showErrorAlert()
                return nil
            }
            
            guard let district = district else {
                showErrorAlert()
                return nil
            }
            
            guard let commune = commune else {
                showErrorAlert()
                return nil
            }
            
            guard let village = village else {
                showErrorAlert()
                return nil
            }
            
            address.provinceID = province
            address.districtID = district
            address.communeID = commune
            address.villageID = village
        }
        
        return address
    }
    
    func storeAddress() -> Address {
        let address = Address()
        address.countryID = country
        
        address.provinceID = province
        address.districtID = district
        address.communeID = commune
        address.villageID = village
        
        return address
    }
    
    func getAddressString(country digit: Int = 3) -> String? {
        observeErrorField()
        
        if provinceText.isEmpty || districtText.isEmpty || communeText.isEmpty || villageText.isEmpty {
            return nil
        }
        let country = country?.key.prefix(digit).map { "\($0)" }.joined() ?? ""
        return [country, provinceText, districtText, communeText, villageText].filter { $0.isNotEmpty }.joined(separator: ", ")
    }
    
    override func setDefaultValues(model: RegisterUserValidateParam) {
        super.setDefaultValues(model: model)
        
        if let country = model.currentAddressText?.toAddress().countryID {
            provinceTextField.toggleEnable(true)
            countryTextField.setDefault(value: .init(key: country.key, value: country.value))
        }else if let country = model.currentAddress {
            provinceTextField.toggleEnable(true)
            countryTextField.setDefault(value: .init(key: country.countryID?.key ?? "", value: country.countryID?.value ?? ""))
        }else if let addressString = model.currentAddressString {
            let splitted = addressString.components(separatedBy: ", ")
            let counter = splitted.count
            
            if counter >= 5 {
                let countryKey = splitted.first ?? ""
                countryTextField.setValueWhere(key: countryKey)
                
                provinceInputTextField.setText(splitted[1])
                districtInputTextField.setText(splitted[2])
                communeInputTextField.setText(splitted[3])
                villageInputTextField.setText(splitted[4])
            }
        }
    }
    
    override func observeErrorField() {
        countryTextField.setErrorTextField(country == nil)
        
        if let country = country, country.key == "KHM" {
            provinceTextField.setErrorTextField(province == nil)
            districtTextField.setErrorTextField(district == nil)
            communeTextField.setErrorTextField(commune == nil)
            villageTextField.setErrorTextField(village == nil)
        }else {
            provinceInputTextField.setErrorTextField(provinceText.isEmpty)
            districtInputTextField.setErrorTextField(districtText.isEmpty)
            communeInputTextField.setErrorTextField(communeText.isEmpty)
            villageInputTextField.setErrorTextField(villageText.isEmpty)
        }
    }
    
    override func setupTextField() {
        super.setupTextField()
        
        countryTextField.setPlaceholder("country".localize, isRequired: true)
        
        communeInputTextField.setPlaceholder("commune".localize, isRequired: true)
        districtInputTextField.setPlaceholder("district".localize, isRequired: true)
        provinceInputTextField.setPlaceholder("province".localize, isRequired: true)
        villageInputTextField.setPlaceholder("village".localize, isRequired: true)
        
        inputStackView.isHidden = true
        
        if !Constants.countryListResponse.isEmpty {
            countryTextField.setDatasource(Constants.countryListResponse.map { .init(key: $0.isoCode ?? "", value: $0.toLocalizedCountry()) })
        }
        
        countryTextField.setOnValueSelected { [weak self] selected in
            guard let self = self else { return }
            
            self.observeCountry(model: selected)
            
            if selected.key == "KHM" {
                self.provinceTextField.toggleEnable(true)
                self.districtTextField.toggleEnable(false)
                self.communeTextField.toggleEnable(false)
                self.villageTextField.toggleEnable(false)
                
                self.provinceTextField.reset()
                self.districtTextField.reset()
                self.communeTextField.reset()
                self.villageTextField.reset()
            }else {
                self.provinceInputTextField.textField.text = ""
                self.districtInputTextField.textField.text = ""
                self.communeInputTextField.textField.text = ""
                self.villageInputTextField.textField.text = ""
            }
        }
    }
    
    func setCountry(where model: KeyValueModel?) {
        guard let model = model else { return }
        
        countryTextField.setValueWhere(key: model.key)
        observeCountry(model: model)
    }
    
    func reset() {
        countryTextField.reset()
        provinceTextField.reset()
        districtTextField.reset()
        communeTextField.reset()
        villageTextField.reset()
        
        provinceTextField.toggleEnable(false)
        districtTextField.toggleEnable(false)
        communeTextField.toggleEnable(false)
        villageTextField.toggleEnable(false)

        provinceInputTextField.setText("")
        districtInputTextField.setText("")
        communeInputTextField.setText("")
        villageInputTextField.setText("")
    }
}
