//
//  OtherIdentityCustomerInformationEditView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/11/21.
//

import UIKit

class OtherIdentityCustomerInformationEditView: UIView {
  
  private let stackView: UIStackView = {
    let stackView = UIStackView()
    stackView.setDefaultVerticalStackView(spacing: 16)
    return stackView
  }()
  
  private let customerInformationStackView: UIStackView = {
    let stackView = UIStackView()
    stackView.setDefaultVerticalStackView(spacing: 16)
    
    return stackView
  }()
  private let customerInformationContainerView: GroupWrapperView = {
    let view = GroupWrapperView()
    view.setPlaceholder("customer_information".localize)
    
    return view
  }()
  private let identityTypeTextField = BaseDropdownTextField()
  private let identityNumberTextField = BaseTextField()
  private let expireDateTextField = BaseDatePickerTextField()
  private let khmerLastNameTextField = BaseTextField()
  private let khmerFirstNameTextField = BaseTextField()
  private let englishLastNameTextField = BaseTextField()
  private let englishFirstNameTextField = BaseTextField()
  private let titlesTextField = BaseDropdownTextField()
  private let maritalStatusTextField = BaseDropdownTextField()
  private let phoneNumberTextField = BaseTextField()
  private let nationalityTextField = BaseDropdownTextField()
  private let genderTextField = BaseDropdownTextField()
  private let dateOfBirthTextField = BaseDatePickerTextField()
  
  private let placeOfBirthView = CustomerPlaceOfBirtEditView()
  let addressView = OtherIdentityCustomerAddressEditView()
  
  private let sourceOfFundTextField = BaseDropdownTextField()
  private let montlyIncomeTextField = BaseDropdownTextField()
  private let sectorTextField = BaseDropdownTextField()
  private let occupationTextField = BaseDropdownTextField()
  
  let accountInformationContainerView: GroupWrapperView = {
    let view = GroupWrapperView()
    view.setPlaceholder("account_information".localize)
    
    return view
  }()
  private let accountInformationStackView: UIStackView = {
    let stackView = UIStackView()
    stackView.axis = .vertical
    stackView.alignment = .fill
    stackView.distribution = .fill
    stackView.spacing = 16
    
    return stackView
  }()
  
  let accountCreationPurposeView = BaseDropdownTextField()
  private let currencyTextField = BaseDropdownTextField()
  private let accountTypeTextField = BaseDropdownTextField()
  private let cashOutPackageTextField = BaseDropdownTextField()
  private let purposeOfBankingTextField = BaseDropdownTextField()
  
  // MARK: - Properties
  var identityType: KeyValueModel? {
    return identityTypeTextField.selectedDatasource
  }
  var identityNumber: String {
    return identityNumberTextField.textField.text ?? ""
  }
  var expiredDate: String {
    return expireDateTextField.textField.text ?? ""
  }
  var khmerLastName: String {
    return khmerLastNameTextField.textField.text ?? ""
  }
  var khmerFirstName: String {
    return khmerFirstNameTextField.textField.text ?? ""
  }
  var englishLastName: String {
    return englishLastNameTextField.textField.text ?? ""
  }
  var englishFirstName: String {
    return englishFirstNameTextField.textField.text ?? ""
  }
  var titles: KeyValueModel? {
    return titlesTextField.selectedDatasource
  }
  var maritalStatus: KeyValueModel? {
    return maritalStatusTextField.selectedDatasource
  }
  var phoneNumber: String {
    return phoneNumberTextField.textField.text ?? ""
  }
  var nationality: KeyValueModel? {
    return nationalityTextField.selectedDatasource
  }
  var gender: KeyValueModel? {
    return genderTextField.selectedDatasource
  }
  var dateOfBirth: String {
    return dateOfBirthTextField.textField.text ?? ""
  }
  
  var placeOfBirthProvince: KeyValueModel? {
    return placeOfBirthView.province
  }
  var placeOfBirthDistrict: KeyValueModel? {
    return placeOfBirthView.district
  }
  var placeOfBirthCommune: KeyValueModel? {
    return placeOfBirthView.commune
  }
  
  var houseNumber: String {
    return addressView.houseNumber
  }
  var streetNumber: String {
    return addressView.streetNumber
  }
  var addressCountry: KeyValueModel? {
    return addressView.country
  }
  var addressProvince: KeyValueModel? {
    return addressView.province
  }
  var addressDistrict: KeyValueModel? {
    return addressView.district
  }
  var addressCommune: KeyValueModel? {
    return addressView.commune
  }
  var addressVillage: KeyValueModel? {
    return addressView.village
  }
  
  var sourceOfFund: KeyValueModel? {
    return sourceOfFundTextField.selectedDatasource
  }
  var monthlyIncome: KeyValueModel? {
    return montlyIncomeTextField.selectedDatasource
  }
  var sector: KeyValueModel? {
    return sectorTextField.selectedDatasource
  }
  var occupation: KeyValueModel? {
    return occupationTextField.selectedDatasource
  }
  
  var accountCreationPurpose: KeyValueModel? {
    return accountCreationPurposeView.selectedDatasource
  }
  var currency: KeyValueModel? = nil
  var cashoutPackage: KeyValueModel? = nil
  var accountCreationSelectedIndex: Int = 0
  var onAccountCreationPurposeChanged: (() -> Void)?
  var accountType: KeyValueModel? {
    return accountTypeTextField.selectedDatasource
  }
  var purposeOfBanking: KeyValueModel? {
    return purposeOfBankingTextField.selectedDatasource
  }
  private var isUpgradeAccount: Bool = false
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupTextField()
    observeTextFieldValueChanged()
    
    addSubview(stackView)
    stackView.fillInSuperView()
    
    stackView.addArrangedSubview(customerInformationContainerView)
    stackView.addArrangedSubview(accountInformationContainerView)
    
    customerInformationContainerView.addCustomView(view: customerInformationStackView)
    
    customerInformationStackView.addArrangedSubview(identityTypeTextField)
    customerInformationStackView.addArrangedSubview(horizonStackView([identityNumberTextField, expireDateTextField]))
    customerInformationStackView.addArrangedSubview(horizonStackView([khmerLastNameTextField, khmerFirstNameTextField]))
    customerInformationStackView.addArrangedSubview(horizonStackView([englishLastNameTextField, englishFirstNameTextField]))
    customerInformationStackView.addArrangedSubview(horizonStackView([titlesTextField, maritalStatusTextField]))
    customerInformationStackView.addArrangedSubview(phoneNumberTextField)
    customerInformationStackView.addArrangedSubview(nationalityTextField)
    customerInformationStackView.addArrangedSubview(horizonStackView([genderTextField, dateOfBirthTextField]))
    customerInformationStackView.addArrangedSubview(placeOfBirthView)
    customerInformationStackView.addArrangedSubview(addressView)
    customerInformationStackView.addArrangedSubview(sourceOfFundTextField)
    customerInformationStackView.addArrangedSubview(montlyIncomeTextField)
    customerInformationStackView.addArrangedSubview(horizonStackView([sectorTextField, occupationTextField]))
    
    accountInformationContainerView.addCustomView(view: accountInformationStackView)
    
    accountInformationStackView.addArrangedSubview(accountCreationPurposeView)
    accountInformationStackView.addArrangedSubview(currencyTextField)
    accountInformationStackView.addArrangedSubview(accountTypeTextField)
    accountInformationStackView.addArrangedSubview(cashOutPackageTextField)
    accountInformationStackView.addArrangedSubview(purposeOfBankingTextField)
    
    customerInformationContainerView.setCornerRadius(radius: 16)
    
    //        accountCreationPurposeView.isHidden = true
    accountCreationPurposeView.isHidden = Constants.authentication.applicationID == .wcxEKYC
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func autoFillMockupData() {
    identityTypeTextField.setValueWhere(index: 0)
    identityNumberTextField.setText("010123456")
    expireDateTextField.setDateWith(string: "01/01/2025", format: "dd/MM/yyyy")
    khmerLastNameTextField.setText("ABC")
    khmerFirstNameTextField.setText("ABC")
    englishLastNameTextField.setText("ABC")
    englishFirstNameTextField.setText("ABC")
    titlesTextField.setValueWhere(index: 0)
    maritalStatusTextField.setValueWhere(index: 0)
    phoneNumberTextField.setText("069778829")
    nationalityTextField.setValueWhere(index: 0)
    genderTextField.setValueWhere(index: 0)
    dateOfBirthTextField.setDateWith(string: "01/01/1990", format: "dd/MM/yyyy")
    
    sourceOfFundTextField.setValueWhere(index: 0)
    montlyIncomeTextField.setValueWhere(index: 0)
    sectorTextField.setValueWhere(index: 0)
    occupationTextField.setValueWhere(index: 0)
    
    currencyTextField.setValueWhere(index: 0)
    accountTypeTextField.setValueWhere(index: 0)
    cashOutPackageTextField.setValueWhere(index: 0)
    purposeOfBankingTextField.setValueWhere(index: 0)
  }
  
  func setViewForUpgrade(_ bool: Bool) {
    isUpgradeAccount = bool
    currencyTextField.isHidden = bool
    accountTypeTextField.isHidden = bool
  }
  
  func generateRegisterParam() -> RegisterUserValidateParam? {
    observeErrorField()
    
    guard let idType = identityType else {
      showErrorAlert()
      return nil
    }
    guard identityNumber.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    guard let expiryDate = expiredDate.toDate(from: "dd/MM/yyyy"), KYCHelper.shared.isValid(expiry: expiryDate) else {
      showErrorAlert("expiry_date_expired".localize)
      return nil
    }
    
    guard englishLastName.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    guard englishFirstName.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    guard let titles = titles else {
      showErrorAlert()
      return nil
    }
    guard let maritalStatus = maritalStatus else {
      showErrorAlert()
      return nil
    }
    
    guard phoneNumber.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    guard KYCHelper.shared.isValid(phoneNumber: phoneNumber) else {
      showErrorAlert("please_input_a_valid_phone_number".localize)
      return nil
    }
    guard let nationality = nationality else {
      showErrorAlert()
      return nil
    }
    guard let gender = gender else {
      showErrorAlert()
      return nil
    }
    guard dateOfBirth.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    
    if let dobDate = dateOfBirth.toDate(from: "dd/MM/yyyy") {
      guard KYCHelper.shared.isValid(age: dobDate) else {
        showErrorAlert("invalid_dob_age".localize)
        return nil
      }
    }
    
    guard let sourceOfFund = sourceOfFund else {
      showErrorAlert()
      return nil
    }
    
    guard let monthlyIncome = monthlyIncome else {
      showErrorAlert()
      return nil
    }
    
    guard let sector = sector else {
      showErrorAlert()
      return nil
    }
    
    guard let occupation = occupation else {
      showErrorAlert()
      return nil
    }
    
    guard let cashoutPackage = cashoutPackage else {
      return nil
    }
    
    guard let purposeOfBanking = purposeOfBanking else {
      showErrorAlert()
      return nil
    }
    
    let param = RegisterUserValidateParam()
    
    if placeOfBirthView.isCambodia {
      guard let placeOfBirthAddress = placeOfBirthView.getAddress() else {
        showErrorAlert()
        return nil
      }
      param.birthAddress = placeOfBirthAddress
      param.birthAddressTextModel = nil
      param.birthAddressString = nil
    }else {
      guard let placeOfBirthAddressString = placeOfBirthView.getAddressString() else {
        showErrorAlert()
        return nil
      }
      param.birthAddress = nil
      param.birthAddressTextModel = nil
      param.birthAddressString = placeOfBirthAddressString
    }
    
    if addressView.isCambodia {
      guard let currentAddress = addressView.getAddress() else {
        showErrorAlert()
        return nil
      }
      param.currentAddress = currentAddress
      param.currentAddressTextModel = nil
      param.currentAddressString = nil
    }else {
      guard let currentAddressString = addressView.getAddressString() else {
        showErrorAlert()
        return nil
      }
      param.currentAddress = nil
      param.currentAddressTextModel = nil
      param.currentAddressString = currentAddressString
    }
    
    if !isUpgradeAccount {
      guard let currency = currency else {
        showErrorAlert()
        return nil
      }
      
      guard let accountType = accountType else {
        showErrorAlert()
        return nil
      }
      
      param.currency = currency
      param.accountType = accountType
    }
    
    param.idType = idType
    param.idNumber = identityNumber
    param.expiryDate = expiredDate
    param.lastNameKh = khmerLastName
    param.firstNameKh = khmerFirstName
    param.lastName = englishLastName
    param.firstName = englishFirstName
    param.titles = titles
    param.maritalStauts = maritalStatus
    param.phoneNumber = phoneNumber
    param.nationality = nationality
    param.gender = gender
    param.dob = dateOfBirth
    param.houseNo = houseNumber
    param.streetNo = streetNumber
    param.sourceOfFund = sourceOfFund
    param.monthlyAvgIncome = monthlyIncome
    param.sector = sector
    param.occupationID = occupation
    param.cashOutPackage = cashoutPackage
    param.bankingPurpose = purposeOfBanking
    
    return param
  }
  
  func generateUpgradeParam() -> UpgradeUserValidateParam? {
    observeErrorField()
    
    guard let idType = identityType else {
      showErrorAlert()
      return nil
    }
    guard identityNumber.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    guard expiredDate.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    //        guard khmerLastName.isNotEmpty else {
    //            showErrorAlert()
    //            return nil
    //        }
    //        guard khmerFirstName.isNotEmpty else {
    //            showErrorAlert()
    //            return nil
    //        }
    guard englishLastName.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    guard englishFirstName.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    guard let titles = titles else {
      showErrorAlert()
      return nil
    }
    guard let maritalStatus = maritalStatus else {
      showErrorAlert()
      return nil
    }
    
    guard phoneNumber.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    
    guard KYCHelper.shared.isValid(phoneNumber: phoneNumber) else {
      showErrorAlert("please_input_a_valid_phone_number".localize)
      return nil
    }
    
    guard let nationality = nationality else {
      showErrorAlert()
      return nil
    }
    guard let gender = gender else {
      showErrorAlert()
      return nil
    }
    guard dateOfBirth.isNotEmpty else {
      showErrorAlert()
      return nil
    }
    
    if let dobDate = dateOfBirth.toDate(from: "dd/MM/yyyy") {
      guard KYCHelper.shared.isValid(age: dobDate) else {
        showErrorAlert("invalid_dob_age".localize)
        return nil
      }
    }
    
    guard let sourceOfFund = sourceOfFund else {
      showErrorAlert()
      return nil
    }
    
    guard let monthlyIncome = monthlyIncome else {
      showErrorAlert()
      return nil
    }
    
    guard let sector = sector else {
      showErrorAlert()
      return nil
    }
    
    guard let occupation = occupation else {
      showErrorAlert()
      return nil
    }
    
    guard let cashoutPackage = cashoutPackage else {
      showErrorAlert()
      return nil
    }
    
    guard let purposeOfBanking = purposeOfBanking else {
      showErrorAlert()
      return nil
    }
    
    let param = UpgradeUserValidateParam()
    
    if placeOfBirthView.isCambodia {
      guard let placeOfBirthAddress = placeOfBirthView.getAddress() else { return nil }
      param.birthAddress = placeOfBirthAddress
      param.birthAddressTextModel = nil
      param.birthAddressString = nil
    }else {
      guard let placeOfBirthAddressString = placeOfBirthView.getAddressString() else { return nil }
      param.birthAddress = nil
      param.birthAddressTextModel = nil
      param.birthAddressString = placeOfBirthAddressString
    }
    
    if addressView.isCambodia {
      guard let currentAddress = addressView.getAddress() else { return nil }
      param.currentAddress = currentAddress
      param.currentAddressTextModel = nil
      param.currentAddressString = nil
    }else {
      guard let currentAddressString = addressView.getAddressString() else { return nil }
      param.currentAddress = nil
      param.currentAddressTextModel = nil
      param.currentAddressString = currentAddressString
    }
    
    param.idType = idType
    param.idNumber = identityNumber
    param.expiryDate = expiredDate
    param.lastNameKh = khmerLastName
    param.firstNameKh = khmerFirstName
    param.lastName = englishLastName
    param.firstName = englishFirstName
    param.titles = titles
    param.maritalStauts = maritalStatus
    param.phoneNumber = phoneNumber
    param.nationality = nationality
    param.gender = gender
    param.dob = dateOfBirth
    param.houseNo = houseNumber
    param.streetNo = streetNumber
    param.sourceOfFund = sourceOfFund
    param.monthlyAvgIncome = monthlyIncome
    param.sector = sector
    param.occupationID = occupation
    param.cashOutPackage = cashoutPackage
    param.bankingPurpose = purposeOfBanking
    
    return param
  }
  
  func setonAccountCreationPurposeChanged(handler: @escaping () -> Void) {
    self.onAccountCreationPurposeChanged = handler
  }
  
  private func observeErrorField() {
    identityTypeTextField.setErrorTextField(identityType == nil)
    identityNumberTextField.setErrorTextField(identityNumber.isEmpty)
    expireDateTextField.setErrorTextField(expiredDate.isEmpty)
    englishLastNameTextField.setErrorTextField(englishLastName.isEmpty)
    englishFirstNameTextField.setErrorTextField(englishFirstName.isEmpty)
    titlesTextField.setErrorTextField(titles == nil)
    maritalStatusTextField.setErrorTextField(maritalStatus == nil)
    phoneNumberTextField.setErrorTextField(phoneNumber.isEmpty)
    nationalityTextField.setErrorTextField(nationality == nil)
    genderTextField.setErrorTextField(gender == nil)
    dateOfBirthTextField.setErrorTextField(dateOfBirth.isEmpty)
    if let dobDate = dateOfBirth.toDate(from: "dd/MM/yyyy") {
      dateOfBirthTextField.setErrorTextField(!KYCHelper.shared.isValid(age: dobDate))
    }
    sourceOfFundTextField.setErrorTextField(sourceOfFund == nil)
    montlyIncomeTextField.setErrorTextField(monthlyIncome == nil)
    sectorTextField.setErrorTextField(sector == nil)
    occupationTextField.setErrorTextField(occupation == nil)
    cashOutPackageTextField.setErrorTextField(cashoutPackage == nil)
    purposeOfBankingTextField.setErrorTextField(purposeOfBanking == nil)
    
    placeOfBirthView.observeErrorField()
    addressView.observeErrorField()
    
    if !isUpgradeAccount {
      currencyTextField.setErrorTextField(currency == nil)
      accountTypeTextField.setErrorTextField(accountType == nil)
    }
  }
  
  private func setupTextField() {
    identityTypeTextField.setPlaceholder("identity_type".localize, isRequired: true)
    identityNumberTextField.setPlaceholder("identity_number".localize, isRequired: true)
    expireDateTextField.setPlaceholder("expire_date".localize, isRequired: true)
    khmerLastNameTextField.setPlaceholder("last_name_kh".localize)
    khmerFirstNameTextField.setPlaceholder("first_name_kh".localize)
    englishLastNameTextField.setPlaceholder("last_name_en".localize, isRequired: true)
    englishFirstNameTextField.setPlaceholder("first_name_en".localize, isRequired: true)
    titlesTextField.setPlaceholder("titles".localize, isRequired: true)
    maritalStatusTextField.setPlaceholder("marital_status".localize, isRequired: true)
    phoneNumberTextField.setPlaceholder("phone_number".localize, isRequired: true)
    nationalityTextField.setPlaceholder("nationality".localize, isRequired: true)
    genderTextField.setPlaceholder("gender".localize, isRequired: true)
    dateOfBirthTextField.setPlaceholder("date_of_birth".localize, isRequired: true)
    sourceOfFundTextField.setPlaceholder("source_of_fund".localize, isRequired: true)
    montlyIncomeTextField.setPlaceholder("monthly_income".localize, isRequired: true)
    sectorTextField.setPlaceholder("sector".localize, isRequired: true)
    occupationTextField.setPlaceholder("occupation".localize, isRequired: true)
    purposeOfBankingTextField.setPlaceholder("purpose_of_banking", isRequired: true)
    
    let accountCreationPurposeDatasource: [KeyValueModel] = [
      KeyValueModel(key: "0", value: "purpose_personal_account".localize),
      KeyValueModel(key: "1", value: "purpose_merchant_account".localize),
    ]
    accountCreationPurposeView.setPlaceholder("account_creation_purpose".localize, isRequired: true)
    accountCreationPurposeView.setDatasource(accountCreationPurposeDatasource)
    accountCreationPurposeView.setOnValueSelected { [weak self] selectedValue in
      guard let self = self else { return }
      
      let index = Int(selectedValue.key) ?? 0
      self.accountCreationSelectedIndex = index
      
      self.accountCreationPurposeView.setText(selectedValue.value)
      self.onAccountCreationPurposeChanged?()
    }
    
    currencyTextField.setPlaceholder("currency".localize, isRequired: true)
    accountTypeTextField.setPlaceholder("account_type".localize, isRequired: true)
    cashOutPackageTextField.setPlaceholder("cashout_package".localize, isRequired: true)
    purposeOfBankingTextField.setPlaceholder("purpose_of_banking".localize, isRequired: true)
    
    phoneNumberTextField.textField.keyboardType = .phonePad
    
    guard let enumeration = Constants.enumerationResponse else { return }
    
    if let idType = enumeration.idType {
      identityTypeTextField.setDatasource(idType.map { .init(key: $0.idType ?? "", value: $0.idTypeDescription?.toLocalized() ?? "")})
    }
    
    if let values = enumeration.maritalStatus {
      maritalStatusTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
    }
    
    if let values = enumeration.customerTitle {
      titlesTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
    }
    
    if let values = enumeration.incomeSource {
      sourceOfFundTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
    }
    
    if let values = enumeration.mlAvgInc {
      montlyIncomeTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
    }
    
    if let values = enumeration.sectorID {
      sectorTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
    }
    
    if let values = enumeration.occupationList {
      occupationTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
    }
    
    if !Constants.countryListResponse.isEmpty {
      nationalityTextField.setDatasource(Constants.countryListResponse.map { .init(key: $0.isoCode ?? "", value: $0.toLocalizedNationality()) })
    }
    
    if let values = enumeration.bankingPurpose {
      purposeOfBankingTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
    }
    
    if Constants.authentication.applicationID == .wingEKYC {
      if Constants.authentication.userRole == .employee || Constants.authentication.userRole == .iPay {
        if let values = enumeration.accountTypesKYC {
          accountTypeTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
        }
      }else {
        if let values = enumeration.accountTypes {
          accountTypeTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
        }
      }
    }else {
      if let values = enumeration.accountTypes {
        accountTypeTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
      }
    }
    
    genderTextField.setDatasource(Constants.genderDatasource)
    currencyTextField.setDatasource(Constants.currencyDatasource)
    
    nationalityTextField.setOnValueSelected { [weak self] selected in
      guard let self = self else { return }
      
      self.placeOfBirthView.observeCountry(model: selected)
    }
  }
  
  private func setCashoutPackageDatasoruce() {
    guard let enumeration = Constants.enumerationResponse else { return }
    
    let cashoutPackageDatasource: [CashOutPackage] = (currency?.key == "USD" ? enumeration.cashOutPackageUSD : currency?.key == "KHR" ? enumeration.cashOutPackageKHR : []) ?? []
    self.cashOutPackageTextField.setDatasource(cashoutPackageDatasource.map { .init(key: $0.subID ?? "", value: $0.classDescription?.toLocalized() ?? "") })
  }
  
  private func observeTextFieldValueChanged() {
    guard let enumeration = Constants.enumerationResponse else { return }
    
    currencyTextField.setOnValueSelected { [weak self] selected in
      guard let self = self else { return }
      
      self.currency = selected
      self.setCashoutPackageDatasoruce()
      
      if let selectedCashoutPackage = self.cashoutPackage {
        let id = selectedCashoutPackage.key
        let cashoutPackageDatasource: [CashOutPackage] = (selected.key == "USD" ? enumeration.cashOutPackageUSD :  enumeration.cashOutPackageKHR) ?? []
        
        self.cashoutPackage = cashoutPackageDatasource.filter { $0.subID == id }.map { KeyValueModel(key: $0.subID ?? "", value: $0.classDescription?.toLocalized() ?? "") }.first
        self.cashOutPackageTextField.setValueWhere(key: self.cashoutPackage?.key ?? "")
      }
    }
    
    cashOutPackageTextField.setOnValueSelected { [weak self] selected in
      guard let self = self else { return }
      
      self.cashoutPackage = selected
    }
    
    cashOutPackageTextField.setOnTextFieldBeginEditing { [weak self] in
      guard let self = self else { return }
      
      if self.cashOutPackageTextField.datasources.isEmpty {
        self.showErrorAlert("please_select_currency_before_hand".localize)
      }else {
        self.cashOutPackageTextField.presentSelectionView()
      }
    }
  }
  
  func setDefaultValues(isUpgrade: Bool, model: RegisterUserValidateParam) {
    identityTypeTextField.setValueWhere(key: model.idType?.key ?? "")
    identityNumberTextField.setText(model.idNumber ?? "")
    expireDateTextField.setDateWith(string: model.expiryDate ?? "")
    khmerLastNameTextField.setText(model.lastNameKh ?? "")
    khmerFirstNameTextField.setText(model.firstNameKh ?? "")
    englishLastNameTextField.setText(model.lastName ?? "")
    englishFirstNameTextField.setText(model.firstName ?? "")
    titlesTextField.setValueWhere(key: model.titles?.key ?? "")
    maritalStatusTextField.setValueWhere(key: model.maritalStauts?.key ?? "")
    phoneNumberTextField.setText(model.phoneNumber ?? "")
    nationalityTextField.setValueWhere(key: model.nationality?.key ?? "")
    genderTextField.setValueWhere(key: model.gender?.key ?? "")
    dateOfBirthTextField.setDateWith(string: model.dob ?? "")
    sourceOfFundTextField.setValueWhere(key: model.sourceOfFund?.key ?? "")
    montlyIncomeTextField.setValueWhere(key: model.monthlyAvgIncome?.key ?? "")
    sectorTextField.setValueWhere(key: model.sector?.key ?? "")
    occupationTextField.setValueWhere(key: model.occupationID?.key ?? "")
    accountTypeTextField.setValueWhere(key: model.accountType?.key ?? "")
    purposeOfBankingTextField.setValueWhere(key: model.bankingPurpose?.key ?? "")
    placeOfBirthView.setDefaultValues(model: model)
    addressView.setDefaultValues(model: model)
    phoneNumberTextField.toggleEnable(!isUpgrade)
    
    currencyTextField.setValueWhere(key: model.currency?.key ?? "")
    currency = currencyTextField.selectedDatasource
    setCashoutPackageDatasoruce()
    
    cashOutPackageTextField.setValueWhere(key: model.cashOutPackage?.key ?? "")
    if let value = model.cashOutPackage, value.key.isNotEmpty {
      cashOutPackageTextField.toggleEnable(!model.isExistingUserSubscription)
    }else {
      cashOutPackageTextField.toggleEnable(true)
    }
    cashoutPackage = cashOutPackageTextField.selectedDatasource
  }
  
  func storeRegisterValidateParam() -> RegisterUserValidateParam {
    let param = RegisterUserValidateParam()
    
    param.idType = identityType
    param.idNumber = identityNumber
    param.expiryDate = expiredDate.replacingOccurrences(of: "/", with: "")
    param.lastNameKh = khmerLastName
    param.firstNameKh = khmerFirstName
    param.lastName = englishLastName
    param.firstName = englishFirstName
    param.titles = titles
    param.maritalStauts = maritalStatus
    param.phoneNumber = phoneNumber
    param.nationality = nationality
    param.gender = gender
    param.dob = dateOfBirth.replacingOccurrences(of: "/", with: "")
    param.sourceOfFund = sourceOfFund
    param.monthlyAvgIncome = monthlyIncome
    param.sector = sector
    param.occupationID = occupation
    param.currency = currency
    param.accountType = accountType
    param.bankingPurpose = purposeOfBanking
    param.cashOutPackage = cashoutPackage
    
    if placeOfBirthView.isCambodia {
      param.birthAddress = placeOfBirthView.storeAddress()
      param.birthAddressTextModel = nil
      param.birthAddressString = nil
    }
    
    if addressView.isCambodia {
      param.currentAddress = addressView.storeAddress()
      param.currentAddressTextModel = nil
      param.currentAddressString = nil
    }
    
    return param
  }
  
  func storeUpgradeValidateParam() -> UpgradeUserValidateParam {
    let param = UpgradeUserValidateParam()
    
    param.idType = identityType
    param.idNumber = identityNumber
    param.expiryDate = expiredDate.replacingOccurrences(of: "/", with: "")
    param.lastNameKh = khmerLastName
    param.firstNameKh = khmerFirstName
    param.lastName = englishLastName
    param.firstName = englishFirstName
    param.titles = titles
    param.maritalStauts = maritalStatus
    param.phoneNumber = phoneNumber
    param.nationality = nationality
    param.gender = gender
    param.dob = dateOfBirth.replacingOccurrences(of: "/", with: "")
    param.sourceOfFund = sourceOfFund
    param.monthlyAvgIncome = monthlyIncome
    param.sector = sector
    param.occupationID = occupation
    param.currency = currency
    param.accountType = accountType
    param.cashOutPackage = cashoutPackage
    param.bankingPurpose = purposeOfBanking
    
    if placeOfBirthView.isCambodia {
      param.birthAddress = placeOfBirthView.storeAddress()
      param.birthAddressTextModel = nil
      param.birthAddressString = nil
    }
    
    if addressView.isCambodia {
      param.currentAddress = addressView.storeAddress()
      param.currentAddressTextModel = nil
      param.currentAddressString = nil
    }
    
    return param
  }
}
