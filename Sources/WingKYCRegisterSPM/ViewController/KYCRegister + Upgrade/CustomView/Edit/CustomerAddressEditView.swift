//
//  CustomerAddressEditView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//


import UIKit

class CustomerAddressEditView: UIView {
    
    private let containerView = TitleComponentWrapperView(top: 2)
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    let houseNumberTextField = BaseTextField()
    let streetNumberTextField = BaseTextField()
    
    let provinceTextField = BaseDropdownTextField()
    let districtTextField = BaseDropdownTextField()
    let communeTextField = BaseDropdownTextField()
    let villageTextField = BaseDropdownTextField()
    
    var showHouseAndStreet: Bool = false {
        didSet {
            houseNumberTextField.isHidden = !showHouseAndStreet
            streetNumberTextField.isHidden = !showHouseAndStreet
        }
    }
    var houseNumber: String {
        return houseNumberTextField.textField.text ?? ""
    }
    var streetNumber: String {
        return streetNumberTextField.textField.text ?? ""
    }
    var province: KeyValueModel? {
        return provinceTextField.selectedDatasource
    }
    var district: KeyValueModel? {
        return districtTextField.selectedDatasource
    }
    var commune: KeyValueModel? {
        return communeTextField.selectedDatasource
    }
    var village: KeyValueModel? {
        return villageTextField.selectedDatasource
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        houseNumberTextField.isHidden = true
        streetNumberTextField.isHidden = true
        setupTextField()
     
        addSubview(containerView)
        containerView.fillInSuperView()
        
        let wrapper = ContainerView(background: .addressBackgroundColor)
        wrapper.setCornerRadius(radius: 16)
        containerView.addCustomView(view: wrapper)
        
        wrapper.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 8, left: 8, bottom: 8, right: 8))
        
        stackView.addArrangedSubview(horizonStackView([provinceTextField, districtTextField]))
        stackView.addArrangedSubview(horizonStackView([communeTextField, villageTextField]))
        stackView.addArrangedSubview(horizonStackView([houseNumberTextField, streetNumberTextField]))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupTextField() {
        containerView.setPlaceholder("address".localize, isRequired: true)
        
        houseNumberTextField.setPlaceholder("house_number".localize)
        streetNumberTextField.setPlaceholder("street_number".localize)
        provinceTextField.setPlaceholder("province".localize, isRequired: true)
        districtTextField.setPlaceholder("district".localize, isRequired: true)
        communeTextField.setPlaceholder("commune".localize, isRequired: true)
        villageTextField.setPlaceholder("village".localize, isRequired: true)
        
        houseNumberTextField.textFieldBackgroundColor = .white
        streetNumberTextField.textFieldBackgroundColor = .white
        provinceTextField.textFieldBackgroundColor = .white
        districtTextField.textFieldBackgroundColor = .white
        communeTextField.textFieldBackgroundColor = .white
        villageTextField.textFieldBackgroundColor = .white
        
        
        districtTextField.toggleEnable(false)
        communeTextField.toggleEnable(false)
        villageTextField.toggleEnable(false)
        
        if let province = Constants.provinceListResponse.province, !province.isEmpty {
            provinceTextField.setDatasource(province.map { .init(key: $0.regionID ?? "", value: $0.toLocalized()) })
        }
        
        provinceTextField.setOnValueSelected { [weak self] selectedValue in
            guard let self = self else { return }
            
            self.getRegion(selectedValue)
        }
        
        districtTextField.setOnValueSelected { [weak self] selectedValue in
            guard let self = self else { return }
            
            self.getRegion(selectedValue)
        }
        
        communeTextField.setOnValueSelected { [weak self] selectedValue in
            guard let self = self else { return }
                
            self.getRegion(selectedValue)
        }
        
        districtTextField.setOnNoDatasourceAction { [weak self] in
            guard let self = self else { return }
            
            let presenter = RegionPresenter(delegate: self)
            presenter.getRegion(param: self.provinceTextField.selectedDatasource?.key ?? "") { response in
                guard let district = response?.district else { return }

                self.districtTextField.setDatasource(district.map { .init(key: $0.regionID ?? "", value: $0.toLocalized()) })
                self.districtTextField.presentSelectionView()
            }
        }
        
        communeTextField.setOnNoDatasourceAction { [weak self] in
            guard let self = self else { return }
            
            let presenter = RegionPresenter(delegate: self)
            presenter.getRegion(param: self.districtTextField.selectedDatasource?.key ?? "") { response in
                guard let commune = response?.commune else { return }

                self.communeTextField.setDatasource(commune.map { .init(key: $0.regionID ?? "", value: $0.toLocalized()) })
                self.communeTextField.presentSelectionView()
            }
        }
        
        villageTextField.setOnNoDatasourceAction { [weak self] in
            guard let self = self else { return }
            
            let presenter = RegionPresenter(delegate: self)
            presenter.getRegion(param: self.communeTextField.selectedDatasource?.key ?? "") { response in
                guard let village = response?.village else { return }

                self.villageTextField.setDatasource(village.map {
                    let region = KeyValueModel(key: $0.regionID ?? "", value: $0.toLocalized())
                    region.additionalValue = $0.cbcCode
                    
                    return region
                })
                self.villageTextField.presentSelectionView()
            }
        }
    }
    
    func setDefaultValues(model: RegisterUserValidateParam) {
        if let address = model.currentAddress {
            if let location = address.provinceID, location.key.isNotEmpty {
                districtTextField.toggleEnable(true)
                provinceTextField.setDefault(value: .init(key: location.key, value: location.value))
            }else {
                districtTextField.toggleEnable(false)
                communeTextField.toggleEnable(false)
                villageTextField.toggleEnable(false)
            }
            
            
            if let location = address.districtID, location.key.isNotEmpty {
                communeTextField.toggleEnable(true)
                districtTextField.setDefault(value: .init(key: location.key, value: location.value))
            }else {
                communeTextField.toggleEnable(false)
                villageTextField.toggleEnable(false)
            }
            
            if let location = address.communeID, location.key.isNotEmpty {
                villageTextField.toggleEnable(true)
                communeTextField.setDefault(value: .init(key: location.key, value: location.value))
            }else {
                villageTextField.toggleEnable(false)
            }
            
            if let location = address.villageID, location.key.isNotEmpty {
                let region = KeyValueModel(key: location.key, value: location.value)
                region.additionalValue = address.cbcCode
                
                villageTextField.setDefault(value: region)
            }
        }else if let address = model.currentAddressText?.toAddress() {
            if let province = address.provinceID {
                districtTextField.toggleEnable(true)
                provinceTextField.setDefault(value: .init(key: province.key, value: province.value))
            }else {
                districtTextField.toggleEnable(false)
                communeTextField.toggleEnable(false)
            }
            
            if let district = address.districtID {
                communeTextField.toggleEnable(true)
                districtTextField.setDefault(value: .init(key: district.key, value: district.value))
            }else {
                communeTextField.toggleEnable(false)
                villageTextField.toggleEnable(false)
            }
            
            if let commune = address.communeID {
                villageTextField.toggleEnable(true)
                communeTextField.setDefault(value: .init(key: commune.key, value: commune.value))
            }else {
                villageTextField.toggleEnable(false)
            }
            
            if let village = address.villageID {
                villageTextField.setDefault(value: .init(key: village.key, value: village.value))
            }
        }else {
            provinceTextField.toggleEnable(true)
            districtTextField.toggleEnable(false)
            communeTextField.toggleEnable(false)
            villageTextField.toggleEnable(false)
        }
    }
    
    func setDefaultValues(address: Address) {
        provinceTextField.setDefault(value: .init(key: address.provinceID?.key ?? "", value: address.provinceID?.value ?? ""))
        districtTextField.setDefault(value: .init(key: address.districtID?.key ?? "", value: address.districtID?.value ?? ""))
        communeTextField.setDefault(value: .init(key: address.communeID?.key ?? "", value: address.communeID?.value ?? ""))
        villageTextField.setDefault(value: .init(key: address.villageID?.key ?? "", value: address.villageID?.value ?? ""))
        
        provinceTextField.toggleEnable(true)
        districtTextField.toggleEnable(true)
        communeTextField.toggleEnable(true)
        villageTextField.toggleEnable(true)
    }
    
    func getRegion(_ value: KeyValueModel) {
        let presenter = RegionPresenter(delegate: self)
        presenter.getRegion(param: value.key)
    }
    
    func getAddress() -> Address? {
        observeErrorField()
        guard let province = province else {
            return nil
        }
        
        guard let district = district else {
            return nil
        }
        
        guard let commune = commune else {
            return nil
        }
        
        guard let village = village else {
            return nil
        }

        let address = Address()
        if let country = Constants.countryListResponse.filter({ $0.isoCode == "KHM"}).first {
            address.countryID = .init(key: country.isoCode ?? "KHM", value: country.toLocalizedCountry())
        }
        address.provinceID = province
        address.districtID = district
        address.communeID = commune
        address.villageID = village
        address.cbcCode = village.additionalValue as? String
        
        return address
    }
    
    func observeErrorField() {
        provinceTextField.setErrorTextField(province == nil)
        districtTextField.setErrorTextField(district == nil)
        communeTextField.setErrorTextField(commune == nil)
        villageTextField.setErrorTextField(village == nil)
    }
}

extension CustomerAddressEditView: RegionDelegate {
    func response(region response: RegionResponse?, error: Error?) {
        guard error == nil else {
            return
        }
        
        guard let response = response else { return }
        
        if response.isDistrict, let district = response.district {
            districtTextField.setDatasource(district.map { .init(key: $0.regionID ?? "", value: $0.toLocalized()) })
            districtTextField.toggleEnable(true)
            districtTextField.reset()
            communeTextField.toggleEnable(false)
            communeTextField.reset()
        }else if response.isCommune, let commune = response.commune {
            communeTextField.setDatasource(commune.map { .init(key: $0.regionID ?? "", value: $0.toLocalized()) })
            communeTextField.toggleEnable(true)
            communeTextField.reset()
            villageTextField.toggleEnable(false)
            villageTextField.reset()
        }else if response.isVillage, let village = response.village {
            villageTextField.setDatasource(village.map {
                let region = KeyValueModel(key: $0.regionID ?? "", value: $0.toLocalized())
                region.additionalValue = $0.cbcCode
                
                return region
            })
            villageTextField.toggleEnable(true)
            villageTextField.reset()
        }
    }
}
