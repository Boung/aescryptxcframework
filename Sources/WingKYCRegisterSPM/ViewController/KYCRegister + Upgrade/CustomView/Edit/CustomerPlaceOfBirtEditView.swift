//
//  CustomerPlaceOfBirtEditView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//

import UIKit

class CustomerPlaceOfBirtEditView: UIView {
    
    let containerView = TitleComponentWrapperView(top: 2)
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let communeTextField = BaseDropdownTextField()
    private let districtTextField = BaseDropdownTextField()
    private let provinceTextField = BaseDropdownTextField()
    
    private let communeInputTextField = BaseTextField()
    private let districtInputTextField = BaseTextField()
    private let provinceInputTextField = BaseTextField()
    
    var isCambodia: Bool = true
    var country: KeyValueModel?
    
    var province: KeyValueModel? {
        return provinceTextField.selectedDatasource
    }
    var district: KeyValueModel? {
        return districtTextField.selectedDatasource
    }
    var commune: KeyValueModel? {
        return communeTextField.selectedDatasource
    }
    var provinceText: String {
        return provinceInputTextField.textField.text ?? ""
    }
    var districtText: String {
        return districtInputTextField.textField.text ?? ""
    }
    var communeText: String {
        return communeInputTextField.textField.text ?? ""
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTextField()
        
        addSubview(containerView)
        containerView.fillInSuperView()
        
        let wrapper = ContainerView(background: .addressBackgroundColor)
        wrapper.setCornerRadius(radius: 16)
        containerView.addCustomView(view: wrapper)
        
        wrapper.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 8, left: 8, bottom: 8, right: 8))
        
        stackView.addArrangedSubview(provinceTextField)
        
        let locationView = horizonStackView([districtTextField, communeTextField])
        stackView.addArrangedSubview(locationView)
        
        stackView.setCustomSpacing(0, after: locationView)
        stackView.addArrangedSubview(provinceInputTextField)
        stackView.addArrangedSubview(horizonStackView([districtInputTextField, communeInputTextField]))
        
        provinceInputTextField.textFieldBackgroundColor = .white
        districtInputTextField.textFieldBackgroundColor = .white
        communeInputTextField.textFieldBackgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func observeCountry(model: KeyValueModel) {
        let dropdownTextFields = [provinceTextField, districtTextField, communeTextField]
        let inputTextFields = [provinceInputTextField, districtInputTextField, communeInputTextField]
        
        isCambodia = model.key == "KHM"
        
        dropdownTextFields.forEach {
            $0.isHidden = !isCambodia
            $0.selectedDatasource = nil
        }
        
        inputTextFields.forEach {
            $0.isHidden = isCambodia
            $0.textField.text = ""
        }
        
        if model.value.isEmpty {
            if let country = Constants.countryListResponse.filter({ $0.isoCode == model.key }).first {
                self.country = .init(key: country.isoCode ?? "", value: country.toLocalizedCountry())
            }else {
                country = model
            }
        }else {
            country = model
        }
    }
    
    func getAddress() -> Address? {
        observeErrorField()
        
        guard let province = province else {
            return nil
        }
        
        guard let district = district else {
            return nil
        }
        
        guard let commune = commune else {
            return nil
        }

        let address = Address()
        if let country = country {
            address.countryID = country
        }else {
            if let country = Constants.countryListResponse.filter({ $0.isoCode == "KHM"}).first {
                address.countryID = .init(key: country.isoCode ?? "KHM", value: country.toLocalizedCountry())
            }
        }
        address.provinceID = province
        address.districtID = district
        address.communeID = commune
        
        return address
    }
    
    func storeAddress() -> Address {
        let address = Address()
        if let country = country {
            address.countryID = country
        }else {
            if let country = Constants.countryListResponse.filter({ $0.isoCode == "KHM"}).first {
                address.countryID = .init(key: country.isoCode ?? "KHM", value: country.toLocalizedCountry())
            }
        }
        address.provinceID = province
        address.districtID = district
        address.communeID = commune
        
        return address
    }
    
    func getAddressString(country digit: Int = 3) -> String? {
        observeErrorField()
        
        if provinceText.isEmpty || districtText.isEmpty || communeText.isEmpty {
            showErrorAlert()
            return nil
        }
        
        let country = country?.key.prefix(digit).map { "\($0)" }.joined() ?? ""
        return [country, provinceText, districtText, communeText].filter { $0.isNotEmpty }.joined(separator: ", ")
    }
    
    private func setupTextField() {
        containerView.setPlaceholder("place_of_birth".localize, isRequired: true)
        
        provinceTextField.setPlaceholder("province".localize, isRequired: true)
        districtTextField.setPlaceholder("district".localize, isRequired: true)
        communeTextField.setPlaceholder("commune".localize, isRequired: true)
        
        provinceInputTextField.setPlaceholder("province".localize, isRequired: true)
        districtInputTextField.setPlaceholder("district".localize, isRequired: true)
        communeInputTextField.setPlaceholder("commune".localize, isRequired: true)
        
        districtTextField.toggleEnable(false)
        communeTextField.toggleEnable(false)
        
        provinceInputTextField.isHidden = true
        districtInputTextField.isHidden = true
        communeInputTextField.isHidden = true
        
        provinceTextField.textFieldBackgroundColor = .white
        districtTextField.textFieldBackgroundColor = .white
        communeTextField.textFieldBackgroundColor = .white
        
        if let province = Constants.provinceListResponse.province, !province.isEmpty {
            provinceTextField.setDatasource(province.map { .init(key: $0.regionID ?? "", value: $0.toLocalized()) })
        }
        
        provinceTextField.setOnValueSelected { [weak self] selectedValue in
            guard let self = self else { return }
            
            self.getRegion(selectedValue)
        }
        
        districtTextField.setOnValueSelected { [weak self] selectedValue in
            guard let self = self else { return }
            
            self.getRegion(selectedValue)
        }
        
        districtTextField.setOnNoDatasourceAction { [weak self] in
            guard let self = self else { return }
            
            let presenter = RegionPresenter(delegate: self)
            presenter.getRegion(param: self.provinceTextField.selectedDatasource?.key ?? "") { response in
                guard let district = response?.district else { return }

                self.districtTextField.setDatasource(district.map { .init(key: $0.regionID ?? "", value: $0.toLocalized()) })
                self.districtTextField.presentSelectionView()
            }
        }
        
        communeTextField.setOnNoDatasourceAction { [weak self] in
            guard let self = self else { return }
            
            let presenter = RegionPresenter(delegate: self)
            presenter.getRegion(param: self.districtTextField.selectedDatasource?.key ?? "") { response in
                guard let commune = response?.commune else { return }

                self.communeTextField.setDatasource(commune.map { .init(key: $0.regionID ?? "", value: $0.toLocalized()) })
                self.communeTextField.presentSelectionView()
            }
        }
    }
    
    private func getRegion(_ value: KeyValueModel) {
        let presenter = RegionPresenter(delegate: self)
        presenter.getRegion(param: value.key)
    }
    
    func observeErrorField() {
        provinceTextField.setErrorTextField(province == nil)
        districtTextField.setErrorTextField(district == nil)
        communeTextField.setErrorTextField(commune == nil)
        
        provinceInputTextField.setErrorTextField(provinceText.isEmpty)
        districtInputTextField.setErrorTextField(districtText.isEmpty)
        communeInputTextField.setErrorTextField(communeText.isEmpty)
    }
    
    func setDefaultValues(model: RegisterUserValidateParam) {
        if let address = model.birthAddress {
            if let location = address.provinceID, location.key.isNotEmpty {
                districtTextField.toggleEnable(true)
                provinceTextField.setDefault(value: .init(key: location.key, value: location.value))
            }else {
                districtTextField.toggleEnable(false)
                communeTextField.toggleEnable(false)
            }
            
            if let location = address.districtID, location.key.isNotEmpty {
                communeTextField.toggleEnable(true)
                districtTextField.setDefault(value: .init(key: location.key, value: location.value))
            }else {
                communeTextField.toggleEnable(false)
            }
            
            if let location = address.communeID, location.key.isNotEmpty {
                communeTextField.setDefault(value: .init(key: location.key, value: location.value))
            }
        }else if let address = model.birthAddressText?.toAddress() {
            if let province = address.provinceID {
                districtTextField.toggleEnable(true)
                provinceTextField.setDefault(value: .init(key: province.key, value: province.value))
            }else {
                districtTextField.toggleEnable(false)
                communeTextField.toggleEnable(false)
            }
            
            if let district = address.districtID {
                communeTextField.toggleEnable(true)
                districtTextField.setDefault(value: .init(key: district.key, value: district.value))
            }else {
                communeTextField.toggleEnable(false)
            }
            
            if let commune = address.communeID {
                communeTextField.setDefault(value: .init(key: commune.key, value: commune.value))
            }
        }else if let addressString = model.birthAddressString {
            let splitedAddress = addressString.components(separatedBy: ", ")
            
            if splitedAddress.count >= 3 {
                provinceInputTextField.setText(splitedAddress[0])
                districtInputTextField.setText(splitedAddress[1])
                communeInputTextField.setText(splitedAddress[2])
            }
        }else {
            provinceTextField.toggleEnable(true)
            districtTextField.toggleEnable(false)
            communeTextField.toggleEnable(false)
        }
    }
}

extension CustomerPlaceOfBirtEditView: RegionDelegate {
    func response(region response: RegionResponse?, error: Error?) {
        guard error == nil else {
            return
        }
        
        guard let response = response else { return }
        
        if response.isDistrict, let district = response.district {
            districtTextField.setDatasource(district.map { .init(key: $0.regionID ?? "", value: $0.toLocalized()) })
            districtTextField.toggleEnable(true)
            communeTextField.toggleEnable(false)
        }else if response.isCommune, let commune = response.commune {
            communeTextField.setDatasource(commune.map { .init(key: $0.regionID ?? "", value: $0.toLocalized()) })
            communeTextField.toggleEnable(true)
        }
    }
}
