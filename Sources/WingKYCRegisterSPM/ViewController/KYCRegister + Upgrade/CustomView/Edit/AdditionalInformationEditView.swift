//
//  AdditionalInformationEditView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//

import UIKit

class AdditionalInformationEditView: UIView {
  
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("additional_information".localize)
        
        return view
    }()
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    
    let phoneNumberTextField = BaseTextField()
    let maritalStatusTextField = BaseDropdownTextField()
    let titlesTextField = BaseDropdownTextField()
    let sourceOfFundTextField = BaseDropdownTextField()
    let montlyIncomeTextField = BaseDropdownTextField()
    let sectorTextField = BaseDropdownTextField()
    let occupationTextField = BaseDropdownTextField()
    
    var phoneNumber: String {
        return phoneNumberTextField.textField.text ?? ""
    }
    var maritalStatus: KeyValueModel? {
        return maritalStatusTextField.selectedDatasource
    }
    var titles: KeyValueModel? {
        return titlesTextField.selectedDatasource
    }
    var sourceOfFund: KeyValueModel? {
        return sourceOfFundTextField.selectedDatasource
    }
    var monthlyIncome: KeyValueModel? {
        return montlyIncomeTextField.selectedDatasource
    }
    var sector: KeyValueModel? {
        return sectorTextField.selectedDatasource
    }
    var occupation: KeyValueModel? {
        return occupationTextField.selectedDatasource
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        setupTextField()
        
        addSubview(wrapper)
        wrapper.fillInSuperView()
        
        wrapper.addCustomView(view: vStackView)
        
        vStackView.addArrangedSubview(phoneNumberTextField)
        vStackView.addArrangedSubview(horizonStackView([maritalStatusTextField, titlesTextField]))
        vStackView.addArrangedSubview(sourceOfFundTextField)
        vStackView.addArrangedSubview(montlyIncomeTextField)
        vStackView.addArrangedSubview(horizonStackView([sectorTextField, occupationTextField]))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func autoFillMockupData() {
        phoneNumberTextField.setText("069778829")
        maritalStatusTextField.setValueWhere(index: 0)
        titlesTextField.setValueWhere(index: 0)
        sourceOfFundTextField.setValueWhere(index: 0)
        montlyIncomeTextField.setValueWhere(index: 0)
        sectorTextField.setValueWhere(index: 0)
        occupationTextField.setValueWhere(index: 0)
    }
    
    private func setupTextField() {
        phoneNumberTextField.setPlaceholder("phone_number".localize, isRequired: true)
        maritalStatusTextField.setPlaceholder("marital_status".localize, isRequired: true)
        titlesTextField.setPlaceholder("titles".localize, isRequired: true)
        sourceOfFundTextField.setPlaceholder("source_of_fund".localize, isRequired: true)
        montlyIncomeTextField.setPlaceholder("monthly_income".localize, isRequired: true)
        sectorTextField.setPlaceholder("sector".localize, isRequired: true)
        occupationTextField.setPlaceholder("occupation".localize, isRequired: true)
        
        phoneNumberTextField.textField.keyboardType = .phonePad
        
        guard let enumeration = Constants.enumerationResponse else { return }
        
        if let values = enumeration.maritalStatus {
            maritalStatusTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
        }
        
        if let values = enumeration.customerTitle {
            titlesTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
        }
        
        if let values = enumeration.incomeSource {
            sourceOfFundTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
        }
        
        if let values = enumeration.mlAvgInc {
            montlyIncomeTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
        }
        
        if let values = enumeration.sectorID {
            sectorTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
        }
        
        if let values = enumeration.occupationList {
            occupationTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
        }
    }
    
    func setDefaultValues(isUpgrade: Bool, model: RegisterUserValidateParam) {
        if let phoneNumber = model.phoneNumber {
            phoneNumberTextField.setText(phoneNumber)
        }
        
        maritalStatusTextField.setValueWhere(key: model.maritalStauts?.key ?? "")
        titlesTextField.setValueWhere(key: model.titles?.key ?? "")
        sourceOfFundTextField.setValueWhere(key: model.sourceOfFund?.key ?? "")
        montlyIncomeTextField.setValueWhere(key: model.monthlyAvgIncome?.key ?? "")
        sectorTextField.setValueWhere(key: model.sector?.key ?? "")
        occupationTextField.setValueWhere(key: model.occupationID?.key ?? "")
        
        phoneNumberTextField.toggleEnable(!isUpgrade)
    }
}
