//
//  AccountInformationEditView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//

import UIKit

class AccountInformationEditView: UIView {
  
  let wrapper: GroupWrapperView = {
    let view = GroupWrapperView()
    view.setPlaceholder("account_information".localize)
    
    return view
  }()
  private let vStackView: UIStackView = {
    let stackView = UIStackView()
    stackView.axis = .vertical
    stackView.alignment = .fill
    stackView.distribution = .fill
    stackView.spacing = 8
    
    return stackView
  }()
  
  let accountCreationPurposeView = BaseDropdownTextField()
  let currencyTextField = BaseDropdownTextField()
  let accountTypeTextField = BaseDropdownTextField()
  let cashOutPackageTextField = BaseDropdownTextField()
  let purposeOfBankingTextField = BaseDropdownTextField()
  
  var currency: KeyValueModel? = nil
  var cashoutPackage: KeyValueModel? = nil
  var onAccountCreationPurposeChanged: (() -> Void)?
  var accountCreationSelectedIndex: Int = 0
  
  var accountCreationPurpose: KeyValueModel? {
    return accountCreationPurposeView.selectedDatasource
  }
  var accountType: KeyValueModel? {
    return accountTypeTextField.selectedDatasource
  }
  var purposeOfBanking: KeyValueModel? {
    return purposeOfBankingTextField.selectedDatasource
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupTextField()
    observeTextFieldValueChanged()
    
    addSubview(wrapper)
    wrapper.fillInSuperView()
    
    wrapper.addCustomView(view: vStackView)
    
    vStackView.addArrangedSubview(accountCreationPurposeView)
    vStackView.addArrangedSubview(currencyTextField)
    vStackView.addArrangedSubview(purposeOfBankingTextField)
    vStackView.addArrangedSubview(accountTypeTextField)
    vStackView.addArrangedSubview(cashOutPackageTextField)
    
    //        accountCreationPurposeView.isHidden = true
    accountCreationPurposeView.isHidden = Constants.authentication.applicationID == .wcxEKYC
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setViewForUpgrade(_ bool: Bool) {
    currencyTextField.isHidden = bool
    accountTypeTextField.isHidden = bool
  }
  
  func setonAccountCreationPurposeChanged(handler: @escaping () -> Void) {
    self.onAccountCreationPurposeChanged = handler
  }
  
  func setDefaultValues(model: RegisterUserValidateParam) {
    accountTypeTextField.setValueWhere(key: model.accountType?.key ?? "")
    purposeOfBankingTextField.setValueWhere(key: model.bankingPurpose?.key ?? "")
    
    currencyTextField.setValueWhere(key: model.currency?.key ?? "")
    currency = currencyTextField.selectedDatasource
    
    setCashoutPackageDatasoruce()
    cashOutPackageTextField.setValueWhere(key: model.cashOutPackage?.key ?? "")
    if let value = model.cashOutPackage, value.key.isNotEmpty {
      cashOutPackageTextField.toggleEnable(!model.isExistingUserSubscription)
    }else {
      cashOutPackageTextField.toggleEnable(true)
    }
    cashoutPackage = cashOutPackageTextField.selectedDatasource
  }
  
  func autoFillMockupData() {
    currencyTextField.setValueWhere(index: 0)
    accountTypeTextField.setValueWhere(index: 0)
    cashOutPackageTextField.setValueWhere(index: 0)
    purposeOfBankingTextField.setValueWhere(index: 0)
    
  }
  
  private func setupTextField() {
    let accountCreationPurposeDatasource: [KeyValueModel] = [
      KeyValueModel(key: "0", value: "purpose_personal_account".localize),
      KeyValueModel(key: "1", value: "purpose_merchant_account".localize),
    ]
    accountCreationPurposeView.setPlaceholder("account_creation_purpose".localize, isRequired: true)
    accountCreationPurposeView.setDatasource(accountCreationPurposeDatasource)
    accountCreationPurposeView.setOnValueSelected { [weak self] selectedValue in
      guard let self = self else { return }
      
      let index = Int(selectedValue.key) ?? 0
      self.accountCreationSelectedIndex = index
      
      self.accountCreationPurposeView.setText(selectedValue.value)
      self.onAccountCreationPurposeChanged?()
    }
    
    currencyTextField.setPlaceholder("currency".localize, isRequired: true)
    accountTypeTextField.setPlaceholder("account_type".localize, isRequired: true)
    cashOutPackageTextField.setPlaceholder("cashout_package".localize, isRequired: true)
    purposeOfBankingTextField.setPlaceholder("purpose_of_banking".localize, isRequired: true)
    
    guard let enumeration = Constants.enumerationResponse else { return }
    
    if Constants.authentication.applicationID == .wingEKYC {
      if Constants.authentication.userRole == .employee || Constants.authentication.userRole == .iPay {
        if let values = enumeration.accountTypesKYC {
          accountTypeTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
        }
      }else {
        if let values = enumeration.accountTypes {
          accountTypeTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
        }
      }
    }else {
      if let values = enumeration.accountTypes {
        accountTypeTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
      }
    }
    
    if let values = enumeration.bankingPurpose {
      purposeOfBankingTextField.setDatasource(values.map { .init(key: $0.enumID ?? "", value: $0.toLocalized(), isSelected: $0.isDefault) })
    }
    
    currencyTextField.setDatasource(Constants.currencyDatasource)
  }
  
  private func setCashoutPackageDatasoruce() {
    guard let enumeration = Constants.enumerationResponse else { return }
    
    let cashoutPackageDatasource: [CashOutPackage] = (currency?.key == "USD" ? enumeration.cashOutPackageUSD : currency?.key == "KHR" ? enumeration.cashOutPackageKHR : []) ?? []
    self.cashOutPackageTextField.setDatasource(cashoutPackageDatasource.map { .init(key: $0.subID ?? "", value: $0.classDescription?.toLocalized() ?? "") })
  }
  
  private func observeTextFieldValueChanged() {
    guard let enumeration = Constants.enumerationResponse else { return }
    
    currencyTextField.setOnValueSelected { [weak self] selected in
      guard let self = self else { return }
      
      self.currency = selected
      self.setCashoutPackageDatasoruce()
      
      if let selectedCashoutPackage = self.cashoutPackage {
        let id = selectedCashoutPackage.key
        let cashoutPackageDatasource: [CashOutPackage] = (selected.key == "USD" ? enumeration.cashOutPackageUSD :  enumeration.cashOutPackageKHR) ?? []
        
        self.cashoutPackage = cashoutPackageDatasource.filter { $0.subID == id }.map { KeyValueModel(key: $0.subID ?? "", value: $0.classDescription?.toLocalized() ?? "") }.first
        self.cashOutPackageTextField.setValueWhere(key: self.cashoutPackage?.key ?? "")
      }
    }
    
    cashOutPackageTextField.setOnValueSelected { [weak self] selected in
      guard let self = self else { return }
      
      self.cashoutPackage = selected
    }
    
    cashOutPackageTextField.setOnTextFieldBeginEditing { [weak self] in
      guard let self = self else { return }
      
      if self.cashOutPackageTextField.datasources.isEmpty {
        self.showErrorAlert("please_select_currency_before_hand".localize)
      }else {
        self.cashOutPackageTextField.presentSelectionView()
      }
    }
  }
}
