//
//  CustomLoadingSpinnerView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 29/12/21.
//

import UIKit

class CustomLoadingSpinnerView: UIView {

    private var size: CGFloat = 0
    private let spinner = CAShapeLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configSpinner()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configSpinner() {
        frame = .init(x: 0, y: 0, width: 100, height: 100)
        let rect = bounds
        let circularPath = UIBezierPath(ovalIn: rect)
        
        spinner.path = circularPath.cgPath
        spinner.fillColor = UIColor.clear.cgColor
        spinner.strokeColor = UIColor.white.cgColor
        spinner.lineWidth = 10
        spinner.strokeEnd = 1
        spinner.lineCap = .round
        spinner.lineDashPattern = [1, NSNumber(value: spinner.lineWidth * 2)]
        
        layer.addSublayer(spinner)
    }
    
    func startAnimation() {
        UIView.animate(withDuration: 2, delay: 0, options: .curveLinear) {
            self.transform = .init(rotationAngle: .pi)
        } completion: { bool in
            UIView.animate(withDuration: 2, delay: 0, options: .curveLinear) {
                self.transform = .init(rotationAngle: 0)
            } completion: { bool in
                self.startAnimation()
            }
        }

    }
    
    func customSpinner(handler: @escaping (CAShapeLayer) -> Void) {
        handler(spinner)
    }
}
