//
//  AgreementView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/11/21.
//

import Foundation
import UIKit

class AgreementView: UIView {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_deselected_checkbox").withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .primaryColor
        imageView.contentMode = .scaleAspectFit
        imageView.constrainWidth(constant: 25)
        imageView.constrainHeight(constant: 25)
        
        return imageView
    }()
    private let descriptionLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .subTitleColor
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 13)
        label.numberOfLines = 0
        
        return label
    }()
    
    var isAgreed: Bool = false {
        didSet {
            let selectedImage: UIImage = .localImage("ic_selected_checkbox").withRenderingMode(.alwaysTemplate)
            let deSelectedImage: UIImage = .localImage("ic_deselected_checkbox").withRenderingMode(.alwaysTemplate)
            
            UIView.animate(withDuration: 0.2, delay: 0, options: .transitionCrossDissolve) { [weak self] in
                guard let self = self else { return }
                
                self.iconImageView.alpha = 0
            } completion: { [weak self] _ in
                guard let self = self else { return }
                
                self.iconImageView.image = self.isAgreed ? selectedImage : deSelectedImage
                self.iconImageView.alpha = 1
            }
        }
    }
    private var labelActionHandler: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setDescription()
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        stackView.addArrangedSubview(iconImageView)
        stackView.addArrangedSubview(descriptionLabel)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapHandler))
        
        iconImageView.isUserInteractionEnabled = true
        iconImageView.addGestureRecognizer(tapGesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func onTapHandler() {
        isAgreed.toggle()
    }
    
    func setDescription(string: String = "\("me".localize) \(Constants.authentication.agentAccount.agentName) \("agreed_the_document_is_valid".localize)") {
        let string = string
        let attributeString = NSMutableAttributedString(string: string)
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineSpacing = 3
        attributeString.addAttribute(.paragraphStyle, value: paragraph, range: .init(location: 0, length: attributeString.length))
        
        descriptionLabel.attributedText = attributeString
    }
    
    func setDescriptionWithAction(descriptionString: String, highlightText: String, additionalDescription: String? = nil, highlightAction: @escaping () -> Void ) {
        let attributeString = NSMutableAttributedString(string: descriptionString)
        let highlightString = NSMutableAttributedString(string: highlightText, attributes: [.foregroundColor: UIColor.secondaryColor])
        
        attributeString.append(highlightString)
        
        if let additionalDescription = additionalDescription {
            let additionalString = NSMutableAttributedString(string: additionalDescription)
            attributeString.append(additionalString)
        }
        
        descriptionLabel.attributedText = attributeString
        
        self.labelActionHandler = highlightAction
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onActionLabelTap))
        descriptionLabel.isUserInteractionEnabled = true
        descriptionLabel.addGestureRecognizer(tapGesture)
    }
    
    @objc private func onActionLabelTap() {
        guard let handler = labelActionHandler else { return }
        handler()
    }
}
