//
//  IdentityValidationFailedView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/1/22.
//

import UIKit

class IdentityValidationFailedView: UIView {

    private let navigationBarView = ContainerView(background: .primaryColor)
    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_failed_validation")
        imageView.constrainWidth(constant: 180)
        imageView.constrainHeight(constant: 180)
        imageView.contentMode = .scaleToFill
        
        return imageView
    }()
    private let descriptionLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "verification_failed_description".localize
        label.textAlignment = .center
        label.textColor = .white
        label.font = .systemFont(ofSize: 18)
        label.numberOfLines = 0
        
        return label
    }()
    private let tryAgainButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "try_again_button".localize)
        button.backgroundColor = .secondaryColor
        button.setTitleColor(.white, for: .normal)
        button.setDefaultHeight()
        button.tag = 502
         
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .primaryColor
        
        addSubview(navigationBarView)
        navigationBarView.constrainHeight(constant: UIConstants.navigationHeight + UIConstants.topSafeArea)
        navigationBarView.anchor(top: topAnchor,
                                 leading: leadingAnchor,
                                 bottom: nil,
                                 trailing: trailingAnchor)
        
        addSubview(iconImageView)
        iconImageView.centerXInSuperview()
        iconImageView.anchor(top: navigationBarView.bottomAnchor,
                             leading: nil,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 32, left: 0, bottom: 0, right: 0))
        
        addSubview(descriptionLabel)
        descriptionLabel.centerXInSuperview()
        descriptionLabel.lessThenOrEqualTo(bottom: bottomAnchor, constant: UIConstants.bottomSafeArea + 16)
        descriptionLabel.anchor(top: iconImageView.bottomAnchor,
                                leading: leadingAnchor,
                                bottom: nil,
                                trailing: nil,
                                padding: .init(top: 64, left: 16, bottom: 0, right: 0))
        
        addSubview(tryAgainButton)
        tryAgainButton.setDefaultHeight()
        tryAgainButton.greaterOrEqualTo(top: descriptionLabel.bottomAnchor, constant: 16)
        tryAgainButton.anchor(top: nil,
                              leading: leadingAnchor,
                              bottom: bottomAnchor,
                              trailing: trailingAnchor,
                              padding: .init(top: 0, left: 16, bottom: UIConstants.bottomSafeArea + 16, right: 16))
        
        tryAgainButton.setRoundedView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
