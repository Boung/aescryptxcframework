//
//  CustomerInformationEditViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//

import UIKit

class CustomerInformationEditViewController: BaseViewController {
    
    private let scrollView = ScrollViewContainerView()
    private var containerView: UIView {
        return scrollView.containerView
    }
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let imageContainerView = ContainerView(background: .white)
    private var imageView: CustomerImagesView?
    
    private let innerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let informationView = CustomerInformationEditView()
    
    private let customerPlaceOfBirthView = CustomerPlaceOfBirtEditView()
    private let customerAddressView = OtherIdentityCustomerAddressEditView()
    
    private let saveButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "save".localize)
        button.backgroundColor = .primaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    var isNID: Bool = false
    var isRedirected: Bool = false
    var userValidateParam: RegisterUserValidateParam?
    var onSaveButtonHandler: ((RegisterUserValidateParam) -> Void)?
    
    override func viewDidLoad() {
        imageView = .init(stackView: .vertical, isNID: isNID)
        super.viewDidLoad()
        
        if isRedirected {
            removeBackButton()
        }
        
        setupDocumentImages()
        
        informationView.setOnNationalityChanged { [weak self] selected in
            guard let self = self else { return }
            
            self.customerPlaceOfBirthView.observeCountry(model: selected)
        }
        
        saveButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.userValidateParam?.expiryDate = self.informationView.expiredDate
            self.userValidateParam?.dob = self.informationView.dateOfBirth
            
            let isPassport = self.userValidateParam?.isPassport ?? false
            let isValidExpiry = self.userValidateParam?.isValidExpiryDate ?? false
            let isValidAge = self.userValidateParam?.isValidAge ?? false
            
            guard isValidAge, isValidExpiry else {
                let expiryIDMessage = !isValidExpiry ? "\(isPassport ? "passport_expired" : "id_card_expired")".localize : ""
                let ageInvalidMessage = !isValidAge ? "age_not_eligible".localize : ""
                let message = "invalid_customer_information".localize + expiryIDMessage + ageInvalidMessage
                
                AlertManager.shared.showSingleButtonAlert(title: "we_got_wrong_information".localize, message: message, alertButton: .init(title: "retry".localize, style: .default, handler: {
                    self.navigationController?.popTo(viewController: CustomerPreviewViewController.self)
                }))
                return
            }
            
            guard
                let registerUserParam = self.generateRegisterParam(),
                let handler = self.onSaveButtonHandler
            else { return }
            
            handler(registerUserParam)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = "title_edit_information".localize
        
        guard let registerUserValidateParam = userValidateParam else { return }

        if let nationality = registerUserValidateParam.nationality {
            customerPlaceOfBirthView.observeCountry(model: .init(key: nationality.key, value: ""))
        }else if let passportCountry = registerUserValidateParam.passportCountryCode, passportCountry.isNotEmpty {
            customerPlaceOfBirthView.observeCountry(model: .init(key: passportCountry, value: ""))
        }

        informationView.setDefaultValues(model: registerUserValidateParam)
        customerPlaceOfBirthView.setDefaultValues(model: registerUserValidateParam)
        customerAddressView.setDefaultValues(model: registerUserValidateParam)
    }
    
    override func autoFillMockupData() {
        super.autoFillMockupData()
        
        informationView.autoFillMockupData()
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.backgroundColor = .backgroundColor
        
        view.addSubview(scrollView)
        scrollView.fillInSuperView()
        
        containerView.addSubview(vStackView)
        vStackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        vStackView.addArrangedSubview(imageContainerView)
        vStackView.addArrangedSubview(innerStackView)
        vStackView.addArrangedSubview(saveButton)
        
        imageContainerView.addSubview(imageView!)
        imageView?.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        let customerPlaceOfBirthWrapper = ContainerView(background: .white)
        customerPlaceOfBirthWrapper.setCornerRadius(radius: 16)
        customerPlaceOfBirthWrapper.addSubview(customerPlaceOfBirthView)
        customerPlaceOfBirthView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        let customerAddressWrapper = ContainerView(background: .white)
        customerAddressWrapper.setCornerRadius(radius: 16)
        customerAddressWrapper.addSubview(customerAddressView)
        customerAddressView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        innerStackView.addArrangedSubview(informationView)
        innerStackView.addArrangedSubview(customerPlaceOfBirthWrapper)
        innerStackView.addArrangedSubview(customerAddressWrapper)
        
        imageContainerView.setCornerRadius(radius: 16)
        saveButton.setRoundedView()
    }
    
    func generateRegisterParam() -> RegisterUserValidateParam? {
        observeErrorField()
        
        guard let idType = informationView.identityType else {
            showErrorAlert()
            return nil
        }
        guard informationView.identityNumber.isNotEmpty else {
            showErrorAlert()
            return nil
        }
        guard informationView.englishLastName.isNotEmpty else {
            showErrorAlert()
            return nil
        }
        guard informationView.englishFirstName.isNotEmpty else {
            showErrorAlert()
            return nil
        }
        guard let nationality = informationView.nationality else {
            showErrorAlert()
            return nil
        }
        guard let gender = informationView.gender else {
            showErrorAlert()
            return nil
        }
        guard informationView.dateOfBirth.isNotEmpty else {
            showErrorAlert()
            return nil
        }
        
        guard let dobDate = informationView.dateOfBirth.toDate(from: "dd/MM/yyyy"), KYCHelper.shared.isValid(age: dobDate) else {
            showErrorAlert("invalid_dob_age".localize)
            return nil
        }
        
        guard let expiryDate = informationView.expiredDate.toDate(from: "dd/MM/yyyy"), KYCHelper.shared.isValid(expiry: expiryDate) else {
            showErrorAlert("expiry_date_expired".localize)
            return nil
        }
        
        let param = RegisterUserValidateParam()
        
        if customerPlaceOfBirthView.isCambodia {
            guard let birthAddress = customerPlaceOfBirthView.getAddress() else { return nil }
            
            param.birthAddress = birthAddress
            param.birthAddressTextModel = nil
            param.birthAddressString = nil
        }else {
            guard let birthAddressString = customerPlaceOfBirthView.getAddressString() else { return nil }
            param.birthAddress = nil
            param.birthAddressTextModel = nil
            param.birthAddressString = birthAddressString
        }
        
        if customerAddressView.isCambodia {
            guard let address = customerAddressView.getAddress() else {
                showErrorAlert()
                return nil
            }
            param.currentAddress = address
            param.currentAddressTextModel = nil
            param.currentAddressString = nil
        }else {
            guard let addressString = customerAddressView.getAddressString() else {
                showErrorAlert()
                return nil
            }
            param.currentAddress = nil
            param.currentAddressTextModel = nil
            param.currentAddressString = addressString
        }
        
        param.idType = idType
        param.idNumber = informationView.identityNumber
        param.lastNameKh = informationView.khmerLastName
        param.firstNameKh = informationView.khmerFirstName
        param.lastName = informationView.englishLastName
        param.firstName = informationView.englishFirstName
        param.nationality = nationality
        param.gender = gender
        param.dob = informationView.dateOfBirth
        param.expiryDate = informationView.expiredDate
        
        param.houseNo = customerAddressView.houseNumber
        param.streetNo = customerAddressView.streetNumber
        
        return param
    }
    
    private func observeErrorField() {
        informationView.identityTypeTextField.setErrorTextField(informationView.identityType == nil)
        informationView.identityNumberTextField.setErrorTextField(informationView.identityNumber.isEmpty)
        informationView.englishLastNameTextField.setErrorTextField(informationView.englishLastName.isEmpty)
        informationView.englishFirstNameTextField.setErrorTextField(informationView.englishFirstName.isEmpty)
        informationView.nationalityTextField.setErrorTextField(informationView.nationality == nil)
        informationView.genderTextField.setErrorTextField(informationView.gender == nil)
        informationView.dateOfBirthTextField.setErrorTextField(informationView.dateOfBirth.isEmpty)
        if let dobDate = informationView.dateOfBirth.toDate(from: "dd/MM/yyyy") {
            informationView.dateOfBirthTextField.setErrorTextField(!KYCHelper.shared.isValid(age: dobDate))
        }
        
        if let expiryDate = informationView.expiredDate.toDate(from: "dd/MM/yyyy") {
            informationView.expireDateTextField.setErrorTextField(!KYCHelper.shared.isValid(expiry: expiryDate))
        }
        
        customerPlaceOfBirthView.observeErrorField()
        customerAddressView.observeErrorField()
    }
    
    private func setupDocumentImages() {
        imageView?.setupDetails(customerImage: userValidateParam?.customerImage,
                                idFront: userValidateParam?.identityFrontImage,
                                idBack: isNID ? nil : userValidateParam?.identityBackImage)
    }
}
