//
//  UpgradeAcountInputViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 15/11/21.
//

import Foundation
import UIKit

class UpgradeAcountInputViewController: BaseViewController {
    
    private let containerView = ContainerView(background: .white)
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let wingAccountTextField = BaseTextField()
    private let pinCodeTextField = BaseTextField()
    private let continueButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "continue".localize)
        button.setDefaultHeight()
        
        return button
    }()
    
    var isNID: Bool = false
    var huaweiSDKHandler: ((UpgradeUserAccountInformationResponse?, Bool) -> Void)?
    var wingAccount: String {
        return wingAccountTextField.textField.text ?? ""
    }
    var secureCode: String {
        return pinCodeTextField.textField.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "title_upgrade_wing_account".localize
        setupTextField()
        
        continueButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.view.endEditing(true)
            self.observeErrorField()
            guard self.wingAccount.isNotEmpty else {
                self.showErrorAlert()
                return
            }
            
            guard self.secureCode.isNotEmpty, self.secureCode.count >= 6 else {
                self.showErrorAlert()
                return
            }
            
            let presenter = UpgradeUserAccountInformationPresenter(delegate: self)
            let param = UpgradeUserAccountInformationParam(wingAccount: self.wingAccount, confirmCode: self.secureCode)
            
            presenter.getUserAccountInformation(param: param)
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.backgroundColor = .backgroundColor
        
        view.addSubview(containerView)
        containerView.anchor(top: view.layoutMarginsGuide.topAnchor,
                          leading: view.leadingAnchor,
                          bottom: nil,
                          trailing: view.trailingAnchor,
                          padding: .init(top: 16, left: 16, bottom: 0, right: 16))
        
        containerView.addSubview(vStackView)
        vStackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        vStackView.addArrangedSubview(wingAccountTextField)
        vStackView.addArrangedSubview(pinCodeTextField)
        vStackView.addArrangedSubview(continueButton)
        
        containerView.setCornerRadius(radius: 16)
        continueButton.setRoundedView()
    }
    
    private func setupTextField() {
        wingAccountTextField.setPlaceholder("wing_account_number".localize, isRequired: true)
        pinCodeTextField.setPlaceholder("input_6_digits_secure_code".localize, isRequired: true)
        
        wingAccountTextField.textField.keyboardType = .numberPad
        
        pinCodeTextField.textField.isSecureTextEntry = true
        pinCodeTextField.maxInputLength = 6
        pinCodeTextField.textField.keyboardType = .numberPad
    }
    
    private func observeErrorField() {
        wingAccountTextField.setErrorTextField(wingAccount.isEmpty)
        pinCodeTextField.setErrorTextField(secureCode.isEmpty || secureCode.count < 6)
    }
}

extension UpgradeAcountInputViewController: UpgradeUserAccountInformationDelegate {
    func response(upgradeInformation response: UpgradeUserAccountInformationResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        
        if isNID {
            showCustomerCameraVC { [weak self] customerImage in
                guard let self = self else { return }
             
                response.customerImage = customerImage
                guard let handler = self.huaweiSDKHandler else { return }
                handler(response, self.isNID)
            }
        }else {
            showCustomerCameraVC { [weak self] customerImage in
                guard let self = self else { return }
            
                response.customerImage = customerImage
                self.showFrontDocumentCameraVC { [weak self] frontIDImage in
                    guard let self = self else { return }
                    
                    response.frontIDImage = frontIDImage
                    if self.isNID {
                        guard let handler = self.huaweiSDKHandler else { return }
                        handler(response, self.isNID)
                    }else {
                        self.showBackDocumentCameraVC { [weak self] backIDImage in
                            guard let self = self else { return }
                            
                            response.backIDImage = backIDImage
                            guard let handler = self.huaweiSDKHandler else { return }
                            handler(response, self.isNID)
                        }
                    }
                }
            }
        }
    }
    
    private func showCustomerCameraVC(completion: @escaping (UIImage) -> Void) {
        let vc = CustomerCameraViewController()
        vc.isUpgradeAccount = true
        vc.setOnContinueButtonTap(handler: completion)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showFrontDocumentCameraVC(completion: @escaping (UIImage) -> Void) {
        let vc = FrontDocumentCameraViewController()
        vc.isUpgradeAccount = true
        vc.setOnContinueButtonTap(handler: completion)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showBackDocumentCameraVC(completion: @escaping (UIImage) -> Void) {
        let vc = BackDocumentCameraViewController()
        vc.isUpgradeAccount = true
        vc.setOnContinueButtonTap(handler: completion)
        
        navigationController?.pushViewController(vc, animated: true)
    }
}
