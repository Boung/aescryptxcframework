//
//  CustomerAdditionalInformationViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//

import UIKit

class CustomerAdditionalInformationViewController: BaseViewController {
    
    private let scrollView = ScrollViewContainerView()
    private var containerView: UIView {
        return scrollView.containerView
    }
    let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.numberOfLines = 1
        
        let attributeString = NSAttributedString.init(string: "customer_addition_information".localize, attributes: [
            .font: UIFont.boldSystemFont(ofSize: 18),
            .underlineStyle: NSUnderlineStyle.thick.rawValue,
            .underlineColor: UIColor.subTitleColor,
            .foregroundColor: UIColor.subTitleColor,
        ])
        label.attributedText = attributeString
        
        return label
    }()
    
    let additionalInformationView = AdditionalInformationEditView()
    let accountInformationView = AccountInformationEditView()
    let otherRequestInformationView = OtherRequestInformationEditView()
    
    let agreementView = AgreementView()
    
    let continueButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "continue".localize)
        button.setDefaultHeight()
        
        return button
    }()
    
    var isNID: Bool = false
    var isUpgradeAccount: Bool = false
    var registerUserValidateParam = RegisterUserValidateParam()
    var upgradeUserValidateParam = UpgradeUserValidateParam()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        navigationItem.title = !isUpgradeAccount ? "title_register_wing_account".localize : "title_upgrade_wing_account".localize
        setBackButtonAction { [weak self] in
            guard let self = self else { return }
            
            self.storeInputedValue()
            self.navigationController?.popViewController(animated: true)
        }
        
        
        accountInformationView.setViewForUpgrade(isUpgradeAccount)
        continueButton.setAction { [weak self] in
            guard let self = self else { return }
            
            if self.validateRegisterParam() {
                if !self.agreementView.isAgreed {
                    self.showErrorAlert("error_please_agree_the_valid_information".localize)
                    return
                }
                
                let vc = CustomerOverrallInformationViewController()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.isNID = self.isNID
                vc.isUpgradeAccount = self.isUpgradeAccount
                vc.registerUserValidateParam = self.registerUserValidateParam
                vc.upgradeUserValidateParam = self.upgradeUserValidateParam
                
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                self.showErrorAlert()
            }
        }
        
        otherRequestInformationView.setOnValueChangeObserverAction { [weak self] in
            guard let self = self else { return }
            
            if self.otherRequestInformationView.isRequestWingCard {
                self.registerUserValidateParam.kitNumber = self.otherRequestInformationView.kitNumber
                self.upgradeUserValidateParam.kitNumber = self.otherRequestInformationView.kitNumber
            }else {
                self.registerUserValidateParam.kitNumber = nil
                self.upgradeUserValidateParam.kitNumber = nil
            }
            
            if self.otherRequestInformationView.isRequestDebitCard {
                self.registerUserValidateParam.debitCardInformation = self.otherRequestInformationView.debitCardInformationResponse
                self.upgradeUserValidateParam.debitCardInformation = self.otherRequestInformationView.debitCardInformationResponse
            }else {
                self.registerUserValidateParam.debitCardInformation = nil
                self.upgradeUserValidateParam.debitCardInformation = nil
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        additionalInformationView.setDefaultValues(isUpgrade: isUpgradeAccount, model: isUpgradeAccount ? upgradeUserValidateParam : registerUserValidateParam)
        accountInformationView.setDefaultValues(model: isUpgradeAccount ? upgradeUserValidateParam : registerUserValidateParam)
        otherRequestInformationView.setDefaultValues(isUpgrade: isUpgradeAccount, model: isUpgradeAccount ? upgradeUserValidateParam : registerUserValidateParam)
    }
    
    override func autoFillMockupData() {
        super.autoFillMockupData()
        
        additionalInformationView.autoFillMockupData()
        accountInformationView.autoFillMockupData()
    }
    
    override func generateUI() {
        super.generateUI()
     
        view.backgroundColor = .backgroundColor
        
        view.addSubview(scrollView)
        scrollView.anchor(top: view.layoutMarginsGuide.topAnchor,
                          leading: view.leadingAnchor,
                          bottom: view.layoutMarginsGuide.bottomAnchor,
                          trailing: view.trailingAnchor)
        
        containerView.addSubview(vStackView)
        vStackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        vStackView.addArrangedSubview(titleLabel)
        vStackView.addArrangedSubview(additionalInformationView)
        vStackView.addArrangedSubview(accountInformationView)
        vStackView.addArrangedSubview(otherRequestInformationView)
        vStackView.addArrangedSubview(agreementView)
        vStackView.addArrangedSubview(continueButton)
        
        continueButton.setRoundedView()
    }
    
    func validateRegisterParam() -> Bool {
        observeErrorField()
        
        guard additionalInformationView.phoneNumber.isNotEmpty else {
            return false
        }
        
        guard KYCHelper.shared.isValid(phoneNumber: additionalInformationView.phoneNumber) else {
            showErrorAlert("please_input_a_valid_phone_number".localize)
            return false
        }
        
        guard let maritalStatus = additionalInformationView.maritalStatus else {
            return false
        }
        
        guard let titles = additionalInformationView.titles else {
            return false
        }
        
        guard let sourceOfFund = additionalInformationView.sourceOfFund else {
            return false
        }
        
        guard let monthlyIncome = additionalInformationView.monthlyIncome else {
            return false
        }
        
        guard let sector = additionalInformationView.sector else {
            return false
        }
        
        guard let occupation = additionalInformationView.occupation else {
            return false
        }
        
        guard let cashOutPackage = accountInformationView.cashoutPackage else {
            return false
        }
        
        guard let purposeOfBanking = accountInformationView.purposeOfBanking else {
            return false
        }
        
        registerUserValidateParam.phoneNumber = additionalInformationView.phoneNumber
        registerUserValidateParam.maritalStauts = maritalStatus
        registerUserValidateParam.titles = titles
        registerUserValidateParam.sourceOfFund = sourceOfFund
        registerUserValidateParam.monthlyAvgIncome = monthlyIncome
        registerUserValidateParam.sector = sector
        registerUserValidateParam.occupationID = occupation
        registerUserValidateParam.cashOutPackage = accountInformationView.cashoutPackage
        registerUserValidateParam.bankingPurpose = purposeOfBanking
        
        upgradeUserValidateParam.phoneNumber = additionalInformationView.phoneNumber
        upgradeUserValidateParam.maritalStauts = maritalStatus
        upgradeUserValidateParam.titles = titles
        upgradeUserValidateParam.sourceOfFund = sourceOfFund
        upgradeUserValidateParam.monthlyAvgIncome = monthlyIncome
        upgradeUserValidateParam.sector = sector
        upgradeUserValidateParam.occupationID = occupation
        upgradeUserValidateParam.cashOutPackage = cashOutPackage
        upgradeUserValidateParam.bankingPurpose = purposeOfBanking
        
        if !isUpgradeAccount {
            guard let currency = accountInformationView.currency else {
                return false
            }
            
            guard let accountType = accountInformationView.accountType else {
                return false
            }
            
            registerUserValidateParam.currency = currency
            registerUserValidateParam.accountType = accountType
        }
        
        if otherRequestInformationView.isRequestWingCard {
            guard otherRequestInformationView.kitNumber.isNotEmpty else {
                showErrorAlert()
                registerUserValidateParam.kitNumber = nil
                upgradeUserValidateParam.kitNumber = nil
                return false
            }
            
            registerUserValidateParam.registerWith = Constants.kitRegister
            registerUserValidateParam.kitNumber = otherRequestInformationView.kitNumber
            upgradeUserValidateParam.kitNumber = otherRequestInformationView.kitNumber
        }else {
            registerUserValidateParam.registerWith = Constants.phoneRegister
            registerUserValidateParam.kitNumber = nil
            upgradeUserValidateParam.kitNumber = nil
        }
        
        if otherRequestInformationView.isRequestDebitCard {
            guard let debitCardInformation = otherRequestInformationView.debitCardInformationResponse else {
                showErrorAlert()
                registerUserValidateParam.debitCardInformation = nil
                upgradeUserValidateParam.debitCardInformation = nil
                return false
            }
            
            registerUserValidateParam.debitCardInformation = debitCardInformation
            upgradeUserValidateParam.debitCardInformation = debitCardInformation
        }else {
            registerUserValidateParam.debitCardInformation = nil
            upgradeUserValidateParam.debitCardInformation = nil
        }
        
        return true
    }
    
    private func observeErrorField() {
        additionalInformationView.phoneNumberTextField.setErrorTextField(additionalInformationView.phoneNumber.isEmpty)
        additionalInformationView.maritalStatusTextField.setErrorTextField(additionalInformationView.maritalStatus == nil)
        additionalInformationView.titlesTextField.setErrorTextField(additionalInformationView.titles == nil)
        additionalInformationView.sourceOfFundTextField.setErrorTextField(additionalInformationView.sourceOfFund == nil)
        additionalInformationView.montlyIncomeTextField.setErrorTextField(additionalInformationView.monthlyIncome == nil)
        additionalInformationView.sectorTextField.setErrorTextField(additionalInformationView.sector == nil)
        additionalInformationView.occupationTextField.setErrorTextField(additionalInformationView.occupation == nil)
        accountInformationView.cashOutPackageTextField.setErrorTextField(accountInformationView.cashoutPackage == nil)
        accountInformationView.purposeOfBankingTextField.setErrorTextField(accountInformationView.purposeOfBanking == nil)
        
        if !isUpgradeAccount {
            accountInformationView.currencyTextField.setErrorTextField(accountInformationView.currency == nil)
            accountInformationView.accountTypeTextField.setErrorTextField(accountInformationView.accountType == nil)
        }
        
        if otherRequestInformationView.isRequestWingCard {
            otherRequestInformationView.observeErrorFields()
        }
        
        if otherRequestInformationView.isRequestDebitCard {
            otherRequestInformationView.observeErrorFields()
        }
    }
    
    private func storeInputedValue() {
        if additionalInformationView.phoneNumber.isNotEmpty, KYCHelper.shared.isValid(phoneNumber: additionalInformationView.phoneNumber) {
            registerUserValidateParam.phoneNumber = additionalInformationView.phoneNumber
            upgradeUserValidateParam.phoneNumber = additionalInformationView.phoneNumber
        }
        
        if let maritalStatus = additionalInformationView.maritalStatus {
            registerUserValidateParam.maritalStauts = maritalStatus
            upgradeUserValidateParam.maritalStauts = maritalStatus
        }
        
        
        if let titles = additionalInformationView.titles {
            registerUserValidateParam.titles = titles
            upgradeUserValidateParam.titles = titles
        }
        
        if let sourceOfFund = additionalInformationView.sourceOfFund {
            registerUserValidateParam.sourceOfFund = sourceOfFund
            upgradeUserValidateParam.sourceOfFund = sourceOfFund
        }
        
        if let monthlyIncome = additionalInformationView.monthlyIncome {
            registerUserValidateParam.monthlyAvgIncome = monthlyIncome
            upgradeUserValidateParam.monthlyAvgIncome = monthlyIncome
        }
        
        if let sector = additionalInformationView.sector {
            registerUserValidateParam.sector = sector
            upgradeUserValidateParam.sector = sector
        }
        
        if let occupation = additionalInformationView.occupation {
            registerUserValidateParam.occupationID = occupation
            upgradeUserValidateParam.occupationID = occupation
        }
        
        if let purposeOfBanking = accountInformationView.purposeOfBanking{
            registerUserValidateParam.bankingPurpose = purposeOfBanking
            upgradeUserValidateParam.bankingPurpose = purposeOfBanking
        }
        
        if !isUpgradeAccount {
            if let currency = accountInformationView.currency {
                registerUserValidateParam.currency = currency
            }
            
            if let accountType = accountInformationView.accountType {
                registerUserValidateParam.accountType = accountType
            }
        }
        
        if let cashoutPackage = accountInformationView.cashoutPackage {
            registerUserValidateParam.cashOutPackage = cashoutPackage
            upgradeUserValidateParam.cashOutPackage = cashoutPackage
        }
    }
}
