//
//  LoadingViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 24/11/21.
//

import UIKit

class LoadingViewController: BaseViewController {
    let indicatorView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.transform = .init(scaleX: 1.5, y: 1.5)
        indicator.color = .primaryColor
        indicator.startAnimating()
        
        return indicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func generateUI() {
        super.generateUI()
        
//        view.backgroundColor = .blurBackground
        setupBlurBackground()
        
        view.addSubview(indicatorView)
        indicatorView.centerInSuperview()
    }
}
