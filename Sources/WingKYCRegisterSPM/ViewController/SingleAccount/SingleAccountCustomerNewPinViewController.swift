//
//  SingleAccountCustomerNewPinViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 15/2/23.
//

import UIKit

class SingleAccountCustomerNewPinViewController: BaseViewController {
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultVerticalStackView(spacing: 16)
        stackView.alignment = .center
        return stackView
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "single_account_new_customer_otp_title".localize
        label.numberOfLines = 2
        label.textColor = .subTitleColor
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 18)
        
        return label
    }()
    private let subTitleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "single_account_new_customer_otp_sub_title".localize
        label.numberOfLines = 2
        label.textColor = .titleColor
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 15)
        
        return label
    }()
    private let otpView = OTPView(otpType: .otp4, otpMask: .dot)
    
    var singleAccountValidateParam = SingleAccountRegisterValidateParam()
    var isUpgradeAccount: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDetails()
        otpView.setOnOTPCompleted { [weak self] newPin in
            guard let self = self else { return }
            
            let vc = SingleAccountCustomerConfirmPinViewController()
            vc.singleAccountValidateParam = self.singleAccountValidateParam
            vc.newCustomerPin = newPin
            vc.onPasswordMismatchHandler = { [weak self] in
                guard let self = self else { return }

                self.showErrorAlert("new_customer_pin_mismatch".localize) { [weak self] in
                    guard let self = self else { return }
                    
                    self.otpView.resetOTP()
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = isUpgradeAccount ? "title_upgrade_wing_account".localize : "title_register_wing_account".localize
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(stackView)
        stackView.anchor(top: view.layoutMarginsGuide.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 64, left: 16, bottom: 0, right: 16))
        
        stackView.addArrangedSubview(titleLabel)
//        stackView.addArrangedSubview(subTitleLabel)
        stackView.addArrangedSubview(otpView)
    }
    
    private func setupDetails() {
        subTitleLabel.text = String(format: "single_account_new_customer_otp_sub_title".localize, singleAccountValidateParam.phoneNumber.orEmptyString)
    }
}
