//
//  SingleAccountSuccessViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/2/23.
//

import UIKit

class SingleAccountSuccessViewController: BaseViewController {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultVerticalStackView(spacing: 16)
        stackView.alignment = .center
        return stackView
    }()
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_success").withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .primaryColor.withAlphaComponent(0.8)
        imageView.backgroundColor = .white
        imageView.constrainWidth(constant: 100)
        imageView.constrainHeight(constant: 100)
        
        return imageView
    }()
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "single_account_creation_success".localize
        label.textAlignment = .center
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 24)
        
        return label
    }()
    let containerView = ContainerView(background: .white)
    let tableView = UITableView()
    let doneButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "done".localize)
        button.backgroundColor = .secondaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    let tableCell = "KeyValueTableViewCell"
    var datasource: [KeyValueModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(KeyValueTableViewCell.self, forCellReuseIdentifier: tableCell)
        tableView.backgroundColor = .white
        tableView.showsVerticalScrollIndicator = false
        
        doneButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.backgroundColor = .primaryColor
        
        view.addSubview(doneButton)
        doneButton.anchor(top: nil,
                          leading: view.leadingAnchor,
                          bottom: view.layoutMarginsGuide.bottomAnchor,
                          trailing: view.trailingAnchor,
                          padding: .init(top: 0, left: 16, bottom: 16, right: 16))
        
        view.addSubview(stackView)
        stackView.anchor(top: view.layoutMarginsGuide.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 64, left: 16, bottom: 0, right: 16))
        
        stackView.addArrangedSubview(iconImageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(containerView)
        
        stackView.setCustomSpacing(32, after: titleLabel)
        
        containerView.addSubview(tableView)
        tableView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        containerView.constrainWidth(constant: UIConstants.screenWidth - (16 * 2))
        
        let estimateRowHeight: CGFloat = 40
        let tableHeight = CGFloat(datasource.count) * estimateRowHeight
        let expectHeight = UIConstants.screenHeight * 0.5
        
        if tableHeight >= expectHeight {
            tableView.constrainHeight(constant: expectHeight)
        }else {
            tableView.constrainHeight(constant: tableHeight)
        }
        
        containerView.setCornerRadius(radius: 16)
        iconImageView.setRoundedView()
        doneButton.setRoundedView()
    }
}

extension SingleAccountSuccessViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCell, for: indexPath) as! KeyValueTableViewCell
        let selected = datasource[indexPath.row]
        cell.configCell(model: selected)
        cell.setupKeyValueLabelColor(.subTitleColor, value: selected.isSelected ? .secondaryColor : .subTitleColor)
        cell.setupFont(.boldSystemFont(ofSize: 15), value: selected.isSelected ? .boldSystemFont(ofSize: 18) : .boldSystemFont(ofSize: 15))
        cell.selectionStyle = .none
        
        return cell
    }
}
