//
//  SingleAccountOTPViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/2/23.
//

import UIKit

class SingleAccountOTPViewController: BaseViewController {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultVerticalStackView(spacing: 16)
        stackView.alignment = .center
        
        return stackView
    }()
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "input_OTP_code".localize
        label.textAlignment = .center
        label.textColor = .subTitleColor
        label.font = .systemFont(ofSize: 18)
        
        return label
    }()
    let descriptionLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.textColor = .secondaryColor
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 0
        
        return label
    }()
    let otpView: OTPView = {
        let view = OTPView(otpType: .otp6)
        
        return view
    }()
    
    let timerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultVerticalStackView(spacing: 0)
        stackView.alignment = .center
        
        return stackView
    }()
    let resendButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setTitle(string: "resend".localize)
        button.setTitleColor(.secondaryColor, for: .normal)
        button.isHidden = true
        
        return button
    }()
    let timerHStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultHorizonStackView(spacing: 16)
//        stackView.alignment = .top
        
        return stackView
    }()
    let timerView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.subTitleColor, value: .secondaryColor)
        view.setTextAlign(.right, value: .left)
        view.setFont(.systemFont(ofSize: 13), value: .systemFont(ofSize: 15))
        
        return view
    }()
    let circularView: FillCircularProgressView = {
        let view = FillCircularProgressView()
        view.tintColor = .secondaryColor
        view.constrainWidth(constant: 20)
        return view
    }()
    let contactSupportView: LeadingLabelButtonView = {
        let view = LeadingLabelButtonView()
        view.setupDetail(label: "contact_us_now".localize, buttonText: Constants.careCenterContactNumber)
        return view
    }()
    
    let OTP_TIME = 120
    var isUpgradeAccount: Bool = false
    var singleAccountValidateParam = SingleAccountRegisterValidateParam()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startTimer()
        setupDetail()
        
        circularView.delegate = self
        
        otpView.setOnOTPCompleted { [weak self] otp in
            guard let self = self else { return }
            
            // TODO: - Call Validate OTP -> Navigate to SingleAccountCustomerNewPinViewController()
            let decodedOTP = EncodingManager.shared.decode(encoded: (self.singleAccountValidateParam.otpCode).orEmptyString).orEmptyString
            guard otp == decodedOTP else {
                self.otpView.resetOTP()
                return self.showErrorAlert("otp_mismatch".localize)
            }
            
            self.singleAccountValidateParam.otpCode = otp
            
            let vc = SingleAccountCustomerNewPinViewController()
            vc.singleAccountValidateParam = self.singleAccountValidateParam
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        contactSupportView.setButtonAction {
            UIApplication.shared.callNumber(phone: Constants.careCenterContactNumber.replacingOccurrences(of: " ", with: ""))
        }
        
        resendButton.setAction { [weak self] in
            guard let self = self else { return }
            
            let presenter = SingleAccountPresenter()
            presenter.validateSingleAccountRegister(param: self.singleAccountValidateParam) { [weak self] response, error in
                guard let self = self else { return }
                guard error == nil else { return self.handleError(error) }
                
                guard let response = response else { return }
                print("⏩ Temporary OTP: \(response.decodedOTP)")
                
                self.singleAccountValidateParam.sessionID = response.sessionID
                self.singleAccountValidateParam.otpCode = response.confirmCode
                
                self.otpView.resetOTP()
                self.startTimer()
                self.showTimer(bool: true)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = isUpgradeAccount ? "title_upgrade_wing_account".localize : "title_register_wing_account".localize
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(stackView)
        stackView.anchor(top: view.layoutMarginsGuide.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 64, left: 16, bottom: 0, right: 32))
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(otpView)
        stackView.addArrangedSubview(timerStackView)
        stackView.addArrangedSubview(contactSupportView)
        
        timerStackView.addArrangedSubview(timerHStackView)
        timerStackView.addArrangedSubview(resendButton)
        
        timerHStackView.addArrangedSubview(timerView)
        timerHStackView.addArrangedSubview(circularView)
    }
    
    func setupDetail() {
        descriptionLabel.text = String(format: "otp_description_text".localize, singleAccountValidateParam.phoneNumber ?? "### ######")
    }
    
    private func startTimer() {
        timerView.setupDetails("resend_code_at".localize, value: TimerManager.shared.secondToTimeString(second: OTP_TIME, showMinute: true))
        
        circularView.setProgress(duration: CGFloat(OTP_TIME), withAnimation: true)
        TimerManager.shared.startCountDownTimer(second: OTP_TIME, showMinute: true) { [weak self] timeString, completed in
            guard let self = self else { return }
            
            self.timerView.setupDetails("resend_code_at".localize, value: timeString)
            
            if completed {
                self.otpView.enableInput(false)
                self.showTimer(bool: false)
            }
        }
    }
    
    func showTimer(bool: Bool) {
        self.timerHStackView.isHidden = !bool
        self.resendButton.isHidden = bool
    }
}

extension SingleAccountOTPViewController: CircularProgressBarDelegate {
    func OTPExpired() {
        
    }
}

