//
//  EnableLocationServiceViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 12/1/23.
//

import UIKit

class EnableLocationServiceViewController: BaseViewController {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    let imageView: UIImageView = {
        let imageView = UIImageView(image: .localImage("ic_location_service"))
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        imageView.constrainWidth(constant: 200)
        imageView.constrainHeight(constant: 200)
        
        return imageView
    }()
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "enable_location_title".localize
        label.textAlignment = .center
        label.textColor = .subTitleColor
        label.font = .boldSystemFont(ofSize: 18)
        
        return label
    }()
    let subTitleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "enable_location_sub_title".localize
        label.textAlignment = .center
        label.textColor = .subTitleColor
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 2
        
        return label
    }()
    let enableButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "enable_location".localize)
        button.backgroundColor = .secondaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enableButton.setAction { [weak self] in
            guard let self = self else { return }
            
            LocationManager.shared.requestLocationAuthorization { [weak self] status in
                guard let self = self else { return }
                
                switch status {
                case .denied, .restricted:
                    UIApplication.shared.openAppSettings { [weak self] in
                        guard let self = self else { return }
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                default:
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(enableButton)
        enableButton.anchor(top: nil,
                            leading: view.leadingAnchor,
                            bottom: view.layoutMarginsGuide.bottomAnchor,
                            trailing: view.trailingAnchor,
                            padding: .init(top: 0, left: 16, bottom: 16, right: 16))
        
        view.addSubview(stackView)
        stackView.centerInSuperview()
        stackView.anchor(top: nil,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: nil,
                         padding: .init(top: 0, left: 32, bottom: 0, right: 32))
     
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subTitleLabel)
        
        stackView.setCustomSpacing(32, after: imageView)
        
        enableButton.setRoundedView()
    }
}
