//
//  FullScreenMapViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/1/23.
//

import UIKit
@_implementationOnly import GoogleMaps

class FullScreenMapViewController: BaseViewController {
    
    let mapView = GoogleMapView()
    let markerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_map_marker")
        imageView.contentMode = .scaleAspectFit
        imageView.constrainWidth(constant: UIConstants.markerSize)
        imageView.constrainHeight(constant: UIConstants.markerSize)
        imageView.backgroundColor = .clear
        
        return imageView
    }()
//    let currentLocationButton: BaseButton = {
//        let button = BaseButton()
//        button.backgroundColor = .primaryColor
//        button.setImage(.localImage("ic_current_location").withRenderingMode(.alwaysTemplate), for: .normal)
//        button.imageView?.tintColor = .white
//        button.addImagePadding(UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16))
//        button.constrainWidth(constant: 70)
//        button.constrainHeight(constant: 70)
//
//        return button
//    }()
    let addLocationButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "add_location".localize)
        button.backgroundColor = .secondaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    private var marker: GMSMarker? {
        return mapView.marker
    }
    private var onAddLocationActionHandler: ((CLLocationCoordinate2D?) -> Void)?
    var currentLocation: CLLocationCoordinate2D? = LocationManager.shared.currentLocation
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "shop_location".localize
        
        mapView.showMyLocationMarker = true
        mapView.animateTo(currentLocation)
        
        addLocationButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.addLocation()
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(mapView)
        mapView.anchor(top: view.topAnchor,
                       leading: view.leadingAnchor,
                       bottom: view.bottomAnchor,
                       trailing: view.trailingAnchor)
        
        view.addSubview(markerImageView)
        markerImageView.centerInSuperview(x: 0, y: -UIConstants.markerSize / 2)
        
        view.addSubview(addLocationButton)
        addLocationButton.anchor(top: nil,
                                 leading: view.leadingAnchor,
                                 bottom: view.layoutMarginsGuide.bottomAnchor,
                                 trailing: view.trailingAnchor,
                                 padding: .init(top: 0, left: 16, bottom: 8, right: 16))
        
        addLocationButton.setRoundedView()
    }
    
    func setOnAddLocationAction(handler: @escaping (CLLocationCoordinate2D?) -> Void) {
        self.onAddLocationActionHandler = handler
    }
    
    @objc private func addLocation() {
        let lat = mapView.mapView.projection.coordinate(for: mapView.center).latitude
        let lng = mapView.mapView.projection.coordinate(for: mapView.center).longitude
        let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        
        print(location)
        
        onAddLocationActionHandler?(location)
        navigationController?.popViewController(animated: true)
    }
}
