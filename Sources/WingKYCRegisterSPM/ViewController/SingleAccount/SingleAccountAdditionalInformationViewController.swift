//
//  SingleAccountAdditionalInformationViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/1/23.
//

import UIKit

class SingleAccountAdditionalInformationViewController: CustomerAdditionalInformationViewController {
    
    let shopInformationView = ShopInformationView()
    let paymentMethodView = PaymentMethodView()
    let ecomerceOtherRequestView = EcomerceOtherRequestView()
    
    // MARK: - TOBE Remove
    let businessHour = BusinessHourView()
    let optionView = OptionRadioButtonListView()
    let fileContainer = TitleComponentWrapperView(top: 4, left: 8, bottom: 8, right: 8)
    let fileView = FileSelectionView()
    
    private var isMapSelected: Bool = false
    private var isPersonalAccount: Bool = false
    var singleAccountValidateParam = SingleAccountRegisterValidateParam()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupData()
        
        accountInformationView.setonAccountCreationPurposeChanged { [weak self] in
            guard let self = self else { return }
            
            let hidden = self.accountInformationView.accountCreationSelectedIndex == 0
            self.isPersonalAccount = hidden
            
            self.shopInformationView.isHidden = hidden
            self.paymentMethodView.isHidden = hidden
            self.ecomerceOtherRequestView.isHidden = hidden
        }
        accountInformationView.accountCreationPurposeView.setValueWhere(index: 0)
        
        shopInformationView.selectMapButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.openMapFullScreenViewController()
        }
        
        shopInformationView.mapView.setTapAction { [weak self] in
            guard let self = self else { return }
            
            self.openMapFullScreenViewController()
        }
        
        continueButton.setAction { [weak self] in
            guard let self = self else { return }
            
            if self.validateRegisterParam() {
                if !self.agreementView.isAgreed {
                    self.showErrorAlert("error_please_agree_the_valid_information".localize)
                    return
                }
                
                if self.isPersonalAccount {
                    let vc = CustomerOverrallInformationViewController()
                    vc.isNID = self.isNID
                    vc.isUpgradeAccount = self.isUpgradeAccount
                    vc.registerUserValidateParam = self.registerUserValidateParam
                    vc.upgradeUserValidateParam = self.upgradeUserValidateParam
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }else {
                    let vc = SingleAccountCustomerOverallInformationViewController()
                    vc.isNID = self.isNID
                    vc.isUpgradeAccount = self.isUpgradeAccount
                    vc.registerUserValidateParam = self.registerUserValidateParam
                    vc.upgradeUserValidateParam = self.upgradeUserValidateParam
                    vc.singleAccountValidateParam = self.singleAccountValidateParam
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
            }else {
                self.showErrorAlert()
            }
        }
    }
    
    override func autoFillMockupData() {
        super.autoFillMockupData()
        
        shopInformationView.autoFillMockupData()
        paymentMethodView.autoFillMockupData()
        ecomerceOtherRequestView.autoFillMockupData()
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.backgroundColor = .backgroundColor
        
        if let index = vStackView.arrangedSubviews.firstIndex(of: otherRequestInformationView) {
            vStackView.insertArrangedSubview(shopInformationView, at: index + 1)
            vStackView.insertArrangedSubview(paymentMethodView, at: index + 2)
            //            vStackView.insertArrangedSubview(ecomerceOtherRequestView, at: index + 3)
            //            vStackView.insertArrangedSubview(businessHour, at: index + 4)
            //            vStackView.insertArrangedSubview(optionView, at: index + 5)
            //            vStackView.insertArrangedSubview(fileContainer, at: index + 6)
        }
        
        shopInformationView.isHidden = true
        paymentMethodView.isHidden = true
        ecomerceOtherRequestView.isHidden = true
        
        fileContainer.addCustomView(view: fileView)
        fileContainer.customStackView { stackView in
            stackView.alignment = .leading
        }
        fileContainer.setPlaceholder("វិញ្ញាបនបត្រចុះបញ្ជីអាជីវកម្ម")
    }
    
    override func validateRegisterParam() -> Bool {
        if isPersonalAccount {
            return super.validateRegisterParam()
        }else {
            shopInformationView.observeErrorField()
            paymentMethodView.observeErrorField()
            
            guard super.validateRegisterParam() else { return false }
            guard shopInformationView.validateField() else { return false }
            guard let paymentMethod = paymentMethodView.getPaymentMethod() else { return false }
            
            singleAccountValidateParam.appendRegisterValidateParam(param: registerUserValidateParam)
            singleAccountValidateParam.tillNumber = shopInformationView.tillNumber
            singleAccountValidateParam.shopName = shopInformationView.shopName
            singleAccountValidateParam.businessType = shopInformationView.shopCategroy
            singleAccountValidateParam.shopAddress = shopInformationView.shopAddress
            singleAccountValidateParam.latitude = shopInformationView.shopCoordination?.latitude ?? 0.0
            singleAccountValidateParam.longitude = shopInformationView.shopCoordination?.longitude ?? 0.0
            singleAccountValidateParam.patantImage = shopInformationView.patantImage
            singleAccountValidateParam.ownershipImage = shopInformationView.ownershipImage
            singleAccountValidateParam.ownerIDFrontImage = shopInformationView.ownerIDFrontImage
            singleAccountValidateParam.ownerIDBackImage = shopInformationView.ownerIDBackImage
            singleAccountValidateParam.shopImage = shopInformationView.shopImage
            singleAccountValidateParam.merchantName = shopInformationView.shopName
            singleAccountValidateParam.website = shopInformationView.website
            singleAccountValidateParam.companyHistory = shopInformationView.shopHistory
            singleAccountValidateParam.majorProductService = shopInformationView.shopService
            singleAccountValidateParam.loyaltyStatus = (paymentMethod.wingPoint == nil) ? false : true
            
            
            let addressDetail = AddressDetail()
            addressDetail.addressLine1 = shopInformationView.addressView.houseNumber
            addressDetail.addressLine2 = shopInformationView.addressView.streetNumber
            addressDetail.provinceID = shopInformationView.shopAddress?.provinceID
            addressDetail.districtID = shopInformationView.shopAddress?.districtID
            addressDetail.communeID = shopInformationView.shopAddress?.communeID
            addressDetail.villageID = shopInformationView.shopAddress?.villageID
            addressDetail.latitude = shopInformationView.shopCoordination?.latitude ?? 0.0
            addressDetail.longitude = shopInformationView.shopCoordination?.longitude ?? 0.0
            
            singleAccountValidateParam.companyDetail = MerchantDetail(addressDetail: addressDetail)
            singleAccountValidateParam.paymentMethod = paymentMethod
            
            return true
        }
    }
    
    private func setupData() {
        let datasource: [KeyValueModel] = [
            .init(key: "brown", value: "Brown", isSelected: true),
            .init(key: "brown", value: "Sliver"),
            .init(key: "brown", value: "Gold"),
            .init(key: "brown", value: "Platinum"),
            .init(key: "brown", value: "Diamond"),
        ]
        optionView.setupDetail(placeholder: "គម្រោង", datasource: datasource)
    }
    
    private func openMapFullScreenViewController() {
        switch LocationManager.shared.currentStatus {
        case .authorizedWhenInUse, .authorizedAlways:
            gotoFullScreenVC()
        default:
            LocationManager.shared.requestLocationAuthorization { [weak self] status in
                guard let self = self else { return }
                
                if status == .authorizedAlways || status == .authorizedWhenInUse {
                    self.gotoFullScreenVC()
                }else {
                    let vc = EnableLocationServiceViewController()
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    private func gotoFullScreenVC() {
        let vc = FullScreenMapViewController()
        vc.currentLocation = self.shopInformationView.mapView.marker?.position ?? LocationManager.shared.currentLocation
        vc.setOnAddLocationAction { [weak self] location in
            guard let self = self else { return }
            
            self.isMapSelected = true
            self.shopInformationView.mapView.isHidden = false
            self.shopInformationView.shopCoordination = location
            self.shopInformationView.selectMapButton.isHidden = true
            self.shopInformationView.mapView.addMarker(at: location)
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
