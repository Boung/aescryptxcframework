//
//  GoogleMapView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/1/23.
//

import UIKit
@_implementationOnly import GoogleMaps

class GoogleMapView: UIView {
    
    var mapView: GMSMapView = {
        let mapView = GMSMapView()
        
        return mapView
    }()
    let markerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_map_marker")
        imageView.contentMode = .scaleAspectFit
        imageView.constrainWidth(constant: UIConstants.markerSize)
        imageView.constrainHeight(constant: UIConstants.markerSize)
        imageView.backgroundColor = .clear
        imageView.isHidden = true
        
        return imageView
    }()
    
    private let locationNotAvailableLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.textColor = .white
        label.font = .systemFont(ofSize: 13)
        label.numberOfLines = 2
        
        return label
    }()
    
    private var onTapActionHandler: (() -> Void)? {
        didSet {
            isUserInteractionEnabled = true
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
            mapView.isUserInteractionEnabled = false
            
            addGestureRecognizer(tapGesture)
        }
    }
    
    var showMyLocationMarker: Bool = false {
        didSet {
            mapView.isMyLocationEnabled = showMyLocationMarker
        }
    }
    var centerMapCoordiation: CLLocationCoordinate2D {
        let lat = mapView.projection.coordinate(for: mapView.center).latitude
        let lng = mapView.projection.coordinate(for: mapView.center).longitude
        
        return CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
    var marker: GMSMarker? {
        didSet {
            markerImageView.isHidden = marker == nil
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(mapView)
        mapView.anchor(top: topAnchor,
                       leading: leadingAnchor,
                       bottom: bottomAnchor,
                       trailing: trailingAnchor)
        
        addSubview(markerImageView)
        markerImageView.centerInSuperview(x: 0, y: -(UIConstants.markerSize / 2))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addMarker(at coordination: CLLocationCoordinate2D?) {
        guard let coordination = coordination else { return }
        
        mapView.clear()
        
        marker = GMSMarker()
        marker?.icon = .localImage("")
        marker?.position = coordination
        marker?.map = mapView
        
        let camera = GMSCameraPosition(latitude: coordination.latitude, longitude: coordination.longitude, zoom: 15)
        mapView.animate(to: camera)
    }
    
    func animateTo(_ coordination: CLLocationCoordinate2D?) {
        guard let coordination = coordination else { return }
        
        let camera = GMSCameraPosition(latitude: coordination.latitude, longitude: coordination.longitude, zoom: 15)
        mapView.animate(to: camera)
    }
    
    func addMarkerOnCurrentLocation() {
        addMarker(at: LocationManager.shared.locationManager.location?.coordinate)
    }
    
    func setTapAction(handler: @escaping () -> Void) {
        self.onTapActionHandler = handler
    }
    
    @objc private func onTap() {
        onTapActionHandler?()
    }
}
