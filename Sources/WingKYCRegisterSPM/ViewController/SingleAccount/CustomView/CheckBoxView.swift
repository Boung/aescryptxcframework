//
//  CheckBoxView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/1/23.
//

import UIKit

class CheckBoxView: UIView {
    
    let checkBoxButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setImage(.localImage("ic_deselected"), for: .normal)
        button.constrainWidth(constant: 25)
        button.constrainHeight(constant: 25)
        return button
    }()
    
    var isSelected: Bool = false {
        didSet {
            let selected: UIImage = .localImage("ic_select")
            let deSelected: UIImage = .localImage("ic_deselected")
            
            checkBoxButton.setImage(isSelected ? selected : deSelected, for: .normal)
        }
    }
    
    init(size: CGFloat = 25) {
        super.init(frame: .zero)
        
        addSubview(checkBoxButton)
        checkBoxButton.fillInSuperView()
        
        checkBoxButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.isSelected.toggle()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
