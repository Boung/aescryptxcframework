//
//  AutorizedPersonImagePreviewView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/2/23.
//

import UIKit

class AutorizedPersonImagePreviewView: UIView {
    
    let containerView = ContainerView(background: .backgroundColor)
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    let authorizedPersonView = LeadingLabelButtonView(buttonWidth: 100)
    let authorizedPersonIDFrontView = LeadingLabelButtonView(buttonWidth: 100)
    let authorizedPersonIDBackView = LeadingLabelButtonView(buttonWidth: 100)
    
    var viewController: UIViewController?
    
    init(viewController: UIViewController?) {
        super.init(frame: .zero)
        
        self.viewController = viewController
        
        addSubview(containerView)
        containerView.fillInSuperView()
        
        containerView.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 8, left: 8, bottom: 8, right: 8))
        
        stackView.addArrangedSubview(authorizedPersonView)
        stackView.addArrangedSubview(authorizedPersonIDFrontView)
        stackView.addArrangedSubview(authorizedPersonIDBackView)
        
        containerView.setCornerRadius(radius: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
    
    func setupDetail(model: SingleAccountRegisterValidateParam?) {
        authorizedPersonView.setupDetail(label: "ownership_image".localize, buttonText: "view".localize)
        authorizedPersonIDFrontView.setupDetail(label: "owner_id_front_image".localize, buttonText: "view".localize)
        authorizedPersonIDBackView.setupDetail(label: "owner_id_back_image".localize, buttonText: "view".localize)
        
        authorizedPersonView.setButtonAction { [weak self] in
            guard let self = self else { return }
            
            guard let image = model?.ownershipImage else {
                return AlertManager.shared.showMessageAlert("no_images".localize)
            }
            self.viewFullscreenImage(image: image)
        }
        authorizedPersonIDFrontView.setButtonAction { [weak self] in
            guard let self = self else { return }
            
            guard let image = model?.ownerIDFrontImage else {
                return AlertManager.shared.showMessageAlert("no_images".localize)
            }
            self.viewFullscreenImage(image: image)
        }
        authorizedPersonIDBackView.setButtonAction { [weak self] in
            guard let self = self else { return }
            
            guard let image = model?.ownerIDBackImage else {
                return AlertManager.shared.showMessageAlert("no_images".localize)
            }
            self.viewFullscreenImage(image: image)
        }
    }
    
    private func viewFullscreenImage(image: UIImage?) {
        let vc = FullScreenImagePreviewViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.image = image
        
        self.viewController?.present(vc, animated: true)
    }
}
