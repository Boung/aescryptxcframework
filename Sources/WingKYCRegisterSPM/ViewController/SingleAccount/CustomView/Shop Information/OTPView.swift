//
//  OTPView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/2/23.
//

import UIKit

enum OTPTypeEnum {
    case otp4
    case otp6
    case otpCustom(digit: Int)
    
    var inputCount: Int {
        switch self {
        case .otp4:
            return 4
        case .otp6:
            return 6
        case .otpCustom(let digit):
            return digit
        }
    }
}

enum OTPMaskType {
    case dot
    case star
    case x
    case custom(string: Character)
    
    var mask: Character {
        switch self {
        case .dot:
            return "•"
        case .star:
            return "✶"
        case .x:
            return "x"
        case .custom(let mask):
            return mask
        }
    }
}

class OTPView: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultHorizonStackView(spacing: 8)
        stackView.distribution = .fill
        
        return stackView
    }()
    var otpViews: [LabelContainerView] = []
    var hiddenTextField: BaseTextField = {
        let textField = BaseTextField()
        textField.textField.autocorrectionType = .no
        textField.textField.keyboardType = .numberPad
        textField.isHidden = true
        textField.maxInputLength = 6
        
        return textField
    }()
    
    private let filledColor: UIColor = .secondaryColor
    private let unFilledColor: UIColor = .secondaryColor.withAlphaComponent(0.6)
    private var onOTPCompleted: ((String) -> Void)?
    private var otpArray: [String] = [] {
        didSet {
            otpViews.forEach { $0.setText(inputMask?.mask ?? "0") }
            
            observeTextFieldColor()
            otpArray.enumerated().forEach { [weak self] index, element in
                guard let self = self else { return }
                
                self.otpViews[index].setText(element)
            }
        }
    }
    
    var enableTapEdit: Bool = true
    var otpType: OTPTypeEnum = .otp6
    var inputMask: OTPMaskType? = nil
    
    init(otpType: OTPTypeEnum, otpMask: OTPMaskType? = nil) {
        super.init(frame: .zero)
        
        self.otpType = otpType
        self.inputMask = otpMask
        
        setupOTPViews()
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        addSubview(hiddenTextField)
        hiddenTextField.fillInSuperView()
        
        enableInput(true)
        
        hiddenTextField.onValueChanged { [weak self] text in
            guard let self = self else { return }
            
            self.otpArray = Array(text).map { "\(self.inputMask?.mask ?? $0)" }
            
            if text.count == self.otpType.inputCount {
                self.hiddenTextField.textField.resignFirstResponder()
                self.onOTPCompleted?(text)
            }
        }
        
        isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onOTPViewTap))
        addGestureRecognizer(tapGesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        otpViews.forEach {
            $0.constrainHeight(constant: 50)
            $0.constraintWidthGreater(constant: 35)
            $0.setBorder(width: 1, color: .secondaryColor)
            $0.setCornerRadius(radius: 8)
            $0.layoutSubviews()
        }
    }
    
    func setOnOTPCompleted(handler: @escaping (String) -> Void) {
        self.onOTPCompleted = handler
    }
    
    private func setupOTPViews() {
        for _ in 0..<otpType.inputCount {
            let view = LabelContainerView(padding: .init(top: 16, left: 8, bottom: 16, right: 8))
            view.backgroundColor = unFilledColor
            view.setText(inputMask?.mask ?? "0")
            view.configLabel { label in
                label.textColor = .white
                label.textAlignment = .center
                label.font = .boldSystemFont(ofSize: 20)
            }
            
            otpViews.append(view)
        }
        
        otpViews.forEach {
            stackView.addArrangedSubview($0)
        }
    }
    
    private func observeTextFieldColor() {
        guard otpViews.count == otpType.inputCount else { return }
        
        let count = otpArray.count
        
        for index in 0..<otpType.inputCount {
            otpViews[index].backgroundColor = count > index ? filledColor : unFilledColor
        }
//        otpViews[0].backgroundColor = count > 0 ? filledColor : unFilledColor
//        otpViews[1].backgroundColor = count > 1 ? filledColor : unFilledColor
//        otpViews[2].backgroundColor = count > 2 ? filledColor : unFilledColor
//        otpViews[3].backgroundColor = count > 3 ? filledColor : unFilledColor
//        otpViews[4].backgroundColor = count > 4 ? filledColor : unFilledColor
//        otpViews[5].backgroundColor = count > 5 ? filledColor : unFilledColor
    }
    
    func enableInput(_ bool: Bool) {
        self.enableTapEdit = bool
        if bool {
            hiddenTextField.textField.becomeFirstResponder()
        }else {
            hiddenTextField.textField.resignFirstResponder()
        }
    }
    
    func resetOTP() {
        otpViews.forEach { $0.setText(inputMask?.mask ?? "0") }
        self.hiddenTextField.textField.text = nil
        self.otpArray = []
        
        enableInput(true)
    }
    
    @objc private func onOTPViewTap() {
        if enableTapEdit {
            hiddenTextField.textField.becomeFirstResponder()
        }
    }
}
