//
//  ShopInformationInformationView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/2/23.
//

import UIKit

class ShopInformationInformationView: UIView {
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("shop_information".localize)
        
        return view
    }()
    private let tillNumberView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setTextAlign(.left, value: .right)
        view.setNumberOfLine(1, value: 2)
        view.keyLabel.constraintWidthGreater(constant: 50)
        
        return view
    }()
    private let shopNameView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setTextAlign(.left, value: .right)
        view.setNumberOfLine(1, value: 2)
        view.keyLabel.constraintWidthGreater(constant: 50)
        
        return view
    }()
    private let businessTypeView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setTextAlign(.left, value: .right)
        view.setNumberOfLine(1, value: 2)
        view.keyLabel.constraintWidthGreater(constant: 50)
        
        return view
    }()
    private let shopAddressView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setTextAlign(.left, value: .right)
        view.setNumberOfLine(1, value: 0)
        view.keyLabel.constraintWidthGreater(constant: 50)
        
        return view
    }()
    
    private let mapViewContainer = TitleComponentWrapperView(top: 2)
    private let mapView = GoogleMapView()
    
    private let patantImageView = LeadingLabelButtonView(buttonWidth: 100)
    private let authorizedImageView: AutorizedPersonImagePreviewView?
    private let shopImageView = LeadingLabelButtonView(buttonWidth: 100)
    
    private let websiteView = LeadingLabelButtonView()
    private let shopProfileView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setTextAlign(.left, value: .right)
        view.setNumberOfLine(1, value: 2)
        view.keyLabel.constraintWidthGreater(constant: 50)
        
        return view
    }()
    private let shopMajorityView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setTextAlign(.left, value: .right)
        view.setNumberOfLine(1, value: 2)
        view.keyLabel.constraintWidthGreater(constant: 50)
        
        return view
    }()
    
    private var viewController: UIViewController?
    private var websiteURL: URL?
    
    init(viewController: UIViewController) {
        self.authorizedImageView = AutorizedPersonImagePreviewView(viewController: viewController)
        
        super.init(frame: .zero)
        
        self.viewController = viewController
     
        addSubview(wrapper)
        wrapper.fillInSuperView()
        
        wrapper.addCustomView(view: vStackView)
        
        vStackView.addArrangedSubview(shopNameView)
        vStackView.addArrangedSubview(businessTypeView)
        vStackView.addArrangedSubview(shopAddressView)
        vStackView.addArrangedSubview(mapViewContainer)
        vStackView.addArrangedSubview(patantImageView)
        vStackView.addArrangedSubview(authorizedImageView!)
        vStackView.addArrangedSubview(shopImageView)
        vStackView.addArrangedSubview(websiteView)
        vStackView.addArrangedSubview(shopProfileView)
        vStackView.addArrangedSubview(shopMajorityView)
        
        mapViewContainer.setPlaceholder("shop_location".localize)
        mapViewContainer.addCustomView(view: mapView)
        mapView.isUserInteractionEnabled = false
        mapView.constrainHeight(constant: 150)
        mapView.setCornerRadius(radius: 8)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetails(model: SingleAccountRegisterValidateParam?) {
        guard let model = model else { return }
        tillNumberView.setupDetails("till_number".localize, value: model.tillNumber.orEmptyString)
        shopNameView.setupDetails("shop_name".localize, value: model.shopName.orEmptyString)
        businessTypeView.setupDetails("shop_category".localize, value: (model.businessType?.value).orEmptyString)
        shopAddressView.setupDetails("address".localize, value: model.toDisplayAddressString())
        
        let lat = model.latitude ?? 0.0
        let lng = model.longitude ?? 0.0
        mapView.addMarker(at: .init(latitude: lat, longitude: lng))
        
        patantImageView.setupDetail(label: "patant_image".localize, buttonText: "view".localize)
        shopImageView.setupDetail(label: "shop_image".localize, buttonText: "view".localize)
        
        patantImageView.setButtonAction { [weak self] in
            guard let self = self else { return }
            
            guard let image = model.patantImage else {
                return AlertManager.shared.showMessageAlert("no_images".localize)
            }
            
            self.viewFullscreenImage(image: image)
        }
        authorizedImageView?.setupDetail(model: model)
        shopImageView.setButtonAction { [weak self] in
            guard let self = self else { return }
            
            guard let image = model.shopImage else {
                return AlertManager.shared.showMessageAlert("no_images".localize)
            }
            
            self.viewFullscreenImage(image: image)
        }
        
        if let urlString = model.website, urlString.isNotEmpty {
            let attribute: [NSAttributedString.Key : Any] = [
                .underlineStyle: NSUnderlineStyle.thick.rawValue,
                .underlineColor: UIColor.secondaryColor,
                .foregroundColor: UIColor.secondaryColor,
            ]
            let attributedWebsite = NSMutableAttributedString(string: urlString, attributes: attribute)
            
            websiteView.setupDetail(label: "shop_website".localize, buttonAttributeString: attributedWebsite)
            websiteView.setButtonAction { [weak self] in
                guard let self = self else { return }
                let webProtocol = urlString.contains("https://") ? "" : "https://"
                
                let vc = BaseWebViewController()
                vc.titleString = "shop_website".localize
                vc.urlString = webProtocol + urlString
                
                self.viewController?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        shopProfileView.setupDetails("shop_history".localize, value: (model.companyHistory).orEmptyString)
        shopMajorityView.setupDetails("shop_service".localize, value: (model.majorProductService).orEmptyString)
        
        shopProfileView.setColor(.subTitleColor, value: .subTitleColor)
        shopMajorityView.setColor(.subTitleColor, value: .subTitleColor)
    }
    
    private func viewFullscreenImage(image: UIImage?) {
        let vc = FullScreenImagePreviewViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.image = image
        
        self.viewController?.present(vc, animated: true)
    }
}
