//
//  AuthorizedPersonImageSelectionView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/1/23.
//

import UIKit

class AuthorizedPersonImageSelectionView: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let ownershipLetterView: TitleImageSelectionView = {
        let view = TitleImageSelectionView()
        view.setupDetail(title: "ownership_image".localize)
        
        return view
    }()
    let ownerIDFrontView: TitleImageSelectionView = {
        let view = TitleImageSelectionView()
        view.setupDetail(title: "owner_id_front_image".localize)
        
        return view
    }()
    let ownerIDBackView: TitleImageSelectionView = {
        let view = TitleImageSelectionView()
        view.setupDetail(title: "owner_id_back_image".localize)
        
        return view
    }()
    
    var ownershipImage: UIImage? {
        return ownershipLetterView.image
    }
    var ownerIDFrontImage: UIImage? {
        return ownerIDFrontView.image
    }
    var ownerIDBackImage: UIImage? {
        return ownerIDBackView.image
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .addressBackgroundColor
        
        addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 8, left: 8, bottom: 8, right: 8))
        
        stackView.addArrangedSubview(ownershipLetterView)
        stackView.addArrangedSubview(ownerIDFrontView)
        stackView.addArrangedSubview(ownerIDBackView)
        
        [ownershipLetterView, ownerIDFrontView, ownerIDBackView].forEach {
            $0.imageSelectionView.containerView.backgroundColor = .white
        }
        
        setCornerRadius(radius: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func autoFillMockupData() {
        ownershipLetterView.autoFillMockupData()
        ownerIDFrontView.autoFillMockupData()
        ownerIDBackView.autoFillMockupData()
    }
}
