//
//  ShopInformationView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/1/23.
//

import UIKit
@_implementationOnly import GoogleMaps

class ShopInformationView: UIView {
    
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("shop_information".localize)
        
        return view
    }()
    
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    
    let tillNumberTextField = BaseTextField()
    let shopNameTextField = BaseTextField()
    let shopCategoryTextField = BaseDropdownTextField()
    let addressView = CustomerAddressEditView()
    
    let mapContainer = TitleComponentWrapperView(top: 0)
    let mapStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 0
        
        return stackView
    }()
    let mapView = GoogleMapView()
    let selectMapButton: LeadingIconButton = {
        let button = LeadingIconButton(iconSize: .init(width: 30, height: 30))
        button.setupDetails(.localImage("ic_fullscreen").withRenderingMode(.alwaysTemplate), title: "select_location_on_map".localize)
        button.setIconColor(.secondaryColor)
        button.setTextColor(.secondaryColor)
        button.backgroundColor = .clear
        
        return button
    }()
    
    let patantContainerView = TitleComponentWrapperView(top: 8, left: 8, bottom: 8, right: 8)
    let patantImageView = ImageSelectionView()
    
    let authorizedPersonContainerView = TitleComponentWrapperView(top: 8)
    let authorizedPersonImageView = AuthorizedPersonImageSelectionView()
    
    let shopImageContainerView = TitleComponentWrapperView(top: 8, left: 8, bottom: 8, right: 8)
    let shopImageView = ImageSelectionView()
    
    let websiteTextField = BaseTextField()
    let shopHistoryTextField = BaseTextField()
    let serviceTextField = BaseTextField()
    
    var tillNumber: String {
        return tillNumberTextField.textField.text ?? ""
    }
    var shopName: String {
        return shopNameTextField.textField.text ?? ""
    }
    var shopCategroy: KeyValueModel? {
        return shopCategoryTextField.selectedDatasource
    }
    var shopAddress: Address? {
        return addressView.getAddress()
    }
    var shopCoordination: CLLocationCoordinate2D?
    var patantImage: UIImage? {
        return patantImageView.image
    }
    var ownershipImage: UIImage? {
        return authorizedPersonImageView.ownershipLetterView.image
    }
    var ownerIDFrontImage: UIImage? {
        return authorizedPersonImageView.ownerIDFrontView.image
    }
    var ownerIDBackImage: UIImage? {
        return authorizedPersonImageView.ownerIDBackView.image
    }
    var shopImage: UIImage? {
        return shopImageView.image
    }
    var website: String {
        return websiteTextField.textField.text ?? ""
    }
    var shopHistory: String {
        return shopHistoryTextField.textField.text ?? ""
    }
    var shopService: String {
        return serviceTextField.textField.text ?? ""
    }
    
    func validateField() -> Bool {
        observeErrorField()
        
        guard
            tillNumber.isNotEmpty,
            shopName.isNotEmpty,
            shopCategroy != nil,
            shopAddress != nil,
            shopCoordination != nil,
            shopImage != nil
        else { return false }
        
        return true
    }
    
    func observeErrorField() {
        tillNumberTextField.setErrorTextField(tillNumber.isEmpty)
        shopNameTextField.setErrorTextField(shopName.isEmpty)
        shopCategoryTextField.setErrorTextField(shopCategroy == nil)
        mapContainer.toggleError(shopCoordination == nil)
        addressView.observeErrorField()
        shopImageView.toggleError(shopImage == nil)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTextField()
        
        addSubview(wrapper)
        wrapper.fillInSuperView()
        
        wrapper.addCustomView(view: vStackView)
        
        vStackView.addArrangedSubview(tillNumberTextField)
        vStackView.addArrangedSubview(shopNameTextField)
        vStackView.addArrangedSubview(shopCategoryTextField)
        vStackView.addArrangedSubview(addressView)
        vStackView.addArrangedSubview(mapContainer)
        vStackView.addArrangedSubview(patantContainerView)
        vStackView.addArrangedSubview(authorizedPersonContainerView)
        vStackView.addArrangedSubview(shopImageContainerView)
        vStackView.addArrangedSubview(websiteTextField)
        vStackView.addArrangedSubview(shopHistoryTextField)
        vStackView.addArrangedSubview(serviceTextField)
        
        mapContainer.wrapper.backgroundColor = .backgroundColor
        mapContainer.addCustomView(view: mapStackView)
        mapContainer.setPlaceholder("shop_location".localize, isRequired: true)
        mapStackView.addArrangedSubview(mapView)
        mapStackView.addArrangedSubview(selectMapButton)
        
        selectMapButton.constrainHeight(constant: 150)
        selectMapButton.setCornerRadius(radius: 8)
        
        mapView.isHidden = true
        mapView.mapView.isUserInteractionEnabled = false
        mapView.constrainHeight(constant: 150)
        mapView.setCornerRadius(radius: 8)
        
        patantImageView.containerView.backgroundColor = .white
        patantContainerView.addCustomView(view: patantImageView)
        patantContainerView.customStackView { stackView in
            stackView.alignment = .leading
        }
        patantContainerView.wrapper.backgroundColor = .addressBackgroundColor
        
        shopImageView.containerView.backgroundColor = .white
        shopImageContainerView.addCustomView(view: shopImageView)
        shopImageContainerView.customStackView { stackView in
            stackView.alignment = .leading
        }
        shopImageContainerView.wrapper.backgroundColor = .addressBackgroundColor
        
        authorizedPersonContainerView.addCustomView(view: authorizedPersonImageView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupTextField() {
        addressView.showHouseAndStreet = true
        
        tillNumberTextField.textField.keyboardType = .numberPad
        tillNumberTextField.setPlaceholder("till_number".localize, isRequired: true)
        shopNameTextField.setPlaceholder("shop_name".localize, isRequired: true)
        shopCategoryTextField.setPlaceholder("shop_category".localize, isRequired: true)
        websiteTextField.setPlaceholder("shop_website".localize)
        shopHistoryTextField.setPlaceholder("shop_history".localize, isRequired: true)
        serviceTextField.setPlaceholder("shop_service".localize, isRequired: true)
        
        patantContainerView.setPlaceholder("patant_image".localize)
        authorizedPersonContainerView.setPlaceholder("authoried_person_image".localize)
        shopImageContainerView.setPlaceholder("shop_image".localize, isRequired: true)
        
        shopCategoryTextField.setDatasource(Constants.businessType.map { .init(key: $0.code ?? "", value: $0.categoryId ?? "") })
    }
    
    func autoFillMockupData() {
        tillNumberTextField.setText("123")
        shopNameTextField.setText("ABC")
        shopCategoryTextField.setValueWhere(index: 0)
        websiteTextField.setText("facebook.com")
        shopHistoryTextField.setText("ABC")
        serviceTextField.setText("ABC")
        
        patantImageView.autoFillMockupData()
        authorizedPersonImageView.autoFillMockupData()
        shopImageView.autoFillMockupData()
    }
}
