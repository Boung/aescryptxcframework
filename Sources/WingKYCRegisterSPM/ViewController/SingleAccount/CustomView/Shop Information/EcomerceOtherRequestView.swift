//
//  EcomerceOtherRequestView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/1/23.
//

import UIKit

class EcomerceOtherRequestView: UIView {
    
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("other_request".localize)
        
        return view
    }()
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 0
        
        return stackView
    }()
    let wingmallServiceView: SelectableDropDownTextField = {
        let view = SelectableDropDownTextField()
        view.setText("wing_mall_service".localize)
        view.setupForNonDropdown()
        
        return view
    }()
    let wingMarketServiceView: SelectableDropDownTextField = {
        let view = SelectableDropDownTextField()
        view.setText("wing_market_service".localize)
        view.setupForNonDropdown()
        
        return view
    }()
    let wingAgriServiceView: SelectableDropDownTextField = {
        let view = SelectableDropDownTextField()
        view.setText("wing_agri_service".localize)
        view.setupForNonDropdown()
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(wrapper)
        wrapper.fillInSuperView()
        
        wrapper.addCustomView(view: stackView)
        
        stackView.addArrangedSubview(wingmallServiceView)
        stackView.addArrangedSubview(wingMarketServiceView)
        stackView.addArrangedSubview(wingAgriServiceView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func autoFillMockupData() {
        wingmallServiceView.checkBoxButton.isSelected = true
    }
}
