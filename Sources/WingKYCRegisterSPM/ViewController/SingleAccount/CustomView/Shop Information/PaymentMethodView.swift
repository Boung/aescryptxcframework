//
//  PaymentMethodView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/1/23.
//

import UIKit

class PaymentMethodView: UIView {
    
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("payment_method".localize)
        
        return view
    }()
    private let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 0
        
        return stackView
    }()
    
    private let wingPayView = SelectableDropDownTextField()
    private let wingPointView = SelectableDropDownTextField()
    private let alipayView = SelectableDropDownTextField()
    private let weChatView = SelectableDropDownTextField()
    private let mpqrView = SelectableDropDownTextField()
    private let khqrView = SelectableDropDownTextField()
    
    var wingPay: KeyValueModel? {
        return wingPayView.selectedDatasource
    }
    var wingPoint: KeyValueModel? {
        return wingPointView.selectedDatasource
    }
    var alipay: KeyValueModel? {
        return alipayView.selectedDatasource
    }
    var weChat: KeyValueModel? {
        return weChatView.selectedDatasource
    }
    var mpqr: KeyValueModel? {
        return mpqrView.selectedDatasource
    }
    var khqr: KeyValueModel? {
        return khqrView.selectedDatasource
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTextField()
        
        addSubview(wrapper)
        wrapper.fillInSuperView()
        
        wrapper.addCustomView(view: vStackView)
        
        vStackView.addArrangedSubview(wingPayView)
        vStackView.addArrangedSubview(wingPointView)
        vStackView.addArrangedSubview(alipayView)
        vStackView.addArrangedSubview(weChatView)
        vStackView.addArrangedSubview(mpqrView)
        vStackView.addArrangedSubview(khqrView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupTextField() {
        wingPayView.setPlaceholderNoTitle("wing_pay".localize)
        wingPointView.setPlaceholderNoTitle("wing_ecommerce".localize)
        alipayView.setPlaceholderNoTitle("alipay".localize)
        weChatView.setPlaceholderNoTitle("weChat".localize)
        mpqrView.setPlaceholderNoTitle("mpqr".localize)
        khqrView.setPlaceholderNoTitle("khqr".localize)
        
        wingPayView.setText("wing_pay".localize)
        wingPointView.setText("wing_ecommerce".localize)
        alipayView.setText("alipay".localize)
        weChatView.setText("weChat".localize)
        mpqrView.setText("mpqr".localize)
        khqrView.setText("khqr".localize)
        
        wingPayView.setOnSelectedChanged { [weak self] bool in
            guard let self = self else { return }
            
            if bool {
                if self.wingPay == nil {
                    self.wingPayView.setValueWhere(index: 0)
                }
            }else {
                self.wingPayView.reset()
                self.wingPayView.setText("wing_pay".localize)
            }
        }
        wingPayView.setOnValueSelected { [weak  self] selected in
            guard let self = self else { return }
            
            self.wingPayView.setText("wing_pay".localize + " • \(selected.value)")
        }
        
        wingPointView.setOnSelectedChanged { [weak self] bool in
            guard let self = self else { return }
            
            if bool {
                if self.wingPoint == nil {
                    self.wingPointView.setValueWhere(index: 0)
                }
            }else {
                self.wingPointView.reset()
                self.wingPointView.setText("wing_ecommerce".localize)
            }
        }
        wingPointView.setOnValueSelected { [weak  self] selected in
            guard let self = self else { return }
            
            self.wingPointView.setText("wing_ecommerce".localize + " • \(selected.value)")
        }
        
        alipayView.setOnSelectedChanged { [weak self] bool in
            guard let self = self else { return }
            
            if bool {
                if self.alipay == nil {
                    self.alipayView.setValueWhere(index: 0)
                }
            } else {
                self.alipayView.reset()
                self.alipayView.setText("alipay".localize)
            }
        }
        alipayView.setOnValueSelected { [weak  self] selected in
            guard let self = self else { return }
            
            self.alipayView.setText("alipay".localize + " • \(selected.value)")
        }
        
        weChatView.setOnSelectedChanged { [weak self] bool in
            guard let self = self else { return }
            
            if bool {
                if self.weChat == nil {
                    self.weChatView.setValueWhere(index: 0)
                }
            }else {
                self.weChatView.reset()
                self.weChatView.setText("weChat".localize)
            }
        }
        weChatView.setOnValueSelected { [weak  self] selected in
            guard let self = self else { return }
            
            self.weChatView.setText("weChat".localize + " • \(selected.value)")
        }
        
        mpqrView.setOnSelectedChanged { [weak self] bool in
            guard let self = self else { return }
            
            if bool {
                if self.mpqr == nil {
                    self.mpqrView.setValueWhere(index: 0)
                }
            }else {
                self.mpqrView.reset()
                self.mpqrView.setText("mpqr".localize)
            }
        }
        mpqrView.setOnValueSelected { [weak  self] selected in
            guard let self = self else { return }
            
            self.mpqrView.setText("mpqr".localize + " • \(selected.value)")
        }
        
        khqrView.setOnSelectedChanged { [weak self] bool in
            guard let self = self else { return }
            
            if bool {
                if self.khqr == nil {
                    self.khqrView.setValueWhere(index: 0)
                }
            }else {
                self.khqrView.reset()
                self.khqrView.setText("khqr".localize)
            }
        }
        khqrView.setOnValueSelected { [weak  self] selected in
            guard let self = self else { return }
            
            self.khqrView.setText("khqr".localize + " • \(selected.value)")
        }
        
        wingPayView.setDatasource((Constants.paymentMethod.payWithWingPackage ?? []).map { .init(key: $0.packageID ?? "", value: $0.packageName ?? "")})
        wingPointView.setDatasource((Constants.paymentMethod.giiftPackage ?? []).map { .init(key: $0.packageID ?? "", value: $0.packageName ?? "")})
        alipayView.setDatasource((Constants.paymentMethod.alipayPackage ?? []).map { .init(key: $0.packageID ?? "", value: $0.packageName ?? "")})
        weChatView.setDatasource((Constants.paymentMethod.weChatPackage ?? []).map { .init(key: $0.packageID ?? "", value: $0.packageName ?? "")})
        mpqrView.setDatasource((Constants.paymentMethod.mpqrPackage ?? []).map { .init(key: $0.packageID ?? "", value: $0.packageName ?? "")})
        khqrView.setDatasource((Constants.paymentMethod.khqrPackage ?? []).map { .init(key: $0.packageID ?? "", value: $0.packageName ?? "")})
        
        wingPayView.checkBoxButton.isSelected = true
        wingPayView.setValueWhere(index: 0)
    }
    
    func autoFillMockupData() {
        wingPayView.checkBoxButton.isSelected = true
        wingPointView.checkBoxButton.isSelected = true
        alipayView.checkBoxButton.isSelected = true
        weChatView.checkBoxButton.isSelected = true
        mpqrView.checkBoxButton.isSelected = true
        khqrView.checkBoxButton.isSelected = true
        
        wingPayView.setValueWhere(index: 0)
        wingPointView.setValueWhere(index: 0)
        alipayView.setValueWhere(index: 0)
        weChatView.setValueWhere(index: 0)
        mpqrView.setValueWhere(index: 0)
        khqrView.setValueWhere(index: 0)
    }
    
    func getPaymentMethod() -> PaymentMethod? {
        let isWingPay = wingPayView.isSelected
        let isWingPoint = wingPointView.isSelected
        let isAlipay = alipayView.isSelected
        let isWeChat = weChatView.isSelected
        let isMPQR = mpqrView.isSelected
        let isKHQR = khqrView.isSelected
        
        observeErrorField()
        
        if !isWingPay, !isWingPoint, !isAlipay, !isWeChat, !isMPQR, !isKHQR {
            showErrorAlert("please_select_payment_menthod".localize)
            return nil
        }
        
        let param = PaymentMethod()
        param.payWithWing = isWingPay ? wingPayView.selectedDatasource : nil
        param.wingPoint = isWingPoint ? wingPointView.selectedDatasource : nil
        param.alipay = isAlipay ? alipayView.selectedDatasource : nil
        param.weChat = isWeChat ? weChatView.selectedDatasource : nil
        param.mpqr = isMPQR ? mpqrView.selectedDatasource : nil
        param.khqr = isKHQR ? khqrView.selectedDatasource : nil
        
        return param
    }
    
    func observeErrorField() {
        let isWingPay = wingPayView.isSelected
        let isWingPoint = wingPointView.isSelected
        let isAlipay = alipayView.isSelected
        let isWeChat = weChatView.isSelected
        let isMPQR = mpqrView.isSelected
        let isKHQR = khqrView.isSelected
        
        if !isWingPay, !isWingPoint, !isAlipay, !isWeChat, !isMPQR, !isKHQR {
            wrapper.toggleError(true)
        }else {
            wrapper.toggleError(false)
        }
    }
}
