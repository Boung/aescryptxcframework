//
//  PaymentMethodDisplayView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/2/23.
//

import UIKit

class PaymentMethodDisplayView: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultVerticalStackView(spacing: 16)
        
        return stackView
    }()
    let wrapper: GroupWrapperView = {
        let view = GroupWrapperView()
        view.setPlaceholder("payment_method".localize)
        
        return view
    }()
    var paymentMethodViews: [KeyValueLabelView] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(wrapper)
        wrapper.fillInSuperView()
        
        wrapper.addCustomView(view: stackView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetail(model: SingleAccountRegisterValidateParam?) {
        stackView.removeAllArrangedSubView()
        paymentMethodViews.removeAll()
        
        let paymentMethod = model?.paymentMethod
        
        if let value = paymentMethod?.payWithWing {
            paymentMethodViews.append(getKeyValueView(key: "wing_pay".localize, value: value))
        }
        
        if let value = paymentMethod?.wingPoint {
            paymentMethodViews.append(getKeyValueView(key: "wing_ecommerce".localize, value: value))
        }
        
        if let value = paymentMethod?.alipay {
            paymentMethodViews.append(getKeyValueView(key: "alipay".localize, value: value))
        }
        
        if let value = paymentMethod?.weChat {
            paymentMethodViews.append(getKeyValueView(key: "weChat".localize, value: value))
        }
        
        if let value = paymentMethod?.mpqr {
            paymentMethodViews.append(getKeyValueView(key: "mpqr".localize, value: value))
        }
        
        if let value = paymentMethod?.khqr {
            paymentMethodViews.append(getKeyValueView(key: "khqr".localize, value: value))
        }
        
        paymentMethodViews.forEach {
            stackView.addArrangedSubview($0)
        }
    }
    
    func getKeyValueView(key: String, value: KeyValueModel) -> KeyValueLabelView {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setupDetails(key, value: value.value)
        view.setTextAlign(.left, value: .right)
        view.setNumberOfLine(1, value: 2)
        view.keyLabel.constraintWidthGreater(constant: 50)
        
        return view
    }
}
