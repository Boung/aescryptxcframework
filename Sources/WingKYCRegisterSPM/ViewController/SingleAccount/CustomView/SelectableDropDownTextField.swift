//
//  SelectableDropDownTextField.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/1/23.
//

import UIKit

class SelectableDropDownTextField: BaseDropdownTextField {
    
    let checkBoxButton = CheckBoxView()
    
    private let leftButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        
        return button
    }()
    
    private var onSelectedChanged: ((Bool) -> Void)?
    var isNonDropDown: Bool = false
    var isSelected: Bool {
        return checkBoxButton.isSelected
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
        textField.addLeftView(checkBoxButton, padding: .init(top: 0, left: 8, bottom: 0, right: 8))
        
        addSubview(leftButton)
        leftButton.constrainWidth(constant: 50)
        leftButton.anchor(top: textField.topAnchor,
                          leading: textField.leadingAnchor,
                          bottom: textField.bottomAnchor,
                          trailing: nil,
                          padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        
        leftButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.checkBoxButton.isSelected.toggle()
            self.onSelectedChanged?(self.checkBoxButton.isSelected)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupForNonDropdown() {
        isNonDropDown = true
        textField.rightView = nil
        textField.rightViewMode = .never
    }
    
    func setOnSelectedChanged(handler: @escaping (Bool) -> Void) {
        self.onSelectedChanged = handler
    }
}
