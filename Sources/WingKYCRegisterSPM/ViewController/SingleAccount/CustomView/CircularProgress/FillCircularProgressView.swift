//
//  FillCircularProgressView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 24/2/23.
//

import UIKit

protocol CircularProgressBarDelegate {
    func OTPExpired()
}

class FillCircularProgressView: UIView {
    
    var timer: Timer!
    
    //MARK: Public
    public var lineWidth:CGFloat {
        return radius * 2
        
    }
    var remainingTime: Double = 0
    var maximumTime: Double = 120
    var delegate: CircularProgressBarDelegate?
    
    public func setProgress(duration: Double, withAnimation: Bool) {
        let fromValue = duration > maximumTime ? maximumTime : duration / maximumTime
        
        remainingTime = duration
        
        if withAnimation {
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.fromValue = fromValue
            animation.toValue = 0
            animation.duration = duration
            backgroundLayer.add(animation, forKey: "foregroundAnimation")
        }
        
        if timer != nil {
            timer.invalidate()
        }
        
        var currentTime: Double = 0
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            
            if currentTime >= duration {
                timer.invalidate()
                guard let delegate = self.delegate else { return }
                delegate.OTPExpired()
            } else {
                currentTime += 1
                self.remainingTime = duration - currentTime
            }
        }
    }
    
    
    
    
    //MARK: Private
    private let backgroundLayer = CAShapeLayer()
    private var radius: CGFloat = 4
    
    private var pathCenter: CGPoint{ get{ return self.convert(self.center, from:self.superview) } }
    private func makeBar() {
        self.layer.sublayers = nil
        drawBackgroundLayer()
    }
    
    private func drawBackgroundLayer(){
        let path = UIBezierPath(roundedRect: .init(x: 0, y: 5, width: radius * 2, height: radius * 2), cornerRadius: radius)
        self.backgroundLayer.path = path.cgPath
        self.backgroundLayer.strokeColor = UIColor.secondaryColor.cgColor
        self.backgroundLayer.lineWidth = lineWidth
        self.backgroundLayer.fillColor = UIColor.clear.cgColor
        
        /// Make Animation resume after enter background
        self.backgroundLayer.makeAnimationsPersistent()
        /// Add sub layer
        self.layer.addSublayer(backgroundLayer)
    }
    
    private func setupView() {
        makeBar()
    }
    
    //Layout Sublayers
    private var layoutDone = false
    
    override func layoutSublayers(of layer: CALayer) {
        setupView()
    }
}

