//
//  ImageSelectionView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/1/23.
//

import UIKit

class ImageSelectionView: UIView {
    
    let containerView = ContainerView(background: .backgroundColor)
    let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_camera").withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .lightGray
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        
        return imageView
    }()
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "select_photo".localize
        label.textAlignment = .center
        label.textColor = .darkGray
        label.font = .systemFont(ofSize: 13)
        label.numberOfLines = 2
        label.scaleToWidth(to: 0.8)
        
        return label
    }()
    let photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = .clear
        
        return imageView
    }()
    
    var image: UIImage? {
        return photoView.image
    }
    
    init(size: CGFloat = 80) {
        super.init(frame: .zero)
        
        addSubview(containerView)
        containerView.constrainWidth(constant: size)
        containerView.constrainHeight(constant: size)
        containerView.fillInSuperView()
        
        containerView.addSubview(vStackView)
        vStackView.fillInSuperView(padding: .init(top: 8, left: 16, bottom: 8, right: 16))
        
        vStackView.addArrangedSubview(iconImageView)
        vStackView.addArrangedSubview(titleLabel)
        
        let iconSize = size * 0.3
        iconImageView.constrainWidth(constant: iconSize)
        iconImageView.constrainHeight(constant: iconSize)
        
        addSubview(photoView)
        photoView.fillInSuperView()
        
        setCornerRadius(radius: 8)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
        addGestureRecognizer(tapGesture)
        isUserInteractionEnabled = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func toggleError(_ bool: Bool) {
        containerView.backgroundColor = bool ? .errorTextField : .backgroundColor
    }
    
    func autoFillMockupData() {
        photoView.image = .localImage("thumbnail_id")
    }
    
    @objc private func onTap() {
        guard let topVC = UIApplication.shared.getTopViewController() else { return }
        
        let alert = UIAlertController(title: "select_photo".localize, message: nil, preferredStyle: .actionSheet)
        if let image = image {
            alert.addAction(.init(title: "view_photo".localize, style: .default, handler: { _ in
                let vc = FullScreenImagePreviewViewController()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .coverVertical
                vc.image = image
                
                topVC.present(vc, animated: true)
            }))
        }
        alert.addAction(.init(title: "camera".localize, style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            
            self.presentPhotoController(sourceType: .camera, controller: topVC)
        }))
        alert.addAction(.init(title: "photo_gallery".localize, style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            
            self.presentPhotoController(sourceType: .photoLibrary, controller: topVC)
        }))
        alert.addAction(.init(title: "cancel".localize, style: .cancel, handler: { (_) in
            topVC.dismiss(animated: true, completion: nil)
        }))
        
        topVC.present(alert, animated: true)
    }
    
    private func presentPhotoController(sourceType: UIImagePickerController.SourceType, controller: UIViewController) {
        let imageController = UIImagePickerController()
        
        imageController.delegate = self
        imageController.sourceType = sourceType
        
        if sourceType == .camera {
            imageController.showsCameraControls = true
        }
        
        controller.present(imageController, animated: true, completion: nil)
    }
}

extension ImageSelectionView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var newImage: UIImage

        if let possibleImage = info[.editedImage] as? UIImage {
            newImage = possibleImage
        } else if let possibleImage = info[.originalImage] as? UIImage {
            newImage = possibleImage
        } else {
            return
        }

        guard let topVC = UIApplication.shared.getTopViewController() else { return }
        topVC.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            
            self.photoView.image = newImage
        }
    }
}
