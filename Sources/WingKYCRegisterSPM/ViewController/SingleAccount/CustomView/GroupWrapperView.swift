//
//  GroupWrapperView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/1/23.
//

import UIKit

class GroupWrapperView: UIView {
    
    let containerView = ContainerView(background: .white)
    let headerContainer = ContainerView(background: .headerBackgroundColor)
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.textColor = .headerTitleColor
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 2
        
        return label
    }()
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 0
        
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(containerView)
        containerView.fillInSuperView()
        
        containerView.addSubview(headerContainer)
        headerContainer.constraintHeightGreater(constant: 50)
        headerContainer.anchor(top: containerView.topAnchor,
                               leading: containerView.leadingAnchor,
                               bottom: nil,
                               trailing: containerView.trailingAnchor)
        
        containerView.addSubview(stackView)
        stackView.anchor(top: headerContainer.bottomAnchor,
                         leading: containerView.leadingAnchor,
                         bottom: containerView.bottomAnchor,
                         trailing: containerView.trailingAnchor,
                         padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        headerContainer.addSubview(titleLabel)
        titleLabel.fillInSuperView(padding: .init(top: 8, left: 8, bottom: 8, right: 8))
        
        containerView.setCornerRadius(radius: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setPlaceholder(_ string: String) {
        titleLabel.text = string
    }
    
    func addCustomView(view: UIView) {
        stackView.addArrangedSubview(view)
    }
    
    func customStackView(callBack: (UIStackView) -> Void) {
        callBack(stackView)
    }
    
    func toggleError(_ bool: Bool) {
        containerView.backgroundColor = bool ? .errorTextField : .white
    }
}
