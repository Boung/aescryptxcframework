//
//  RadioButtonContainerView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/1/23.
//

import UIKit

class RadioButtonContainerView: UIView {
    
    let containerView = TitleComponentWrapperView()
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 4
        
        return stackView
    }()
    
    var selectedIndex: Int = 0 {
        didSet {
            if selectedIndex > radioButtonViews.count - 1 { return }
            
            radioButtonViews.forEach { $0.isSelected = false }
            radioButtonViews[selectedIndex].isSelected = true
        }
    }
    var items: [String] = []
    var radioButtonViews: [BaseRadioButton] = []
    var onValueChangedHandler: ((Int) -> Void)?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(containerView)
        containerView.fillInSuperView()
        
        containerView.addCustomView(view: stackView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setPlaceholder(_ string: String) {
        containerView.setPlaceholder(string)
    }
    
    func setOnValueChangedHandler(_ handler: @escaping (Int) -> Void) {
        self.onValueChangedHandler = handler
    }
    
    func setupRadioButton(items: [String], defaultSelectIndex: Int = 0) {
        self.items = items
        
        radioButtonViews.removeAll()
        stackView.arrangedSubviews.forEach {
            $0.isHidden = true
            $0.removeFromSuperview()
        }
        
        items.enumerated().forEach { item in
            let view = BaseRadioButton()
            view.setTitle(item.element)
            view.setNumberOfLines(2)
            view.setAction { [weak self] in
                guard let self = self else { return }
                self.selectedIndex = item.offset
                self.onValueChangedHandler?(self.selectedIndex)
            }
            
            radioButtonViews.append(view)
        }
        
        radioButtonViews.forEach {
            stackView.addArrangedSubview($0)
        }
        
        self.selectedIndex = defaultSelectIndex
    }
    
    func setErrorTextField(_ bool: Bool) {
        containerView.setBorder(width: 1, color: bool ? .errorTextField : .titleColor)
    }
}
