//
//  LeadingLabelButtonView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/2/23.
//

import UIKit

class LeadingLabelButtonView: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let label: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .left
        label.numberOfLines = 2
        
        return label
    }()
    let button: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setTitleColor(.secondaryColor, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 13)
        button.contentHorizontalAlignment = .right
        
        return button
    }()
    
    init(buttonWidth: CGFloat? = nil) {
        super.init(frame: .zero)
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(button)
        
        if let size = buttonWidth {
            button.constrainWidth(constant: size)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetail(label string: String, buttonText: String? = nil, buttonAttributeString: NSMutableAttributedString? = nil) {
        label.text = string
        
        if let text = buttonText {
            button.setTitle(string: text)
        }
        
        if let attributedString = buttonAttributeString {
            button.setAttributedTitle(attributedString, for: .normal)
        }
    }
    
    func setButtonAction(completion: @escaping () -> Void) {
        button.setAction {
            completion()
        }
    }
}
