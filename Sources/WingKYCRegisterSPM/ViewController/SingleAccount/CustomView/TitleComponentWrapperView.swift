//
//  TitleComponentWrapperView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/1/23.
//

import UIKit

class TitleComponentWrapperView: UIView {
    var containerView = ContainerView(background: .clear)
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .titleColor
        label.textInsets = .init(top: 2, left: 0, bottom: 2, right: 4)
        label.scaleToWidth(to: 0.5)
        
        return label
    }()
    
    let wrapper = ContainerView(background: .clear)
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 0
        
        return stackView
    }()
    
    var isRequired: Bool = false {
        didSet {
            if isRequired {
                titleLabel.addStar()
            }
        }
    }
    
    init(top: CGFloat = 2, left: CGFloat = 0, bottom: CGFloat = 0, right: CGFloat = 0) {
        super.init(frame: .zero)
        
        addSubview(titleLabel)
        titleLabel.lessThenOrEqualTo(trailing: trailingAnchor, constant: 16)
        titleLabel.anchor(top: topAnchor,
                          leading: leadingAnchor,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        
        addSubview(containerView)
        containerView.anchor(top: titleLabel.bottomAnchor,
                             leading: leadingAnchor,
                             bottom: bottomAnchor,
                             trailing: trailingAnchor,
                             padding: .init(top: -10, left: 0, bottom: 0, right: 0))
        
        containerView.addSubview(wrapper)
        wrapper.anchor(top: titleLabel.bottomAnchor,
                         leading: containerView.leadingAnchor,
                         bottom: containerView.bottomAnchor,
                         trailing: containerView.trailingAnchor,
                         padding: .init(top: 2, left: 0, bottom: 0, right: 0))
        
        wrapper.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: top, left: left, bottom: bottom, right: right))
        
        bringSubviewToFront(titleLabel)
        setCornerRadius(radius: 8)
        wrapper.setCornerRadius(radius: 8)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setPlaceholder(_ string: String, isRequired: Bool = false) {
        titleLabel.text = string
        self.isRequired = isRequired
    }
    
    func addCustomView(view: UIView) {
        stackView.addArrangedSubview(view)
    }
    
    func customStackView(callBack: (UIStackView) -> Void) {
        callBack(stackView)
    }
    
    func toggleError(_ bool: Bool) {
        wrapper.backgroundColor = bool ? .errorTextField : .clear
    }
}
