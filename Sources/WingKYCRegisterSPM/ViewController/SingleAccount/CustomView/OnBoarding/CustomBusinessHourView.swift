//
//  CustomBusinessHourView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/1/23.
//

import UIKit

struct BusinessHourModel {
    var isSelected: Bool
    var startTime: Date?
    var endTime: Date?
}

class CustomBusinessHourView: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let mondayView = OpenHourView()
    let tuesdayView = OpenHourView()
    let wednesdayView = OpenHourView()
    let thursdayView = OpenHourView()
    let fridayView = OpenHourView()
    let saturdayView = OpenHourView()
    let sundayView = OpenHourView()
    
    var businessHours: [BusinessHourModel] {
        return [
            .init(isSelected: mondayView.isSelected, startTime: mondayView.startHour, endTime: mondayView.stopHour),
            .init(isSelected: tuesdayView.isSelected, startTime: tuesdayView.startHour, endTime: tuesdayView.stopHour),
            .init(isSelected: wednesdayView.isSelected, startTime: wednesdayView.startHour, endTime: wednesdayView.stopHour),
            .init(isSelected: thursdayView.isSelected, startTime: thursdayView.startHour, endTime: thursdayView.stopHour),
            .init(isSelected: fridayView.isSelected, startTime: fridayView.startHour, endTime: fridayView.stopHour),
            .init(isSelected: saturdayView.isSelected, startTime: saturdayView.startHour, endTime: saturdayView.stopHour),
            .init(isSelected: sundayView.isSelected, startTime: sundayView.startHour, endTime: sundayView.stopHour),
        ]
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        stackView.addArrangedSubview(mondayView)
        stackView.addArrangedSubview(tuesdayView)
        stackView.addArrangedSubview(wednesdayView)
        stackView.addArrangedSubview(thursdayView)
        stackView.addArrangedSubview(fridayView)
        stackView.addArrangedSubview(saturdayView)
        stackView.addArrangedSubview(sundayView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        mondayView.setTitle("monday".localize)
        tuesdayView.setTitle("tuesday".localize)
        wednesdayView.setTitle("wednesday".localize)
        thursdayView.setTitle("thursday".localize)
        fridayView.setTitle("friday".localize)
        saturdayView.setTitle("saturday".localize)
        sundayView.setTitle("sunday".localize)
        
        mondayView.hasCheckBox = true
        tuesdayView.hasCheckBox = true
        wednesdayView.hasCheckBox = true
        thursdayView.hasCheckBox = true
        fridayView.hasCheckBox = true
        saturdayView.hasCheckBox = true
        sundayView.hasCheckBox = true
    }
}
