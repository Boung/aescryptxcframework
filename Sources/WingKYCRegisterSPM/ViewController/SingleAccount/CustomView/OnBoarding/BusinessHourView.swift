//
//  BusinessHourView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/1/23.
//

import UIKit

class BusinessHourView: UIView {
    
    let container = TitleComponentWrapperView(top: 8, left: 8, bottom: 8, right: 8)
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let radioButton: RadioButtonContainerView = {
        let radioButton = RadioButtonContainerView()
        radioButton.setupRadioButton(items: ["every_day".localize, "custom".localize], defaultSelectIndex: 0)
        
        return radioButton
    }()
    
    let everyDateTime: OpenHourView = {
        let view = OpenHourView()
        view.setTitle("start".localize)
        
        return view
    }()
    let customBusinessHourView = CustomBusinessHourView()
    
    var isOpenEveryday: Bool {
        return radioButton.selectedIndex == 0
    }
    var everydayHour: BusinessHourModel {
        return .init(isSelected: false, startTime: everyDateTime.startHour, endTime: everyDateTime.stopHour)
    }
    var customHours: [BusinessHourModel] {
        return customBusinessHourView.businessHours
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        radioButton.setOnValueChangedHandler { index in
            self.performFadeAnimate { [weak self] in
                guard let self = self else { return }
                
                self.everyDateTime.isHidden = index == 1
                self.customBusinessHourView.isHidden = index == 0
                
                self.stackView.layoutIfNeeded()
            }
        }
        
        customBusinessHourView.isHidden = true
        
        addSubview(container)
        container.fillInSuperView()
        container.setPlaceholder("business_hour".localize)
        container.wrapper.backgroundColor = .white
        
        container.addCustomView(view: stackView)
        
        stackView.addArrangedSubview(radioButton)
        stackView.addArrangedSubview(everyDateTime)
        stackView.addArrangedSubview(customBusinessHourView)
        
        stackView.setCustomSpacing(16, after: radioButton)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
