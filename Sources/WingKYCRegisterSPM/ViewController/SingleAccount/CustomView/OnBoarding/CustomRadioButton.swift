//
//  CustomRadioButton.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 18/1/23.
//

import UIKit

class CustomRadioButton: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultHorizonStackView()
        
        return stackView
    }()
    let radioButton = BaseRadioButton()
    let promotionView = LabelContainerView(padding: .init(top: 4, left: 8, bottom: 4, right: 8))
    
    var onTapAction: (() -> Void)?
    
    var isSelected: Bool = false {
        didSet {
            radioButton.isSelected = isSelected
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        addSubview(stackView)
        stackView.lessThenOrEqualTo(trailing: trailingAnchor, constant: 16)
        stackView.anchor(top: topAnchor,
                         leading: leadingAnchor,
                         bottom: bottomAnchor,
                         trailing: nil,
                         padding: .init(top: 16, left: 16, bottom: 16, right: 0))
        
        stackView.addArrangedSubview(radioButton)
        stackView.addArrangedSubview(promotionView)
        
        setCornerRadius(radius: 8)
        
        promotionView.constraintWidthGreater(constant: 50)
        promotionView.setRoundedView()
        
        isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
        addGestureRecognizer(tapGesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetail(model: KeyValueModel, configuration: @escaping (LabelContainerView) -> Void) {
        radioButton.setTitle(model.value)
        radioButton.setDefaultSeleceted(bool: model.isSelected)
        configuration(promotionView)
    }
    
    func setAction(handler: @escaping () -> Void) {
        self.onTapAction = handler
    }
    
    @objc private func onTap() {
        onTapAction?()
    }
}
