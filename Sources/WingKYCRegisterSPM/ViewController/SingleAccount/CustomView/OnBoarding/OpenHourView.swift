//
//  OpenHourView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/1/23.
//

import UIKit

class OpenHourView: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 4
        
        return stackView
    }()
    let checkBox = CheckBoxView()
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .left
        label.textColor = .subTitleColor
        label.font = .systemFont(ofSize: 15)
        
        return label
    }()
    let saperatorLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "to".localize
        label.textAlignment = .center
        label.textColor = .subTitleColor
        label.font = .systemFont(ofSize: 15)
        
        return label
    }()
    let startTimeTextField = BaseTimePickerTextField()
    let stopTimeTextField = BaseTimePickerTextField()
    
    var startHour: Date? {
        return startTimeTextField.timePicker.date
    }
    var stopHour: Date? {
        return stopTimeTextField.timePicker.date
    }
    
    var hasCheckBox: Bool = false {
        didSet {
            checkBox.isHidden = !hasCheckBox
        }
    }
    
    var isSelected: Bool = false {
        willSet {
            checkBox.isSelected = isSelected
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
     
        startTimeTextField.setDefaultTime("07:00")
        startTimeTextField.textField.backgroundColor = .white
        startTimeTextField.setBorder(width: 1, color: .lightGray)
        
        stopTimeTextField.setDefaultTime("20:00")
        stopTimeTextField.textField.backgroundColor = .white
        stopTimeTextField.setBorder(width: 1, color: .lightGray)
        
        saperatorLabel.constrainWidth(constant: 30)
        startTimeTextField.constrainWidth(constant: 70)
        stopTimeTextField.constrainWidth(constant: 70)
        
        checkBox.isHidden = true
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        stackView.addArrangedSubview(checkBox)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(startTimeTextField)
        stackView.addArrangedSubview(saperatorLabel)
        stackView.addArrangedSubview(stopTimeTextField)
        
        startTimeTextField.setCornerRadius(radius: 8)
        stopTimeTextField.setCornerRadius(radius: 8)
        
        isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
        addGestureRecognizer(tapGesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setTitle(_ string: String) {
        titleLabel.text = string
    }
    
    @objc private func onTap() {
        isSelected.toggle()
    }
}
