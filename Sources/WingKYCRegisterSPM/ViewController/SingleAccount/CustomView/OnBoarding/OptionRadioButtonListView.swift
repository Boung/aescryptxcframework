//
//  OptionRadioButtonListView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 18/1/23.
//

import UIKit

class OptionRadioButtonListView: UIView {
    
    let container = TitleComponentWrapperView(top: 4, left: 8, bottom: 8, right: 8)
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultVerticalStackView()
        
        return stackView
    }()
    var radioButtonViewList: [CustomRadioButton] = [] {
        didSet {
            stackView.removeAllArrangedSubView()
            radioButtonViewList.forEach {
                stackView.addArrangedSubview($0)
            }
        }
    }
    
    var datasource: [KeyValueModel] = [] {
        didSet {
            radioButtonViewList = datasource.map { model in
                let view = CustomRadioButton()
                view.setupDetail(model: model) { label in
                    label.configLabel { label in
                        label.text = "new".localize
                        label.textColor = .white
                    }
                    label.setBackgroundColor(.secondaryColor)
                }
                view.setAction { [weak self] in
                    guard let self = self else { return }
                    
                    self.datasource.forEach { $0.isSelected = false }
                    self.radioButtonViewList.forEach { $0.isSelected = false }
                    
                    model.isSelected = true
                    view.isSelected = true
                }
                
                return view
            }
        }
    }
    
    var selectedDatasource: KeyValueModel? {
        return datasource.first { $0.isSelected }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(container)
        container.wrapper.backgroundColor = .addressBackgroundColor
        container.fillInSuperView()
        
        container.addCustomView(view: stackView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetail(placeholder: String, datasource: [KeyValueModel]) {
        self.container.setPlaceholder(placeholder)
        self.datasource = datasource
    }
}
