//
//  CustomPinCodeOTPView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 15/2/23.
//

import UIKit

class CustomPinCodeOTPView: UIView {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultHorizonStackView(spacing: 16)
        stackView.alignment = .center
        
        return stackView
    }()
    
    private var otpViews: [UIView] = []
    private var otpType: OTPTypeEnum = .otp4
    var filledColor: UIColor = .white
    var unFilledColor: UIColor = .clear
    var borderColor: UIColor = .white
    
    init(otpType: OTPTypeEnum) {
        super.init(frame: .zero)
        
        self.otpType = otpType
        
        setupUI()
        
        addSubview(stackView)
        stackView.fillInSuperView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        otpViews.forEach {
            $0.constrainWidth(constant: 25)
            $0.constrainHeight(constant: 25)
            $0.setRoundedView()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        otpViews.removeAll()
        stackView.removeAllArrangedSubView()
        
        for _ in 0..<otpType.inputCount {
            let view = UIView()
            view.backgroundColor = unFilledColor
            view.setBorder(width: 0.5, color: borderColor)
            
            otpViews.append(view)
        }
        
        otpViews.forEach {
            stackView.addArrangedSubview($0)
        }
    }
    
    func setInputValue(string: String) {
        observeTextFieldColor(value: string)
    }
    
    func reset() {
        otpViews.forEach { $0.backgroundColor = unFilledColor }
    }
    
    private func observeTextFieldColor(value: String) {
        guard otpViews.count == otpType.inputCount else { return }
        
        let count = value.count
        
        for index in 0..<otpType.inputCount {
            otpViews[index].backgroundColor = count > index ? filledColor : unFilledColor
        }
    }
}
