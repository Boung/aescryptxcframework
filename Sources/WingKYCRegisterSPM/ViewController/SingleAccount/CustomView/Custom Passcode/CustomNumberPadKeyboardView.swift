//
//  CustomNumberPadKeyboardView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 15/2/23.
//

import UIKit

class CustomNumberPadKeyboardView: UIView {
    
    let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultVerticalStackView(spacing: 16)
        stackView.distribution = .fillEqually
        
        return stackView
    }()
    let firstRowStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultHorizonStackView(spacing: 16)
        stackView.alignment = .center
        stackView.distribution = .fill
        
        return stackView
    }()
    let secondRowStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultHorizonStackView(spacing: 16)
        stackView.alignment = .center
        stackView.distribution = .fill
        
        return stackView
    }()
    let thirdRowStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultHorizonStackView(spacing: 16)
        stackView.alignment = .center
        stackView.distribution = .fill
        
        return stackView
    }()
    let forthRowStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultHorizonStackView(spacing: 16)
        stackView.alignment = .center
        stackView.distribution = .fill
        
        return stackView
    }()
    
    let cleanButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setTitle(string: "C")
        button.titleLabel?.font = .boldSystemFont(ofSize: 20)
        
        return button
    }()
    let clearButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.tintColor = .white
        button.setImage(.localImage("ic_delete").withRenderingMode(.alwaysTemplate), for: .normal)
        button.addImagePadding(.init(top: 8, left: 8, bottom: 8, right: 8))
        
        return button
    }()
    
    private var otpType: OTPTypeEnum = .otp4
    private var otpArray: [String] = [] {
        didSet {
            let code = otpArray.joined()
            
            if otpArray.count == otpType.inputCount {
                onCompletedOTP?(code)
            }
        }
    }
    var buttons: [BaseButton] = []
    var onValueChanged: ((String) -> Void)?
    var onCompletedOTP: ((String) -> Void)?
    
    init(otpType: OTPTypeEnum) {
        super.init(frame: .zero)
        
        self.otpType = otpType
        
        setupUI()
        
        addSubview(vStackView)
        vStackView.fillInSuperView()
        
        vStackView.addArrangedSubview(firstRowStackView)
        vStackView.addArrangedSubview(secondRowStackView)
        vStackView.addArrangedSubview(thirdRowStackView)
        vStackView.addArrangedSubview(forthRowStackView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        buttons.forEach { $0.setRoundedView() }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reset() {
        self.otpArray = []
    }
    
    func setOnValueChanged(handler: @escaping (String) -> Void) {
        self.onValueChanged = handler
    }
    
    func setOnCompletedOTP(handler: @escaping (String) -> Void) {
        self.onCompletedOTP = handler
    }
    
    private func setupUI() {
        let buttonWidth = (UIConstants.screenWidth / 3) * 0.7
        
        for index in 0...9 {
            let button = BaseButton()
            button.backgroundColor = UIColor.backgroundColor.withAlphaComponent(0.3)
            button.setTitle(string: "\(index)")
            button.titleLabel?.font = .boldSystemFont(ofSize: 20)
            button.constrainWidth(constant: buttonWidth)
            button.constrainHeight(constant: buttonWidth)
            button.setBorder(width: 0.5, color: .white)
            button.setAction { [weak self] in
                guard let self = self else { return }
                
                guard self.otpArray.count < self.otpType.inputCount else { return }
                
                self.otpArray.append("\(index)")
                self.onValueChanged?(self.otpArray.joined())
            }
            
            
            buttons.append(button)
        }
        
        cleanButton.constrainWidth(constant: buttonWidth)
        cleanButton.constrainHeight(constant: buttonWidth)
        
        
        clearButton.constrainWidth(constant: buttonWidth)
        clearButton.constrainHeight(constant: buttonWidth)
        
        cleanButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.otpArray = []
            self.onValueChanged?(self.otpArray.joined())
        }
        clearButton.setAction { [weak self] in
            guard let self = self else { return }
            
            guard !self.otpArray.isEmpty else { return }
            self.otpArray.removeLast()
            self.onValueChanged?(self.otpArray.joined())
        }
        
        guard buttons.count == 10 else { return }
        
        firstRowStackView.addArrangedSubview(buttons[1])
        firstRowStackView.addArrangedSubview(buttons[2])
        firstRowStackView.addArrangedSubview(buttons[3])
        
        secondRowStackView.addArrangedSubview(buttons[4])
        secondRowStackView.addArrangedSubview(buttons[5])
        secondRowStackView.addArrangedSubview(buttons[6])
        
        thirdRowStackView.addArrangedSubview(buttons[7])
        thirdRowStackView.addArrangedSubview(buttons[8])
        thirdRowStackView.addArrangedSubview(buttons[9])
        
        forthRowStackView.addArrangedSubview(cleanButton)
        forthRowStackView.addArrangedSubview(buttons[0])
        forthRowStackView.addArrangedSubview(clearButton)
    }
}
