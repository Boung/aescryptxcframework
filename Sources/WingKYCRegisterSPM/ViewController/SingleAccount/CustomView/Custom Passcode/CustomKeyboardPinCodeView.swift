//
//  CustomKeyboardPinCodeView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/2/23.
//

import UIKit

class CustomKeyboardPinCodeView: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultVerticalStackView(spacing: 16)
        stackView.alignment = .center
        
        return stackView
    }()
    let dummyView = UIView()
    var otpView: CustomPinCodeOTPView?
    var keyboardView: CustomNumberPadKeyboardView?
    
    private var otpType: OTPTypeEnum = .otp4
    private var onCompletedOTP: ((String) -> Void)?
    
    init(otpType: OTPTypeEnum) {
        super.init(frame: .zero)
        
        self.otpType = otpType
        self.otpView = CustomPinCodeOTPView(otpType: otpType)
        self.keyboardView = CustomNumberPadKeyboardView(otpType: otpType)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        stackView.addArrangedSubview(otpView ?? UIView())
        stackView.addArrangedSubview(dummyView)
        stackView.addArrangedSubview(keyboardView ?? UIView())
        
        keyboardView?.setOnValueChanged { [weak self] otp in
            guard let self = self else { return }
            
            self.otpView?.setInputValue(string: otp)
        }
        
        keyboardView?.setOnCompletedOTP { [weak self] otp in
            guard let self = self else { return }

            self.onCompletedOTP?(otp)
        }
    }
    
    func setOnCompletedOTP(handler: @escaping (String) -> Void) {
        self.onCompletedOTP = handler
    }
    
    func reset() {
        otpView?.reset()
        keyboardView?.reset()
    }
}
