//
//  TitleImageSelectionView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/1/23.
//

import UIKit

class TitleImageSelectionView: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "".localize
        label.textAlignment = .left
        label.textColor = .darkGray
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 0
        
        return label
    }()
    let imageSelectionView = ImageSelectionView()
    
    var image: UIImage? {
        return imageSelectionView.image
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(imageSelectionView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetail(title: String) {
        titleLabel.text = title
    }
    
    func autoFillMockupData() {
        imageSelectionView.autoFillMockupData()
    }
}
