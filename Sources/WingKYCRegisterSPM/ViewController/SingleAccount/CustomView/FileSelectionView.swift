//
//  FileSelectionView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 19/1/23.
//

import UIKit

class FileSelectionView: UIView {
    
    let containerView = ContainerView(background: .backgroundColor)
    let vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_file").withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .lightGray
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        
        return imageView
    }()
    let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "select_file".localize
        label.textAlignment = .center
        label.textColor = .darkGray
        label.font = .systemFont(ofSize: 13)
        label.numberOfLines = 2
        label.scaleToWidth(to: 0.8)
        
        return label
    }()
    let photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = .clear
        
        return imageView
    }()
    
    init(size: CGFloat = 80) {
        super.init(frame: .zero)
        
        addSubview(containerView)
        containerView.constrainWidth(constant: size)
        containerView.constrainHeight(constant: size)
        containerView.fillInSuperView()
        
        containerView.addSubview(vStackView)
        vStackView.fillInSuperView(padding: .init(top: 8, left: 16, bottom: 8, right: 16))
        
        vStackView.addArrangedSubview(iconImageView)
        vStackView.addArrangedSubview(titleLabel)
        
        let iconSize = size * 0.3
        iconImageView.constrainWidth(constant: iconSize)
        iconImageView.constrainHeight(constant: iconSize)
        
        addSubview(photoView)
        photoView.fillInSuperView()
        
        setCornerRadius(radius: 8)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
        addGestureRecognizer(tapGesture)
        isUserInteractionEnabled = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func onTap() {
        guard let topVC = UIApplication.shared.getTopViewController() else { return }
        
        var documentPicker: UIDocumentPickerViewController?
        if #available(iOS 14.0, *) {
            documentPicker = UIDocumentPickerViewController(forOpeningContentTypes: [.jpeg, .png])
        }else {
            documentPicker = UIDocumentPickerViewController(documentTypes: ["public.item"], in: .import)
        }
        
        documentPicker?.delegate = self
        documentPicker?.modalPresentationStyle = .overFullScreen
        
        if let vc = documentPicker {
            topVC.present(vc, animated: true)
        }
    }
}

extension FileSelectionView: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print(urls)
    }
}
