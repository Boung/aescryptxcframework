//
//  SingleAccountCustomerOverallInformationViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/2/23.
//

import Foundation

class SingleAccountCustomerOverallInformationViewController: CustomerOverrallInformationViewController {
    
    var shopInformationView: ShopInformationInformationView?
    var paymentMethodView = PaymentMethodDisplayView()
    
    var singleAccountValidateParam = SingleAccountRegisterValidateParam()
    
    override func viewDidLoad() {
        shopInformationView = ShopInformationInformationView(viewController: self)
        
        super.viewDidLoad()
        
        shopInformationView?.setupDetails(model: singleAccountValidateParam)
        paymentMethodView.setupDetail(model: singleAccountValidateParam)
        
        titleLabel.text = "verify_below_information".localize
        
        registerButton.setAction { [weak self] in
            guard let self = self else { return }
            
            let presenter = SingleAccountPresenter()
            presenter.validateSingleAccountRegister(param: self.singleAccountValidateParam) { [weak self] response, error in
                guard let self = self else { return }
                guard error == nil else { return self.handleError(error) }
                
                guard let validateResponse = response else { return }
                print("⏩ Temporary OTP: \(validateResponse.decodedOTP)")
                
                let uuidParam = SingleAccountGetUUIDParam(customerPhone: self.singleAccountValidateParam.phoneNumber)
                presenter.getSingleAccountUUID(param: uuidParam) { [weak self] response, error in
                    guard let self = self else{ return }
                    guard error == nil else {
                        return self.handleError(error)
                    }
                    
                    guard let uuidResponse = response else { return }
                    
                    self.singleAccountValidateParam.sessionID = validateResponse.sessionID
                    self.singleAccountValidateParam.otpCode = validateResponse.confirmCode
                    self.singleAccountValidateParam.uuid = uuidResponse.uuid
                    
                    presenter.uploadMandatoryImages(param: self.singleAccountValidateParam) { [weak self] in
                        guard let self = self else { return }
                        
                        let vc = SingleAccountOTPViewController()
                        vc.isUpgradeAccount = self.isUpgradeAccount
                        vc.singleAccountValidateParam = self.singleAccountValidateParam
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
        if let index = vStackView.arrangedSubviews.firstIndex(of: otherRequestInformationView) {
            vStackView.insertArrangedSubview(shopInformationView!, at: index + 1)
            vStackView.insertArrangedSubview(paymentMethodView, at: index + 2)
        }
    }
}
