//
//  FullScreenImagePreviewViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 12/1/23.
//

import UIKit

class FullScreenImagePreviewViewController: BaseViewController {
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .black
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    let closeButton: BaseButton = {
        let button = BaseButton()
        button.setImage(.localImage("ic_close_x"), for: .normal)
        button.backgroundColor = .white
        button.constrainWidth(constant: 30)
        button.constrainHeight(constant: 30)
        button.addImagePadding(.init(top: 8, left: 8, bottom: 8, right: 8))
        
        return button
    }()
    
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        imageView.image = image
        navigationItem.title = "image".localize
        
        closeButton.setAction { [weak self] in
            guard let self = self else { return }

            self.dismiss(animated: true)
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.backgroundColor = .black

        view.addSubview(imageView)
        imageView.fillInSuperView()
        
        view.addSubview(closeButton)
        closeButton.anchor(top: view.layoutMarginsGuide.topAnchor,
                           leading: nil,
                           bottom: nil,
                           trailing: view.trailingAnchor,
                           padding: .init(top: 16, left: 0, bottom: 0, right: 16))
        
        closeButton.setRoundedView()
    }
}
