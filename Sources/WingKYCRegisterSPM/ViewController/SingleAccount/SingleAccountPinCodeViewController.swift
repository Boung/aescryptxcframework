//
//  SingleAccountPinCodeViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 15/2/23.
//

import UIKit

class SingleAccountPinCodeViewController: BaseViewController {
  
  let cancelButton: BaseButton = {
    let button = BaseButton()
    button.backgroundColor = .clear
    button.setTitle(string: "cancel".localize)
    button.contentHorizontalAlignment = .left
    
    return button
  }()
  let vStackView: UIStackView = {
    let stackView = UIStackView()
    stackView.setDefaultVerticalStackView(spacing: 16)
    stackView.alignment = .center
    
    return stackView
  }()
  let iconImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = .localImage("ic_passcode")
    imageView.constrainWidth(constant: 80)
    imageView.constrainHeight(constant: 80)
    
    return imageView
  }()
  let titleLabel: BaseLabel = {
    let label = BaseLabel()
    label.text = "input_pin_code".localize
    label.textAlignment = .center
    label.textColor = .white
    label.font = .boldSystemFont(ofSize: 18)
    label.numberOfLines = 2
    
    return label
  }()
  let subTitleLabel: BaseLabel = {
    let label = BaseLabel()
    label.text = "input_wcx_pin_code_sub_title".localize
    label.textAlignment = .center
    label.textColor = .white
    label.font = .systemFont(ofSize: 15)
    label.numberOfLines = 0
    
    return label
  }()
  let otpView = CustomKeyboardPinCodeView(otpType: .otp4)
  
  var isUpgradeAccount: Bool = false
  var singleAccountValidateParam = SingleAccountRegisterValidateParam()
  var onCommitSuccess: ((SingleAccountRegisterCommitResponse) -> Void)?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    subTitleLabel.text = String(format: "input_wcx_pin_code_sub_title".localize, Constants.authentication.agentAccount.defaultAccount)
    
    otpView.setOnCompletedOTP { [weak self] wcxPin in
      guard let self = self else { return }
      
      self.otpView.reset()
      
      // TODO: - commit -> navigation SingleAccountSuccessViewController()
      let presenter = SingleAccountPresenter()
      let param = SingleAccountRegisterCommitParam(sessionID: self.singleAccountValidateParam.sessionID,
                                                   customerPin: self.singleAccountValidateParam.customerPin,
                                                   ekycPin: wcxPin,
                                                   uuid: self.singleAccountValidateParam.uuid,
                                                   otpCode: self.singleAccountValidateParam.otpCode)
      
      presenter.commitSingleAccountRegister(param: param) { [weak self] response, error in
        guard let self = self else { return }
        guard error == nil else {
          return self.handleError(error)
        }
        
        guard let response = response else { return }
        
        self.singleAccountValidateParam.newCustomerWingAccount = response.wingID
        presenter.uploadOptionalImages(param: self.singleAccountValidateParam)
        
        self.dismiss(animated: true) { [weak self] in
          guard let self = self else { return }
          
          self.onCommitSuccess?(response)
        }
      }
      
    }
    cancelButton.setAction { [weak self] in
      guard let self = self else { return }
      
      self.dismiss(animated: true)
    }
  }
  
  override func generateUI() {
    super.generateUI()
    
    view.addSubview(cancelButton)
    cancelButton.anchor(top: view.layoutMarginsGuide.topAnchor,
                        leading: view.leadingAnchor,
                        bottom: nil,
                        trailing: nil,
                        padding: .init(top: 16, left: 16, bottom: 0, right: 0))
    
    view.addSubview(vStackView)
    vStackView.anchor(top: cancelButton.bottomAnchor,
                      leading: view.leadingAnchor,
                      bottom: view.layoutMarginsGuide.bottomAnchor,
                      trailing: view.trailingAnchor,
                      padding: .init(top: 16, left: 24, bottom: 16, right: 24))
    
    vStackView.addArrangedSubview(iconImageView)
    vStackView.addArrangedSubview(titleLabel)
    vStackView.addArrangedSubview(subTitleLabel)
    vStackView.addArrangedSubview(otpView)
    setupBlurBackground()
  }
}
