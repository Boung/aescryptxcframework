//
//  SingleAccountCustomerConfirmPinViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 15/2/23.
//

import UIKit

class SingleAccountCustomerConfirmPinViewController: BaseViewController {
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.setDefaultVerticalStackView(spacing: 16)
        stackView.alignment = .center
        return stackView
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "single_account_confirm_customer_otp_title".localize
        label.numberOfLines = 2
        label.textColor = .subTitleColor
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 18)
        
        return label
    }()
    private let subTitleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "single_account_confirm_customer_otp_sub_title".localize
        label.numberOfLines = 2
        label.textColor = .titleColor
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 15)
        
        return label
    }()
    private let otpView = OTPView(otpType: .otp4, otpMask: .dot)
    
    var singleAccountValidateParam = SingleAccountRegisterValidateParam()
    var newCustomerPin = ""
    var isUpgradeAccount: Bool = false
    var onPasswordMismatchHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDetails()
        
        otpView.setOnOTPCompleted { [weak self] newPin in
            guard let self = self else { return }
            
            guard self.newCustomerPin == newPin else {
                self.navigationController?.popViewController(animated: true)
                self.onPasswordMismatchHandler?()
                return
            }
            
            // TODO: - Call API createCustomerPin -> present SingleAccountPinCodeViewController()
            let presenter = SingleAccountPresenter()
            let param = SingleAccountPinValidateParam(dateOfBirth: self.singleAccountValidateParam.dob,
                                                      contactNo: self.singleAccountValidateParam.phoneNumber,
                                                      newPin: newPin)
            
            self.singleAccountValidateParam.customerPin = newPin
            
            presenter.validatePinSingleAccount(param: param) { [weak self] response, error in
                guard let self = self else { return }
                guard error == nil else {
                    return self.handleError(error)
                }
                
                guard let _ = response else { return }
                
                let vc = SingleAccountPinCodeViewController()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.singleAccountValidateParam = self.singleAccountValidateParam
                vc.onCommitSuccess = { [weak self] commitResponse in
                    guard let self = self else { return }
                    
                    let vc = SingleAccountSuccessViewController()
                    vc.datasource = commitResponse.toDatasource()
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                self.present(vc, animated: true)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = isUpgradeAccount ? "title_upgrade_wing_account".localize : "title_register_wing_account".localize
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(stackView)
        stackView.anchor(top: view.layoutMarginsGuide.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 64, left: 16, bottom: 0, right: 16))
        
        stackView.addArrangedSubview(titleLabel)
//        stackView.addArrangedSubview(subTitleLabel)
        stackView.addArrangedSubview(otpView)
    }
    
    private func setupDetails() {
        subTitleLabel.text = String(format: "single_account_new_customer_otp_sub_title".localize, singleAccountValidateParam.phoneNumber.orEmptyString)
    }
}
