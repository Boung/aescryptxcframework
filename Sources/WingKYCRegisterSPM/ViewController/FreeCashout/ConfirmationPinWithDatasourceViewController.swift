//
//  ConfirmationPinWithDatasourceViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/8/22.
//

import Foundation
import UIKit

class ConfirmationPinWithDatasourceViewController: BaseViewController {
    
    private let containerView = ContainerView(background: .primaryColor)
    private let headerContainerView = ContainerView(background: .white)
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "confirmation".localize
        label.textAlignment = .center
        
        return label
    }()
    private let closeButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setImage(.localImage("ic_close").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .systemRed
        button.constrainWidth(constant: 30)
        button.constrainHeight(constant: 30)
        
        return button
    }()
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        return tableView
    }()
    private let pinTextField: BaseTextField = {
        let textField = BaseTextField()
        textField.backgroundColor = .clear
        textField.textField.textAlignment = .center
        textField.textField.isSecureTextEntry = true
        textField.textField.keyboardType = .numberPad
        textField.maxInputLength = 4
        
        return textField
    }()
    private let cancelButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "abort_button".localize)
        button.backgroundColor = .secondaryColor
        button.setTitleColor(.white, for: .normal)
        button.setDefaultHeight()
        
        return button
    }()
    private let confirmButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "confirm".localize)
        button.backgroundColor = .secondaryColor
        button.setTitleColor(.white, for: .normal)
        button.setDefaultHeight()
        
        return button
    }()
    
    private let tableViewCell = "KeyValueTableViewCell"
    private let singleLabelTableViewCell = "SingleLabelTableViewCell"
    private var confirmButtonHandler: ((String) -> Void)?
    private var datasource: [KeyValueModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(KeyValueTableViewCell.self, forCellReuseIdentifier: tableViewCell)
        tableView.register(SingleLabelTableViewCell.self, forCellReuseIdentifier: singleLabelTableViewCell)
        
        closeButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true, completion: nil)
        }
        
        cancelButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true, completion: nil)
        }
        
        confirmButton.setAction { [weak self] in
            guard let self = self else { return }
            
            let pinCode = self.pinTextField.textField.text ?? ""
            if pinCode.isEmpty || pinCode.count < 4 {
                self.showErrorAlert("please_input_pin_code".localize)
                return
            }
            
            guard let confirmButtonHandler = self.confirmButtonHandler else { return }
            confirmButtonHandler(pinCode)
        }
    }
    
    
    override func generateUI() {
        super.generateUI()
        
//        view.backgroundColor = .blurBackground
        
        view.addSubview(containerView)
        containerView.centerInSuperview()
        containerView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 0, left: 32, bottom: 0, right: 0))
        
        containerView.addSubview(headerContainerView)
        headerContainerView.constrainHeight(constant: 50)
        headerContainerView.anchor(top: containerView.topAnchor,
                                   leading: containerView.leadingAnchor,
                                   bottom: nil,
                                   trailing: containerView.trailingAnchor)
        
        headerContainerView.addSubview(closeButton)
        closeButton.centerYInSuperview()
        closeButton.anchor(top: nil,
                           leading: nil,
                           bottom: nil,
                           trailing: headerContainerView.trailingAnchor,
                           padding: .init(top: 0, left: 0, bottom: 0, right: 16))
        
        headerContainerView.addSubview(titleLabel)
        titleLabel.centerInSuperview()
        titleLabel.anchor(top: nil,
                          leading: nil,
                          bottom: nil,
                          trailing: closeButton.leadingAnchor,
                          padding: .init(top: 0, left: 0, bottom: 0, right: 8))
        
        containerView.addSubview(tableView)
        tableView.constrainHeight(constant: UIConstants.screenHeight * 0.3)
        tableView.anchor(top: headerContainerView.bottomAnchor,
                         leading: containerView.leadingAnchor,
                         bottom: nil,
                         trailing: containerView.trailingAnchor,
                         padding: .init(top: 8, left: 8, bottom: 0, right: 8))
        
        containerView.addSubview(pinTextField)
        pinTextField.anchor(top: tableView.bottomAnchor,
                            leading: containerView.leadingAnchor,
                            bottom: nil,
                            trailing: containerView.trailingAnchor,
                            padding: .init(top: 24, left: 16, bottom: 0, right: 16))
        
        let stackView = horizonStackView([cancelButton, confirmButton])
        containerView.addSubview(stackView)
        stackView.anchor(top: pinTextField.bottomAnchor,
                         leading: pinTextField.leadingAnchor,
                         bottom: containerView.bottomAnchor,
                         trailing: pinTextField.trailingAnchor,
                         padding: .init(top: 16, left: 0, bottom: 16, right: 0))
        
        cancelButton.setRoundedView()
        confirmButton.setRoundedView()
        setupBlurBackground()
    }
    
    func setupController(placeholder: String, datasource: [KeyValueModel], onConfirmHandler: @escaping (String) -> Void) {
        self.pinTextField.setPlaceholderNoTitle(placeholder)
        self.confirmButtonHandler = onConfirmHandler
        self.datasource = datasource
    }
}

extension ConfirmationPinWithDatasourceViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if datasource[indexPath.row].value.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: singleLabelTableViewCell, for: indexPath) as! SingleLabelTableViewCell
            cell.configCell(datasource[indexPath.row].key)
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCell, for: indexPath) as! KeyValueTableViewCell
            cell.configCell(model: datasource[indexPath.row])
            cell.setMaxLines(0, value: 0)
            cell.setTextAlighn(.left, value: .right)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
