//
//  BaseWebviewViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 1/12/22.
//

import UIKit
import WebKit

enum JSBridgeNameEnum: String, CaseIterable {
    case undefined
    
    static func getAllJSBridges() -> [String] {
        return JSBridgeNameEnum.allCases.map { $0.rawValue }
    }
}

class BaseWebViewController: BaseViewController {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private var webView: WKWebView!
    private let contentController = WKUserContentController()
    private var pullControl = UIRefreshControl()
    
    private let buttonContainer = ContainerView(background: .clear)
    private let hStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        
        return stackView
    }()
    private let agreeButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "ព្រម")
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .secondaryColor
        button.constrainHeight(constant: 40)
        button.constrainWidth(constant: UIConstants.screenWidth * 0.6)
        
        return button
    }()
    private let disagreeButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "មិនព្រម")
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .init(hexString: "DE002C")
        button.constrainHeight(constant: 40)
        button.constrainWidth(constant: UIConstants.screenWidth * 0.6)
        
        return button
    }()
    
    @objc var titleString = ""
    @objc var urlString = ""
    @objc var isFullscreen = false
    @objc var isPullRefreshAvailable = true
    @objc var isBounceEnabled = true
    @objc var onDismissHandler: (() -> Void)?
    @objc var buttonActionCallback: ((Bool) -> Void)?
    
    private var userAgent: String {
        let wcxDefaultAccount = Constants.authentication.agentAccount.defaultAccount
        let appVersion = Constants.appVersion
        
        return String(format: "WCXApp:%@:%@:iOS:%@", appVersion, "KH", wcxDefaultAccount)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        configWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = titleString
        navigationController?.setNavigationBarHidden(isFullscreen, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func configWebView() {
        let contentController = WKUserContentController()
        
        let jsBridgeNames = JSBridgeNameEnum.getAllJSBridges()
        jsBridgeNames.forEach {
            contentController.removeScriptMessageHandler(forName:$0)
            contentController.add(self, name: $0)
        }
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        webView = WKWebView(frame: .zero, configuration: config)
        webView.customUserAgent = userAgent
        webView.navigationDelegate = self
        webView.allowsLinkPreview = false
        webView.scrollView.bounces = isBounceEnabled
        
        if isPullRefreshAvailable {
            pullControl.attributedTitle = NSAttributedString(string: "សូមមេត្តារងចាំ")
            pullControl.addTarget(self, action: #selector(onPullRefreshHandler), for: .valueChanged)
            webView.scrollView.addSubview(pullControl)
        }
        
        view.addSubview(stackView)
        stackView.anchor(top: view.layoutMarginsGuide.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: view.layoutMarginsGuide.bottomAnchor,
                         trailing: view.trailingAnchor)
        
        stackView.addArrangedSubview(webView)
        stackView.addArrangedSubview(buttonContainer)
        
        buttonContainer.addSubview(hStackView)
        hStackView.fillInSuperView(padding: .init(top: 0, left: 16, bottom: 0, right: 16))
        
        hStackView.addArrangedSubview(disagreeButton)
        hStackView.addArrangedSubview(agreeButton)
        
        webView.constrainWidth(constant: UIConstants.screenWidth)
        agreeButton.setRoundedView()
        disagreeButton.setRoundedView()
        
        guard let url = URL(string: urlString.trimmingCharacters(in: .whitespacesAndNewlines)) else { return }
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 60)
        //        let refreshToken = UserDefaultManager.shared.getValue(String.self, forKey: UDKey.refreshToken) ?? ""
        //        let token = "Bearer " + refreshToken
        
        //        urlRequest.setValue(token, forHTTPHeaderField: "Authorization")
        
        print(urlString)
        print(userAgent)
        
        webView.load(urlRequest)
        
        if let handler = buttonActionCallback {
            buttonContainer.isHidden = false
            
            agreeButton.setAction { [weak self] in
                guard let self = self else { return }
                
                self.navigationController?.popViewController(animated: true)
                handler(true)
            }
            
            disagreeButton.setAction { [weak self] in
                guard let self = self else { return }
                
                self.navigationController?.popViewController(animated: true)
                handler(false)
            }
        }else {
            buttonContainer.isHidden = true
        }
    }
    
    @objc func onPullRefreshHandler() {
        webView.reload()
    }
    
    @objc func setOnDismiss(handler: @escaping () -> Void) {
        self.onDismissHandler = handler
    }
    
    @objc func setButtonAction(handler: @escaping (Bool) -> Void) {
        self.buttonActionCallback = handler
    }
}

extension BaseWebViewController: WKNavigationDelegate, WKUIDelegate {
    //    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
    //        showLoading()
    //    }
    //
    //    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
    //        hideLoading()
    //    }
    //
    //    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    //        hideLoading()
    //    }
}

extension BaseWebViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("[Message Name]: \(message.name)")
        print("[Body]: \(message.body)")
        
        var jsonPayload: [String: Any] = [:]
        
        if let payload = message.body as? [String: Any] {
            jsonPayload = payload
        }
        
        let bridgingName: JSBridgeNameEnum = JSBridgeNameEnum(rawValue: message.name) ?? .undefined
        
        switch bridgingName {
        default:
            break
        }
    }
}
