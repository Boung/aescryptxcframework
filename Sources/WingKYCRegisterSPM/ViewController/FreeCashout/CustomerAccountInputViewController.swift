//
//  CustomerAccountInputViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 2/8/22.
//

import Foundation

class CustomerAccountInputViewController: BaseViewController {
    
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "input_customer_account".localize
        label.textColor = .darkGray
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 18)
        return label
    }()
    private let accountTextField: BaseTextField = {
        let textField = BaseTextField()
        textField.setPlaceholderNoTitle("input_customer_account".localize)
        textField.textField.keyboardType = .numberPad
        textField.textField.backgroundColor = .white
        textField.maxInputLength = 10
        
        return textField
    }()
    private let contintueButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "continue".localize)
        button.backgroundColor = .secondaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    var navigationTitle = ""
    var isSubscribe = false
    var upgradeKYCHandler: (() -> Void)?
    
    // MARK: - Subscribe
    var selectedPlanOptionType: PlanOptionTypeListResponse?
    private var subscribeValidateResponse: SubscribeValidateResponse?
    
    // MARK: - Unsubscribe
    
    private var customerAccount: String {
        return accountTextField.textField.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = navigationTitle
        
        contintueButton.setAction { [weak self] in
            guard let self = self else { return }
            
            guard self.customerAccount.isNotEmpty else {
                self.showSingleButtonAlert(title: "error".localize, message: "input_customer_account_alert_message".localize, buttonTitle: "done".localize) { [weak self] in
                    guard let self = self else { return }
                    
                    self.dismiss(animated: true, completion: nil)
                }
                return
            }
            
            if self.isSubscribe {
                print("Haha")
                let presenter = SubscribeValidatePresenter(delegate: self)
                let param = SubscribeValidateParam(customerAccountNumber: self.customerAccount, subType: self.selectedPlanOptionType)
                
                presenter.validateSubscribe(param: param)
            }else {
                let presenter = UnsubscribeValidatePresenter(delegate: self)
                let param = UnsubscribeValidateParam(customerAccountNumber: self.customerAccount)
                
                presenter.validateUnsubscribe(param: param)
            }
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(titleLabel)
        titleLabel.anchor(top: view.layoutMarginsGuide.topAnchor,
                          leading: view.leadingAnchor,
                          bottom: nil,
                          trailing: view.trailingAnchor,
                          padding: .init(top: 48, left: 16, bottom: 0, right: 16))
        
        view.addSubview(accountTextField)
        accountTextField.anchor(top: titleLabel.bottomAnchor,
                                leading: titleLabel.leadingAnchor,
                                bottom: nil,
                                trailing: titleLabel.trailingAnchor,
                                padding: .init(top: 24, left: 0, bottom: 0, right: 0))
        
        view.addSubview(contintueButton)
        contintueButton.anchor(top: nil,
                               leading: view.leadingAnchor,
                               bottom: view.layoutMarginsGuide.bottomAnchor,
                               trailing: view.trailingAnchor,
                               padding: .init(top: 0, left: 32, bottom: 16, right: 32))
        
        accountTextField.lessThenOrEqualTo(bottom: contintueButton.topAnchor, constant: 16)
        
        contintueButton.setRoundedView()
    }
}

extension CustomerAccountInputViewController {
    private func showPinViewController(placeholder: String, datasource: [KeyValueModel], completion: @escaping (String) -> Void) {
        let vc = ConfirmationPinWithDatasourceViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.setupController(placeholder: placeholder, datasource: datasource, onConfirmHandler: completion)
        
        present(vc, animated: true, completion: nil)
    }
}

extension CustomerAccountInputViewController: SubscribeValidateDelegate {
    func response(subscribeValidate response: SubscribeValidateResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        self.subscribeValidateResponse = response
        
        if response.errorCode != "200" {
            showErrorAlert(response.toLocalizedMessage())
            return
        }
        
        if response.kycStatus == "FULL" {
            self.commitSubscribe()
        }else if response.kycStatus == "BASIC" {
            showUpgradeKYCPopup { [weak self] in
                guard let self = self else { return }
                
                self.dismiss(animated: true) { [weak self] in
                    guard let self = self else { return }
                    
                    guard let updateKYC = self.upgradeKYCHandler else { return }
                    updateKYC()
                }
            } upgradeLaterHandler: { [weak self] in
                guard let self = self else { return }
                
                self.commitSubscribe()
            }
        }
    }
    
    private func commitSubscribe() {
        if subscribeValidateResponse?.existingBenefits?.isEmpty ?? false{
            performCommitSubscribe()
        }else {
            showExistingPlanDetailPopup { [weak self] in
                guard let self = self else { return }
                
                self.performCommitSubscribe()
            }
        }
    }
    
    private func performCommitSubscribe() {
        self.showCustomerPinPopup { [weak self] customerPin in
            guard let self = self else { return }
            
            self.showWCXPinPopup { [weak self] wcxPin in
                guard let self = self else { return }
                
                let presenter = SubscribeCommitPresenter(delegate: self)
                let param = SubscribeCommitParam(sessionID: self.subscribeValidateResponse?.sessionID ?? "",
                                                 currency: self.subscribeValidateResponse?.currency ?? "",
                                                 customerWingAccount: self.subscribeValidateResponse?.initiateeWingAccount ?? "",
                                                 customerPin: customerPin,
                                                 wcxPin: wcxPin,
                                                 subType: self.selectedPlanOptionType)
                
                presenter.commitSubscribe(param: param)
            }
        }
    }
    
    private func showUpgradeKYCPopup(upgradeNow handler: @escaping () -> Void, upgradeLaterHandler: @escaping () -> Void) {
        let vc = UpgradeKYCPopupViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.setUpgradeNowAction { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true) {
                handler()
            }
        }
        
        vc.setUpgradeLaterAction { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true) {
                upgradeLaterHandler()
            }
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    private func showExistingPlanDetailPopup(agreeButtonHandler action: @escaping () -> Void) {
        let datasource: [KeyValueModel] = subscribeValidateResponse?.toExistingBenefitDatasource() ?? []
        let vc = ExistingPlanDetailPopupViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.setupController(datasource: datasource) { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true) {
                action()
            }
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    private func showCustomerPinPopup(completion: @escaping (String) -> Void) {
        let customerDatasource: [KeyValueModel] = subscribeValidateResponse?.toCustomerDatasource() ?? []
        showPinViewController(placeholder: "input_customer_pin".localize, datasource: customerDatasource) { [weak self] customerPin in
            guard let self = self else { return }
            
            self.dismiss(animated: true) {
                completion(customerPin)
            }
        }
    }
    
    private func showWCXPinPopup(completion: @escaping (String) -> Void) {
        let wcxDatasource: [KeyValueModel] = subscribeValidateResponse?.toWCXDatasource() ?? []
        showPinViewController(placeholder: "input_wcx_pin".localize, datasource: wcxDatasource) { [weak self] wcxPin in
            guard let self = self else { return }

            self.dismiss(animated: true) {
                completion(wcxPin)
            }
        }
    }
}

extension CustomerAccountInputViewController: SubscribeCommitDelegate {
    func response(subscribeCommit response: SubscribeCommitResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        showSuccessTransactionDetail(datasource: response.toDatasource())
    }
    
    private func showSuccessTransactionDetail(datasource: [KeyValueModel]) {
        let datasource: [KeyValueModel] = datasource
        let vc = SuccessfulDetailViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.setupController(datasource: datasource) { [weak self] in
            guard let self = self else { return }

            self.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }

                self.navigationController?.popToRootViewController(animated: true)
            }
        }

        present(vc, animated: true, completion: nil)
    }
}

extension CustomerAccountInputViewController: UnsubscribeValidateDelegate {
    func response(unsubscribeValidate response: UnsubscribeValidateResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }

        let vc = UnsubscribeDetailViewController()
        vc.unsubscribeValidateResponse = response
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
