//
//  ReportTransactionTableViewCell.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation
import UIKit

class ReportTransactionTableViewCell: BaseTableViewCell {
    
    private let containerView = ContainerView(background: .white)
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let issueDateView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.darkGray, value: .darkGray)
        view.setTextAlign(.left, value: .right)
        view.setFont(.systemFont(ofSize: 15), value: .systemFont(ofSize: 15))
        
        return view
    }()
    private let transactionTypeView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.darkGray, value: .darkGray)
        view.setTextAlign(.left, value: .right)
        view.setFont(.systemFont(ofSize: 15), value: .systemFont(ofSize: 15))
        
        return view
    }()
    private let planOptionTypeView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.darkGray, value: .darkGray)
        view.setTextAlign(.left, value: .right)
        view.setFont(.systemFont(ofSize: 15), value: .systemFont(ofSize: 15))
        
        return view
    }()
    private let customerAccountNumberView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.darkGray, value: .darkGray)
        view.setTextAlign(.left, value: .right)
        view.setFont(.systemFont(ofSize: 15), value: .systemFont(ofSize: 15))
        
        return view
    }()
    private let customerPhoneNumberView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.darkGray, value: .darkGray)
        view.setTextAlign(.left, value: .right)
        view.setFont(.systemFont(ofSize: 15), value: .systemFont(ofSize: 15))
        
        return view
    }()
    private let commissionView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.darkGray, value: .darkGray)
        view.setTextAlign(.left, value: .right)
        view.setFont(.systemFont(ofSize: 15), value: .systemFont(ofSize: 15))
        
        return view
    }()
    private let transactionIDView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.darkGray, value: .darkGray)
        view.setTextAlign(.left, value: .right)
        view.setFont(.systemFont(ofSize: 15), value: .systemFont(ofSize: 15))
        
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = .clear
        
        addSubview(containerView)
        containerView.fillInSuperView(padding: .init(top: 8, left: 16, bottom: 8, right: 16))
        
        containerView.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        stackView.addArrangedSubview(issueDateView)
        stackView.addArrangedSubview(transactionTypeView)
        stackView.addArrangedSubview(planOptionTypeView)
        stackView.addArrangedSubview(customerAccountNumberView)
        stackView.addArrangedSubview(customerPhoneNumberView)
        stackView.addArrangedSubview(commissionView)
        stackView.addArrangedSubview(transactionIDView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerView.setCornerRadius(radius: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configCell(model: PlanTransactionListModel) {
        issueDateView.setupDetails("transaction_issue_date".localize, value: model.subscriptionOn ?? "")
        transactionTypeView.setupDetails("tranasction_type".localize, value: (model.isSubscription ?? "") == "Y" ? "Subscribe".localize : "Unsubscribe".localize)
        planOptionTypeView.setupDetails("plan_option_type".localize, value: model.subscriptionName ?? "")
        customerAccountNumberView.setupDetails("customer_account_number".localize, value: model.accountNo ?? "")
        customerPhoneNumberView.setupDetails("customer_phone_number".localize, value: model.phoneNumber ?? "")
        commissionView.setupDetails("commission".localize, value: model.commission ?? "")
        transactionIDView.setupDetails("transaction_id".localize, value: model.txnID ?? "")
    }
}
