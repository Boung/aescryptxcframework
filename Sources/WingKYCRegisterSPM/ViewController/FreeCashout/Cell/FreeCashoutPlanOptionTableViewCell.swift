    //
    //  FreeCashoutPlanOptionTableViewCell.swift
    //  WingKYCRegisterFormSDK
    //
    //  Created by Wing Specialized Bank on 5/8/22.
    //

    import Foundation
    import UIKit

    class FreeCashoutPlanOptionTableViewCell: BaseTableViewCell {
        
        private let containerView = ContainerView(background: .white)
        private let titleLabel: BaseLabel = {
            let label = BaseLabel()
            label.textColor = UIColor(hexString: "3D639D")
            label.textAlignment = .left
            label.font = .setKhmerMoolFont(15)
            
            return label
        }()
        private let priceLabel: BaseLabel = {
            let label = BaseLabel()
            label.textColor = .darkGray
            label.textAlignment = .left
            label.font = .systemFont(ofSize: 26, weight: .heavy)
            
            return label
        }()
        private let durationLabel: BaseLabel = {
            let label = BaseLabel()
            label.textColor = .darkGray
            label.textAlignment = .left
            label.font = .systemFont(ofSize: 15)
            
            return label
        }()
        private let primaryDescriptionLabel: BaseLabel = {
            let label = BaseLabel()
            label.textColor = .secondaryColor
            label.textAlignment = .right
            label.font = .systemFont(ofSize: 15)
            label.numberOfLines = 0
            
            return label
        }()
        private let vStackView: UIStackView = {
            let stackView = UIStackView()
            stackView.axis = .vertical
            stackView.alignment = .fill
            stackView.distribution = .fill
            stackView.spacing = 8
            
            return stackView
        }()
        
        private let selectionRadioButton = BaseRadioButton(size: 40)
        private let expendableIconImageView: UIImageView = {
            let imageView = UIImageView()
            imageView.tintColor = .secondaryColor
            imageView.contentMode = .scaleAspectFit
            imageView.constrainWidth(constant: 20)
            imageView.constrainHeight(constant: 20)
            
            return imageView
        }()
        private let expendableButton: BaseButton = {
            let button = BaseButton()
            button.backgroundColor = .clear

            button.setDefaultHeight()
            
            return button
        }()
        
        
        private var optionViewList: [BaseLabel] = []

        private var isExpended: Bool = false
        private var isOptionSelected: Bool = false
        private var expendButtonHandler: (() -> Void)?
        private var benefitDatasource: [String] = [] {
            didSet {
                optionViewList.removeAll()
                vStackView.arrangedSubviews.forEach {
                    vStackView.removeArrangedSubview($0)
                    vStackView.layoutIfNeeded()
                }
                
                benefitDatasource.forEach {
                    let label = BaseLabel()
                    label.text = $0
                    label.numberOfLines = 0
                    
                    vStackView.addArrangedSubview(label)
                    vStackView.layoutIfNeeded()
                    optionViewList.append(label)
                }
            }
        }
        
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            backgroundColor = .clear
            selectionStyle = .none
            
            addSubview(containerView)
            containerView.anchor(top: topAnchor,
                                 leading: leadingAnchor,
                                 bottom: bottomAnchor,
                                 trailing: trailingAnchor,
                                 padding: .init(top: 8, left: 16, bottom: 8, right: 16))
            
            containerView.addSubview(selectionRadioButton)
            selectionRadioButton.anchor(top: containerView.topAnchor,
                                        leading: nil,
                                        bottom: nil,
                                        trailing: containerView.trailingAnchor,
                                        padding: .init(top: 16, left: 0, bottom: 0, right: 16))
            
            containerView.addSubview(titleLabel)
            titleLabel.anchor(top: containerView.topAnchor,
                              leading: containerView.leadingAnchor,
                              bottom: nil,
                              trailing: selectionRadioButton.leadingAnchor,
                              padding: .init(top: 16, left: 16, bottom: 0, right: 8))
            
            containerView.addSubview(priceLabel)
            priceLabel.anchor(top: titleLabel.bottomAnchor,
                              leading: titleLabel.leadingAnchor,
                              bottom: nil,
                              trailing: nil,
                              padding: .init(top: 0, left: 0, bottom: 0, right: 8))
            
            containerView.addSubview(durationLabel)
            durationLabel.lessThenOrEqualTo(trailing: selectionRadioButton.leadingAnchor, constant: 8)
            durationLabel.anchor(top: nil,
                                 leading: priceLabel.trailingAnchor,
                                 bottom: priceLabel.bottomAnchor,
                                 trailing: nil,
                                 padding: .init(top: 0, left: 8, bottom: 5, right: 0))
            
            containerView.addSubview(primaryDescriptionLabel)
            primaryDescriptionLabel.anchor(top: durationLabel.topAnchor,
                                           leading: durationLabel.trailingAnchor,
                                           bottom: durationLabel.bottomAnchor,
                                           trailing: containerView.trailingAnchor,
                                           padding: .init(top: 0, left: 8, bottom: 0, right: 16))
            
            
            containerView.addSubview(vStackView)
            vStackView.anchor(top: primaryDescriptionLabel.bottomAnchor,
                              leading: containerView.leadingAnchor,
                              bottom: containerView.bottomAnchor,
                              trailing: containerView.trailingAnchor,
                              padding: .init(top: 16, left: 16, bottom: 16, right: 16))
            
            selectionRadioButton.isUserInteractionEnabled = false
            
//            containerView.addSubview(expendableButton)
//            expendableButton.anchor(top: vStackView.bottomAnchor,
//                                    leading: containerView.leadingAnchor,
//                                    bottom: containerView.bottomAnchor,
//                                    trailing: containerView.trailingAnchor,
//                                    padding: .init(top: 0, left: 0, bottom: 0, right: 0))
            
//            containerView.addSubview(expendableIconImageView)
//            expendableIconImageView.centerXTo(view: expendableButton.centerXAnchor)
//            expendableIconImageView.centerYTo(view: expendableButton.centerYAnchor)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            let expendImage: UIImage = .localImage("ic_expend").withRenderingMode(.alwaysTemplate)
            let collapseImage: UIImage = .localImage("ic_collapse").withRenderingMode(.alwaysTemplate)
            expendableIconImageView.image = isExpended ? collapseImage : expendImage
            
            containerView.setCornerRadius(radius: 18)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func configCell(model: PlanOptionTypeDatasource) {
            optionViewList.forEach {
                $0.removeFromSuperview()
            }
            vStackView.arrangedSubviews.forEach {
                vStackView.removeArrangedSubview($0)
            }
            optionViewList.removeAll()
            
            isExpended = model.isExpended
            isOptionSelected = model.isSelected
            selectionRadioButton.isSelected = isOptionSelected
            
            titleLabel.text = model.planOption?.toLocalizedTitle()
            priceLabel.text = model.planOption?.toLocalizedPrice()
            durationLabel.text = model.planOption?.toLocalizedDuration()
            primaryDescriptionLabel.text = model.planOption?.toLocalizedDescription()
            optionViewList = model.planOption?.cashOutBenefitLabels ?? []
            optionViewList.forEach {
                vStackView.addArrangedSubview($0)
            }
//            toggleExpend()
            
//            expendableButton.setAction { [weak self] in
//                guard let self = self else { return }
//
//                guard let expendButtonHandler = self.expendButtonHandler else { return }
//                expendButtonHandler()
//            }
        }
        
        func setExpendButtonAction(handler: @escaping () -> Void) {
            self.expendButtonHandler = handler
        }
        
        private func toggleExpend() {
            if optionViewList.count <= 2 {
                return
            }
            
            if isExpended {
                optionViewList.forEach {
                    $0.isHidden = false
                }
            }else {
                optionViewList.forEach {
                    $0.isHidden = true
                }
            }
            
            vStackView.layoutIfNeeded()
        }
    }
