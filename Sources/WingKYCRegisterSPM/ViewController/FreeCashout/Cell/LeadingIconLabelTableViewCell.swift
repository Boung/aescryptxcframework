//
//  LeadingIconLabelTableViewCell.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 2/8/22.
//

import Foundation
import UIKit

class LeadingIconLabelTableViewCell: BaseTableViewCell {
    
    private let wrapperView = ContainerView(background: .white)
    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .darkGray
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 15)
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        selectionStyle = .none
        
        addSubview(wrapperView)
        wrapperView.fillInSuperView(padding: .init(top: 4, left: 8, bottom: 4, right: 8))
        
        wrapperView.addSubview(iconImageView)
        iconImageView.constrainWidth(constant: 70)
        iconImageView.constrainHeight(constant: 70)
        iconImageView.anchor(top: wrapperView.topAnchor,
                             leading: wrapperView.leadingAnchor,
                             bottom: wrapperView.bottomAnchor,
                             trailing: nil,
                             padding: .init(top: 8, left: 16, bottom: 8, right: 0))
        
        wrapperView.addSubview(titleLabel)
        titleLabel.centerYInSuperview()
        titleLabel.anchor(top: nil,
                          leading: iconImageView.trailingAnchor,
                          bottom: nil,
                          trailing: wrapperView.trailingAnchor,
                          padding: .init(top: 0, left: 16, bottom: 0, right: 16))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        wrapperView.setCornerRadius(radius: 8)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configCell(model: KeyValueModel) {
        iconImageView.image = model.icon
        titleLabel.text = model.value
    }
}
