//
//  SingleLabelTableViewCell.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 18/8/22.
//

import Foundation
import UIKit

class SingleLabelTableViewCell: BaseTableViewCell {
    
    private let descriptionLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        label.scaleToWidth()

        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        
        addSubview(descriptionLabel)
        descriptionLabel.fillInSuperView(padding: .init(top: 8, left: 16, bottom: 8, right: 16))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configCell(_ description: String) {
        descriptionLabel.text = description
    }
    
    func setAlignment(_ align: NSTextAlignment) {
        descriptionLabel.textAlignment = align
    }
    
    func setColor(_ color: UIColor) {
        descriptionLabel.textColor = color
    }
}
