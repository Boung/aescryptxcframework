//
//  ExistingPlanDetailPopupViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/8/22.
//

import Foundation
import UIKit

class ExistingPlanDetailPopupViewController: BaseViewController {
    
    private let containerView = ContainerView(background: .white)
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let descriptionLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "subscribe_existing_plan_description".localize
        label.textAlignment = .center
        label.textColor = .darkGray
        label.font = .boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        
        return label
    }()
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.constrainHeight(constant: UIConstants.screenHeight * 0.2)
        return tableView
    }()
    private let agreeButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "agree".localize)
        button.backgroundColor = .secondaryColor
        button.setTitleColor(.white, for: .normal)
        button.setDefaultHeight()
        
        return button
    }()
    private let dismissButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        
        return button
    }()
    
    private let tableViewCell = "KeyValueTableViewCell"
    private var onAgreeButtonHandler: (() -> Void)?
    private var datasource: [KeyValueModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(KeyValueTableViewCell.self, forCellReuseIdentifier: tableViewCell)
        
        agreeButton.setAction { [weak self] in
            guard let self = self else { return }
            
            guard let onAgreeButtonHandler = self.onAgreeButtonHandler else { return }
            onAgreeButtonHandler()
        }
        
        dismissButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
//        view.backgroundColor = .blurBackground
        
        view.addSubview(containerView)
        containerView.centerInSuperview()
        containerView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 0, left: 16, bottom: 0, right: 0))
        
        containerView.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 24, left: 16, bottom: 24, right: 16))
        
        view.addSubview(dismissButton)
        dismissButton.fillInSuperView()
        
        view.sendSubviewToBack(dismissButton)
        
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(tableView)
        stackView.addArrangedSubview(agreeButton)
        
        agreeButton.setRoundedView()
        containerView.setCornerRadius(radius: 16)
        setupBlurBackground()
    }
    
    func setupController(datasource: [KeyValueModel], onAgreeButtonHandler: @escaping () -> Void) {
        self.datasource = datasource
        self.onAgreeButtonHandler = onAgreeButtonHandler
    }
}

extension ExistingPlanDetailPopupViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCell, for: indexPath) as! KeyValueTableViewCell
        cell.setupKeyValueLabelColor(.lightGray, value: .lightGray)
        cell.configCell(model: datasource[indexPath.row])
        cell.setMaxLines(0, value: 1)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
