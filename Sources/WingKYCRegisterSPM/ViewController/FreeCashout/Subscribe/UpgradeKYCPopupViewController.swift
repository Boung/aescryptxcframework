//
//  UpgradeKYCPopupViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/8/22.
//

import Foundation
import UIKit

class UpgradeKYCPopupViewController: BaseViewController {
    
    private let containerView = ContainerView(background: .white)
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let descriptionLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "subscribe_upgrade_account_description".localize
        label.textAlignment = .center
        label.textColor = .darkGray
        label.font = .boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        
        return label
    }()
    private let upgradeButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "upgrade_account_now".localize)
        button.backgroundColor = .secondaryColor
        button.setTitleColor(.white, for: .normal)
        button.setDefaultHeight()
        
        return button
    }()
    private let upgradeLaterButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "upgrade_account_later".localize)
        button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        button.setTitleColor(.lightGray, for: .normal)
        button.setDefaultHeight()
        
        return button
    }()
    private let dismissButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        
        return button
    }()
    
    private var upgradeNowHandler: (() -> Void)?
    private var upgradeLaterHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        upgradeButton.setAction { [weak self] in
            guard let self = self else { return }
            
            guard let upgradeNowHandler = self.upgradeNowHandler else { return }
            upgradeNowHandler()
        }
        
        upgradeLaterButton.setAction { [weak self] in
            guard let self = self else { return }
            
            guard let upgradeLaterHandler = self.upgradeLaterHandler else { return }
            upgradeLaterHandler()
        }
        
        dismissButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func generateUI() {
        super.generateUI()
     
//        view.backgroundColor = .blurBackground
        
        view.addSubview(containerView)
        containerView.centerInSuperview()
        containerView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 0, left: 16, bottom: 0, right: 0))
        
        containerView.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 24, left: 16, bottom: 24, right: 16))
        
        view.addSubview(dismissButton)
        dismissButton.fillInSuperView()
        
        view.sendSubviewToBack(dismissButton)
        
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(upgradeButton)
        stackView.addArrangedSubview(upgradeLaterButton)
        
        containerView.setCornerRadius(radius: 16)
        upgradeButton.setRoundedView()
        upgradeLaterButton.setRoundedView()
        setupBlurBackground()
    }
    
    func setUpgradeNowAction(_ handler: @escaping () -> Void) {
        self.upgradeNowHandler = handler
    }
    
    func setUpgradeLaterAction(_ handler: @escaping () -> Void) {
        self.upgradeLaterHandler = handler
    }
}
