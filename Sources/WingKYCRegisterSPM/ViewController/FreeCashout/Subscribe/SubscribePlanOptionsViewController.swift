//
//  SubscribePlanOptionsViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/8/22.
//

import Foundation
import UIKit

class SubscribePlanOptionsViewController: BaseViewController {
    
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "select_below_plan".localize
        label.textColor = .darkGray
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 18)
        
        return label
    }()
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        return tableView
    }()
    private let agreementView = AgreementView()
    private let continueButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "continue".localize)
        button.backgroundColor = .secondaryColor
        button.setTitleColor(.white, for: .normal)
        button.setDefaultHeight()
        
        return button
    }()
    
    private let tableViewCell = "FreeCashoutPlanOptionTableViewCell"
    private var datasource: [PlanOptionTypeDatasource] = [] {
        didSet {
            if datasource.isEmpty { return }
            tableView.reloadData()
        }
    }
    private var selectedOptionType: PlanOptionTypeListResponse?
    
    var upgradeKYCHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "subscribe".localize
        
        getOptionTypeList()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(FreeCashoutPlanOptionTableViewCell.self, forCellReuseIdentifier: tableViewCell)
        
        agreementView.setDescriptionWithAction(descriptionString: "ខ្ញុំ \(Constants.authentication.agentAccount.agentName). សូមបញ្ជាក់ថាអតិថិជនពិតជាបានអាននូវ ", highlightText: "ខចែង និងលក្ខខណ្ឌ", additionalDescription: "សម្រាប់ការភ្ជាប់គម្រោង") { [weak self] in
            guard let self = self else { return }
            
            let vc = BaseWebViewController()
            vc.titleString = "ខចែង និងលក្ខខណ្ឌ"
            vc.urlString = Constants.fcoTermAndConditionURLString
            vc.isPullRefreshAvailable = false
            vc.setButtonAction { [weak self] agreed in
                guard let self = self else { return }
                
                self.agreementView.isAgreed = agreed
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        continueButton.setAction { [weak self] in
            guard let self = self else { return }
            guard let selectedOptionType = self.selectedOptionType else {
                self.showSingleButtonAlert(title: "selection".localize, message: "select_option_type_alert_message".localize, buttonTitle: "done".localize) { [weak self] in
                    guard let self = self else { return }
                    self.dismiss(animated: true, completion: nil)
                }
                return
            }
            
            if !self.agreementView.isAgreed {
                self.showSingleButtonAlert(title: "error".localize, message: "fco_agree_term_condition_alert_message".localize, buttonTitle: "done".localize) { [weak self] in
                    guard let self = self else { return }
                    self.dismiss(animated: true, completion: nil)
                }
                return
            }
            
            let vc = CustomerAccountInputViewController()
            vc.navigationTitle = "subscribe".localize
            vc.isSubscribe = true
            vc.selectedPlanOptionType = selectedOptionType
            vc.upgradeKYCHandler = self.upgradeKYCHandler
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(titleLabel)
        titleLabel.anchor(top: view.layoutMarginsGuide.topAnchor,
                          leading: view.leadingAnchor,
                          bottom: nil,
                          trailing: view.trailingAnchor,
                          padding: .init(top: 16, left: 16, bottom: 0, right: 16))
        
        view.addSubview(tableView)
        tableView.anchor(top: titleLabel.bottomAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        
        view.addSubview(agreementView)
        agreementView.anchor(top: tableView.bottomAnchor,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 16, left: 16, bottom: 0, right: 16))
        
        view.addSubview(continueButton)
        continueButton.anchor(top: agreementView.bottomAnchor,
                              leading: view.leadingAnchor,
                              bottom: view.layoutMarginsGuide.bottomAnchor,
                              trailing: view.trailingAnchor,
                              padding: .init(top: 8, left: 16, bottom: 16, right: 16))
        
        continueButton.setRoundedView()
    }
    
    private func getOptionTypeList() {
        let presenter = PlanOptionTypeListPresenter(delegate: self)
        presenter.getPlanOptionTypeList(currency: "USD")
    }
}

extension SubscribePlanOptionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCell, for: indexPath) as! FreeCashoutPlanOptionTableViewCell
        cell.configCell(model: datasource[indexPath.row])
        cell.setExpendButtonAction { [weak self] in
            guard let self = self else { return }
            
            self.datasource[indexPath.row].isExpended.toggle()
            tableView.reloadWithAnimation()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        datasource.forEach { $0.isSelected = false }
        
        datasource[indexPath.row].isSelected = true
        selectedOptionType = datasource[indexPath.row].planOption
        
        tableView.reloadData()
    }
}

extension SubscribePlanOptionsViewController: PlanOptionTypeListDelegate {
    func response(planOptionList response: [PlanOptionTypeListResponse]?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        self.datasource = response.map { .init(planOption: $0) }
    }
}
