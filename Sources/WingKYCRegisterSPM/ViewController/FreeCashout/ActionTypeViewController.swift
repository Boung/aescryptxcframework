//
//  ActionTypeViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 2/8/22.
//

import Foundation
import UIKit

class ActionTypeViewController: BaseViewController {
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        return tableView
    }()
    
    private let tableViewCell = "LeadingIconLabelTableViewCell"
    private let datasource: [KeyValueModel] = [
        KeyValueModel(icon: .localImage("ic_subscribe"), value: "subscribe".localize),
        KeyValueModel(icon: .localImage("ic_unsubscribe"), value: "un_subscribe".localize),
        KeyValueModel(icon: .localImage("ic_report"), value: "plan_report".localize),
    ]
    
    var upgradeKYCHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "title_plan".localize
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(LeadingIconLabelTableViewCell.self, forCellReuseIdentifier: tableViewCell)
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(tableView)
        tableView.fillInSuperView(padding: .init(top: 4, left: 0, bottom: 4, right: 0))
    }
}

extension ActionTypeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCell, for: indexPath) as! LeadingIconLabelTableViewCell
        cell.configCell(model: datasource[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = SubscribePlanOptionsViewController()
            vc.upgradeKYCHandler = upgradeKYCHandler
            
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = CustomerAccountInputViewController()
            vc.navigationTitle = "un_subscribe".localize
            vc.isSubscribe = false
            
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc = PlanReportViewController()
            
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
}
