//
//  ClickableAgreementView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/12/22.
//

import UIKit

class ClickableAgreementView: UIView {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_deselected_checkbox").withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .primaryColor
        imageView.contentMode = .scaleAspectFit
        imageView.constrainWidth(constant: 25)
        imageView.constrainHeight(constant: 25)
        
        return imageView
    }()
    
    var isAgreed: Bool = false {
        didSet {
            let selectedImage: UIImage = .localImage("ic_selected_checkbox").withRenderingMode(.alwaysTemplate)
            let deSelectedImage: UIImage = .localImage("ic_deselected_checkbox").withRenderingMode(.alwaysTemplate)
            
            UIView.animate(withDuration: 0.2, delay: 0, options: .transitionCrossDissolve) { [weak self] in
                guard let self = self else { return }
                
                self.iconImageView.alpha = 0
            } completion: { [weak self] _ in
                guard let self = self else { return }
                
                self.iconImageView.image = self.isAgreed ? selectedImage : deSelectedImage
                self.iconImageView.alpha = 1
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
