//
//  LabelContainerView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 5/8/22.
//

import Foundation
import UIKit

class LabelContainerView: UIView {
    
    private let label: BaseLabel = {
        let label = BaseLabel()
        label.textAlignment = .center
        label.textColor = .darkGray
        label.font = .systemFont(ofSize: 15)
        
        return label
    }()
    
    init(padding: UIEdgeInsets) {
        super.init(frame: .zero)
        
        addSubview(label)
        label.fillInSuperView(padding: padding)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setText(_ string: String) {
        label.text = string
    }
    
    func setText(_ character: Character?) {
        guard let character = character else { return }
        label.text = String("\(character)")
    }
    
    func configLabel(callBack: (BaseLabel) -> Void) {
        callBack(label)
    }
    
    func setBackgroundColor(_ color: UIColor?) {
        backgroundColor = color
    }
}
