//
//  MonthlyReportDetailView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation
import UIKit

class MonthlyReportDetailView: UIView {
    
    private let containerView = ContainerView(background: .white)
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let totalTransactionView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.darkGray, value: .darkGray)
        view.setTextAlign(.left, value: .right)
        view.setFont(.boldSystemFont(ofSize: 15), value: .systemFont(ofSize: 15))
        
        return view
    }()
    private let totalSubscribeView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.darkGray, value: .darkGray)
        view.setTextAlign(.left, value: .right)
        view.setFont(.boldSystemFont(ofSize: 15), value: .systemFont(ofSize: 15))
        
        return view
    }()
    private let totalUnsubscribeView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setColor(.darkGray, value: .darkGray)
        view.setTextAlign(.left, value: .right)
        view.setFont(.boldSystemFont(ofSize: 15), value: .systemFont(ofSize: 15))
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        addSubview(containerView)
        containerView.fillInSuperView()
     
        containerView.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        stackView.addArrangedSubview(totalTransactionView)
        stackView.addArrangedSubview(totalSubscribeView)
        stackView.addArrangedSubview(totalUnsubscribeView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerView.setCornerRadius(radius: 16)
        setupShadowView(x: 1, y: 1, color: .lightGray, shadowRadius: 16, radius: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetail(totalTransaction: String, totalSubscribe: String, totalUnsubscribe: String) {
        totalTransactionView.setupDetails("total_transaction_report".localize, value: totalTransaction)
        totalSubscribeView.setupDetails("total_subscribe_report".localize, value: totalSubscribe)
        totalUnsubscribeView.setupDetails("total_unsubscribe_report".localize, value: totalUnsubscribe)
    }
}
