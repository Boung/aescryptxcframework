//
//  RemainingBenefitProgressView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/8/22.
//

import Foundation
import UIKit

class RemainingBenefitProgressView: UIView {
    
    private let descriptionLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .lightGray
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 13)
        label.numberOfLines = 2
        
        return label
    }()
    private let progressView: UIProgressView = {
        let progressView = UIProgressView()
        progressView.trackTintColor = UIColor.lightGray.withAlphaComponent(0.3)
        
        return progressView
    }()
    private let benefitValueLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .darkGray
        label.textAlignment = .right
        label.font = .boldSystemFont(ofSize: 15)
        
        return label
    }()
    
    private var progress = Progress()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(descriptionLabel)
        descriptionLabel.anchor(top: topAnchor,
                                leading: leadingAnchor,
                                bottom: nil,
                                trailing: trailingAnchor)
        
        addSubview(progressView)
        progressView.anchor(top: descriptionLabel.bottomAnchor,
                            leading: descriptionLabel.leadingAnchor,
                            bottom: nil,
                            trailing: descriptionLabel.trailingAnchor,
                            padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        
        addSubview(benefitValueLabel)
        benefitValueLabel.anchor(top: progressView.bottomAnchor,
                                 leading: descriptionLabel.leadingAnchor,
                                 bottom: bottomAnchor,
                                 trailing: descriptionLabel.trailingAnchor,
                                 padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        
        progressView.transform = .init(scaleX: 1, y: 2)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let floatValue = Float(progress.fractionCompleted)
        progressView.setProgress(floatValue, animated: true)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setDescription(_ string: String, valueText: String,  maxValue: CGFloat, currentValue: CGFloat) {
        descriptionLabel.text = string
        benefitValueLabel.text = valueText
        
        progress = Progress(totalUnitCount: Int64(maxValue))
        progress.completedUnitCount = Int64(maxValue - (maxValue - currentValue))
        
        print("=== \(maxValue)")
        print("=== \(currentValue)")
        print(maxValue - (maxValue - currentValue))
    }
    
    func setProgressColor(color: UIColor) {
        progressView.progressTintColor = color
    }
}
