//
//  PlanReportViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/8/22.
//

import Foundation
import UIKit

class PlanReportViewController: BaseViewController {
    
    private let segmentedController: UISegmentedControl = {
        let segmentedController = UISegmentedControl()
        segmentedController.insertSegment(withTitle: "plan_report".localize, at: 0, animated: true)
        segmentedController.insertSegment(withTitle: "plan_transaction".localize, at: 1, animated: true)
        segmentedController.selectedSegmentIndex = 0
        
        if #available(iOS 13.0, *) {
            segmentedController.selectedSegmentTintColor = .primaryColor
            segmentedController.backgroundColor = .white
            segmentedController.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
            segmentedController.setTitleTextAttributes([.foregroundColor: UIColor.darkGray], for: .normal)
        }else {
            segmentedController.tintColor = .primaryColor
            segmentedController.backgroundColor = .primaryColor
            segmentedController.setTitleTextAttributes([.foregroundColor: UIColor.darkGray], for: .selected)
            segmentedController.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .normal)
        }
        
        return segmentedController
    }()
    private let containerView = ContainerView(background: .clear)
    private lazy var monthlyReportViewController: MonthlyReportViewController = {
        let viewController = MonthlyReportViewController()
        
        add(asChildViewController: viewController)
        return viewController
    }()
    private lazy var reportTransactionViewController: ReportTransactionViewController = {
        let viewController = ReportTransactionViewController()
        
        add(asChildViewController: viewController)
        return viewController
    }()
    
    private var selectedSegmentedIndex: Int {
        return segmentedController.selectedSegmentIndex
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        navigationItem.title = "plan_report".localize
        
        getReport()
        segmentedController.addTarget(self, action: #selector(onSegmentedControllerValueChange), for: .valueChanged)
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(segmentedController)
        segmentedController.constrainHeight(constant: 50)
        segmentedController.anchor(top: view.layoutMarginsGuide.topAnchor,
                                   leading: view.leadingAnchor,
                                   bottom: nil,
                                   trailing: view.trailingAnchor,
                                   padding: .init(top: 16, left: 16, bottom: 0, right: 16))
        
        view.addSubview(containerView)
        containerView.anchor(top: segmentedController.bottomAnchor,
                             leading: view.leadingAnchor,
                             bottom: view.layoutMarginsGuide.bottomAnchor,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 16, left: 0, bottom: 0, right: 0))
    }
    
    @objc private func onSegmentedControllerValueChange() {
        observeController()
    }
}

extension PlanReportViewController {
    private func observeController() {
        if selectedSegmentedIndex == 0 {
            remove(asChildViewController: reportTransactionViewController)
            add(asChildViewController: monthlyReportViewController)
        } else {
            remove(asChildViewController: monthlyReportViewController)
            add(asChildViewController: reportTransactionViewController)
        }
    }
    
    private func add(asChildViewController viewController: BaseViewController) {
        addChild(viewController)
        containerView.addSubview(viewController.view)

        viewController.view.fillInSuperView()
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: BaseViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
}

extension PlanReportViewController: TransactionReportDelegate {
    func response(transactionReport response: TransactionReportResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        monthlyReportViewController.monthlyReport = response.transactionReport
        
        NotificationCenter.default.post(name: .setReportMonthlyReport, object: nil)
    }
    
    private func getReport() {
        let presenter = TransactionReportPresenter(delegate: self)
        let param = TransactionReportParam(page: 1)
        
        presenter.getTransactionReport(param: param)
    }
}
