//
//  ReportTransactionViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation
import UIKit

class ReportTransactionViewController: BaseViewController {
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        return tableView
    }()
    private let noRecordLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "no_transaction_list".localize
        label.textAlignment = .center
        label.textColor = .darkGray
        label.font = .boldSystemFont(ofSize: 15)
        label.isHidden = true
        
        return label
    }()
    private var pullControl: UIRefreshControl = {
        let pullControl = UIRefreshControl()
        pullControl.tintColor = .primaryColor
        pullControl.attributedTitle = NSAttributedString(string: "button_label_loading".localize)
        
        return pullControl
    }()
    
    private let tableViewCell = "ReportTransactionTableViewCell"
    private var totalPage = 0
    private var currentPage = 1
    private var datasource: [PlanTransactionListModel] = [] {
        didSet {
            noRecordLabel.isHidden = !datasource.isEmpty
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        getReport()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ReportTransactionTableViewCell.self, forCellReuseIdentifier: tableViewCell)
        
        pullControl.addTarget(self, action: #selector(onPullRefreshHandler), for: .valueChanged)
    }
        
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(tableView)
        tableView.fillInSuperView()
        
        tableView.addSubview(pullControl)
        
        view.addSubview(noRecordLabel)
        noRecordLabel.centerInSuperview()
        noRecordLabel.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 0, left: 16, bottom: 0, right: 0))
    }
    
    @objc func onPullRefreshHandler() {
        currentPage = 1
        getReport()
    }
    
    private func getReport() {
        let presenter = TransactionReportPresenter(delegate: self)
        let param = TransactionReportParam(page: currentPage)
        
        presenter.getTransactionList(param: param)
    }
}

extension ReportTransactionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCell, for: indexPath) as! ReportTransactionTableViewCell
        cell.configCell(model: datasource[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == datasource.count - 1 {
            if currentPage < totalPage {
                currentPage += 1
                getReport()
            }
        }
    }
}

extension ReportTransactionViewController: TransactionListDelegate {
    func response(transactionReport response: TransactionReportResponse?, error: Error?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            guard let self = self else { return }
            
            self.pullControl.endRefreshing()
        }
        
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        self.totalPage = response.totalPage
        
        if currentPage == 1 {
            self.datasource = response.transactionList ?? []
        }else {
            self.datasource.append(contentsOf: response.transactionList ?? [])
        }
    }
}
