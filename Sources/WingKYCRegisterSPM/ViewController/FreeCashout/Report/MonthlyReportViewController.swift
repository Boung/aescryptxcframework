//
//  MonthlyReportViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation
import UIKit

class MonthlyReportViewController: BaseViewController {
    
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "report_summary_monthly".localize
        label.textAlignment = .left
        label.textColor = .darkGray
        label.font = .boldSystemFont(ofSize: 18)
        
        return label
    }()
    private let tableContainerView = ContainerView(background: .white)
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        return tableView
    }()
    private let noRecordLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "no_monthly_report".localize
        label.textAlignment = .center
        label.textColor = .darkGray
        label.font = .boldSystemFont(ofSize: 15)
        label.isHidden = true
        
        return label
    }()
    
    private let tableViewCell = "KeyValueTableViewCell"
    private let estimateRowHeight: CGFloat = 44
    var monthlyReport: [PlanTransactionReportModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.estimatedRowHeight = estimateRowHeight
        tableView.register(KeyValueTableViewCell.self, forCellReuseIdentifier: tableViewCell)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setReportDetail), name: .setReportMonthlyReport, object: nil)
    }
    
    override func generateUI() {
        super.generateUI()

        view.addSubview(titleLabel)
        titleLabel.anchor(top: view.layoutMarginsGuide.topAnchor,
                          leading: view.leadingAnchor,
                          bottom: nil,
                          trailing: view.trailingAnchor,
                          padding: .init(top: 16, left: 16, bottom: 0, right: 16))
        
        view.addSubview(tableContainerView)
        tableContainerView.anchor(top: titleLabel.bottomAnchor,
                                  leading: titleLabel.leadingAnchor,
                                  bottom: nil,
                                  trailing: titleLabel.trailingAnchor,
                                  padding: .init(top: 16, left: 0, bottom: 0, right: 0))
        
        tableContainerView.addSubview(tableView)
        tableView.fillInSuperView(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        view.addSubview(noRecordLabel)
        noRecordLabel.centerInSuperview()
        noRecordLabel.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 0, left: 16, bottom: 0, right: 0))
        
        tableContainerView.setCornerRadius(radius: 16)
    }
    
    @objc private func setReportDetail() {
        guard let monthlyReport = monthlyReport else { return }
        
        tableContainerView.constrainHeight(constant: estimateRowHeight * CGFloat(monthlyReport.count))
        tableView.reloadData()
        
        tableContainerView.isHidden = monthlyReport.isEmpty
        noRecordLabel.isHidden = !monthlyReport.isEmpty
    }
}

extension MonthlyReportViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return monthlyReport?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCell, for: indexPath) as! KeyValueTableViewCell
        if let model = monthlyReport?[indexPath.row].toKeyValueValue() {
            cell.configCell(model: model)
            cell.setupKeyValueLabelColor(.darkGray, value: .darkGray)
            cell.selectionStyle = .none
        }
        
        return cell
    }
}
