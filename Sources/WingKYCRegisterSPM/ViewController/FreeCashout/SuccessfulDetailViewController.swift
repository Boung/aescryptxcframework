//
//  SuccessfulDetailViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/8/22.
//

import Foundation
import UIKit

class SuccessfulDetailViewController: BaseViewController {
    
    private let containerView = ContainerView(background: .primaryColor)
    private let headerContainerView = ContainerView(background: .white)
    private let successIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_success").withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .primaryColor
        imageView.contentMode = .scaleAspectFit
        imageView.constrainWidth(constant: 100)
        imageView.constrainHeight(constant: 100)
        
        return imageView
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "transaction_success".localize
        label.textAlignment = .center
        label.textColor = .secondaryColor
        label.font = .boldSystemFont(ofSize: 18)
        
        return label
    }()
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        return tableView
    }()
    private let doneButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .secondaryColor
        button.setTitle(string: "done".localize)
        button.setDefaultHeight()
        return button
    }()
    
    private let tableViewCell = "KeyValueTableViewCell"
    private var datasource: [KeyValueModel] = []
    private var doneButtonHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(KeyValueTableViewCell.self, forCellReuseIdentifier: tableViewCell)
        
        doneButton.setAction { [weak self] in
            guard let self = self else { return }
            
            guard let doneButtonHandler = self.doneButtonHandler else { return }
            doneButtonHandler()
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
//        view.backgroundColor = .blurBackground
        
        view.addSubview(containerView)
        containerView.centerInSuperview()
        containerView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: nil,
                             padding: .init(top: 0, left: 16, bottom: 0, right: 0))
        
        containerView.addSubview(headerContainerView)
        headerContainerView.anchor(top: containerView.topAnchor,
                                   leading: containerView.leadingAnchor,
                                   bottom: nil,
                                   trailing: containerView.trailingAnchor)
        
        headerContainerView.addSubview(successIconImageView)
        successIconImageView.centerXInSuperview()
        successIconImageView.anchor(top: headerContainerView.topAnchor,
                                    leading: nil,
                                    bottom: nil,
                                    trailing: nil,
                                    padding: .init(top: 24, left: 0, bottom: 0, right: 0))
        
        headerContainerView.addSubview(titleLabel)
        titleLabel.anchor(top: successIconImageView.bottomAnchor,
                          leading: headerContainerView.leadingAnchor,
                          bottom: headerContainerView.bottomAnchor,
                          trailing: headerContainerView.trailingAnchor,
                          padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        containerView.addSubview(tableView)
        tableView.constrainHeight(constant: UIConstants.screenHeight * 0.3)
        tableView.anchor(top: headerContainerView.bottomAnchor,
                         leading: containerView.leadingAnchor,
                         bottom: nil,
                         trailing: containerView.trailingAnchor,
                         padding: .init(top: 8, left: 16, bottom: 0, right: 16))
        
        containerView.addSubview(doneButton)
        doneButton.anchor(top: tableView.bottomAnchor,
                          leading: containerView.leadingAnchor,
                          bottom: containerView.bottomAnchor,
                          trailing: containerView.trailingAnchor,
                          padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        doneButton.setRoundedView()
        setupBlurBackground()
    }
    
    func setupController(datasource: [KeyValueModel], donebuttonActionHandler: @escaping () -> Void) {
        self.datasource = datasource
        self.doneButtonHandler = donebuttonActionHandler
    }
}

extension SuccessfulDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCell, for: indexPath) as! KeyValueTableViewCell
        cell.configCell(model: datasource[indexPath.row])
        cell.setMaxLines(0, value: 0)
        
        return cell
    }
}
