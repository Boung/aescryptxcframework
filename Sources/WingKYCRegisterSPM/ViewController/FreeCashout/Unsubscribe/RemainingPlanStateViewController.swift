//
//  RemainingPlanStateViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/8/22.
//

import Foundation
import UIKit

class RemainingPlanStateViewController: BaseViewController {
    
    private let containerView = ContainerView(background: .white)
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "remaining_plan_benefit".localize
        label.textColor = .darkGray
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 18)
        
        return label
    }()
    private let dismissButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear

        return button
    }()
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private var progressViewList: [RemainingBenefitProgressView] = []
    
    var limitationResponse: LimitationResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupProgressView()
        
        dismissButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
//        view.backgroundColor = .blurBackground
        
        view.addSubview(containerView)
        containerView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: view.bottomAnchor,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        
        containerView.addSubview(stackView)
        stackView.anchor(top: containerView.topAnchor,
                         leading: containerView.leadingAnchor,
                         bottom: view.layoutMarginsGuide.bottomAnchor,
                         trailing: containerView.trailingAnchor,
                         padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        view.addSubview(dismissButton)
        dismissButton.anchor(top: view.topAnchor,
                             leading: view.leadingAnchor,
                             bottom: containerView.topAnchor,
                             trailing: view.trailingAnchor)
        
        containerView.setupCornerRadiusOnly(corners: [.topLeft, .topRight], radius: 16)
        setupBlurBackground()
    }
    
    private func setupProgressView() {
        guard let datas = limitationResponse?.datas else { return }
        stackView.arrangedSubviews.forEach {
            stackView.removeArrangedSubview($0)
        }
        
        stackView.addArrangedSubview(titleLabel)
        datas.forEach {
            let view = RemainingBenefitProgressView()
            view.setDescription($0.toLocalizedLabel(), valueText: $0.toLocalizedValueLabel(), maxValue: $0.maxValueFloat, currentValue: $0.currentValueFloat)
            view.setProgressColor(color: UIColor(hexString: $0.color ?? ""))
            
            stackView.addArrangedSubview(view)
        }
    }
}
