//
//  UnsubscribeDetailViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/8/22.
//

import Foundation
import UIKit

class UnsubscribeDetailViewController: BaseViewController {
    
    private let containerView = ContainerView(background: .white)
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "unsbuscribe_customer_plan".localize
        label.textColor = .darkGray
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 18)
        
        return label
    }()
    private let planTypeView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.stackView.alignment = .top
        view.setTextAlign(.left, value: .right)
        view.setColor(.darkGray, value: .darkGray)
        view.setNumberOfLine(1, value: 0)
        
        return view
    }()
    private let customerAccountView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.stackView.alignment = .top
        view.setTextAlign(.left, value: .right)
        view.setColor(.darkGray, value: .darkGray)
        view.setNumberOfLine(1, value: 0)
        
        return view
    }()
    private let customerNameView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.stackView.alignment = .top
        view.setTextAlign(.left, value: .right)
        view.setColor(.darkGray, value: .darkGray)
        
        return view
    }()
    private let customerPhoneNumberView: KeyValueLabelView = {
        let view = KeyValueLabelView()
        view.stackView.axis = .horizontal
        view.setTextAlign(.left, value: .right)
        view.setColor(.darkGray, value: .darkGray)
        
        return view
    }()
    private let noteLabel: BaseLabel = {
        let label = BaseLabel()
        label.text = "unsubscribe_notes".localize
        label.textAlignment = .left
        label.textColor = .lightGray
        label.font = .systemFont(ofSize: 15)
        label.numberOfLines = 0
        
        return label
    }()
    private let remainingButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        button.setTitleColor(.secondaryColor, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 13)
        button.setTitle(string: "view_remaining_plan".localize)
        
        return button
    }()
    private let agreementView: AgreementView = {
        let view = AgreementView()
        
        return view
    }()
    private let continueButton: BaseButton = {
        let button = BaseButton()
        button.setTitle(string: "continue".localize)
        button.backgroundColor = .secondaryColor
        button.setDefaultHeight()
        
        return button
    }()
    
    var unsubscribeValidateResponse: UnsubscribeValidateResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupInformationDetail()
        
        agreementView.setDescriptionWithAction(descriptionString: "ខ្ញុំ \(Constants.authentication.agentAccount.agentName). សូមបញ្ជាក់ថាអតិថិជនពិតជាបានអាននូវ ", highlightText: "ខចែង និងលក្ខខណ្ឌ", additionalDescription: "សម្រាប់ការផ្តាច់គម្រោង") { [weak self] in
            guard let self = self else { return }
            
            let vc = BaseWebViewController()
            vc.titleString = "ខចែង និងលក្ខខណ្ឌ"
            vc.urlString = Constants.fcoTermAndConditionURLString
            vc.isPullRefreshAvailable = false
            vc.setButtonAction { [weak self] agreed in
                guard let self = self else { return }
                
                self.agreementView.isAgreed = agreed
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        remainingButton.setAction { [weak self] in
            guard let self = self else { return }
            
            let presenter = LimitationPresenter(delegate: self)
            let param = LimitationParam(customerWingAccount: self.unsubscribeValidateResponse?.initiatorWingAccount ?? "", currency: self.unsubscribeValidateResponse?.currency ?? "")
            
            presenter.getLimitation(param: param)
        }
        
        continueButton.setAction { [weak self] in
            guard let self = self else { return }
            
            if !self.agreementView.isAgreed {
                self.showSingleButtonAlert(title: "error".localize, message: "error_please_agree_the_valid_information".localize, buttonTitle: "done".localize) { [weak self] in
                    guard let self = self else { return }
                    
                    self.dismiss(animated: true, completion: nil)
                }
                return
            }
            
            let customerDatasource: [KeyValueModel] = [
                .init(key: "plan_transaction_type".localize, value: self.unsubscribeValidateResponse?.transactionType ?? ""),
                .init(key: "plan_type".localize, value: self.unsubscribeValidateResponse?.toLocalizedPlanType() ?? ""),
                .init(key: "plan_connected_account".localize, value: self.unsubscribeValidateResponse?.initiatorWingAccount ?? ""),
                .init(key: "unsubscribe_note_customer_pin".localize, value: ""),
            ]
            
            self.showPinViewController(placeholder: "input_customer_pin".localize, datasource: customerDatasource) { [weak self] customerPin in
                guard let self = self else { return }
                
                self.dismiss(animated: true) { [weak self] in
                    guard let self = self else { return }
                    
                    let wcxDatasource: [KeyValueModel] = [
                        .init(key: "plan_transaction_type".localize, value: self.unsubscribeValidateResponse?.transactionType ?? ""),
                        .init(key: "plan_type".localize, value: self.unsubscribeValidateResponse?.toLocalizedPlanType() ?? ""),
                        .init(key: "plan_connected_account".localize, value: self.unsubscribeValidateResponse?.initiatorWingAccount ?? ""),
                        .init(key: "plan_customer_name".localize, value: self.unsubscribeValidateResponse?.fullNameKh ?? self.unsubscribeValidateResponse?.fullNameEn ?? ""),
                        .init(key: "plan_customer_phone_number".localize, value: self.unsubscribeValidateResponse?.phoneNumber ?? ""),
                        .init(key: "wcx_fee".localize, value: self.unsubscribeValidateResponse?.wcxFee ?? ""),
                    ]
                    
                    self.showPinViewController(placeholder: "input_wcx_pin".localize, datasource: wcxDatasource) { [weak self] wcxPin in
                        guard let self = self else { return }
                        
                        let presenter = UnsubscribeCommitPresenter(delegate: self)
                        let param = UnsubscribeCommitParam(sessionID: self.unsubscribeValidateResponse?.sessionID ?? "",
                                                           customerAccountNumber: self.unsubscribeValidateResponse?.initiatorWingAccount ?? "",
                                                           customerPin: customerPin,
                                                           wcxPin: wcxPin,
                                                           currency: self.unsubscribeValidateResponse?.currency ?? "",
                                                           subType: self.unsubscribeValidateResponse?.subType ?? "")
                        
                        presenter.commitUnsubscribe(param: param)
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = "un_subscribe".localize
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(containerView)
        containerView.anchor(top: view.layoutMarginsGuide.topAnchor,
                             leading: view.leadingAnchor,
                             bottom: nil,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 16, left: 16, bottom: 0, right: 16))
        
        containerView.addSubview(stackView)
        stackView.fillInSuperView(padding: .init(top: 16, left: 8, bottom: 16, right: 8))
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(planTypeView)
        stackView.addArrangedSubview(customerAccountView)
        stackView.addArrangedSubview(customerNameView)
        stackView.addArrangedSubview(customerPhoneNumberView)
        
        view.addSubview(noteLabel)
        noteLabel.anchor(top: containerView.bottomAnchor,
                         leading: containerView.leadingAnchor,
                         bottom: nil,
                         trailing: containerView.trailingAnchor,
                         padding: .init(top: 16, left: 0, bottom: 0, right: 0))
        
        view.addSubview(remainingButton)
        remainingButton.anchor(top: noteLabel.bottomAnchor,
                               leading: noteLabel.leadingAnchor,
                               bottom: nil,
                               trailing: noteLabel.trailingAnchor,
                               padding: .init(top: 16, left: 0, bottom: 0, right: 0))
        
        view.addSubview(continueButton)
        continueButton.anchor(top: nil,
                              leading: view.leadingAnchor,
                              bottom: view.layoutMarginsGuide.bottomAnchor,
                              trailing: view.trailingAnchor,
                              padding: .init(top: 0, left: 16, bottom: 16, right: 16))
        
        view.addSubview(agreementView)
        agreementView.greaterOrEqualTo(top: remainingButton.bottomAnchor, constant: 16)
        agreementView.anchor(top: nil,
                             leading: continueButton.leadingAnchor,
                             bottom: continueButton.topAnchor,
                             trailing: continueButton.trailingAnchor,
                             padding: .init(top: 0, left: 0, bottom: 8, right: 0))
        
        continueButton.setRoundedView()
        containerView.setCornerRadius(radius: 8)
    }
}

extension UnsubscribeDetailViewController {
    private func setupInformationDetail() {
        guard let unsubscribeValidateResponse = unsubscribeValidateResponse else { return }
        
        planTypeView.setupDetails("plan_type".localize + "៖", value: unsubscribeValidateResponse.toLocalizedPlanType(), hideOnEmpty: false)
        customerAccountView.setupDetails("plan_connected_account".localize + "៖", value: unsubscribeValidateResponse.initiatorWingAccount ?? "", hideOnEmpty: false)
        customerNameView.setupDetails("plan_customer_name".localize + "៖", value: unsubscribeValidateResponse.toLocalizedCustomerName(), hideOnEmpty: false)
        customerPhoneNumberView.setupDetails("plan_customer_phone_number".localize + "៖", value: unsubscribeValidateResponse.phoneNumber ?? "", hideOnEmpty: false)
    }
    
    private func showPinViewController(placeholder: String, datasource: [KeyValueModel], completion: @escaping (String) -> Void) {
        let vc = ConfirmationPinWithDatasourceViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.setupController(placeholder: placeholder, datasource: datasource, onConfirmHandler: completion)
        
        present(vc, animated: true, completion: nil)
    }
}

extension UnsubscribeDetailViewController: LimitationDelegate {
    func response(limitation response: LimitationResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        
        let vc = RemainingPlanStateViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.limitationResponse = response
        
        self.present(vc, animated: true, completion: nil)
    }
}

extension UnsubscribeDetailViewController: UnsubscribeCommitDelegate {
    func response(unsubscribeCommit response: UnsubscribeCommitResponse?, error: Error?) {
        guard error == nil else {
            handleError(error)
            return
        }
        
        guard let response = response else { return }
        
        self.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            
            let datasource: [KeyValueModel] = response.toDatasource()
            let vc = SuccessfulDetailViewController()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.setupController(datasource: datasource) { [weak self] in
                guard let self = self else { return }
                
                self.dismiss(animated: true) { [weak self] in
                    guard let self = self else { return }
                    
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
            
            self.present(vc, animated: true, completion: nil)
        }
    }
}
