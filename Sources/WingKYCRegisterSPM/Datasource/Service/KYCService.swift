//
//  KYCService.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 12/11/21.
//

import Foundation

class KYCService: BaseService {
    func getCountry(completion: @escaping (Error?) -> Void) {
        request([CountryResponse].self, api: .countryList, shouldShowLoading: false) { response, error in
            guard error == nil else {
                return completion(error)
            }

            guard let response = response else {
                return completion(error)
            }
            
            Constants.countryListResponse = response
            print("\n=======> Country Datasource")
            print(response.map { "\($0.isoCode ?? "") -> \($0.country ?? "") = \($0.nationality ?? "")" }.joined(separator: ",\n"))
            completion(nil)
        }
    }
    
    func getRegion(param: String, completion: @escaping (RegionResponse?, Error?) -> Void) {
        request(RegionResponse.self, api: .regionList(regionID: param), shouldShowLoading: false) { response, error in
            completion(response, error)
        }
    }
    
    func uploadImage(param: UploadImageParam, completion: @escaping (UploadImageResponse?, Error?) -> Void) {
        request(UploadImageResponse.self, api: .uploadImage(param: param), retryAttempt: 5) { response, error in
            completion(response, error)
        }
    }
    
    func getRegionIDByName(param: GetRegionIDByNameParam, completion: @escaping (GetRegionIDByNameResponse?) -> Void) {
        request(GetRegionIDByNameResponse.self, api: .getRegionIDByName(param: param)) { response, _ in
            completion(response)
        }
    }
    
    func getDebitCardInformation(param: String, completion: @escaping (DebitCardInformationResponse?, Error?) -> Void) {
        request(DebitCardInformationResponse.self, api: .getDebitCardInformation(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func validateKitNumber(param: String, completion: @escaping (ValidateKitNumberResponse?, Error?) -> Void) {
        request(ValidateKitNumberResponse.self, api: .validateKitNumber(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func getUUID(completion: @escaping (GetUUIDResponse?, Error?) -> Void) {
        request(GetUUIDResponse.self, api: .getUUID) { response, error in
            completion(response, error)
        }
    }
    
    // MARK: - Single Account
    func getBusinessType(completion: @escaping ([BusinessTypeResponse]?, Error?) -> Void) {
        request([BusinessTypeResponse].self, api: .businessType, shouldShowLoading: false) { response, error in
            completion(response, error)
        }
    }
    
    func getPaymentMethod(completion: @escaping (PaymentMethodResponse?, Error?) -> Void) {
        request(PaymentMethodResponse.self, api: .paymentMethod, shouldShowLoading: false) { response, error in
            completion(response, error)
        }
    }
    
    func validateSingleAccountRegister(param: SingleAccountRegisterValidateParam, completion: @escaping (SingleAccountRegisterValidateResponse?, Error?) -> Void) {
        request(SingleAccountRegisterValidateResponse.self, api: .singleAccountRegisterValidate(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func commitSingleAccountRegister(param: SingleAccountRegisterCommitParam, completion: @escaping (SingleAccountRegisterCommitResponse? , Error?) -> Void) {
        request(SingleAccountRegisterCommitResponse.self, api: .singleAccountRegisterCommit(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func getSingleAccountUUID(param: SingleAccountGetUUIDParam, completion: @escaping (SingleAccountUUIDResponse?, Error?) -> Void) {
        request(SingleAccountUUIDResponse.self, api: .singleAccountGetUUID(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func uploadSingleAccountImages(param: SingleAccountUploadImagesParam, completion: @escaping (Bool) -> Void) {
        request(SingleAccountUploadImageResponse.self, api: .singleAccountUploadImages(param: param)) { response, error in
            let errorCode = response?.errorCode ?? ""
            
            if errorCode == "200" {
                completion(true)
            }else {
                AlertManager.shared.showMessageAlert("failed_to_upload_image_to_server".localize)
                completion(false)
            }
        }
    }
    
    func uploadSingleAccountOptionalImages(param: SingleAccountUploadImagesParam) {
        request(SingleAccountUploadImageResponse.self, api: .singleAccountUploadOptionalImages(param: param)) { _, _ in }
    }
    
    func validatePinSingleAccount(param: SingleAccountPinValidateParam, completion: @escaping (SingleAccountPinValidateResponse?, Error?) -> Void) {
        request(SingleAccountPinValidateResponse.self, api: .singleAccountPinValidation(param: param)) { response, error in
            completion(response, error)
        }
    }
}
