//
//  UpgradeUserService.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation

class UpgradeUserService: BaseService {
    
    func getUserAccountInformation(param: UpgradeUserAccountInformationParam, completion: @escaping (UpgradeUserAccountInformationResponse?, Error?) -> Void) {
        request(UpgradeUserAccountInformationResponse.self, api: .upgradeUserAccountInformation(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func upgradeUserValidate(param: UpgradeUserValidateParam, completion: @escaping (UpgradeUserValidateResponse?, Error?) -> Void) {
        request(UpgradeUserValidateResponse.self, api: .upgradeUserValidate(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func upgradeUserCommit(param: UpgradeUserCommitParam, completion: @escaping (UpgradeUserCommitResponse?, Error?) -> Void) {
        request(UpgradeUserCommitResponse.self, api: .upgradeUserCommit(param: param)) { response, error in
            completion(response, error)
        }
    }
}
