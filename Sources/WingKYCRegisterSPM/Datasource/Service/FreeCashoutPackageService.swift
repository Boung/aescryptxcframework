//
//  FreeCashoutPackageService.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class FreeCashoutPackageService: BaseService {
    func getPlanOptionTypeList(currency: String, completion: @escaping ([PlanOptionTypeListResponse]?, Error?) -> Void) {
        request([PlanOptionTypeListResponse].self, api: .planOptionTypeList(currency: currency)) { response, error in
            completion(response, error)
        }
    }
    
    func getLimitation(param: LimitationParam, completion: @escaping (LimitationResponse?, Error?) -> Void) {
        request(LimitationResponse.self, api: .limitation(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func validateSubscribe(param: SubscribeValidateParam, completion: @escaping (SubscribeValidateResponse?, Error?) -> Void) {
        request(SubscribeValidateResponse.self, api: .subscribePlanValidate(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func commitSubscribe(param: SubscribeCommitParam, completion: @escaping (SubscribeCommitResponse?, Error?) -> Void) {
        request(SubscribeCommitResponse.self, api: .subscribePlanCommit(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func validateUnsubscribe(param: UnsubscribeValidateParam, completion: @escaping (UnsubscribeValidateResponse?, Error?) -> Void) {
        request(UnsubscribeValidateResponse.self, api: .unsubscribePlanValidate(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func commitUnsubscribe(param: UnsubscribeCommitParam, completion: @escaping (UnsubscribeCommitResponse?, Error?) -> Void) {
        request(UnsubscribeCommitResponse.self, api: .unsubscribePlanCommit(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func getTransactionReport(param: TransactionReportParam, completion: @escaping (TransactionReportResponse?, Error?) -> Void) {
        request(TransactionReportResponse.self, api: .transactionReport(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func getTransactionList(param: TransactionReportParam, completion: @escaping (TransactionReportResponse?, Error?) -> Void) {
        request(TransactionReportResponse.self, api: .transactionList(param: param), shouldShowLoading: param.page == 1) { response, error in
            completion(response, error)
        }
    }
}
