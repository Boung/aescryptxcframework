//
//  BaseService.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

private class LoadingViewConstants {
    static let loadingViewController = LoadingViewController()
}

class BaseService: RequestManager {
    
    func showLoading() {
        DispatchQueue.main.async {
            LoadingViewConstants.loadingViewController.dismiss(animated: false) {
                guard let topVC = UIApplication.shared.getTopViewController() else { return }
                
                LoadingViewConstants.loadingViewController.modalPresentationStyle = .overFullScreen
                
                topVC.present(LoadingViewConstants.loadingViewController, animated: false, completion: nil)
            }
        }
    }
    
    func hideLoading(completion: @escaping () -> Void) {
        DispatchQueue.main.async {
            if LoadingViewConstants.loadingViewController.isModal {
                LoadingViewConstants.loadingViewController.dismiss(animated: false) {}
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    completion()
                }
            }else {
                LoadingViewConstants.loadingViewController.dismiss(animated: false)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    completion()
                }
            }
        }
    }
    
    func getAccessToken(shouldShowLoading: Bool = false, completion: @escaping (Error?) -> Void) {
        let jwtKey = "28292A2B2C2D2E2F28292A2B2C2D2E2F"
        request(AuthenticationJWTResponse.self, api: .authentication, shouldShowLoading: shouldShowLoading) { response, error in
            guard error == nil else {
                return completion(error)
            }
            
            guard let response = response else {
                return completion(error)
            }
            
//            do {
//                let jwt = try Jwt.decode(withToken: response.token, andKey: jwtKey, andVerify: true)
//                if let decodedToken = jwt["token"] as? String {
//                    if let decryptedToken = EncodingManager.shared.decode(encoded: decodedToken) {
//                        if let decodedData = decryptedToken.data(using: .utf8) {
//                            if let dict = try JSONSerialization.jsonObject(with: decodedData, options: []) as? [String: Any] {
//                                if let accessToken = dict["access_token"] as? String {
//                                    Constants.accessToken = accessToken
//                                    completion(nil)
//                                }
//                            }
//                        }
//                    }
//                }
//            }catch {
//                completion(error)
//            }
        }
    }
    
    func getEnumeration(completion: @escaping (Error?) -> Void) {
        request(EnumerationResponse.self, api: .enumerations, shouldShowLoading: false) { response, error in
            guard let response = response else {
                return completion(error)
            }
            
            Constants.enumerationResponse = response
            response.printAllComponent()
            completion(nil)
        }
    }
}
