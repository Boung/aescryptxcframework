//
//  RegisterUserService.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation

class RegisterUserService: BaseService {
    func registerUserValidate(param: RegisterUserValidateParam, completion: @escaping (RegisterUserValidateResponse?, Error?) -> Void) {
        request(RegisterUserValidateResponse.self, api: .registerUserValidate(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func registerUserCommit(param: RegisterUserCommitParam, completion: @escaping (RegisterUserCommitResponse?, Error?) -> Void) {
        request(RegisterUserCommitResponse.self, api: .registerUserCommit(param: param)) { response, error in
            completion(response, error)
        }
    }
    
    func registerUserPinValidation(param: RegisterUserPinValidationParam, completion: @escaping (RegisterUserPinValidationResponse?, Error?) -> Void) {
        request(RegisterUserPinValidationResponse.self, api: .registerUserPinValidation(param: param)) { response, error in
            completion(response, error)
        }
    }
}
