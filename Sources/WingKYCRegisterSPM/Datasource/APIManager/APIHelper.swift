//
//  APIHelper.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import Foundation

enum HTTPMethodEnum {
    case get
    case post
    case put
    case delete
    
    var string: String {
        switch self {
        case .get:
            return "GET"
        case .post:
            return "POST"
        case .put:
            return "PUT"
        case .delete:
            return "DELETE"
        }
    }
}

extension APIRouter {
    var isUploadImage: Bool {
        switch self {
        case .uploadImage,
                .singleAccountUploadImages,
                .singleAccountUploadOptionalImages:
            return true
        default:
            return false
        }
    }
    
    // MARK: - URL
    var url: URL {
        var relatedURL: String
        
        switch self {
        case .authentication:
            relatedURL = API.authentication
        case .enumerations:
            relatedURL = API.enumerations
        case .countryList:
            relatedURL = API.countryList
        case .regionList:
            relatedURL = API.regionList
        case .registerUserValidate:
            relatedURL = API.registerUserValidate
        case .registerUserCommit:
            relatedURL = API.registerUserCommit
        case .registerUserPinValidation:
            relatedURL = API.registerUserPinValidation
            
        case .upgradeUserAccountInformation:
            relatedURL = API.upgradeUserAccountInfo
        case .upgradeUserValidate:
            relatedURL = API.upgradeUserValidate
        case .upgradeUserCommit:
            relatedURL = API.upgradeUserCommit
            
        case .getUUID:
            relatedURL = API.getUUID
        case .uploadImage:
            relatedURL = API.uploadImage
            
        case .getRegionIDByName:
            relatedURL = API.getRegionIDByName
        case .getDebitCardInformation(let param):
            relatedURL = API.getDebitCardInformation + "/\(param)"
            
        case .validateKitNumber(let kitNumber):
            relatedURL = API.validateKitNumber + "/\(kitNumber)"
            
        case .planOptionTypeList:
            relatedURL = API.planOptionTypeList
        case .limitation:
            relatedURL = API.limitation
        case .subscribePlanValidate:
            relatedURL = API.subscribePlanValidate
        case .subscribePlanCommit:
            relatedURL = API.subscribePlanCommit
        case .unsubscribePlanValidate:
            relatedURL = API.unsubscribePlanValidate
        case .unsubscribePlanCommit:
            relatedURL = API.unsubscribePlanCommit
            
        case .transactionReport:
            relatedURL = API.transactionReport
        case .transactionList:
            relatedURL = API.transactionList
            
        case .paymentMethod:
            relatedURL = API.paymentMethod
        case .businessType:
            relatedURL = API.businessType
        case .singleAccountRegisterValidate:
            relatedURL = API.singleAccountRegisterValidate
        case .singleAccountRegisterCommit:
            relatedURL = API.singleAccountRegisterCommit
        case .singleAccountGetUUID:
            relatedURL = API.singleAccountGetUUID
        case .singleAccountUploadImages:
            relatedURL = API.singleAccountUploadImages
        case .singleAccountUploadOptionalImages:
            relatedURL = API.singleAccountUploadOptionalImages
        case .singleAccountPinValidation:
            relatedURL = API.singleAccountPinValidation
        }
        
        let urlString = (API.baseURL + relatedURL).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        return URL(string: urlString)!
    }
    
    // MARK: - HTTPMethod
    var method: HTTPMethodEnum {
        switch self {
        case .authentication,
                .regionList,
                .registerUserValidate,
                .registerUserCommit,
                .registerUserPinValidation,
                .upgradeUserAccountInformation,
                .upgradeUserValidate,
                .upgradeUserCommit,
                .uploadImage,
                .getRegionIDByName,
            
                .planOptionTypeList,
                .limitation,
                .subscribePlanValidate,
                .subscribePlanCommit,
                .unsubscribePlanValidate,
                .unsubscribePlanCommit,
                .transactionReport,
                .transactionList,
            
                .singleAccountRegisterValidate,
                .singleAccountRegisterCommit,
                .singleAccountGetUUID,
                .singleAccountUploadImages,
                .singleAccountUploadOptionalImages,
                .singleAccountPinValidation:
            return .post
        case .enumerations,
                .countryList,
                .getDebitCardInformation,
                .validateKitNumber,
            
                .getUUID,
            
                .paymentMethod,
                .businessType:
            return .get
        }
    }
    
    // MARK: - Param
    var param: [String: Any]? {
        switch self {
        case .regionList(let regionID):
            return ["region_id": regionID]
        case .registerUserValidate(let param):
            return param.toBodyParam()
        case .registerUserCommit(let param):
            return param.toBodyParam()
        case .registerUserPinValidation(let param):
            return param.toBodyParam()
            
        case .upgradeUserAccountInformation(let param):
            return param.toBodyParam()
        case .upgradeUserValidate(let param):
            return param.toBodyParam()
        case .upgradeUserCommit(let param):
            return param.toBodyParam()
            
        case .uploadImage(let param):
            return param.toBodyParam()
        case .getRegionIDByName(let param):
            return param.toBodyParam()
            
        case .planOptionTypeList(let currency):
            return [
                "currency": currency
            ]
        case .limitation(let param):
            return param.toBodyParam()
        case .subscribePlanValidate(let param):
            return param.toBodyParam()
        case .subscribePlanCommit(let param):
            return param.toBodyParam()
        case .unsubscribePlanValidate(let param):
            return param.toBodyParam()
        case .unsubscribePlanCommit(let param):
            return param.toBodyParam()
        case .transactionReport(let param):
            return param.toReportParam()
        case .transactionList(let param):
            return param.toBodyParam()
            
        case .singleAccountRegisterValidate(let param):
            return param.toBodyParam()
        case .singleAccountRegisterCommit(let param):
            return param.toBodyParam()
        case .singleAccountGetUUID(let param):
            return param.toBodyParam()
        case .singleAccountUploadImages(let param):
            return param.toBodyParam()
        case .singleAccountUploadOptionalImages(let param):
            return param.toBodyParam()
        case .singleAccountPinValidation(let param):
            return param.toBodyParam()
            
        default:
            return nil
        }
    }
    
    // MARK: - Authentication
    var authRequired: Bool {
        switch self {
        case .authentication:
            return false
        default:
            return true
        }
    }
    
    // MARK: - URLRequest
    var urlRequest: URLRequest {
        var request = URLRequest(url: url)
        request.timeoutInterval = 60
        request.httpMethod = method.string
        
        if let param = param, let data = try? JSONSerialization.data(withJSONObject: param, options: []) {
            request.httpBody = data
        }
        
        if authRequired {
            let token = Constants.accessToken
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        switch self {
        case .authentication:
            var urlComponent = URLComponents()
            urlComponent.queryItems = AuthenticationParam().toURLComponentQueries()
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = urlComponent.query?.data(using: .utf8)
        default:
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        return request
    }
}
