//
//  RequestManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit
import Network

protocol RequestManager: AnyObject {
  func request<T: Codable>(_ type: T.Type, api: APIRouter, retryAttempt: Int,shouldShowLoading: Bool, completion: @escaping (T?, Error?) -> Void)
}

// MARK: - Base API Request
extension RequestManager where Self: BaseService {
  var serverNotFoundNSError: NSError {
    return NSError(domain: "server_not_found".localize, code: 404, userInfo: nil)
  }
  
  var failedToConnectToServerNSError: NSError {
    return NSError(domain: "failed_to_connect_to_server".localize, code: 503, userInfo: nil)
  }
  
  var connectionUnstableNSError: NSError {
    return NSError(domain: "no_internet_connection".localize, code: 503, userInfo: nil)
  }
  
  var generalFailNSError: NSError {
    return NSError(domain: "general_error".localize, code: 0, userInfo: nil)
  }
  
  func request<T>(_ type: T.Type, api: APIRouter, retryAttempt: Int = 3, shouldShowLoading: Bool = true, completion: @escaping (T?, Error?) -> Void) where T : Codable {
    if !NetworkManager.shared.isConnected() {
      return completion(nil, connectionUnstableNSError)
    }
    
    if shouldShowLoading {
      showLoading()
    }
    
    let session = URLSession.shared.dataTask(with: api.urlRequest, completionHandler: { data, response, error in
      self.hideLoading {
        self.performMainThread {
          guard error == nil else {
            let consoleLog = self.generateConsoleLog(api: api, statusCode: 0, data: data ?? Data())
            print(consoleLog)
            print("🚫 Request Error: \(String(describing: error))")
            self.retry(attempt: retryAttempt - 1, type: type, api: api, shouldShowLoading: shouldShowLoading, completion: completion) {
              completion(nil, error)
            }
            return
          }
          
          guard let data = data, !data.isEmpty else {
            self.retry(attempt: retryAttempt - 1, type: type, api: api, shouldShowLoading: shouldShowLoading, completion: completion) {
              completion(nil, self.failedToConnectToServerNSError)
            }
            return
          }
          
          let httpResponse = response as? HTTPURLResponse
          let statusCode = httpResponse?.statusCode ?? 0
          
          let consoleLog = self.generateConsoleLog(api: api, statusCode: statusCode, data: data)
          print("================ Log ================")
          print(consoleLog)
          print("=====================================")
          
          switch statusCode {
            case ServerCode.serverResponseSuccessStartRange...ServerCode.serverResponseSuccessEndRange:
              do {
                let response = try JSONDecoder().decode(T.self, from: data)
                return completion(response, nil)
              }catch {
                print("[Catch Error #1]: \(error)")
              }
              
              do {
                let error = try JSONDecoder().decode(BaseErrorResponse.self, from: data)
                return completion(nil, error.toError())
              }catch {
                print("[Catch Error #2]: \(error)")
              }
              
            case ServerCode.serverNotFound:
              self.retry(attempt: retryAttempt - 1, type: type, api: api, shouldShowLoading: shouldShowLoading, completion: completion) {
                completion(nil, self.serverNotFoundNSError)
              }
            case ServerCode.serverUnauthenticated:
              self.getAccessToken(shouldShowLoading: true) { error in
                guard error == nil else { return }
                
                self.request(type, api: api, completion: completion)
              }
            case ServerCode.serverBadRequest:
              if let errorModel = try? JSONDecoder().decode(BaseErrorResponse.self, from: data) {
                let nsError = NSError(domain: errorModel.toLocalizedMessage(), code: ServerCode.serverBadRequest, userInfo: nil)
                completion(nil, nsError)
              }else {
                completion(nil, self.generateNSError(with: "bad_request".localize, code: ServerCode.serverBadRequest))
              }
            default:
              completion(nil, self.generalFailNSError)
          }
        }
      }
    })
    
    session.resume()
  }
  
  private func retry<T>(attempt: Int, type: T.Type, api: APIRouter, shouldShowLoading: Bool, completion: @escaping (T?, Error?) -> Void, maxAttempt handler: () -> Void) where T: Codable {
    if attempt > 0 {
      request(type, api: api, retryAttempt: attempt, shouldShowLoading: shouldShowLoading, completion: completion)
    }else {
      handler()
    }
  }
  
  private func generateConsoleLog(api: APIRouter, statusCode: Int, data: Data) -> String {
    if api.isUploadImage {
      let param: [String: Any] = api.param ?? [:]
      let paramString = param.map {
        let value = $0.value as? String ?? ""
        return "Upload Image Param: \($0.key): \(value.suffix(10))"
      }.joined(separator: "\n")
      
      let log = """
      ================ Log ================
      [URL]: \(api.urlRequest.url?.absoluteString ?? "")
      [Method]: \(api.urlRequest.httpMethod ?? "")
      [Header]: \(api.urlRequest.allHTTPHeaderFields ?? [:])
      [Param]: \n\(paramString)
      [Response]: \n\(self.generatePrettyJSON(data: data))
      =====================================
      """
      
      return log
    }else {
      let consoleLog = """
      ================ Log ================
      [StatusCode]: \(statusCode)
      [URL]: \(api.urlRequest.url?.absoluteString ?? "")
      [Method]: \(api.urlRequest.httpMethod ?? "")
      [Header]: \(api.urlRequest.allHTTPHeaderFields ?? [:])
      [Param]: \n\(api.param ?? [:])
      [Response]:\n\(generatePrettyJSON(data: data))
      =====================================
      """
      
      return consoleLog
    }
  }
  private func generatePrettyJSON(data: Data) -> String {
    do {
      guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
        print("[Error]: Cannot convert data to JSON object")
        print("[Decoded Data] \(String(describing: String(data: data, encoding: .utf8)))")
        return ""
      }
      guard let prettyJSONData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
        print("[Error]: Cannot convert JSON object to Pretty JSON data")
        return ""
      }
      guard let prettyJSON = String(data: prettyJSONData, encoding: .utf8) else {
        print("[Error]: Couldn't print JSON in String")
        return ""
      }
      
      return prettyJSON
    }catch {
      return error.localizedDescription
    }
  }
  
  private func generateNSError(with message: String, code: Int) -> NSError {
    return NSError(domain: message, code: code, userInfo: nil)
  }
  
  private func performMainThread(handler: @escaping () -> Void) {
    DispatchQueue.main.async {
      handler()
    }
  }
}



