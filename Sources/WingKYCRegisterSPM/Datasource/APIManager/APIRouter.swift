//
//  APIRouter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import Foundation

enum APIRouter {
    
    case authentication
    case enumerations
    case countryList
    case regionList(regionID: String)
    
    case registerUserValidate(param: RegisterUserValidateParam)
    case registerUserCommit(param: RegisterUserCommitParam)
    case registerUserPinValidation(param: RegisterUserPinValidationParam)
    
    case upgradeUserAccountInformation(param: UpgradeUserAccountInformationParam)
    case upgradeUserValidate(param: UpgradeUserValidateParam)
    case upgradeUserCommit(param: UpgradeUserCommitParam)
    
    case getUUID
    case uploadImage(param: UploadImageParam)
    
    case getRegionIDByName(param: GetRegionIDByNameParam)
    case getDebitCardInformation(param: String)
    
    case validateKitNumber(param: String)
    
    case planOptionTypeList(currency: String)
    case limitation(param: LimitationParam)
    case subscribePlanValidate(param: SubscribeValidateParam)
    case subscribePlanCommit(param: SubscribeCommitParam)
    case unsubscribePlanValidate(param: UnsubscribeValidateParam)
    case unsubscribePlanCommit(param: UnsubscribeCommitParam)
    
    case transactionReport(param: TransactionReportParam)
    case transactionList(param: TransactionReportParam)
    
    // MARK: - WingPay
    case paymentMethod
    case businessType
    case singleAccountRegisterValidate(param: SingleAccountRegisterValidateParam)
    case singleAccountRegisterCommit(param: SingleAccountRegisterCommitParam)
    
    case singleAccountGetUUID(param: SingleAccountGetUUIDParam)
    case singleAccountUploadImages(param: SingleAccountUploadImagesParam)
    case singleAccountUploadOptionalImages(param: SingleAccountUploadImagesParam)
    case singleAccountPinValidation(param: SingleAccountPinValidateParam)
}
