//
//  API.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import Foundation

struct API {
    static let baseURL = Constants.baseURL
    
    static let authentication = "/oauth/token"
    static let enumerations = "/api/user/ekyc/v1/enumerations"
    static let countryList = "/api/user/country"
    static let regionList = "/api/user/v1/get_region_list"
    
    static let registerUserValidate = "/api/user/wac_light/v8/register"
    static let registerUserCommit = "/api/user/wac_light/v8/commit"
    
    static let registerUserPinValidation = "/api/user/wac_light/v1/pinValidate"
    
    static let upgradeUserAccountInfo = "/api/user/wac_light/get_account_info"
    static let upgradeUserValidate = "/api/user/wac_light/v2/wcx_upgrade_account_validate"
    static let upgradeUserCommit = "/api/user/wac_light/wcx_upgrade_account_commit"
    
    static let getUUID = "/api/v1/get/uuid"
    static let uploadImage = "/api/user/v4/registration/upload/images"
//  static let uploadImage = "/api/user/v2/registration/upload/images"
    
    static let getRegionIDByName = "/api/user/get_edit_region_list"
    static let getDebitCardInformation = "/api/physical_card_kit/tracking_number"
    
    static let validateKitNumber = "/api/kit_validate/wcx_account/\(Constants.authentication.agentAccount.defaultAccount)/kit_number"
}

// MARK: - Free CashOut Package
extension API {
    static private let apiPath = Constants.authentication.applicationID == .wcxEKYC ? "wcx" : "wingekyc"
    static let planOptionTypeList = "/api/\(apiPath)/freecashout/list/categories"
    static let subscribePlanValidate = "/api/\(apiPath)/freecashout/subscribe/validate"
    static let subscribePlanCommit = "/api/\(apiPath)/freecashout/subscribe/commit"
    static let unsubscribePlanValidate = "/api/\(apiPath)/freecashout/unsubscribe/validate"
    static let unsubscribePlanCommit = "/api/\(apiPath)/freecashout/unsubscribe/commit"
    static let limitation = "/api/\(apiPath)/freecashout/limitation"
    
    static let transactionReport = "/api/\(apiPath)/freecashout/transactionReport"
    static let transactionList = "/api/\(apiPath)/freecashout/listTransactionReport"
}

// MARK: - WingPay
extension API {
    static let businessType = "/api/v1/merchant/category"
    static let paymentMethod = "/api/v4/merchant/packages"
    
    static let singleAccountRegisterValidate = "/api/eKYC/register/retailer/validate"
    static let singleAccountRegisterCommit = "/api/eKYC/register/retailer/commit"
    
    static let singleAccountGetUUID = "/api/eKYC/generateUUID"
    static let singleAccountUploadImages = "/api/image/upload/retailer/images"
    static let singleAccountUploadOptionalImages = "/api/image/upload/retailer/optionalImages"
    static let singleAccountPinValidation = "/api/eKYC/pinValidate"
}
