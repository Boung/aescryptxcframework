//
//  RegisterUserCommitPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/11/21.
//

import Foundation

protocol RegisterUserCommitDelegate {
    func response(registerCommit response: RegisterUserCommitResponse?, error: Error?)
}

class RegisterUserCommitPresenter {
    var service: RegisterUserService?
    var delegate: RegisterUserCommitDelegate?
    
    init(delegate: RegisterUserCommitDelegate) {
        self.service = RegisterUserService()
        self.delegate = delegate
    }
    
    func registerUserCommit(param: RegisterUserCommitParam) {
        service?.registerUserCommit(param: param, completion: { response, error in
            self.delegate?.response(registerCommit: response, error: error)
        })
    }
}
