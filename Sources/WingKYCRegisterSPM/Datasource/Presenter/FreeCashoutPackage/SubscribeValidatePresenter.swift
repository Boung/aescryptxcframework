//
//  SubscribeValidatePresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

protocol SubscribeValidateDelegate {
    func response(subscribeValidate response: SubscribeValidateResponse?, error: Error?)
}

class SubscribeValidatePresenter {
    var service: FreeCashoutPackageService?
    var delegate: SubscribeValidateDelegate?
    
    init(delegate: SubscribeValidateDelegate) {
        self.service = FreeCashoutPackageService()
        self.delegate = delegate
    }
    
    func validateSubscribe(param: SubscribeValidateParam) {
        self.service?.validateSubscribe(param: param, completion: { response, error in
            if response?.errorCode == "200" {
                self.delegate?.response(subscribeValidate: response, error: nil)
            }else {
                self.delegate?.response(subscribeValidate: nil, error: error ?? response?.toError())
            }
        })
    }
}
