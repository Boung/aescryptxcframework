//
//  UnsubscribeCommitPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

protocol UnsubscribeCommitDelegate {
    func response(unsubscribeCommit response: UnsubscribeCommitResponse?, error: Error?)
}

class UnsubscribeCommitPresenter {
    var service: FreeCashoutPackageService?
    var delegate: UnsubscribeCommitDelegate?
    
    init(delegate: UnsubscribeCommitDelegate) {
        self.service = FreeCashoutPackageService()
        self.delegate = delegate
    }
    
    func commitUnsubscribe(param: UnsubscribeCommitParam) {
        self.service?.commitUnsubscribe(param: param, completion: { response, error in
            self.delegate?.response(unsubscribeCommit: response, error: error)
        })
    }
}
