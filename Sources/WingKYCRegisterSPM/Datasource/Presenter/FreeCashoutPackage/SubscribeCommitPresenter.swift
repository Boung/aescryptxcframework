//
//  SubscribeCommitPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

protocol SubscribeCommitDelegate {
    func response(subscribeCommit response: SubscribeCommitResponse?, error: Error?)
}

class SubscribeCommitPresenter {
    var service: FreeCashoutPackageService?
    var delegate: SubscribeCommitDelegate?
    
    init(delegate: SubscribeCommitDelegate) {
        self.service = FreeCashoutPackageService()
        self.delegate = delegate
    }
    
    func commitSubscribe(param: SubscribeCommitParam) {
        self.service?.commitSubscribe(param: param, completion: { response, error in
            self.delegate?.response(subscribeCommit: response, error: error)
        })
    }
}
