//
//  TransactionReportPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

protocol TransactionReportDelegate {
    func response(transactionReport response: TransactionReportResponse?, error: Error?)
}

protocol TransactionListDelegate {
    func response(transactionReport response: TransactionReportResponse?, error: Error?)
}

class TransactionReportPresenter {
    var service: FreeCashoutPackageService?
    var reportDelegate: TransactionReportDelegate?
    var listDelegate: TransactionListDelegate?
    
    init(delegate: TransactionReportDelegate) {
        self.service = FreeCashoutPackageService()
        self.reportDelegate = delegate
    }
    
    init(delegate: TransactionListDelegate) {
        self.service = FreeCashoutPackageService()
        self.listDelegate = delegate
    }
    
    func getTransactionReport(param: TransactionReportParam) {
        self.service?.getTransactionReport(param: param, completion: { response, error in
            self.reportDelegate?.response(transactionReport: response, error: error)
        })
    }
    
    func getTransactionList(param: TransactionReportParam) {
        self.service?.getTransactionList(param: param, completion: { response, error in
            self.listDelegate?.response(transactionReport: response, error: error)
        })
    }
}
