//
//  LimitationPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

protocol LimitationDelegate {
    func response(limitation response: LimitationResponse?, error: Error?)
}

class LimitationPresenter {
    var service: FreeCashoutPackageService?
    var delegate: LimitationDelegate?
    
    init(delegate: LimitationDelegate) {
        self.service = FreeCashoutPackageService()
        self.delegate = delegate
    }
    
    func getLimitation(param: LimitationParam) {
        self.service?.getLimitation(param: param, completion: { response, error in
            self.delegate?.response(limitation: response, error: error)
        })
    }
}
