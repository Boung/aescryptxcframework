//
//  PlanOptionTypeListPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

protocol PlanOptionTypeListDelegate {
    func response(planOptionList response: [PlanOptionTypeListResponse]?, error: Error?)
}

class PlanOptionTypeListPresenter {
    var service: FreeCashoutPackageService?
    var delegate: PlanOptionTypeListDelegate?
    
    init(delegate: PlanOptionTypeListDelegate) {
        self.service = FreeCashoutPackageService()
        self.delegate = delegate
    }
    
    func getPlanOptionTypeList(currency: String) {
        self.service?.getPlanOptionTypeList(currency: currency, completion: { response, error in
            self.delegate?.response(planOptionList: response, error: error)
        })
    }
}
