//
//  UnsubscribeValidatePresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

protocol UnsubscribeValidateDelegate {
    func response(unsubscribeValidate response: UnsubscribeValidateResponse?, error: Error?)
}

class UnsubscribeValidatePresenter {
    var service: FreeCashoutPackageService?
    var delegate: UnsubscribeValidateDelegate?
    
    init(delegate: UnsubscribeValidateDelegate) {
        self.service = FreeCashoutPackageService()
        self.delegate = delegate
    }
    
    func validateUnsubscribe(param: UnsubscribeValidateParam) {
        self.service?.validateUnsubscribe(param: param, completion: { response, error in
            self.delegate?.response(unsubscribeValidate: response, error: error)
        })
    }
}
