//
//  ValidateKitNumberPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 5/2/22.
//

import Foundation

protocol ValidateKitNumberDelegate {
    func response(validateKitNumber response: ValidateKitNumberResponse?, error: Error?)
}

class ValidateKitNumberPresenter {
    var service: KYCService?
    var delegate: ValidateKitNumberDelegate?
    
    init(delegate: ValidateKitNumberDelegate) {
        self.service = KYCService()
        self.delegate = delegate
    }
    
    func validateKitNumber(param: String) {
        service?.validateKitNumber(param: param, completion: { response, error in
            self.delegate?.response(validateKitNumber: response, error: error)
        })
    }
}
