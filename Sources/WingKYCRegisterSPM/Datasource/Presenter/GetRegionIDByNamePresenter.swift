//
//  GetRegionIDByNamePresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/1/22.
//

import Foundation

class GetRegionIDByNamePresenter {
    var service: KYCService = .init()
    
    func getRegionIDByName(param: GetRegionIDByNameParam, completion: @escaping (GetRegionIDByNameResponse?) -> Void) {
        service.getRegionIDByName(param: param, completion: completion)
    }
}
