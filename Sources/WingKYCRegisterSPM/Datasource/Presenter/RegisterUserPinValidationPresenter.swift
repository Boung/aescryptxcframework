//
//  RegisterUserPinValidationPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 20/11/21.
//

import Foundation

protocol RegisterUserPinValidationDelegate {
    func response(pinValidation response: RegisterUserPinValidationResponse?, error: Error?)
}

class RegisterUserPinValidationPresenter {
    var service: RegisterUserService?
    var delegate: RegisterUserPinValidationDelegate?
    
    init(delegate: RegisterUserPinValidationDelegate) {
        self.service = RegisterUserService()
        self.delegate = delegate
    }
    
    func registerUserPinValidation(param: RegisterUserPinValidationParam) {
        service?.registerUserPinValidation(param: param, completion: { response, error in
            self.delegate?.response(pinValidation: response, error: error)
        })
    }
}
