//
//  RegionPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 15/11/21.
//

import Foundation

protocol RegionDelegate {
    func response(region response: RegionResponse?, error: Error?)
}

class RegionPresenter {
 
    var service: KYCService?
    var delegate: RegionDelegate?
    
    init(delegate: RegionDelegate) {
        self.service = KYCService()
        self.delegate = delegate
    }
    
    func getRegion(param: String) {
        service?.getRegion(param: param, completion: { response, error in
            self.delegate?.response(region: response, error: error)
        })
    }
    
    func getRegion(param: String, completion: @escaping (RegionResponse?) -> Void) {
        service?.getRegion(param: param, completion: { response, _ in
            completion(response)
        })
    }
}
