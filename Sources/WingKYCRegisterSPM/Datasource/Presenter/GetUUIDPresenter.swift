//
//  GetUUIDPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 11/8/22.
//

import Foundation

protocol GetUUIDDelegate {
    func response(uuid response: GetUUIDResponse?, error: Error?)
}

class GetUUIDPresenter {
    var service: KYCService?
    var delegate: GetUUIDDelegate?
    
    init(delegate: GetUUIDDelegate) {
        self.service = KYCService()
        self.delegate = delegate
    }
    
    func getUUID() {
        self.service?.getUUID(completion: { response, error in
            self.delegate?.response(uuid: response, error: error)
        })
    }
}
