//
//  RegisterUserValidatePresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/11/21.
//

import Foundation

protocol RegisterUserValidateDelegate {
    func response(registerValidate response: RegisterUserValidateResponse?, error: Error?)
}

class RegisterUserValidatePresenter {
    var service: RegisterUserService?
    var delegate: RegisterUserValidateDelegate?
    
    init(delegate: RegisterUserValidateDelegate) {
        self.service = RegisterUserService()
        self.delegate = delegate
    }
    
    func registerUserValidate(param: RegisterUserValidateParam) {
        service?.registerUserValidate(param: param, completion: { response, error in
            self.delegate?.response(registerValidate: response, error: error)
        })
    }
}
