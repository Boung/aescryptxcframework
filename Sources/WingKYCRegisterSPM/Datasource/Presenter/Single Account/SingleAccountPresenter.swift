//
//  SingleAccountPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/2/23.
//

import Foundation

class SingleAccountPresenter {
    var service: KYCService?
    
    init() {
        service = KYCService()
    }
    
    func getAllComponent() {
        getBusinessType()
        getPaymentMethod()
    }
    
    func validateSingleAccountRegister(param: SingleAccountRegisterValidateParam, completion: @escaping (SingleAccountRegisterValidateResponse?, Error?) -> Void) {
        service?.validateSingleAccountRegister(param: param, completion: completion)
    }
    
    func commitSingleAccountRegister(param: SingleAccountRegisterCommitParam, completion: @escaping (SingleAccountRegisterCommitResponse?, Error?) -> Void) {
        service?.commitSingleAccountRegister(param: param, completion: completion)
    }
    
    func getSingleAccountUUID(param: SingleAccountGetUUIDParam, completion: @escaping (SingleAccountUUIDResponse?, Error?) -> Void) {
        service?.getSingleAccountUUID(param: param, completion: completion)
    }
    
    func uploadMandatoryImages(param: SingleAccountRegisterValidateParam, completion: @escaping () -> Void) {
        uploadIDImages(param: param) {
            self.uploadSelfieAndShopImage(param: param) {
                completion()
            }
        }
    }
    
    func uploadOptionalImages(param: SingleAccountRegisterValidateParam) {
        uploadFirstOptional(param: param)
        uploadSecondOptional(param: param)
    }
    
    func validatePinSingleAccount(param: SingleAccountPinValidateParam, completion: @escaping (SingleAccountPinValidateResponse?, Error?) -> Void) {
        service?.validatePinSingleAccount(param: param, completion: completion)
    }
    
    private func getBusinessType() {
        service?.getBusinessType(completion: { response, error in
            guard error == nil else {
                return self.getBusinessType()
            }
            
            guard let response = response else { return }
            Constants.businessType = response
        })
    }
    
    private func getPaymentMethod() {
        service?.getPaymentMethod(completion: { response, error in
            guard error == nil else {
                return self.getPaymentMethod()
            }
            
            guard let response = response else { return }
            Constants.paymentMethod = response
            response.printAllComponent()
        })
    }
    
    private func uploadIDImages(param: SingleAccountRegisterValidateParam, completion: @escaping () -> Void) {
        let uploadParam = SingleAccountUploadImagesParam(uuid: param.uuid)
        uploadParam.idFrontImage = param.identityFrontImage
        uploadParam.idBackImage = param.identityBackImage
        
        service?.uploadSingleAccountImages(param: uploadParam, completion: { success in
            guard success else { return }
            
            completion()
        })
    }
    
    private func uploadSelfieAndShopImage(param: SingleAccountRegisterValidateParam, completion: @escaping () -> Void) {
        let uploadParam = SingleAccountUploadImagesParam(uuid: param.uuid)
        uploadParam.selfieImage = param.customerImage
        uploadParam.retailerShopImage = param.shopImage
        
        service?.uploadSingleAccountImages(param: uploadParam, completion: { success in
            guard success else { return }
            
            completion()
        })
    }
    
    private func uploadFirstOptional(param: SingleAccountRegisterValidateParam) {
        guard param.patantImage != nil || param.ownershipImage != nil else { return }
        let uploadParam = SingleAccountUploadImagesParam(wingAccount: (param.newCustomerWingAccount).orEmptyString)
        uploadParam.retailerIdPatent = param.patantImage
        uploadParam.retailerIdAuthLetter = param.ownershipImage
        
        service?.uploadSingleAccountOptionalImages(param: uploadParam)
    }
    
    private func uploadSecondOptional(param: SingleAccountRegisterValidateParam) {
        guard param.ownerIDFrontImage != nil || param.ownerIDBackImage != nil else { return }
        let uploadParam = SingleAccountUploadImagesParam(wingAccount: (param.newCustomerWingAccount).orEmptyString)
        uploadParam.retailerFirstIdAuth = param.ownerIDFrontImage
        uploadParam.retailerSecondIdAuth = param.ownerIDBackImage
        
        service?.uploadSingleAccountOptionalImages(param: uploadParam)
    }
}
