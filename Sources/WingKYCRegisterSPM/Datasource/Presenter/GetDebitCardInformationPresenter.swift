//
//  GetDebitCardInformationPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 29/1/22.
//

import Foundation

protocol GetDebitCardInformationDelegate {
    func response(response: DebitCardInformationResponse?, error: Error?)
}

class GetDebitCardInformationPresenter {
    var service: KYCService?
    var delegate: GetDebitCardInformationDelegate?
    
    init(delegate: GetDebitCardInformationDelegate) {
        self.service = KYCService()
        self.delegate = delegate
    }
    
    func getDebitCardInformation(param: String) {
        self.service?.getDebitCardInformation(param: param, completion: { response, error in
            self.delegate?.response(response: response, error: error)
        })
    }
}
