//
//  UpgradeUserValidatePresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation


protocol UpgradeUserValidateDelegate {
    func response(upgradeValidate response: UpgradeUserValidateResponse?, error: Error?)
}

class UpgradeUserValidatePresenter {
    var service: UpgradeUserService?
    var delegate: UpgradeUserValidateDelegate?
    
    init(delegate: UpgradeUserValidateDelegate) {
        self.service = UpgradeUserService()
        self.delegate = delegate
    }
    
    func upgradeUserValidate(param: UpgradeUserValidateParam) {
        self.service?.upgradeUserValidate(param: param, completion: { response, error in
            self.delegate?.response(upgradeValidate: response, error: error)
        })
    }
}
