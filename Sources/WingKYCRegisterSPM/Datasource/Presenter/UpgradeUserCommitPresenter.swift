//
//  UpgradeUserCommitPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation

protocol UpgradeUserCommitDelegate {
    func response(upgradeCommit response: UpgradeUserCommitResponse?, error: Error?)
}

class UpgradeUserCommitPresenter {
    var service: UpgradeUserService?
    var delegate: UpgradeUserCommitDelegate?
    
    init(delegate: UpgradeUserCommitDelegate) {
        self.service = UpgradeUserService()
        self.delegate = delegate
    }
    
    func upgradeUserCommit(param: UpgradeUserCommitParam) {
        self.service?.upgradeUserCommit(param: param, completion: { response, error in
            self.delegate?.response(upgradeCommit: response, error: error)
        })
    }
}
