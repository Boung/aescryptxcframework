//
//  UploadImagePresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 23/11/21.
//

import Foundation

protocol UploadImageDelegate {
    func response(response: UploadImageResponse?, param: UploadImageParam?, error: Error?)
}

class UploadImagePresenter {

    var service: KYCService?
    var delegate: UploadImageDelegate?
    
    init(delegate: UploadImageDelegate) {
        self.service = KYCService()
        self.delegate = delegate
    }
    
    func uploadImage(param: UploadImageParam) {
        self.service?.uploadImage(param: param, completion: { response, error in
            if response?.errorCode == "200" {
                self.delegate?.response(response: response, param: param, error: nil)
            }else {
                if let responseError = response?.toNSError() {
                    self.delegate?.response(response: nil, param: nil, error: responseError)
                }else {
                    self.delegate?.response(response: nil, param: nil, error: error)
                }
            }
        })
    }
}
