//
//  UpgradeUserAccountInformationPresenter.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation

protocol UpgradeUserAccountInformationDelegate {
    func response(upgradeInformation response: UpgradeUserAccountInformationResponse?, error: Error?)
}

class UpgradeUserAccountInformationPresenter {
    var service: UpgradeUserService?
    var delegate: UpgradeUserAccountInformationDelegate?
    
    init(delegate: UpgradeUserAccountInformationDelegate) {
        self.service = UpgradeUserService()
        self.delegate = delegate
    }
    
    func getUserAccountInformation(param: UpgradeUserAccountInformationParam) {
        self.service?.getUserAccountInformation(param: param, completion: { response, error in
            self.delegate?.response(upgradeInformation: response, error: error)
        })
    }
}
