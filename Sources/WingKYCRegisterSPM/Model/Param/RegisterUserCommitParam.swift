//
//  RegisterUserCommitParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/11/21.
//

import Foundation

class RegisterUserCommitParam: BaseParam {
    var sessionID: String
    var WCXPin: String
    var customerPin: String
    var uuid: String
    
    init(sessionID: String, WCXPin: String, customerPin: String, uuid: String) {
        self.sessionID = sessionID
        self.WCXPin = WCXPin
        self.customerPin = customerPin
        self.uuid = uuid
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "sessionId": sessionID,
            "wcx_pin": EncodingManager.shared.encode(string: WCXPin) ?? "",
            "customer_pin": EncodingManager.shared.encode(string: customerPin) ?? "",
            "uuid": uuid
        ]
    }
}
