//
//  UnsubscribeCommitParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class UnsubscribeCommitParam: BaseParam {
    var sessionID: String
    var customerAccountNumber: String
    var customerPin: String
    var wcxPin: String
    var currency: String
    var subType: String?
    
    init(sessionID: String, customerAccountNumber: String, customerPin: String, wcxPin: String, currency: String, subType: String) {
        self.sessionID = sessionID
        self.customerAccountNumber = customerAccountNumber
        self.customerPin = customerPin
        self.wcxPin = wcxPin
        self.currency = currency
        self.subType = subType
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "masterAccountId": Constants.authentication.masterAccountID,
            "wing_account": customerAccountNumber,
            "wcx_account": Constants.authentication.agentAccount.usdAccount,
            "currency": currency,
            "subType": subType ?? "",
            "wcx_pin": EncodingManager.shared.encode(string: wcxPin) ?? "",
            "pin": EncodingManager.shared.encode(string: customerPin) ?? "",
            "session_id": sessionID
        ]
    }
}
