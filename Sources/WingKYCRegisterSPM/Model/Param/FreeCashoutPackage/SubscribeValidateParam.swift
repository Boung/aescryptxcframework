//
//  SubscribeValidateParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class SubscribeValidateParam: BaseParam {
    var customerAccountNumber: String
    var subType: PlanOptionTypeListResponse?
    
    init(customerAccountNumber: String, subType: PlanOptionTypeListResponse?) {
        self.customerAccountNumber = customerAccountNumber
        self.subType = subType
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "masterAccountId": Constants.authentication.masterAccountID,
            "wing_account": customerAccountNumber,
            "wcx_account": Constants.authentication.agentAccount.usdAccount,
            "subType": subType?.subType ?? ""
        ]
    }
}
