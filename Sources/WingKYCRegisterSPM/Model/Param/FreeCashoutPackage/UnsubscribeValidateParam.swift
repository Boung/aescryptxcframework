//
//  UnsubscribeValidateParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class UnsubscribeValidateParam: BaseParam {
    var customerAccountNumber: String
    
    init(customerAccountNumber: String) {
        self.customerAccountNumber = customerAccountNumber
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "masterAccountId": Constants.authentication.masterAccountID,
            "wing_account": customerAccountNumber,
            "wcx_account": Constants.authentication.agentAccount.usdAccount,
        ]
    }
}
