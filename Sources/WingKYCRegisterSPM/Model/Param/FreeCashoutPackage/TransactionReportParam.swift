//
//  TransactionReportParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class TransactionReportParam: BaseParam {
    var page: Int
    
    init(page: Int) {
        self.page = page
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "masterAccountId": Constants.authentication.masterAccountID,
            "wing_account": Constants.authentication.agentAccount.usdAccount,
            "page": page,
            "limit": 10
        ]
    }
    
    func toReportParam() -> [String : Any] {
        return [
            "masterAccountId": Constants.authentication.masterAccountID,
            "wing_account": Constants.authentication.agentAccount.usdAccount
        ]
    }
}
