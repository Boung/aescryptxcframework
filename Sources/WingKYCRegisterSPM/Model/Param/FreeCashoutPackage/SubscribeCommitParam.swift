//
//  SubscribeCommitParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class SubscribeCommitParam: BaseParam {
    var sessionID: String
    var currency: String
    var customerWingAccount: String
    var customerPin: String
    var wcxPin: String
    var subType: PlanOptionTypeListResponse?
    
    init(sessionID: String, currency: String, customerWingAccount: String, customerPin: String, wcxPin: String, subType: PlanOptionTypeListResponse?) {
        self.sessionID = sessionID
        self.currency = currency
        self.customerWingAccount = customerWingAccount
        self.customerPin = customerPin
        self.wcxPin = wcxPin
        self.subType = subType
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "subType": subType?.subType ?? "",
            "wing_account": customerWingAccount,
            "wcx_account": Constants.authentication.agentAccount.usdAccount,
            "currency": currency,
            "pin": EncodingManager.shared.encode(string: customerPin) ?? "",
            "wcx_pin": EncodingManager.shared.encode(string: wcxPin) ?? "",
            "session_id": sessionID,
        ]
    }
}
