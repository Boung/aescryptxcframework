//
//  LimitationParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class LimitationParam: BaseParam {
    var customerWingAccount: String
    var currency: String
    
    init(customerWingAccount: String, currency: String) {
        self.customerWingAccount = customerWingAccount
        self.currency = currency
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "master_account_id": Constants.authentication.masterAccountID,
            "wing_account": customerWingAccount,
            "currency": currency
        ]
    }
}
