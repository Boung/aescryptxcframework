//
//  UpgradeUserAccountInformationParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation

class UpgradeUserAccountInformationParam: BaseParam {
    var wingAccount: String
    var confirmCode: String
    
    init(wingAccount: String, confirmCode: String) {
        self.wingAccount = wingAccount
        self.confirmCode = confirmCode
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "wing_account": wingAccount,
            "confirm_code": EncodingManager.shared.encode(string: confirmCode) ?? ""
        ]
    }
}
