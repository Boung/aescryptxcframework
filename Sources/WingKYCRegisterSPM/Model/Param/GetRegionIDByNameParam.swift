//
//  GetRegionIDByNameParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/1/22.
//

import Foundation

class GetRegionIDByNameParam: BaseParam {
    var birthAddressEdit: AddressEditModel?
    var currentAddressEdit: AddressEditModel?
    
    override func toBodyParam() -> [String : Any] {
        var param: [String: Any] = [:]
        
        if let birthAddressEdit = birthAddressEdit {
            param["birth_address_edit"] = birthAddressEdit.toParam()
        }
        
        if let currentAddressEdit = currentAddressEdit {
            param["current_address_edit"] = currentAddressEdit.toParam()
        }
        
        return param
    }
}

class AddressEditModel {
    var province: String?
    var district: String?
    var commune: String?
    var village: String?
    
    func toAddressTextModel() -> AddressTextModel {
        let address = AddressTextModel()
        address.province = province
        address.district = district
        address.commune = commune
        address.village = village
        
        return address
    }
    
    func toParam() -> [String: Any] {
        var param: [String: Any] = [
            "country": "KHM"
        ]
        
        if let province = province {
            param["province"] = province
        }
        
        if let district = district {
            param["district"] = district
        }
        
        if let commune = commune {
            param["commune"] = commune
        }
        
        if let village = village {
            param["village"] = village
        }
        
        return param
    }
}
