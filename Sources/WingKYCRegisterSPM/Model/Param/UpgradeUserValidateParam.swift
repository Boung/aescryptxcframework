//
//  UpgradeUserValidateParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation

class UpgradeUserValidateParam: RegisterUserValidateParam {
    var customerWingAccount: String?
    
    override func toBodyParam() -> [String : Any] {
        var param = super.toBodyParam()
        
        if let customerWingAccount = customerWingAccount {
            param["customer_wing_account"] = customerWingAccount
        }
        
        return param
    }
}
