//
//  UploadImageParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 23/11/21.
//

import UIKit

enum ImageTypeEnum: Codable {
    case customerImage
    case frontImage
    case backImage
    
    case undefined
}

class UploadImageParam: BaseParam {
    var customerImage: UIImage?
    var identityFrontImage: UIImage?
    var identityBackImage: UIImage?
    var uuid: String?
    
    var customerWingAccount: String
    var imageType: ImageTypeEnum?
    
    init(customerWingAccount: String) {
        self.customerWingAccount = customerWingAccount
        
        super.init()
    }
    
    func toPendingUploadImageParam() -> PendingUploadImageParam {
        let param = PendingUploadImageParam()
        param.customerImage = customerImage?.pngData()
        param.identityFrontImage = identityFrontImage?.pngData()
        param.identityBackImage = identityBackImage?.pngData()
        param.customerWingAccount = customerWingAccount
        param.imageType = imageType
        param.uuid = uuid
        
        return param
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        var param: [String: Any] = [
            "wing_card_number": customerWingAccount
        ]
        
        if let customerImage = customerImage {
            param["selfie_image"] = customerImage.toCompressedBase64String()
        }
        
        if let identityFrontImage = identityFrontImage {
            param["id_front_image"] = identityFrontImage.toCompressedBase64String()
        }
        
        if let identityBackImage = identityBackImage {
            param["id_back_image"] = identityBackImage.toCompressedBase64String()
        }
        
        if let uuid = uuid {
            param["uuid"] = uuid
        }
        
        return param
    }
}

class PendingUploadImageParam: Codable {
    var customerImage: Data?
    var identityFrontImage: Data?
    var identityBackImage: Data?
    var uuid: String?
    
    var customerWingAccount: String?
    var imageType: ImageTypeEnum?
    
    init() {}
    
    func toUploadImageParam() -> UploadImageParam {
        let param = UploadImageParam(customerWingAccount: customerWingAccount ?? "")
        
        if let data = customerImage {
            param.customerImage = UIImage(data: data)
        }
        
        if let data = identityFrontImage {
            param.identityFrontImage = UIImage(data: data)
        }
        
        if let data = identityBackImage {
            param.identityBackImage = UIImage(data: data)
        }
        
        if let uuid = uuid {
            param.uuid = uuid
        }
        
        param.imageType = imageType
        
        return param
    }
}
