//
//  RegisterUserParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 15/11/21.
//

import UIKit

class RegisterUserValidateParam: BaseParam {
    
    var accountType, bankingPurpose, currency: KeyValueModel?
    // MARK: Address for "birth_address" and "current_address"
    var currentAddress, birthAddress: Address?
    // MARK: Address for "birth_address_text" and "current_address_text"
    var currentAddressTextModel, birthAddressTextModel: AddressTextModel?
    // MARK: Address for "birthLocation" and "current_location"
    var currentAddressString, birthAddressString: String?
    var dob, firstName, lastName, firstNameKh, lastNameKh: String?
    var customerType, monthlyAvgIncome, idType, nationality, occupationID: KeyValueModel?
    var sector, sourceOfFund, registerWith: KeyValueModel?
    var contactNo, houseNo, streetNo: String?
    var kitNumber: String?
    var strWingNo, phoneNumber: String?
    var idNumber: String?
    var gender: KeyValueModel?
    var expiryDate: String?
    var maritalStauts: KeyValueModel?
    var titles: KeyValueModel?
    var debitCardInformation: DebitCardInformationResponse?
    var cashOutPackage: KeyValueModel?
    
    // MARK: Store Properties
    var customerImage: UIImage?
    var identityFrontImage: UIImage?
    var identityBackImage: UIImage?
    var birthAddressText, currentAddressText: AddressTextCodable?
    var isPassport: Bool = false
    var passportCountryCode: String?
    var uuid: String?
    
    // MARK: - Added @FCO Required Field [Stored Property]
    var isExistingUserSubscription: Bool = false
    
    var isValidExpiryDate: Bool {
        guard
            let expiryDateString = expiryDate,
            let expiryDate = expiryDateString.toDate(from: "dd/MM/yyyy"),
            KYCHelper.shared.isValid(expiry: expiryDate)
        else { return false }
        
        return true
    }
    var isValidAge: Bool {
        guard
            let dobString = dob,
            let dobDate = dobString.toDate(from: "dd/MM/yyyy"),
            KYCHelper.shared.isValid(age: dobDate)
        else { return false }
        
        return true
    }
    var isValidStepOneRequiredField: Bool {
        return validateStepOneRequiredField() && isValidBirthAddress && isValidCurrentAddress
    }
    
    var isValidBirthAddress: Bool {
        let address = birthAddress?.provinceID != nil && birthAddress?.districtID != nil && birthAddress?.communeID != nil
        let addressText = birthAddressTextModel != nil
        let addressString = birthAddressString != nil
        
        return address || addressText || addressString
    }
    
    var isValidCurrentAddress: Bool {
        let address = currentAddress?.provinceID != nil && currentAddress?.districtID != nil && currentAddress?.communeID != nil && currentAddress?.villageID != nil
        let addressText = currentAddressTextModel != nil
        let addressString = currentAddressString != nil
        
        return address || addressText || addressString
    }
    
    override func toBodyParam() -> [String : Any] {
        var param: [String: Any] = [
            "isFullId": "Y",
            "request_medium": Constants.authentication.applicationID.requestMedium,
            "strWingNo": Constants.authentication.agentAccount.defaultAccount,
            "cash_out_package": cashOutPackage?.key ?? ""
        ]
        
        if let accountType = accountType {
            param["account_type"] = accountType.key
        }
        
        if let bankingPurpose = bankingPurpose {
            param["banking_purpose"] = bankingPurpose.key
        }
        
        if let currency = currency {
            param["currency"] = currency.key
        }
        
        //      MARK: Address Model with ID
        if let currentAddress = currentAddress {
            param["current_address"] = currentAddress.toParam()
        }
        
        if let birthAddress = birthAddress {
            param["birth_address"] = birthAddress.toParam()
        }
        
        //      MARK: Address without ID
        if let currentAddressTextModel = currentAddressTextModel {
            param["current_address_text"] = currentAddressTextModel.toParam()
        }
        
        if let birthAddressTextModel = birthAddressTextModel {
            param["birth_address_text"] = birthAddressTextModel.toParam()
        }
        
        //      MARK: Adress Concated String
        if let currentAddressString = currentAddressString {
            param["current_location"] = currentAddressString
        }
        
        if let birthAddressString = birthAddressString {
            param["birthLocation"] = birthAddressString
        }
        
        if let dob = dob {
            param["dob"] = dob.replacingOccurrences(of: "/", with: "")
        }
        
        if let firstName = firstName {
            param["firstName"] = firstName
        }
        
        if let idType = idType {
            param["idType"] = idType.key
        }
        
        if let lastName = lastName {
            param["lastName"] = lastName
        }
        
        if let monthlyAvgIncome = monthlyAvgIncome {
            param["monthly_avg_income"] = monthlyAvgIncome.key
        }
        
        if let nationality = nationality {
            param["nationality"] = nationality.key
        }
        
        if let occupationID = occupationID {
            param["occupationId"] = occupationID.key
        }
        
        if let sector = sector {
            param["sector"] = sector.key
        }
        
        if let sourceOfFund = sourceOfFund {
            param["source_of_fund"] = sourceOfFund.key
        }
        
        if let houseNo = houseNo {
            param["house_no"] = houseNo
        }
        
        if let streetNo = streetNo {
            param["street_no"] = streetNo
        }
        
        if let registerWith = registerWith {
            param["register_with"] = registerWith.key
        }
        
        if let kitNumber = kitNumber {
            param["kitNumber"] = kitNumber.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        if let firstNameKh = firstNameKh {
            param["first_name_kh"] = firstNameKh
        }
        
        if let lastNameKh = lastNameKh {
            param["last_name_kh"] = lastNameKh
        }

        if let idNumber = idNumber {
            param["idNumber"] = idNumber
        }
        
        if let gender = gender {
            param["gender"] = gender.key
        }
        
        if let expiryDate = expiryDate {
            param["expiryDate"] = expiryDate.replacingOccurrences(of: "/", with: "")
        }
        
        if let phoneNumber = phoneNumber {
            param["strMsisdn"] = phoneNumber
        }
        
        if let maritalStauts = maritalStauts {
            param["marital_status"] = maritalStauts.key
        }
        
        if let titles = titles {
            param["title"] = titles.key
        }
        
        if let debitCardInformation = debitCardInformation {
            param["credit_card_info"] = debitCardInformation.toParam()
        }
        
        return param
    }
    
    func toUploadImageParams(customerWingAccount: String) -> [UploadImageParam] {
        var params: [UploadImageParam] = []
        
        if let customerImage = customerImage {
            let param = UploadImageParam(customerWingAccount: customerWingAccount)
            param.customerImage = customerImage
            param.imageType = .customerImage
            param.uuid = uuid
            
            params.append(param)
        }
        
        if let identityFrontImage = identityFrontImage {
            let param = UploadImageParam(customerWingAccount: customerWingAccount)
            param.identityFrontImage = identityFrontImage
            param.imageType = .frontImage
            param.uuid = uuid
            
            params.append(param)
        }
        
        if let identityBackImage = identityBackImage {
            let param = UploadImageParam(customerWingAccount: customerWingAccount)
            param.identityBackImage = identityBackImage
            param.imageType = .backImage
            param.uuid = uuid
            
            params.append(param)
        }

        
        return params
    }
    
    func toUploadImageParam(customerWingAccount: String) -> UploadImageParam {
        let param = UploadImageParam(customerWingAccount: customerWingAccount)
        param.customerImage = customerImage
        param.identityFrontImage = identityFrontImage
        param.identityBackImage = identityBackImage
        param.uuid = uuid
        
        return param
    }
    
    private func validateStepOneRequiredField() -> Bool {
        let requiredFields: [Any?] = [
            accountType,
            idNumber,
            expiryDate,
            lastNameKh,
            firstNameKh,
            lastName,
            firstName,
            gender,
            dob,
        ]
        
        let compactedFields = requiredFields.compactMap { $0 }
        print(compactedFields)
        
        return requiredFields.count == compactedFields.count
    }
}

class Address {
    var countryID, provinceID, districtID, communeID, villageID: KeyValueModel?
    var cbcCode: String?
    
    func toParam() -> [String: Any] {
        var param: [String: Any] = [:]
        
        if let countryID = countryID {
            param["countryId"] = countryID.key
        }
        
        if let provinceID = provinceID {
            param["provinceId"] = provinceID.key
        }
        
        if let districtID = districtID {
            param["districtId"] = districtID.key
        }
        
        if let communeID = communeID {
            param["communeId"] = communeID.key
        }
        
        if let villageID = villageID {
            param["villageId"] = villageID.key
        }
        
        if let cbcCode = cbcCode {
            param["pin_code"] = cbcCode.isEmpty ? "00000" : cbcCode
        }else {
            param["pin_code"] = "00000"
        }
        
        return param
    }
}

class AddressTextModel {
    let country = "cambodia".localize
    var province: String?
    var district: String?
    var commune: String?
    var village: String?
    
    func toParam() -> [String: Any] {
        var param: [String: Any] = [
            "country": "KHM"
        ]
        
        if let province = province {
            param["province"] = province
        }
        
        if let district = district {
            param["district"] = district
        }
        
        if let commune = commune {
            param["commune"] = commune
        }
        
        if let village = village {
            param["village"] = village
        }
        
        return param
    }
}
