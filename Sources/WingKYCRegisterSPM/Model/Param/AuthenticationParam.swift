//
//  AuthenticationParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 12/11/21.
//

import Foundation

class AuthenticationParam: BaseParam {
    let clientID = "mobileapps_ios"
    let clientSecret = "16681c9ff419d8ecc7cfe479eb02a7a"
    
    func toURLComponentQueries() -> [URLQueryItem] {
        return [
            .init(name: "username", value: Constants.authentication.username),
            .init(name: "password", value: Constants.authentication.password.addingPercentEncoding(withAllowedCharacters: .illegalCharacters)!),
            .init(name: "client_id", value: clientID),
            .init(name: "device_id", value: Constants.deviceUUID),
            .init(name: "client_secret", value: clientSecret),
            .init(name: "grant_type", value: "password"),
            .init(name: "application_id", value: Constants.authentication.applicationID.applicationID)
        ]
    }
}
