//
//  UpgradeUserCommitParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation

class UpgradeUserCommitParam: BaseParam {
    var sessionID: String
    var customerPin: String
    var confirmCode: String
    var wcxPin: String
    
    init(sessionID: String, customerPin: String, confirmCode: String, wcxPin: String) {
        self.sessionID = sessionID
        self.customerPin = customerPin
        self.confirmCode = confirmCode
        self.wcxPin = wcxPin
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "sessionId": sessionID,
            "confirm_code": confirmCode,
            "wcx_pin": EncodingManager.shared.encode(string: wcxPin) ?? "",
            "customer_pin": EncodingManager.shared.encode(string: customerPin) ?? ""
        ]
    }
}
