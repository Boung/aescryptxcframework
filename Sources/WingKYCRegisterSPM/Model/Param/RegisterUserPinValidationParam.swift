//
//  RegisterUserPinValidationParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 20/11/21.
//

import Foundation

class RegisterUserPinValidationParam: BaseParam {
    var pin: String
    var phoneNumber: String
    
    init(pin: String, phoneNumber: String) {
        self.pin = pin
        self.phoneNumber = phoneNumber
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "new_pin": EncodingManager.shared.encode(string: pin) ?? "",
            "contactNo": phoneNumber,
            "strMsisdn": phoneNumber,
        ]
    }
}
