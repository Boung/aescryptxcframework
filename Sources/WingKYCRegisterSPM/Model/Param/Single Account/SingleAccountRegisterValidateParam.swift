//
//  SingleAccountRegisterValidateParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/2/23.
//

import UIKit

class SingleAccountRegisterValidateParam: RegisterUserValidateParam {
    var tillNumber: String?
    var shopName: String?
    var businessType: KeyValueModel? // MARK: - Which one?
    var shopAddress: Address?
    var latitude: CGFloat?
    var longitude: CGFloat?
    
    var patantImage: UIImage?
    var ownershipImage: UIImage?
    var ownerIDFrontImage: UIImage?
    var ownerIDBackImage: UIImage?
    var shopImage: UIImage?
    
    var companyDetail: MerchantDetail?
    var paymentMethod: PaymentMethod?
    
    var merchantName: String?
    var website: String?
    var companyHistory: String?
    var majorProductService: String?
    var loyaltyStatus: Bool? = false
    
    // MARK: - Store Value Property
    var otpCode: String?
    var customerPin: String?
    var sessionID: String?
    var newCustomerWingAccount: String?
    
    func toDisplayAddressString() -> String {
        let address = companyDetail?.addressDetail
        let houseNumber = (address?.addressLine1).orEmptyString
        let streetNumber = (address?.addressLine2).orEmptyString
        
        let houseNo = houseNumber.isNotEmpty ? "\("house_number".localize) \(houseNumber)" : ""
        let streetNo = streetNumber.isNotEmpty ? "\("street_number".localize) \(streetNumber)" : ""
        let province = "\("province".localize) \(address?.provinceID?.value ?? "")"
        let district = "\("district".localize) \(address?.districtID?.value ?? "")"
        let commune = "\("commune".localize) \(address?.communeID?.value ?? "")"
        let village = "\("village".localize) \(address?.villageID?.value ?? "")"
        
        return [houseNo, streetNo, province, district, commune, village].filter { $0.isNotEmpty }.joined(separator: ", ")
    }
    
    override func toBodyParam() -> [String : Any] {
        var param: [String: Any] = [
            "isFullId": "Y",
            "channelCode": Constants.CHANNEL_CODE,
            "requestMedium": Constants.authentication.applicationID.requestMedium,
            "strWingNo": Constants.authentication.agentAccount.defaultAccount,
            "operatingSystem": "iOS",
            "registerBy": Constants.authentication.agentAccount.defaultAccount,
            "registerName": Constants.authentication.agentAccount.defaultAccount,
            "registerWingNo": Constants.authentication.agentAccount.defaultAccount,
        ]
        
        if let accountType = accountType {
            param["accountType"] = accountType.key
        }
        
        if let bankingPurpose = bankingPurpose {
            param["bankingPurpose"] = bankingPurpose.key
        }
        
        //      MARK: Address Model with ID
        if let currentAddress = currentAddress {
            param["registerAddress"] = currentAddress.toParam()
        }
        
        if let birthAddress = birthAddress {
            param["birthAddress"] = birthAddress.toParam()
        }
        
        //      MARK: Address without ID
        if let currentAddressTextModel = currentAddressTextModel {
            param["registerAddressText"] = currentAddressTextModel.toParam()
        }
        
        if let birthAddressTextModel = birthAddressTextModel {
            param["birthAddressText"] = birthAddressTextModel.toParam()
        }
        
        //      MARK: Adress Concated String
        if let currentAddressString = currentAddressString {
            param["registerAddressText"] = currentAddressString
        }
        
        if let birthAddressString = birthAddressString {
            param["birthAddressText"] = birthAddressString
        }
        
        if let dob = dob {
            param["dob"] = dob.replacingOccurrences(of: "/", with: "")
        }
        
        if let firstName = firstName {
            param["firstName"] = firstName
        }
        
        if let idType = idType {
            param["idType"] = idType.key
        }
        
        if let lastName = lastName {
            param["lastName"] = lastName
        }
        
        if let monthlyAvgIncome = monthlyAvgIncome {
            param["monthlyAverageIncome"] = monthlyAvgIncome.key
        }
        
        if let nationality = nationality {
            param["nationality"] = nationality.key
        }
        
        if let occupationID = occupationID {
            param["occupationId"] = occupationID.key
        }
        
        if let sector = sector {
            param["sector"] = sector.key
        }
        
        if let sourceOfFund = sourceOfFund {
            param["sourceOfFund"] = sourceOfFund.key
        }
        
        if let houseNo = houseNo {
            param["house_no"] = houseNo
        }
        
        if let streetNo = streetNo {
            param["street_no"] = streetNo
        }
        
        if let registerWith = registerWith {
            param["register_with"] = registerWith.key
        }
        
        if let kitNumber = kitNumber {
            param["kitNumber"] = kitNumber.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        if let firstNameKh = firstNameKh {
            param["firstNameKh"] = firstNameKh
        }
        
        if let lastNameKh = lastNameKh {
            param["lastNameKh"] = lastNameKh
        }
        
        if let idNumber = idNumber {
            param["idNumber"] = idNumber
        }
        
        if let gender = gender {
            param["gender"] = gender.key
        }
        
        if let expiryDate = expiryDate {
            param["expiryDate"] = expiryDate.replacingOccurrences(of: "/", with: "")
        }
        
        // MARK: - Here
        if let phoneNumber = phoneNumber {
            param["msisdn"] = phoneNumber
        }
        
        if let maritalStauts = maritalStauts {
            param["maritalStatus"] = maritalStauts.key
        }
        
        if let titles = titles {
            param["title"] = titles.key
        }
        
        if let debitCardInformation = debitCardInformation {
            param["credit_card_info"] = debitCardInformation.toParam()
        }
        
        if let cashOutPackage = cashOutPackage {
            param["subscriptionPackage"] = cashOutPackage.key
        }
        
        if let merchantName = merchantName {
            param["merchantName"] = merchantName
        }
        
        param["is_merchant"] = true
        param["tillNumber"] = tillNumber.orEmptyString
        param["trackingNumber"] = debitCardInformation?.trackingNumber.orEmptyString
        param["website"] = website.orEmptyString
        param["companyHistory"] = companyHistory.orEmptyString
        param["majorProductService"] = majorProductService.orEmptyString
        param["currency"] = (currency?.key).orEmptyString
        param["loyaltyStatus"] = (loyaltyStatus ?? false) ? "Y" : "N"
        param["shopName"] = shopName.orEmptyString
        param["businessType"] = (businessType?.key).orEmptyString
        param["shopAddress"] = shopAddress?.toParam()
        param["latitude"] = latitude
        param["longitude"] = longitude
        param["merchantDetail"] = companyDetail?.toParam()
        param["paymentMethod"] = paymentMethod?.toParam()
        param["eKYCAccount"] = Constants.authentication.agentAccount.defaultAccount
        
        return param
    }
    
    func appendRegisterValidateParam(param: RegisterUserValidateParam) {
        accountType = param.accountType
        bankingPurpose = param.bankingPurpose
        currency = param.currency
        currentAddress = param.currentAddress
        birthAddress = param.birthAddress
        currentAddressTextModel = param.currentAddressTextModel
        birthAddressTextModel = param.birthAddressTextModel
        currentAddressString = param.currentAddressString
        birthAddressString = param.birthAddressString
        dob = param.dob
        firstName = param.firstName
        lastName = param.lastName
        firstNameKh = param.firstNameKh
        lastNameKh = param.lastNameKh
        customerType = param.customerType
        monthlyAvgIncome = param.monthlyAvgIncome
        idType = param.idType
        nationality = param.nationality
        occupationID = param.occupationID
        sector = param.sector
        sourceOfFund = param.sourceOfFund
        registerWith = param.registerWith
        contactNo = param.contactNo
        houseNo = param.houseNo
        streetNo = param.streetNo
        kitNumber = param.kitNumber
        strWingNo = param.strWingNo
        phoneNumber = param.phoneNumber
        idNumber = param.idNumber
        gender = param.gender
        expiryDate = param.expiryDate
        maritalStauts = param.maritalStauts
        titles = param.titles
        debitCardInformation = param.debitCardInformation
        cashOutPackage = param.cashOutPackage
        customerImage = param.customerImage
        identityFrontImage = param.identityFrontImage
        identityBackImage = param.identityBackImage
        birthAddressText = param.birthAddressText
        currentAddressText = param.currentAddressText
        isPassport = param.isPassport
        passportCountryCode = param.passportCountryCode
        uuid = param.uuid
    }
    
    func appendUpgradeValidateParam(param: UpgradeUserValidateParam) {
        accountType = param.accountType
        bankingPurpose = param.bankingPurpose
        currency = param.currency
        currentAddress = param.currentAddress
        birthAddress = param.birthAddress
        currentAddressTextModel = param.currentAddressTextModel
        birthAddressTextModel = param.birthAddressTextModel
        currentAddressString = param.currentAddressString
        birthAddressString = param.birthAddressString
        dob = param.dob
        firstName = param.firstName
        lastName = param.lastName
        firstNameKh = param.firstNameKh
        lastNameKh = param.lastNameKh
        customerType = param.customerType
        monthlyAvgIncome = param.monthlyAvgIncome
        idType = param.idType
        nationality = param.nationality
        occupationID = param.occupationID
        sector = param.sector
        sourceOfFund = param.sourceOfFund
        registerWith = param.registerWith
        contactNo = param.contactNo
        houseNo = param.houseNo
        streetNo = param.streetNo
        kitNumber = param.kitNumber
        strWingNo = param.strWingNo
        phoneNumber = param.phoneNumber
        idNumber = param.idNumber
        gender = param.gender
        expiryDate = param.expiryDate
        maritalStauts = param.maritalStauts
        titles = param.titles
        debitCardInformation = param.debitCardInformation
        cashOutPackage = param.cashOutPackage
        customerImage = param.customerImage
        identityFrontImage = param.identityFrontImage
        identityBackImage = param.identityBackImage
        birthAddressText = param.birthAddressText
        currentAddressText = param.currentAddressText
        isPassport = param.isPassport
        passportCountryCode = param.passportCountryCode
        uuid = param.uuid
    }
}

class MerchantDetail {
    var addressDetail: AddressDetail?
    
    init(addressDetail: AddressDetail) {
        self.addressDetail = addressDetail
    }
    
    func toParam() -> [String: Any] {
        return [
            "addressDetail": addressDetail?.toParam() ?? [:],
        ]
    }
}

// MARK: - AddressDetail
class AddressDetail {
    var addressLine1, addressLine2: String?
    var communeID, provinceID, villageID, districtID: KeyValueModel?
    var latitude, longitude: CGFloat?
    
    func toParam() -> [String: Any] {
        return [
            "addressLine1": addressLine1.orEmptyString,
            "addressLine2": addressLine2.orEmptyString,
            
            "latitude": latitude ?? 0.0,
            "longitude": longitude ?? 0.0,
            
            "countryId": "KHM",
            "provinceId": (provinceID?.key).orEmptyString,
            "districtId": (districtID?.key).orEmptyString,
            "communeId": (communeID?.key).orEmptyString,
            "villageId": (villageID?.key).orEmptyString,
        ]
    }
}

class PaymentMethod {
    var payWithWing: KeyValueModel?
    var wingPoint: KeyValueModel?
    var alipay: KeyValueModel?
    var weChat: KeyValueModel?
    var mpqr: KeyValueModel?
    var khqr: KeyValueModel?
    var mvisa: KeyValueModel?
    
    func toParam() -> [String: Any] {
        return [
            "payWithWing": (payWithWing?.key).orEmptyString,
            "alipay": (alipay?.key).orEmptyString,
            "wechat": (weChat?.key).orEmptyString,
            "mpqr": (mpqr?.key).orEmptyString,
            "khqr": (khqr?.key).orEmptyString,
            "giift": (wingPoint?.key).orEmptyString,
            "mvisa": (mvisa?.key).orEmptyString,
        ]
    }
}
