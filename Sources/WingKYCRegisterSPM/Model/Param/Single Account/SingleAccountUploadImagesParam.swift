//
//  SingleAccountUploadImagesParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 20/2/23.
//

import UIKit

class SingleAccountUploadImagesParam: BaseParam {
    var uuid: String?
    
    var retailerIdPatent: UIImage?
    
    var retailerIdAuthLetter: UIImage?
    var retailerFirstIdAuth: UIImage?
    var retailerSecondIdAuth: UIImage?
    
    var retailerFirstOtherImage: UIImage?
    var retailerSecondOtherImage: UIImage?
    var retailerThirdOtherImage: UIImage?
    
    var retailerShopImage: UIImage?
    
    var wingAccount: String?
    var selfieImage: UIImage?
    var idFrontImage: UIImage?
    var idBackImage: UIImage?
    
    init(uuid: String?) {
        self.uuid = uuid
        
        super.init()
    }
    
    init(wingAccount: String?) {
        self.wingAccount = wingAccount
    
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        var param = super.toBodyParam()
        
        if let uuid = uuid {
            param["uuid"] = uuid
        }
        
        if let wingAccount = wingAccount {
            param["wingAccount"] = wingAccount
        }
        
        if let image = retailerIdPatent {
            param["retailerIdPatent"] = image.toCompressedBase64String()
        }
        
        if let image = retailerIdAuthLetter {
            param["retailerIdAuthLetter"] = image.toCompressedBase64String()
        }
        
        if let image = retailerFirstIdAuth {
            param["retailerFirstIdAuth"] = image.toCompressedBase64String()
        }
        
        if let image = retailerSecondIdAuth {
            param["retailerSecondIdAuth"] = image.toCompressedBase64String()
        }
        
        if let image = retailerFirstOtherImage {
            param["retailerFirstOtherImage"] = image.toCompressedBase64String()
        }
        
        if let image = retailerSecondOtherImage {
            param["retailerSecondOtherImage"] = image.toCompressedBase64String()
        }
        
        if let image = retailerThirdOtherImage {
            param["retailerThirdOtherImage"] = image.toCompressedBase64String()
        }
        
        if let image = retailerShopImage {
            param["retailerShopImage"] = image.toCompressedBase64String()
        }
        
        if let image = selfieImage {
            param["retailerSelfieImage"] = image.toCompressedBase64String()
        }
        
        if let image = idFrontImage {
            param["retailerIdFrontImage"] = image.toCompressedBase64String()
        }
        
        if let image = idBackImage {
            param["retailerIdBackImage"] = image.toCompressedBase64String()
        }
        
        return param
    }
}
