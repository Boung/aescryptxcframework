//
//  SingleAccountGetUUIDParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 20/2/23.
//

import Foundation

class SingleAccountGetUUIDParam: BaseParam {
    var customerPhone: String
    
    init(customerPhone: String?) {
        self.customerPhone = customerPhone ?? ""
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "customerMsisdn": customerPhone,
            "eKYCAccount": Constants.authentication.agentAccount.defaultAccount
        ]
    }
}
