//
//  SingleAccountPinValidateParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 20/2/23.
//

import Foundation

class SingleAccountPinValidateParam: BaseParam {
    var dateOfBirth: String
    var contactNo: String
    var newPin: String
    
    init(dateOfBirth: String?, contactNo: String?, newPin: String?) {
        self.dateOfBirth = dateOfBirth ?? ""
        self.contactNo = contactNo ?? ""
        self.newPin = newPin ?? ""
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "contactNo": contactNo,
            "newPin": EncodingManager.shared.encode(string: newPin) ?? "",
            "strMsisdn": contactNo,
            "dob" : dateOfBirth.replacingOccurrences(of: "/", with: "")
        ]
    }
}
