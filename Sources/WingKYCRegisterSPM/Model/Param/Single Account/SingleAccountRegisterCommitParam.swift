//
//  SingleAccountRegisterCommitParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/2/23.
//

import Foundation

class SingleAccountRegisterCommitParam: BaseParam {
    var sessionID: String
    var otpCode: String
    var ekycPin: String
    var customerPin: String
    var uuid: String
    
    init(sessionID: String?, customerPin: String?, ekycPin: String?, uuid: String?, otpCode: String?) {
        self.sessionID = sessionID ?? ""
        self.customerPin = customerPin ?? ""
        self.ekycPin = ekycPin ?? ""
        self.uuid = uuid ?? ""
        self.otpCode = otpCode ?? ""
        
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    override func toBodyParam() -> [String : Any] {
        return [
            "sessionId": sessionID,
            "confirmCode": EncodingManager.shared.encode(string: otpCode).orEmptyString,
            "eKCYPin": EncodingManager.shared.encode(string: ekycPin).orEmptyString,
            "customerNewPin": EncodingManager.shared.encode(string: customerPin).orEmptyString,
            "uuid": uuid,
        ]
    }
}
