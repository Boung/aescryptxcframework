//
//  ThrowableErrorEnum.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import Foundation

enum ThrowableErrorEnum: Error {
    case failedToDecodeException(message: String)
}
