//
//  ScreenViewEnum.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 28/12/21.
//

import Foundation

public enum ScreenViewEnum {
    case identityLivelinessInstructionView
    case identityInstructionView
    case identityCaptureView
    case identityPreviewView
    case identityVerifyView
    case identityValidationFailed
    case identityNoConnection
    
    case identityInstructionIDOnly(hasBackButton: Bool)
    case identityCaptureViewCustom(title: String)
    case identityPreviewViewCustom(title: String)
}
