//
//  RegisterUserCommitResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/11/21.
//

import Foundation

class RegisterUserCommitResponse: Codable {
    let name, wingID, kitNumber, dob: String?
    let strMsisdn, idType, occupationID, nationality: String?
    let accountType, firstName, lastName: String?
    let currency, responseType: String?
    
    let contactNumber: String?
    let accountTypeKh: String?
    let currencyKh: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case wingID = "wingId"
        case kitNumber, dob, strMsisdn, idType
        case occupationID = "occupationId"
        case nationality
        case accountType, firstName, lastName, currency, responseType
        
        case contactNumber, accountTypeKh, currencyKh
    }
    
    func toDatasource() -> [KeyValueModel] {
        return [
            .init(key: "customer_name".localize, value: name ?? ""),
            .init(key: "phone_number".localize, value: contactNumber ?? ""),
            .init(key: "account_type".localize, value: accountTypeKh ?? ""),
            .init(key: "currency".localize, value: currencyKh ?? ""),
            .init(key: "wing_account_number".localize, value: wingID ?? "")
        ]
    }
}
