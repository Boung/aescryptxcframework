//
//  SingleAccountRegisterValidateResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/2/23.
//

import Foundation

class SingleAccountRegisterValidateResponse: Codable {
    var temporaryPin, sessionID, confirmCode: String?
    
    var decodedOTP: String {
        return EncodingManager.shared.decode(encoded: confirmCode ?? "") ?? ""
    }
    
    enum CodingKeys: String, CodingKey {
        case temporaryPin
        case sessionID = "sessionId"
        case confirmCode
    }
}
