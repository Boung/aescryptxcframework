//
//  OnBoardingDynamicFieldResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 26/1/23.
//

import Foundation

class OnBoardingDynamicFieldResponse: Codable {
    let result: Bool?
    let resultCode: Int?
    let resultMessage: String?
    let data: DataClass?
    
    enum CodingKeys: String, CodingKey {
        case result
        case resultCode = "result_code"
        case resultMessage = "result_message"
        case data
    }
}

// MARK: - DataClass
class DataClass: Codable {
    let platforms: [Platform]?
}

// MARK: - Platform
class Platform: Codable {
    let code, name: String?
    let properties: [Property]?
}

// MARK: - Property
class Property: Codable {
    let isMandatory: Bool?
    let dataInputFormula: String?
    let elements: [Platform]?
    let isHiden: Bool?
    let label, id: String?
    let order: Int?
    let type: String?
    let dataInputType: String?
    let platformID: String?
    let options: [PropertyOption]?
    let isAllowMultipleSelection: Bool?
    
    enum CodingKeys: String, CodingKey {
        case isMandatory = "is_mandatory"
        case dataInputFormula = "data_input_formula"
        case elements
        case isHiden = "is_hiden"
        case label, id, order, type
        case dataInputType = "data_input_type"
        case platformID = "platform_id"
        case options
        case isAllowMultipleSelection = "is_allow_multiple_selection"
    }
}

// MARK: - ElementOption
class PropertyOption: Codable {
    let value, label, description: String?
}
