//
//  SingleAccountRegisterCommitResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/2/23.
//

import Foundation

class SingleAccountRegisterCommitResponse: Codable {
    var strMsisdn, accountType, occupationID, name: String?
    var idType, dob, currency, errorCode: String?
    var wingID, temporaryPin, accountTypeKh, nationality: String?
    var currencyKh, expiryDate: String?
    
    enum CodingKeys: String, CodingKey {
        case strMsisdn, accountType
        case occupationID = "occupationId"
        case name, idType, dob, currency, errorCode
        case wingID = "wingId"
        case temporaryPin, accountTypeKh, nationality, currencyKh, expiryDate
    }
    
    func toDatasource() -> [KeyValueModel] {
        return [
            .init(key: "customer_name".localize, value: name.orEmptyString),
            .init(key: "phone_number".localize, value: strMsisdn.orEmptyString, isSelected: true),
            .init(key: "account_type".localize, value: toLocalizedAccountType()),
            .init(key: "currency".localize, value: toLocalizedCurrency()),
            .init(key: "wing_account_number".localize, value: wingID.orEmptyString, isSelected: true),
        ]
    }
    
    private func toLocalizedAccountType() -> String {
        return accountTypeKh ?? accountType ?? ""
    }
    
    private func toLocalizedCurrency() -> String {
        return currencyKh ?? currency ?? ""
    }
}
