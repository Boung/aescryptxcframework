//
//  SingleAccountUUIDResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 20/2/23.
//

import Foundation

class SingleAccountUUIDResponse: Codable {
    var eKYCAccount, sessionID, uuid: String?
    
    enum CodingKeys: String, CodingKey {
        case eKYCAccount
        case sessionID = "sessionId"
        case uuid
    }
}
