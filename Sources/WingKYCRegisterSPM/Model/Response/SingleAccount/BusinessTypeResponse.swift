//
//  BusinessTypeResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/2/23.
//

import Foundation

class BusinessTypeResponse: Codable {
    let categoryId: String?
    let code: String?
}
