//
//  PaymentMethodResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/2/23.
//

import Foundation

class PaymentMethodResponse: Codable {
    let payWithWingPackage, weChatPackage: [Package]?
    let alipayPackage, mpqrPackage: [Package]?
    let giiftPackage, khqrPackage: [Package]?
    
    init() {
        payWithWingPackage = []
        weChatPackage = []
        alipayPackage = []
        mpqrPackage = []
        giiftPackage = []
        khqrPackage = []
    }
    
    func printAllComponent() {
        print("\n=======> Payment Datasource")
        print((payWithWingPackage ?? []).map { "\($0.packageID ?? "") \($0.packageName ?? "")" }.joined(separator: ", "))
        print((weChatPackage ?? []).map { "\($0.packageID ?? "") \($0.packageName ?? "")" }.joined(separator: ", "))
        print((alipayPackage ?? []).map { "\($0.packageID ?? "") \($0.packageName ?? "")" }.joined(separator: ", "))
        print((mpqrPackage ?? []).map { "\($0.packageID ?? "") \($0.packageName ?? "")" }.joined(separator: ", "))
        print((giiftPackage ?? []).map { "\($0.packageID ?? "") \($0.packageName ?? "")" }.joined(separator: ", "))
        print((khqrPackage ?? []).map { "\($0.packageID ?? "") \($0.packageName ?? "")" }.joined(separator: ", "))
    }
}

// MARK: - Package
struct Package: Codable {
    let packageID, packageName, abbreviation: String?
    
    enum CodingKeys: String, CodingKey {
        case packageID = "package_id"
        case packageName = "package_name"
        case abbreviation
    }
}
