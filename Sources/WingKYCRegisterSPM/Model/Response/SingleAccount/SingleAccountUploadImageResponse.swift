//
//  SingleAccountUploadImageResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 20/2/23.
//

import Foundation

class SingleAccountUploadImageResponse: Codable {
    var errorCode: String?
    
    enum CodingKeys: String, CodingKey {
        case errorCode = "error_code"
    }
}
