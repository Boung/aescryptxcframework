//
//  PlanOptionTypeListResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class PlanOptionTypeDatasource {
    var planOption: PlanOptionTypeListResponse?
    var isExpended = false
    var isSelected = false
    
    init(planOption: PlanOptionTypeListResponse?) {
        self.planOption = planOption
    }
}

class PlanOptionTypeListResponse: Codable {
    let categoryKey, subType, currency, nameEn: String?
    let nameKh, nameCh, feeKhr, feeUsd: String?
    let periodEn, periodKh, periodCh, chargeEn: String?
    let chargeKh, chargeCh, titleEn, titleKh: String?
    let titleCh, recommended: String?
    let createdOn, modifiedOn, sequence: Int?
    let status, color: String?
    let cashOutCategoryItems: [CashOutCategoryItem]?
    
    var cashOutBenefitLabels: [BaseLabel] {
        guard let cashOutCategoryItems = cashOutCategoryItems else {
            return []
        }
        
        return cashOutCategoryItems.map {
            let label = BaseLabel()
            label.text = "• \($0.toLocalizedDescription() ?? "")"
            label.textAlignment = .left
            label.textColor = .lightGray
            label.font = .systemFont(ofSize: 15)
            label.numberOfLines = 0
            
            return label
        }
    }
    
    func toLocalizedTitle() -> String {
        return (Constants.applicationLanguage == .khmer ? nameKh : nameEn) ?? ""
    }
    
    func toLocalizedPrice() -> String? {
        let isUSD = currency?.uppercased() == "USD"
        return (isUSD ? feeUsd : feeKhr) ?? ""
    }
    
    func toLocalizedDuration() -> String {
        return (Constants.applicationLanguage == .khmer ? periodKh : periodEn) ?? ""
    }
    
    func toLocalizedDescription() -> String {
        return (Constants.applicationLanguage == .khmer ? titleKh : titleEn) ?? ""
    }
}

// MARK: - CashOutCategoryItem
class CashOutCategoryItem: Codable {
    let desEn, desKh, desCh: String?
    
    func toLocalizedDescription() -> String? {
        return Constants.applicationLanguage == .khmer ? desKh : desEn
    }
}

