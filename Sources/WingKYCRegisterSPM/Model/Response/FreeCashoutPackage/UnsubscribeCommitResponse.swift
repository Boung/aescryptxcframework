//
//  UnsubscribeCommitResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class UnsubscribeCommitResponse: Codable {
    let datas: [FreeCashOutCommitResponse]?
    
    func toDatasource() -> [KeyValueModel] {
        return (datas ?? []).map { .init(key: $0.labelKh ?? "", value: $0.value ?? "")}
    }
}
