//
//  SubscribeValidateResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class SubscribeValidateResponse: Codable {
    let newBenefits: [NewBenefit]?
    let existingBenefits: [ExistingBenefit]?
    let kycStatus, nameKh, nameEn: String?
    let amount, transactionType, phoneNumber, currency: String?
    let initiateeWingAccount, fee, sessionID, fullNameEn: String?
    let fullNameKh: String?
    
    let errorCode, message, messageCh, messageKh: String?
    
    enum CodingKeys: String, CodingKey {
        case kycStatus
        case nameKh = "name_kh"
        case nameEn = "name_en"
        case amount
        case transactionType = "transaction_type"
        case phoneNumber = "phone_number"
        case currency, initiateeWingAccount, fee
        case sessionID = "sessionId"
        case fullNameEn = "full_name_en"
        case fullNameKh = "full_name_kh"
        case newBenefits = "new_benefits"
        case existingBenefits = "existing_benefits"
        
        case errorCode = "error_code"
        case message
        case messageCh = "message_ch"
        case messageKh = "message_kh"
    }
    
    func toLocalizedMessage() -> String {
        Constants.applicationLanguage == .khmer ? (messageKh ?? (message ?? "")) : (message ?? "")
    }
    
    func toError() -> Error {
        return NSError(domain: toLocalizedMessage(), code: 500, userInfo: nil) as Error
    }
    
    func toExistingBenefitDatasource() -> [KeyValueModel] {
        return (existingBenefits ?? []).map { .init(key: $0.toLocalizedLabel(), value: $0.value ?? "") }
    }
    
    private func toNewBenefitDatasource() -> [KeyValueModel] {
        return (newBenefits ?? []).map { .init(key: $0.toLocalizedLabel(), value: $0.value ?? "") }
    }
    
    func toLocalizedPlanType() -> String {
        return (Constants.applicationLanguage == .khmer ? nameKh : nameEn) ?? ""
    }
    
    func toCustomerDatasource() -> [KeyValueModel] {
        var datasource: [KeyValueModel] = [
            .init(key: "plan_transaction_type".localize, value: transactionType ?? ""),
            .init(key: "plan_option_type".localize, value: toLocalizedPlanType()),
            .init(key: "plan_connected_account".localize, value: initiateeWingAccount ?? ""),
        ]
        
        datasource.append(contentsOf: toNewBenefitDatasource())
        
        return datasource
    }
    
    func toWCXDatasource() -> [KeyValueModel] {
        return [
            .init(key: "plan_transaction_type".localize, value: transactionType ?? ""),
            .init(key: "plan_option_type".localize, value: toLocalizedPlanType()),
            .init(key: "plan_connected_account".localize, value: initiateeWingAccount ?? ""),
            .init(key: "plan_customer_name".localize, value: fullNameKh ?? fullNameEn ?? ""),
            .init(key: "plan_customer_phone_number".localize, value: phoneNumber ?? ""),
            .init(key: "wcx_fee".localize, value: fee ?? ""),
        ]
    }
}

class ExistingBenefit: Codable {
    let currency, color, labelEn, labelCh: String?
    let labelParameter: String?
    let isShareable: Bool?
    let value, maxValue, labelKh: String?
    let total: Bool?
    let index: Int?
    
    enum CodingKeys: String, CodingKey {
        case currency, color
        case labelEn = "label_en"
        case labelCh = "label_ch"
        case labelParameter = "label_parameter"
        case isShareable = "is_shareable"
        case value
        case maxValue = "max_value"
        case labelKh = "label_kh"
        case total, index
    }
    
    func toLocalizedLabel() -> String {
        return (Constants.applicationLanguage == .khmer ? labelKh : labelEn) ?? ""
    }
}

// MARK: - NewBenefit
class NewBenefit: Codable {
    let labelEn, labelCh, labelParameter: String?
    let isShareable: Bool?
    let value: String?
    let total: Bool?
    let labelKh: String?
    let index: Int?
    
    enum CodingKeys: String, CodingKey {
        case labelEn = "label_en"
        case labelCh = "label_ch"
        case labelParameter = "label_parameter"
        case isShareable = "is_shareable"
        case value, total
        case labelKh = "label_kh"
        case index
    }
    
    func toLocalizedLabel() -> String {
        return (Constants.applicationLanguage == .khmer ? labelKh : labelEn) ?? ""
    }
}
