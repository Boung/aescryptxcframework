//
//  TransactionReportResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class TransactionReportResponse: Codable {
    let transactionList: [PlanTransactionListModel]?
    let transactionReport: [PlanTransactionReportModel]?
    let totalPage: Int
    
    enum CodingKeys: String, CodingKey {
        case transactionList = "transaction_list"
        case transactionReport = "transaction_report"
        case totalPage = "total_page"
    }
}

// MARK: - TransactionList
class PlanTransactionListModel: Codable {
    let partyID, subType, feeKhr, feeUsd: String?
    let subscriptionName, transactionListDescription, subscriptionOn, createdBy: String?
    let statusID, accountNo, isSubscription, phoneNumber: String?
    let entityCode: String?
    
    let txnID: String?
    let commission: String?
    
    enum CodingKeys: String, CodingKey {
        case partyID = "party_id"
        case subType = "sub_type"
        case feeKhr = "fee_khr"
        case feeUsd = "fee_usd"
        case subscriptionName = "subscription_name"
        case transactionListDescription = "description"
        case subscriptionOn = "subscription_on"
        case createdBy = "created_by"
        case statusID = "status_id"
        case accountNo = "account_no"
        case isSubscription = "is_subscription"
        case phoneNumber = "phone_number"
        case entityCode = "entity_code"
        case txnID = "txn_id"
        case commission = "commission"
    }
}

// MARK: - TransactionReport
class PlanTransactionReportModel: Codable {
    let labelEn, labelKh, labelCh, value: String?
    let index: Int?
    let isShareable: Bool?
    let labelParameter: String?
    let total: Bool?
    
    enum CodingKeys: String, CodingKey {
        case labelEn = "label_en"
        case labelKh = "label_kh"
        case labelCh = "label_ch"
        case value, index
        case isShareable = "is_shareable"
        case labelParameter = "label_parameter"
        case total
    }
    
    func toKeyValueValue() -> KeyValueModel {
        let labelKH = labelKh ?? ""
        let labelEN = labelEn ?? ""
        
        return .init(key: Constants.applicationLanguage == .khmer ? labelKH : labelEN, value: value ?? "0")
    }
}
