//
//  UnsubscribeValidateResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class UnsubscribeValidateResponse: Codable {
    let subType, nameEn, nameKh, errorCode: String?
    let phoneNumber, fullNameEn, fullNameKh, periodKh: String?
    let wcxFee, currency, initiatorWingAccount, sessionID: String?
    let transactionType: String?
    
    enum CodingKeys: String, CodingKey {
        case subType
        case nameEn = "name_en"
        case nameKh = "name_kh"
        case errorCode = "error_code"
        case phoneNumber = "phone_number"
        case fullNameEn = "full_name_en"
        case fullNameKh = "full_name_kh"
        case periodKh = "period_kh"
        case wcxFee, currency, initiatorWingAccount
        case sessionID = "sessionId"
        case transactionType = "transaction_type"
    }
    
    func toLocalizedPlanType() -> String {
        let khmer = nameKh ?? ""
        let english = nameEn ?? ""
        
        return Constants.applicationLanguage == .khmer ? khmer : english
    }
    
    func toLocalizedCustomerName() -> String {
        return fullNameKh ?? fullNameEn ?? ""
    }
}
