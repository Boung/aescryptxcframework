//
//  SubscribeCommitResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation

class SubscribeCommitResponse: Codable {
    let datas: [FreeCashOutCommitResponse]?
    
    func toDatasource() -> [KeyValueModel] {
        return (datas ?? []).map { .init(key: $0.labelKh ?? "", value: $0.value ?? "")}
    }
}

class FreeCashOutCommitResponse: Codable {
    let isShareable: Bool?
    let value: String?
    let total: Bool?
    let labelParameter, labelKh: String?
    let index: Int?
    
    enum CodingKeys: String, CodingKey {
        case isShareable = "is_shareable"
        case value, total
        case labelParameter = "label_parameter"
        case labelKh = "label_kh"
        case index
    }
}
