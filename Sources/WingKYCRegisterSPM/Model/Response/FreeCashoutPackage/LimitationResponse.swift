//
//  LimitationResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/8/22.
//

import Foundation
import UIKit

class LimitationResponse: Codable {
    let datas: [LimitationModel]?
}

class LimitationModel: Codable {
    let color, labelEn, labelCh, maxValue: String?
    let isShareable: Bool?
    let labelParameter, value: String?
    let total: Bool?
    let labelKh, currency: String?
    
    let labelValueEn: String?
    let labelValueKh: String?

    enum CodingKeys: String, CodingKey {
        case color
        case labelEn = "label_en"
        case labelCh = "label_ch"
        case maxValue = "max_value"
        case isShareable = "is_shareable"
        case labelParameter = "label_parameter"
        case value, total
        case labelKh = "label_kh"
        case currency
        case labelValueEn = "label_value_en"
        case labelValueKh = "label_value_kh"
    }
    
    var maxValueFloat: CGFloat {
        return (maxValue ?? "").toCGFloat
    }
    
    var currentValueFloat: CGFloat {
        return (value ?? "").toCGFloat
    }
    
    func toLocalizedLabel() -> String {
        return (Constants.applicationLanguage == .khmer ? labelKh : labelEn) ?? ""
    }
    
    func toLocalizedValueLabel() -> String {
        print(Constants.applicationLanguage)
        return (Constants.applicationLanguage == .khmer ? labelValueKh : labelValueEn) ?? ""
    }
}
