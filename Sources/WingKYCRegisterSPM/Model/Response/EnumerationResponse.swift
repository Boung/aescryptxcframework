//
//  EnumerationResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 11/11/21.
//

import Foundation

// MARK: - EnumerationResponse
class EnumerationResponse: Codable {
    var mlAvgInc, maritalStatus, occupationList, customerType: [AccountType]?
    var bankingPurpose, customerSize, customerTitle, incomeSource: [AccountType]?
    var accountTypes, accountTypesKYC, residentStatus, sectorID: [AccountType]?
    var idType: [IDType]?
    var cashOutPackageUSD: [CashOutPackage]?
    var cashOutPackageKHR: [CashOutPackage]?
    
    enum CodingKeys: String, CodingKey {
        case mlAvgInc = "ML_AVG_INC"
        case maritalStatus = "MARITAL_STATUS"
        case occupationList = "OCCUPATION_LIST"
        case customerType = "CUSTOMER_TYPE"
        case bankingPurpose = "BANKING_PURPOSE"
        case customerSize = "CUSTOMER_SIZE"
        case customerTitle = "CUSTOMER_TITLE"
        case incomeSource = "INCOME_SOURCE"
        case accountTypes = "ACCOUNT_TYPES"
        case residentStatus = "RESIDENT_STATUS"
        case sectorID = "SECTOR_ID"
        case idType = "ID_TYPE"
        case accountTypesKYC = "EKYC_ACCOUNT_TYPES"
        case cashOutPackageUSD = "CASH_OUT_PACKAGE_USD"
        case cashOutPackageKHR = "CASH_OUT_PACKAGE_KHR"
    }
    
    func printAllComponent() {
        print("\n=======> Enumeration Datasource")
        print((mlAvgInc ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((maritalStatus ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((occupationList ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((customerType ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((bankingPurpose ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((customerSize ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((customerTitle ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((incomeSource ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((accountTypes ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((accountTypesKYC ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((residentStatus ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((sectorID ?? []).map { "\($0.enumID ?? "") \($0.toLocalized())" }.joined(separator: ",\n"))
        print((idType ?? []).map { "\($0.idType ?? "") \($0.idTypeDescription?.en ?? "")" }.joined(separator: ",\n"))
        print((cashOutPackageUSD ?? []).map { "\($0.subID ?? "") \($0.classDescription?.en ?? "")" }.joined(separator: ",\n"))
        print((cashOutPackageKHR ?? []).map { "\($0.subID ?? "") \($0.classDescription?.en ?? "")" }.joined(separator: ",\n"))
    }
}

// MARK: - AccountType
class AccountType: Codable {
    var enumID, enumTypeID: String?
    var sequenceNumber: Int?
    var accountTypeDescription, descriptionKh: String?
    var isDefault: Bool?
    
    enum CodingKeys: String, CodingKey {
        case enumID = "enumId"
        case enumTypeID = "enumTypeId"
        case sequenceNumber
        case accountTypeDescription = "description"
        case descriptionKh
        case isDefault
    }
    
    func toLocalized() -> String {
        return Constants.applicationLanguage == .khmer ? (descriptionKh ?? "") : (accountTypeDescription ?? "")
    }
}

// MARK: - IDType
class IDType: Codable {
    var idType: String?
    var idTypeDescription: Description?
    var expiredDate: ExpiredDate?
    var issuedDate: IssuedDate?
    var issuedCountry: IssuedCountry?
    var idImageUpload: IDImageUpload?
    
    enum CodingKeys: String, CodingKey {
        case idType = "id_type"
        case idTypeDescription = "description"
        case expiredDate = "expired_date"
        case issuedDate = "issued_date"
        case issuedCountry = "issued_country"
        case idImageUpload = "id_image_upload"
    }
}

// MARK: - ExpiredDate
class ExpiredDate: Codable {
    var inputType: String?
    var extendedYear: Int?
    
    enum CodingKeys: String, CodingKey {
        case inputType = "input_type"
        case extendedYear = "extended_year"
    }
}

// MARK: - IDImageUpload
class IDImageUpload: Codable {
    var secondaryImage: SecondaryImage?
    
    enum CodingKeys: String, CodingKey {
        case secondaryImage = "secondary_image"
    }
}

// MARK: - SecondaryImage
class SecondaryImage: Codable {
    var secondaryImageRequired: Bool?
    
    enum CodingKeys: String, CodingKey {
        case secondaryImageRequired = "required"
    }
}

// MARK: - Description
class Description: Codable {
    var en, kh: String?
    
    func toLocalized() -> String {
        return Constants.applicationLanguage == .khmer ? (kh ?? "") : (en ?? "")
    }
}

// MARK: - IssuedCountry
class IssuedCountry: Codable {
    var inputType, defaultValue: String?
    
    enum CodingKeys: String, CodingKey {
        case inputType = "input_type"
        case defaultValue = "default_value"
    }
}

// MARK: - IssuedDate
class IssuedDate: Codable {
    var inputType: String?
    
    enum CodingKeys: String, CodingKey {
        case inputType = "input_type"
    }
}

// MARK: - Cashout Package
class CashOutPackage: Codable {
    let subID: String?
    let classDescription: Description?
    
    enum CodingKeys: String, CodingKey {
        case subID = "subId"
        case classDescription = "description"
    }
}
