//
//  UpgradeUserAccountInformationResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation
import UIKit

public class UpgradeUserAccountInformationResponse: Codable {
    let firstName, lastName, strWingNo, currency: String?
    let dob, idNumber, gender, kitNumber: String?
    let idType, strMsisdn, expiryDate, nationality: String?
    let customerWingAccount, occupationID, contactNo: String?
    let isMerchant: Bool?
    let email, packageID, isATM, title: String?
    let issuedDate: String?
    let firstNameKh, lastNameKh, nameOfWorkplace, sourceOfFund: String?
    let monthlyAvgIncome, pepsStatus, bankingPurpose, resident: String?
    let maritalStatus, accountType, cheque, streetNo: String?
    let houseNo: String?
    let birthAddressText, currentAddressText: AddressTextCodable?
    let sector: String?
    
    // MARK: - Added @FCO Required Field
    let userSubscription: UserSubscription?
    
    // MARK: Store Properties
    var customerImage: UIImage?
    var frontIDImage: UIImage?
    var backIDImage: UIImage?
    
    init() {
        firstName = nil
        lastName = nil
        strWingNo = nil
        currency = nil
        dob = nil
        idNumber = nil
        gender = nil
        kitNumber = nil
        idType = nil
        strMsisdn = nil
        expiryDate = nil
        nationality = nil
        customerWingAccount = nil
        occupationID = nil
        contactNo = nil
        isMerchant = nil
        email = nil
        packageID = nil
        isATM = nil
        title = nil
        issuedDate = nil
        firstNameKh = nil
        lastNameKh = nil
        nameOfWorkplace = nil
        sourceOfFund = nil
        monthlyAvgIncome = nil
        pepsStatus = nil
        bankingPurpose = nil
        resident = nil
        maritalStatus = nil
        accountType = nil
        cheque = nil
        streetNo = nil
        houseNo = nil
        birthAddressText = nil
        currentAddressText = nil
        
        sector = nil
        userSubscription = nil
    }
    
    enum CodingKeys: String, CodingKey {
        case firstName, lastName, strWingNo, currency, dob, idNumber, gender, kitNumber, idType, strMsisdn, expiryDate, nationality
        case customerWingAccount = "customer_wing_account"
        case occupationID = "occupation_id"
        case contactNo = "contact_no"
        case isMerchant = "is_merchant"
        case email
        case packageID = "packageId"
        case isATM = "is_atm"
        case title
        case issuedDate = "issued_date"
        case firstNameKh = "first_name_kh"
        case lastNameKh = "last_name_kh"
        case nameOfWorkplace = "name_of_workplace"
        case sourceOfFund = "source_of_fund"
        case monthlyAvgIncome = "monthly_avg_income"
        case pepsStatus = "peps_status"
        case bankingPurpose = "banking_purpose"
        case resident
        case maritalStatus = "marital_status"
        case accountType = "account_type"
        case cheque
        case streetNo = "street_no"
        case houseNo = "house_no"
        case birthAddressText = "birth_address_text"
        case currentAddressText = "current_address_text"
        case sector
        case userSubscription
    }
    
    func toUpgradeUserValidateParam() -> UpgradeUserValidateParam {
        let param = UpgradeUserValidateParam()
        param.firstName = firstName
        param.lastName = lastName
        param.firstNameKh = firstNameKh
        param.lastNameKh = lastNameKh
        param.dob = dob
        param.contactNo = contactNo
        param.houseNo = houseNo
        param.streetNo = streetNo
        param.idNumber = idNumber
        param.expiryDate = expiryDate
        param.strWingNo = strWingNo
        param.phoneNumber = strMsisdn
        param.accountType = .init(key: accountType ?? "", value: "")
        param.bankingPurpose = .init(key: bankingPurpose ?? "", value: "")
        param.currency = .init(key: currency ?? "", value: "")
        param.monthlyAvgIncome = .init(key: monthlyAvgIncome ?? "", value: "")
        param.idType = .init(key: idType ?? "", value: "")
        param.nationality = .init(key: nationality ?? "", value: "")
        param.occupationID = .init(key: occupationID ?? "", value: "")
        param.sector = .init(key: sector ?? "", value: "")
        param.sourceOfFund = .init(key: sourceOfFund ?? "", value: "")
        param.kitNumber = kitNumber
        param.gender = .init(key: gender ?? "", value: "")
        param.maritalStauts = .init(key: maritalStatus ?? "", value: "")
        param.titles = .init(key: title ?? "", value: "")
        param.customerWingAccount = customerWingAccount ?? ""
        
        if let fcoID = userSubscription?.subType, fcoID.isNotEmpty {
            param.cashOutPackage = .init(key: userSubscription?.subType ?? "", value: "")
            param.isExistingUserSubscription = true
        }
        
        param.customerImage = customerImage
        param.identityFrontImage = frontIDImage
        param.identityBackImage = backIDImage
        
        param.birthAddress = birthAddressText?.toAddress()
        param.currentAddress = currentAddressText?.toAddress()
        param.birthAddressText = birthAddressText
        param.currentAddressText = currentAddressText
        
        return param
    }
}

class AddressTextCodable: Codable {
    let country, province, district, commune, village: AddressCodable?
    
    var isEmpty: Bool {
        return province == nil && district == nil && commune == nil && village == nil
    }
    
    init() {
        country = nil
        province = nil
        district = nil
        commune = nil
        village = nil
    }
    
    func toAddress() -> Address {
        let address = Address()
        address.countryID = .init(key: country?.regionID ?? "", value: country?.toLocalizedDescription() ?? "")
        
        if let province = province {
            address.provinceID = .init(key: province.regionID ?? "", value: province.toLocalizedDescription())
        }
        
        if let district = district {
            address.districtID = .init(key: district.regionID ?? "", value: district.toLocalizedDescription())
        }
        
        if let commune = commune {
            address.communeID = .init(key: commune.regionID ?? "", value: commune.toLocalizedDescription())
        }
        
        if let village = village {
            address.villageID = .init(key: village.regionID ?? "", value: village.toLocalizedDescription())
//            address.cbcCode = village.cbcRegionID
        }
        
        return address
    }
    
    func toParam() -> [String: Any] {
        var param: [String: Any] = [:]
        
        if let country = country {
            param["country"] = country.toLocalizedDescription()
        }
        
        if let province = province {
            param["province"] = province.toLocalizedDescription()
        }
        
        if let district = district {
            param["district"] = district.toLocalizedDescription()
        }
        
        if let commune = commune {
            param["commune"] = commune.toLocalizedDescription()
        }
        
        if let village = village {
            param["village"] = village.toLocalizedDescription()
        }
        
        return param
    }
}

// MARK: - Country
class AddressCodable: Codable {
    let regionID, countryDescription, descriptionKh, descriptionCh: String?
    let cbcRegionID: String?

    enum CodingKeys: String, CodingKey {
        case regionID = "region_id"
        case countryDescription = "description"
        case descriptionKh, descriptionCh
        case cbcRegionID = "cbc_region_id"
    }
    
    func toLocalizedDescription() -> String {
        return Constants.applicationLanguage == .khmer ? (descriptionKh ?? "") : (countryDescription ?? "")
    }
}

class UserSubscription: Codable {
    var categoryKey, nameEn, nameKh, nameCh: String?
    var currency, feeKhr, feeUsd, subType: String?
    var color: String?
}
