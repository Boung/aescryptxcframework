//
//  BaseErrorResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 24/12/21.
//

import Foundation

class BaseErrorResponse: Codable {
    let errorCode, message, messageCh, messageKh: String?
    
    enum CodingKeys: String, CodingKey {
        case errorCode = "error_code"
        case message
        case messageCh = "message_ch"
        case messageKh = "message_kh"
    }
    
    func toLocalizedMessage() -> String {
        Constants.applicationLanguage == .khmer ? (messageKh ?? (message ?? "")) : (message ?? "")
    }
    
    func toError() -> Error {
        return NSError(domain: toLocalizedMessage(), code: 500, userInfo: nil) as Error
    }
}
