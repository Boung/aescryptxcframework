//
//  DebitCardInformationResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 29/1/22.
//

import Foundation

class DebitCardInformationResponse: Codable {
    let trackingNumber, cardNumber: String?
    let brand, remark: String?
    let imageUrl: String?
    let label: String?
    
    func toParam() -> [String: Any] {
        return [
            "trackingNumber": trackingNumber ?? "",
            "cardNumber": cardNumber ?? "",
            "brand": brand ?? "",
            "remark": remark ?? ""
        ]
    }
}
