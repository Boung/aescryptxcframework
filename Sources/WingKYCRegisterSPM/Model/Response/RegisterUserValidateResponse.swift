//
//  RegisterUserValidateResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/11/21.
//

import Foundation

class RegisterUserValidateResponse: Codable {
    var sessionID: String?
    
    enum CodingKeys: String, CodingKey {
        case sessionID = "session_id"
    }
}
