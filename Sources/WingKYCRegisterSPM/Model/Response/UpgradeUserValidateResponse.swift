//
//  UpgradeUserValidateResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation

class UpgradeUserValidateResponse: Codable {
    var sessionID: String?
    var confirmCode: String?
    
    enum CodingKeys: String, CodingKey {
        case sessionID = "session_id"
        case confirmCode = "confirmCode"
    }
}
