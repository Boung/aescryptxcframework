//
//  GetUUIDResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 11/8/22.
//

import Foundation

class GetUUIDResponse: Codable {
    let uuid: String?
}
