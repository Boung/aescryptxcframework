//
//  UploadImageResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 23/11/21.
//

import Foundation

class UploadImageResponse: Codable {
    let errorCode, message, messageCh, messageKh: String?
    
    enum CodingKeys: String, CodingKey {
        case errorCode = "error_code"
        case message
        case messageCh = "message_ch"
        case messageKh = "message_kh"
    }
    
    func toNSError() -> NSError {
        return NSError(domain: toLocalizedMessage(), code: 500, userInfo: nil)
    }
    
    func toLocalizedMessage() -> String {
        Constants.applicationLanguage == .khmer ? (messageKh ?? (message ?? "")) : (message ?? "")
    }
}
