//
//  GetRegionIDByNameResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/1/22.
//

import Foundation

class GetRegionIDByNameResponse: Codable {
    let birthAddress, currentAddress: AddressTextCodable?
    
    enum CodingKeys: String, CodingKey {
        case birthAddress = "birth_address"
        case currentAddress = "current_address"
    }
    
    func toBirthAddress() -> Address? {
        guard let birthAddress = birthAddress, !birthAddress.isEmpty else { return nil }

        let address = Address()
        
        if let province = birthAddress.province {
            address.provinceID = .init(key: province.regionID ?? "", value: province.toLocalizedDescription())
        }
        
        if let district = birthAddress.district {
            address.districtID = .init(key: district.regionID ?? "", value: district.toLocalizedDescription())
        }
        
        if let commune = birthAddress.commune {
            address.communeID = .init(key: commune.regionID ?? "", value: commune.toLocalizedDescription())
        }
        
        return  address
    }
    
    func toCurrentAddress() -> Address? {
        guard let currentAddress = currentAddress, !currentAddress.isEmpty else { return nil }
        
        let address = Address()
        address.countryID = .init(key: "KHM", value: "cambodia".localize)
        
        if let province = currentAddress.province {
            address.provinceID = .init(key: province.regionID ?? "", value: province.toLocalizedDescription())
        }
        
        if let district = currentAddress.district {
            address.districtID = .init(key: district.regionID ?? "", value: district.toLocalizedDescription())
        }
        
        if let commune = currentAddress.commune {
            address.communeID = .init(key: commune.regionID ?? "", value: commune.toLocalizedDescription())
        }
        
        if let village = currentAddress.village {
            address.villageID = .init(key: village.regionID ?? "", value: village.toLocalizedDescription())
            address.cbcCode = village.cbcRegionID
        }
        
        return address
    }
}
