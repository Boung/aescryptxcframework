//
//  RegionResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 12/11/21.
//

import Foundation

class RegionResponse: Codable {
    var province: [RegionModel]?
    var district: [RegionModel]?
    var commune: [RegionModel]?
    var village: [RegionModel]?
    
    var isProvince: Bool {
        return province != nil
    }
    var isDistrict: Bool {
        return district != nil
    }
    var isCommune: Bool {
        return commune != nil
    }
    var isVillage: Bool {
        return village != nil
    }
    
    enum CodingKeys: String, CodingKey {
        case province
        case district
        case commune
        case village
    }
}

class RegionModel: Codable {
    var regionID: String?
    var regionDescription: String?
    var descriptionKh: String?
    var cbcCode: String?
    
    var isHasNextRegion: Bool?
    
    enum CodingKeys: String, CodingKey {
        case regionID = "region_id"
        case regionDescription = "description"
        case descriptionKh
        case cbcCode = "cbc_region_id"
        
        case isHasNextRegion
    }
    
    func toLocalized() -> String {
        Constants.applicationLanguage == .khmer ? (descriptionKh ?? "") : (regionDescription ?? "")
    }
}
