//
//  UpgradeUserCommitResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 22/11/21.
//

import Foundation

class UpgradeUserCommitResponse: Codable {
    let expiryDate, responseType, firstName, encoding: String?
    let lastName, wingID: String?
    let isFavorable: Bool?
    let accountType, gender, occupationID, idType: String?
    let currency, cif, name, strMsisdn: String?
    let dob, errorCode, nationality: String?
    
    let contactNumber: String?
    let accountTypeKh: String?
    let currencyKh: String?
    
    enum CodingKeys: String, CodingKey {
        case expiryDate, responseType, firstName, encoding, lastName
        case wingID = "wingId"
        case isFavorable = "is_favorable"
        case accountType, gender
        case occupationID = "occupationId"
        case idType, currency, cif, name, strMsisdn, dob, errorCode, nationality
        
        case contactNumber, accountTypeKh, currencyKh
    }
    
    func toDatasource() -> [KeyValueModel] {
        return [
            .init(key: "customer_name".localize, value: name ?? ""),
            .init(key: "phone_number".localize, value: contactNumber ?? ""),
            .init(key: "account_type".localize, value: accountTypeKh ?? ""),
            .init(key: "currency".localize, value: currencyKh ?? ""),
            .init(key: "wing_account_number".localize, value: wingID ?? "")
        ]
    }
}
