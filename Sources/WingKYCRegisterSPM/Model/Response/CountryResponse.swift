//
//  CountryResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 12/11/21.
//

import Foundation

class CountryResponse: Codable {
    var country, nationality, countryCode, seq: String?
    var isoCode, alpha2Code, numericCode, capital: String?
    var region, subRegion, timeZone, regionalBlocAcronym: String?
    var regionalBlocName: String?
    var flag: String?
    var countryKh, countryCh, nationalityKh, nationalityCh: String?
    
    func toLocalizedCountry() -> String {
        return Constants.applicationLanguage == .khmer ? (countryKh ?? "") : (country ?? "")
    }
    
    func toLocalizedNationality() -> String {
        return Constants.applicationLanguage == .khmer ? (nationalityKh ?? "") : (nationality ?? "")
    }
}

