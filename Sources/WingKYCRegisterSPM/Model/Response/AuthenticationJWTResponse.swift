//
//  AuthenticationJWTResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 12/11/21.
//

import Foundation

class AuthenticationJWTResponse: Codable {
    var token: String?
}
