//
//  LanguageEnum.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

public enum LanguageEnum {
    case english
    case khmer
    
    var languageKey: String {
        switch self {
        case .english:
            return "en-GB"
        case .khmer:
            return "km"
        }
    }
}
