//
//  OCRObjectResponse.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 18/11/21.
//

import Foundation
import UIKit

public class OCRObjectResponse {
    
    public var englishName: String?
    public var khmerName: String?
    public var idNumber: String?
    public var issueDate: String?
    public var expiryDate: String?
    public var dateOfBirth: String?
    public var placeOfBirth: String?
    public var address: String?
    
    public var gender: String?
    public var userPhoto: String?
    public var documentPhoto: String?
    
    public var surname: String?
    public var givenName: String?
    public var passportType: String?
    public var countryCode: String?
    
    public var isPassport: Bool = false
    public var isCambodian: Bool = false
    
    public var similarity: Double?
    
    public func printAll() {
        print("==> \(englishName)")
        print("==> \(khmerName)")
        print("==> \(idNumber)")
        print("==> \(issueDate)")
        print("==> \(expiryDate)")
        print("==> \(dateOfBirth)")
        print("==> \(address)")
        print("==> \(gender)")
        print("==> \(userPhoto)")
        print("==> \(documentPhoto)")
        print("==> \(surname)")
        print("==> \(givenName)")
        print("==> \(passportType)")
        print("==> \(countryCode)")
        print("==> \(similarity)")
    }
    
    public init(object: [AnyHashable: Any]?) {
        guard let object = object else { return }
        
        isPassport = object["passport_type"] != nil
        
        if isPassport {
            surname = (object["surname"] as? String)?.trimmingCharacters(in: .whitespacesAndNewlines)
            givenName = (object["given_name"] as? String)?.trimmingCharacters(in: .whitespacesAndNewlines)
            passportType = object["passport_type"] as? String
            idNumber = object["passport_number"] as? String
            countryCode = object["country_code"] as? String
            placeOfBirth = object["placeOfBirth"] as? String
            
            issueDate = KYCHelper.shared.khmerNumberToNumeric(khmer: object["dateOfIssue"] as? String ?? "")
            expiryDate = KYCHelper.shared.khmerNumberToNumeric(khmer: object["date_of_expiry"] as? String ?? "")
            dateOfBirth = KYCHelper.shared.khmerNumberToNumeric(khmer: object["date_of_birth"] as? String ?? "")
            
            if let countryCode = countryCode, countryCode == "KHM" {
                isCambodian = true
            }
        }else {
            englishName = (object["name_en"] as? String)?.trimmingCharacters(in: .whitespacesAndNewlines)
            khmerName = (object["name_kh"] as? String)?.trimmingCharacters(in: .whitespacesAndNewlines)
            idNumber = object["id_number"] as? String
            placeOfBirth = object["birth_place"] as? String
            address = object["address"] as? String
            
            issueDate = KYCHelper.shared.khmerNumberToNumeric(khmer: object["issue_date"] as? String ?? "")
            expiryDate = KYCHelper.shared.khmerNumberToNumeric(khmer: object["expiry_date"] as? String ?? "")
            dateOfBirth = KYCHelper.shared.khmerNumberToNumeric(khmer: object["birth_date"] as? String ?? "")
            
            isCambodian = true
        }
        
        if isPassport {
            issueDate = issueDate?.replacingOccurrences(of: "-", with: "/")
            expiryDate = expiryDate?.replacingOccurrences(of: "-", with: "/")
            dateOfBirth = dateOfBirth?.replacingOccurrences(of: "-", with: "/")
            
            // MARK: Reformat the date to dd/MM/yyyy
            if let expiryDate = expiryDate, let date = expiryDate.toDate(from: "yyyy/MM/dd") {
                self.expiryDate = date.toDisplayDate(format: "dd/MM/yyyy")
            }
            
            if let dateOfBirth = dateOfBirth, let date = dateOfBirth.toDate(from: "yyyy/MM/dd") {
                self.dateOfBirth = date.toDisplayDate(format: "dd/MM/yyyy")
            }
        }else {
            issueDate = issueDate?.replacingOccurrences(of: ".", with: "/")
            expiryDate = expiryDate?.replacingOccurrences(of: ".", with: "/")
            dateOfBirth = dateOfBirth?.replacingOccurrences(of: ".", with: "/")
        }
        
        gender = KYCHelper.shared.convertGenderKey(gender: object["sex"] as? String)
        userPhoto = object["livenessImageUrl"] as? String
        documentPhoto = object["cerRecognitioImageUrl"] as? String
        
        let similarityString = object["similarity"] as? String ?? ""
        similarity = Double(similarityString) ?? 0.00
    }
    
    func toRegisterUserValidateParam(completion: @escaping (Any) -> Void) {
        print(#function)
        let param = generateValidateParam(ofType: RegisterUserValidateParam.self)
        
        if !requestAddressEdit(param: param, completion: completion) {
            completion(param)
        }
    }
    
    func toUpgradeUserValidateParam(completion: @escaping (Any) -> Void) {
        let param = generateValidateParam(ofType: UpgradeUserValidateParam.self)
        
        if !requestAddressEdit(param: param, completion: completion) {
            completion(param)
        }
    }
    
    func appendRegisterUserValidateParam(origin param: RegisterUserValidateParam, completion: @escaping (Any) -> Void) {
        print(#function)
        
        appendValidateParam(param: param)
        if !requestAddressEdit(param: param, completion: completion) {
            completion(param)
        }
    }
    
    func appendUpgradeUserValidateParam(origin param: UpgradeUserValidateParam, completion: @escaping (Any) -> Void) {
        print(#function)
        
        appendValidateParam(param: param)
        
        if !requestAddressEdit(param: param, completion: completion) {
            completion(param)
        }
    }
    
    private func generateGetRegionIDByNameParam(param: RegisterUserValidateParam) -> GetRegionIDByNameParam {
        let birthAddress = AddressEditModel()
        birthAddress.province = param.birthAddress?.provinceID?.value
        birthAddress.district = param.birthAddress?.districtID?.value
        birthAddress.commune = param.birthAddress?.communeID?.value
        birthAddress.village = param.birthAddress?.villageID?.value
        
        let currentAddress = AddressEditModel()
        currentAddress.province = param.currentAddress?.provinceID?.value
        currentAddress.district = param.currentAddress?.districtID?.value
        currentAddress.commune = param.currentAddress?.communeID?.value
        currentAddress.village = param.currentAddress?.villageID?.value
        
        let param = GetRegionIDByNameParam()
        param.birthAddressEdit = birthAddress
        param.currentAddressEdit = currentAddress
        
        return param
    }
    
    private func generateValidateParam<T: RegisterUserValidateParam>(ofType: T.Type) -> T {
        let param = RegisterUserValidateParam()
        param.isPassport = isPassport
        
        if let gender = Constants.genderDatasource.filter({ $0.key == (gender ?? "") }).first {
            param.gender = gender
        }
        
        if isPassport {
            param.lastName = surname
            param.firstName = givenName
            param.idNumber = idNumber
            param.expiryDate = expiryDate
            param.dob = dateOfBirth
            param.passportCountryCode = countryCode
            param.currentAddressString = address
            
            if isCambodian {
                if let placeOfBirth = placeOfBirth {
                    let birthAddress = Address()
                    birthAddress.provinceID = .init(key: "", value: placeOfBirth)
                    
                    param.birthAddress = birthAddress
                    param.birthAddressTextModel = nil
                    param.birthAddressString = nil
                }
                
                if let country = Constants.countryListResponse.filter({ $0.isoCode == "KHM" }).first {
                    param.nationality = .init(key: country.isoCode ?? "KHM", value: country.toLocalizedNationality())
                }
            }else {
                if let placeOfBirth = placeOfBirth, placeOfBirth.isNotEmpty {
                    param.birthAddress = nil
                    param.birthAddressTextModel = nil
                    param.birthAddressString = placeOfBirth
                }
                
                if let countryCode = countryCode, let country = Constants.countryListResponse.filter({ $0.isoCode == countryCode }).first {
                    param.nationality = .init(key: country.isoCode ?? countryCode, value: country.toLocalizedNationality())
                }
            }
            
            if let idType = Constants.enumerationResponse?.idType, let passportType = idType.filter({ $0.idType == Constants.PASSPORT_KEY }).first {
                param.idType = .init(key: passportType.idType ?? "", value: passportType.idTypeDescription?.toLocalized() ?? "")
            }
        }else {
            param.lastName = KYCHelper.shared.nameSeperator(englishName ?? "").0
            param.firstName = KYCHelper.shared.nameSeperator(englishName ?? "").1
            param.lastNameKh = KYCHelper.shared.nameSeperator(khmerName ?? "").0
            param.firstNameKh = KYCHelper.shared.nameSeperator(khmerName ?? "").1
            param.idNumber = idNumber
            param.expiryDate = expiryDate
            param.dob = dateOfBirth
            param.birthAddressString = placeOfBirth
            param.currentAddressString = address
            param.birthAddress = KYCHelper.shared.mapPlaceOfBirth(from: placeOfBirth ?? "")
            param.currentAddress = KYCHelper.shared.mapCurrentAddress(from: address ?? "")
            
            if let idType = Constants.enumerationResponse?.idType, let passportType = idType.filter({ $0.idType == Constants.NID_KEY }).first {
                param.idType = .init(key: passportType.idType ?? "", value: passportType.idTypeDescription?.toLocalized() ?? "")
            }
            
            if let country = Constants.countryListResponse.filter({ $0.isoCode == "KHM" }).first {
                param.nationality = .init(key: country.isoCode ?? "KHM", value: country.toLocalizedNationality())
            }
        }
        
        if let image = userPhoto {
            param.customerImage = UIImage(contentsOfFile: image)
        }
        
        if let image = documentPhoto {
            param.identityFrontImage = UIImage(contentsOfFile: image)
        }
        
        return param as! T
    }
    
    private func appendValidateParam(param: RegisterUserValidateParam) {
        print(#function)
        
        param.isPassport = isPassport
        
        if let gender = Constants.genderDatasource.filter({ $0.key == (gender ?? "") }).first {
            param.gender = gender
        }
        
        if isPassport {
            param.lastName = surname
            param.firstName = givenName
            param.idNumber = idNumber
            param.expiryDate = expiryDate
            param.dob = dateOfBirth
            param.passportCountryCode = countryCode
            param.currentAddressString = address
            
            if isCambodian {
                if let placeOfBirth = placeOfBirth {
                    let birthAddress = Address()
                    birthAddress.provinceID = .init(key: "", value: placeOfBirth)
                    
                    param.birthAddress = birthAddress
                    param.birthAddressTextModel = nil
                    param.birthAddressString = nil
                }
                
                if let country = Constants.countryListResponse.filter({ $0.isoCode == "KHM" }).first {
                    param.nationality = .init(key: country.isoCode ?? "KHM", value: country.toLocalizedNationality())
                }
            }else {
                if let placeOfBirth = placeOfBirth, placeOfBirth.isNotEmpty {
                    param.birthAddress = nil
                    param.birthAddressTextModel = nil
                    param.birthAddressString = placeOfBirth
                }
                
                if let countryCode = countryCode, let country = Constants.countryListResponse.filter({ $0.isoCode == countryCode }).first {
                    param.nationality = .init(key: country.isoCode ?? countryCode, value: country.toLocalizedNationality())
                }
            }
            
            if let idType = Constants.enumerationResponse?.idType, let passportType = idType.filter({ $0.idType == Constants.PASSPORT_KEY }).first {
                param.idType = .init(key: passportType.idType ?? "", value: passportType.idTypeDescription?.toLocalized() ?? "")
            }
        }else {
            param.lastName = KYCHelper.shared.nameSeperator(englishName ?? "").0
            param.firstName = KYCHelper.shared.nameSeperator(englishName ?? "").1
            param.lastNameKh = KYCHelper.shared.nameSeperator(khmerName ?? "").0
            param.firstNameKh = KYCHelper.shared.nameSeperator(khmerName ?? "").1
            param.idNumber = idNumber
            param.expiryDate = expiryDate
            param.dob = dateOfBirth
            param.birthAddressString = placeOfBirth
            param.currentAddressString = address
            param.birthAddress = KYCHelper.shared.mapPlaceOfBirth(from: placeOfBirth ?? "")
            param.currentAddress = KYCHelper.shared.mapCurrentAddress(from: address ?? "")
            
            if let idType = Constants.enumerationResponse?.idType, let passportType = idType.filter({ $0.idType == Constants.NID_KEY }).first {
                param.idType = .init(key: passportType.idType ?? "", value: passportType.idTypeDescription?.toLocalized() ?? "")
            }
            
            if let country = Constants.countryListResponse.filter({ $0.isoCode == "KHM" }).first {
                param.nationality = .init(key: country.isoCode ?? "KHM", value: country.toLocalizedNationality())
            }
        }
        
        if let image = userPhoto {
            param.customerImage = UIImage(contentsOfFile: image)
        }
        
        if let image = documentPhoto {
            param.identityFrontImage = UIImage(contentsOfFile: image)
        }
    }
    
    private func requestAddressEdit(param: RegisterUserValidateParam, completion: @escaping (RegisterUserValidateParam) -> Void) -> Bool {
        print(#function)
        guard isCambodian else { return false }
        
        if isPassport {
            if param.birthAddress?.provinceID != nil  {
                let presenter = GetRegionIDByNamePresenter()
                let requestParam = generateGetRegionIDByNameParam(param: param)
                
                
                presenter.getRegionIDByName(param: requestParam) { response in
                    guard let response = response else {
                        return completion(param)
                    }
                    
                    if let address = response.toBirthAddress() {
                        param.birthAddress = address
                        param.birthAddressTextModel = nil
                        param.birthAddressString = nil
                    }else {
                        param.birthAddress = nil
                        param.birthAddressTextModel = requestParam.birthAddressEdit?.toAddressTextModel()
                        param.birthAddressString = nil
                    }
                    
                    completion(param)
                }
                
                return true
            }
            
            return false
        }else {
            if param.birthAddress?.provinceID != nil, param.currentAddress != nil {
                
                let presenter = GetRegionIDByNamePresenter()
                let requestParam = generateGetRegionIDByNameParam(param: param)
                
                presenter.getRegionIDByName(param: requestParam) { response in
                    guard let response = response else {
                        return completion(param)
                    }
                    
                    if let address = response.toBirthAddress() {
                        param.birthAddress = address
                        param.birthAddressTextModel = nil
                        param.birthAddressString = nil
                    }else {
                        param.birthAddress = nil
                        param.birthAddressTextModel = requestParam.birthAddressEdit?.toAddressTextModel()
                        param.birthAddressString = nil
                    }
                    
                    if let address = response.toCurrentAddress() {
                        param.currentAddress = address
                        param.currentAddressTextModel = nil
                        param.currentAddressString = nil
                    }else {
                        param.currentAddress = nil
                        param.currentAddressTextModel = requestParam.currentAddressEdit?.toAddressTextModel()
                        param.currentAddressString = nil
                    }
                    
                    completion(param)
                }
                return true
            }
            
            return false
        }
    }
}
