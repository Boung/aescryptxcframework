//
//  BaseParam.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 12/11/21.
//

import Foundation

class BaseParam: Codable {
    
    func toBodyParam() -> [String: Any] {
        return [:]
    }
    
    func toURLParam() -> String {
        return ""
    }
}
