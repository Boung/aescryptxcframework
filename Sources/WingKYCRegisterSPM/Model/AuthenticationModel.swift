//
//  AuthenticationModel.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 12/11/21.
//

import Foundation

public enum ApplicationTypeEnum {
    case wingEKYC
    case wcxEKYC
    case undefined
    
    var applicationID: String {
        switch self {
        case .wingEKYC:
            return "MSF_APP"
        case .wcxEKYC:
            return "WCX_PORTAL_APP"
        case .undefined:
            return ""
        }
    }

    var requestMedium: String {
        switch self {
        case .wingEKYC:
            return "MOBKYC"
        case .wcxEKYC:
            return "WCXKYC"
        case .undefined:
            return ""
        }
    }
}

public enum UserRoleEnum {
    case wcx
    case leader
    case msf
    case employee
    case iPay
    case undefined
}

public class AuthenticationModel {
    var username: String
    var password: String
    var agentAccount: AgentAccountModel
    var applicationID: ApplicationTypeEnum
    var userRole: UserRoleEnum
    
    var masterAccountID: String
    
    public init(username: String, password: String, applicationID: ApplicationTypeEnum, agentAccount: AgentAccountModel, userRole: UserRoleEnum, masterAccountID: String) {
        self.username = username
        self.password = password
        self.applicationID = applicationID
        self.agentAccount = agentAccount
        self.masterAccountID = masterAccountID
        self.userRole = userRole
    }
}

public class AgentAccountModel {
    var defaultAccount: String
    var usdAccount: String
    var khrAccount: String
    var agentName: String
    
    public init(defaultAccount: String, usdAccount: String, khrAccount: String, agentName: String) {
        self.defaultAccount = defaultAccount
        self.usdAccount = usdAccount
        self.khrAccount = khrAccount
        self.agentName = agentName
    }
}
