//
//  Constants.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

class Constants {
    static var deviceUUID: String {
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }
    
    static var appVersion: String {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
    
    static var buildVersion: String {
        return Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
    }
    
    static var bundleID: String {
        return Bundle.main.bundleIdentifier ?? ""
    }
    
    static let searchDelayDuration: TimeInterval = 0.5
    static let encodingPassword: String = "HG58YZ3CR9"
    
    static var baseURL: String = ""
    static var authentication: AuthenticationModel = .init(username: "",
                                                           password: "",
                                                           applicationID: .undefined,
                                                           agentAccount: .init(defaultAccount: "", usdAccount: "", khrAccount: "", agentName: ""),
                                                           userRole: .undefined, masterAccountID: "")
    static var googleAPIKey: String = ""
    static var accessToken: String = ""
    static var applicationLanguage: LanguageEnum = .khmer
    
    static let autoFillMockData: Bool = false
    
    static let KHMER_LANGUAGE_KEY = "km"
    static let ENGLISH_LANGUAGE_KEY = "en"
    
    static let CHANNEL_CODE = "MOBKYC"
    
    static let NID_KEY = "ID_NATIONAL"
    static let PASSPORT_KEY = "ID_PASSPORT"
    
    static var enumerationResponse: EnumerationResponse?
    static var countryListResponse: [CountryResponse] = []
    static var provinceListResponse: RegionResponse = .init()
    static var businessType: [BusinessTypeResponse] = []
    static var paymentMethod: PaymentMethodResponse = .init()
    
    static let careCenterContactNumber = "023 999 988"
    
    static let genderDatasource: [KeyValueModel] = [
        .init(key: "M", value: "male".localize),
        .init(key: "F", value: "female".localize)
    ]
    static let currencyDatasource: [KeyValueModel] = [
        .init(key: "USD", value: "usd".localize),
        .init(key: "KHR", value: "khr".localize)
    ]
    
    static let kitRegister: KeyValueModel = .init(key: "KITNUMBER", value: "request_card_kit".localize)
    static let phoneRegister: KeyValueModel = .init(key: "PHONENUMBER", value: "request_card_none".localize)
    static let requestCardDatasource: [KeyValueModel] = [
        kitRegister, phoneRegister
    ]
    
    static var pendingUploadImageParam: [PendingUploadImageParam] = [] {
        didSet {
            print("Counter: \(pendingUploadImageParam.count)")
        }
    }
    
    static var storedRegisterUserValidateParam: RegisterUserValidateParam?
    static var storedUpgradeUserValidateParam: UpgradeUserValidateParam?
    
    static var fcoTermAndConditionURLString: String {
        return "https://wingsdk.wingmoney.com:334/WingTC/fco_kh.html"
    }
}
