//
//  AlertManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 26/10/22.
//

import UIKit

struct AlertButton {
    var title: String
    var style: UIAlertAction.Style
    var handler: () -> Void
}

class AlertManager {
    static let shared: AlertManager = AlertManager()
    
    private init() {}
    
    func showDoubleButtonAlert(title: String?, message: String, firstButton: AlertButton, secondButton: AlertButton) {
        guard let topVC = UIApplication.shared.getTopViewController() else { return }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(.init(title: firstButton.title, style: firstButton.style, handler: { _ in
            firstButton.handler()
        }))
        
        alert.addAction(.init(title: secondButton.title, style: secondButton.style, handler: { _ in
            secondButton.handler()
        }))
        
        topVC.present(alert, animated: true)
    }
    
    func showSingleButtonAlert(title: String?, message: String, alertButton: AlertButton) {
        guard let topVC = UIApplication.shared.getTopViewController() else { return }
        
        let vc = BaseAlertViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.setSingleAccountAlert(title: title, messsage: message, buttonTitle: alertButton.title, buttonAction: alertButton.handler)
        
        topVC.present(vc, animated: true)
    }
    
    func showMessageAlert(_ message: String, button: AlertButton? = nil) {
            guard let topVC = UIApplication.shared.getTopViewController() else { return }
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            
            if let button = button{
                alert.addAction(.init(title: button.title, style: button.style, handler: { _ in
                    button.handler()
                }))
            }else {
                alert.addAction(.init(title: "done".localize, style: .default, handler: { _ in
                    alert.dismiss(animated: true)
                }))
            }
            
            topVC.present(alert, animated: true)
        }
}
