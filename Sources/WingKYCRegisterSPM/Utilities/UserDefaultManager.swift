//
//  UserDefaultManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 28/11/21.
//

import Foundation

struct UDKey {
    static let PendingRegisterImages = "PendingRegisterImages"
}

class UserDefaultManager {
    static var shared: UserDefaultManager {
        return UserDefaultManager()
    }
    
    private init() {}
    
    func setValue(value: Any, forKey: String) {
        UserDefaults.standard.set(value, forKey: forKey)
    }
    
    func getValue<T>(_ type: T.Type, forKey: String) -> T? {
        return UserDefaults.standard.value(forKey: forKey) as? T
    }
    
    func savePendingRegisterImages() {
        if let datas = try? JSONEncoder().encode(Constants.pendingUploadImageParam) {
            print("Constants.pendingUploadImageParam Data: \(Constants.pendingUploadImageParam.count) - \(datas)")
            UserDefaultManager.shared.setValue(value: datas, forKey: UDKey.PendingRegisterImages)
        }
    }
    
    func getPendingRegisterImages() -> [PendingUploadImageParam] {
//        print(#function)
//        print(UserDefaultManager.shared.getValue(Data.self, forKey: UDKey.PendingRegisterImages))
//        
//        if let loadedRegisterParam = UserDefaultManager.shared.getValue(Data.self, forKey: UDKey.PendingRegisterImages) {
//            if let decodedValue = try? JSONDecoder().decode([PendingUploadImageParam].self, from: loadedRegisterParam) {
//                return decodedValue
//            }
//        }
        
        return []
    }
}
