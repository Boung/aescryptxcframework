//
//  RegisterFontManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 20/8/22.
//

import UIKit

enum RegisterFontError: Error {
    case invalidFontFile
    case fontPathNotFound
    case initFontError
    case registerFailed
}

class RegisterFontManager {
    static let shared = RegisterFontManager()
    private init() {}
    
    func registerFont(withFileName fileName: String) {
        let bundle = Bundle(for: KYCRegisterSDKManager.self)
        guard let pathForResourceString = bundle.path(forResource: fileName, ofType: nil) else {
            print("UIFont+:  Failed to register font - path for resource not found.")
            return
        }
        
        guard let fontData = NSData(contentsOfFile: pathForResourceString) else {
            print("UIFont+:  Failed to register font - font data could not be loaded.")
            return
        }
        
        guard let dataProvider = CGDataProvider(data: fontData) else {
            print("UIFont+:  Failed to register font - data provider could not be loaded.")
            return
        }
        
        guard let font = CGFont(dataProvider) else {
            print("UIFont+:  Failed to register font - font could not be loaded.")
            return
        }
        
        var errorRef: Unmanaged<CFError>? = nil
        if (CTFontManagerRegisterGraphicsFont(font, &errorRef) == false) {
            print("UIFont+:  Failed to register font - register graphics font failed - this font may have already been registered in the main bundle.")
        }
    }
    
    func fontsURLs() -> [URL] {
        let bundle = Bundle(for: KYCRegisterSDKManager.self)
        let fileNames = [
            "./Resource/Fonts/Content-Bold.ttf",
            "./Resource/Fonts/KhmerOS_muol.ttf"
        ]
        
        return fileNames.map({ bundle.url(forResource: $0, withExtension: nil)! })
    }
    
    func register(from url: URL) throws {
        guard let fontDataProvider = CGDataProvider(url: url as CFURL) else {
            throw RegisterFontError.registerFailed
        }
        guard let font = CGFont(fontDataProvider) else {
            throw RegisterFontError.initFontError
        }
        
        var error: Unmanaged<CFError>?
        guard CTFontManagerRegisterGraphicsFont(font, &error) else {
            throw error!.takeUnretainedValue()
        }
    }
}
