//
//  ServerCode.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import Foundation

struct ServerCode {
    static let serverResponseOk = 200
    static let serverResponseSuccessStartRange = 200
    static let serverResponseSuccessEndRange = 299
    
    static let serverUnauthenticated = 401
    static let serverNotFound = 404
    
    static let serverBadRequest = 500
}
