//
//  LocationManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationManager()
    
    private var requestLocationAuthorizationCallback: ((CLAuthorizationStatus) -> Void)?
    var locationManager: CLLocationManager!
    var currentLocation: CLLocationCoordinate2D? {
        return locationManager.location?.coordinate
    }
    var currentStatus: CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    private var needDelegate: Bool = true
    private var onUpdateLocationHandler: ((CLLocation) -> Void)?
    private var onUpdateHeadingHandler: ((CLHeading) -> Void)?
    
    private override init() {
        super.init()
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
    }
    
    public func requestLocationAuthorization(completion: ((CLAuthorizationStatus) -> Void)?) {
        requestLocationAuthorizationCallback = completion
        
        if CLLocationManager.locationServicesEnabled() {
            switch currentStatus {
            // MARK: Only ask authorization if it was never asked before
            case .notDetermined:
                self.locationManager.requestWhenInUseAuthorization()
            case .denied, .restricted:
                guard let handler = completion else { return }
                handler(currentStatus)
            default:
                needDelegate = false
                if #available(iOS 14.0, *) {
                    let accuracyAuthorization = locationManager.accuracyAuthorization
                    
                    if accuracyAuthorization == .fullAccuracy {
                        guard let handler = requestLocationAuthorizationCallback else { return }
                        handler(currentStatus)
                        
                        return
                    }
                }
                
                guard let handler = requestLocationAuthorizationCallback else { return }
                handler(currentStatus)
            }
        }else {
            guard let handler = completion else { return }
            handler(.denied)
        }
    }
    
    func setOnUpdateLocationHandler(handler: @escaping (CLLocation) -> Void) {
        self.onUpdateLocationHandler = handler
    }
    
    func setOnUpdateHeadingHandler(handler: @escaping (CLHeading) -> Void) {
        self.onUpdateHeadingHandler = handler
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager = manager
        
        guard
            let handler = onUpdateLocationHandler,
            let lastLocation = locations.last
        else { return }
        handler(lastLocation)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        guard let handler = onUpdateHeadingHandler else { return  }
        handler(newHeading)
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        
        if #available(iOS 14.0, *) {
            let accuracyStatus = manager.accuracyAuthorization
            let authStatus = locationManager.authorizationStatus
            
            if accuracyStatus == .fullAccuracy {
                if authStatus == .authorizedWhenInUse || authStatus == .authorizedAlways {
                    if needDelegate {
                        guard let handler = requestLocationAuthorizationCallback else { return }

                        handler(currentStatus)
                    }
                }
            }else {
                needDelegate = true
                checkPreciseLocation()
            }
        }else {
            checkPreciseLocation()
        }
    }
    
    private func checkPreciseLocation() {
        guard let handler = requestLocationAuthorizationCallback else { return }
        
        if #available(iOS 14.0, *) {
            let accuracyAuthorization = locationManager.accuracyAuthorization

            switch accuracyAuthorization {
            case .fullAccuracy:
                handler(currentStatus)
            case .reducedAccuracy:
                locationManager.requestTemporaryFullAccuracyAuthorization(withPurposeKey: "RequestAccurateLocation")
            default:
                break
            }
        }else {
            handler(currentStatus)
        }
    }
    
    private func checkStatus(_ status: CLAuthorizationStatus) {
        guard status != .notDetermined else {
            return
        }
        self.requestLocationAuthorizationCallback?(status)
    }
}
