//
//  Date + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/11/21.
//

import Foundation

extension Date {
    var millisecondsSince1970: Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    var nextDay: Date {
        let calendar = Calendar.current
        
        return calendar.date(byAdding: .day, value: 1, to: self)!
    }
    
    init(millisecond: Int) {
        self = Date(timeIntervalSince1970: TimeInterval(millisecond) / 1000)
    }
    
    func toDisplayDate(format: String = "dd/MM/yyyy") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        
        return formatter.string(from: self)
    }
}


