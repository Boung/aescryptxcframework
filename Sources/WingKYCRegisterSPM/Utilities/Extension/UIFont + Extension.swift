//
//  UIFont + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 20/8/22.
//

import Foundation
import UIKit

extension UIFont {
    private static var khmerMoolFont: String {
        return "KhmerOSMuol"
    }
    
    static func setKhmerMoolFont(_ ofSize: CGFloat) -> UIFont {
        return UIFont(name: khmerMoolFont, size: ofSize)!
    }
}
