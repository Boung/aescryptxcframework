//
//  UITextField + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/11/21.
//

import UIKit

//private var _maxInputLengths = [UITextField: Int]()

extension UITextField {
    func setupKeyboardToolbar() {
        let flexibleSpace: UIBarButtonItem = .init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton: UIBarButtonItem = .init(title: "done".localize, style: .done, target: self, action: #selector(doneClick))

        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.setItems([flexibleSpace, doneButton], animated: false)

        inputAccessoryView = toolBar
    }

    @objc private func doneClick(){
        endEditing(true)
    }
    
    func addLeftView(_ view: UIView, padding: UIEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)) {
        let containerView = UIView()
        containerView.addSubview(view)
        view.fillInSuperView(padding: padding)
        
        containerView.bringSubviewToFront(view)

        leftView = containerView
        leftViewMode = .always
      }
    
    func addRightView(_ view: UIView, padding: UIEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)) {
        let containerView = UIView()
        containerView.addSubview(view)
        view.fillInSuperView(padding: padding)

        rightView = containerView
        rightViewMode = .always
      }
    
//    var maxInputLength: Int {
//        get {
//            guard let l = _maxInputLengths[self] else { return 150 }
//            return l
//        }
//        set {
//            _maxInputLengths[self] = newValue
//            addTarget(self, action: #selector(fix), for: .editingChanged)
//        }
//    }
//
//    @objc func fix(textField: UITextField) {
//        let text = textField.text!
//        textField.text = String(text.prefix(maxInputLength))
//    }
}
