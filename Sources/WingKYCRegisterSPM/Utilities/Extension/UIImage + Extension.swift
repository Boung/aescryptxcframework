//
//  UIImage + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

extension UIImage {
  static func localImage(_ named: String) -> UIImage {
    return UIImage(named: named, in: Bundle(for: KYCRegisterSDKManager.self), compatibleWith: nil) ?? UIImage()
  }
  
  func toCompressedBase64String() -> String {
    let image = fixOrientation().resizeImage()
    guard let compressedImageData = image.jpegData(compressionQuality: 0.7) else { return "" }
    
    return compressedImageData.base64EncodedString(options: .lineLength64Characters)
  }
  
  final private func cropImage(rect: CGRect) -> UIImage? {
    guard let cgImage = self.cgImage?.cropping(to: rect) else { return nil }
    let image: UIImage = UIImage(cgImage: cgImage)
    
    return image
  }
  
  final private func resizeImage(targetSize: CGSize = .init(width: 1024, height: 1024)) -> UIImage {
    let size = self.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    var newSize: CGSize
    if widthRatio > heightRatio {
      newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
      newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
    }
    
    
    let rect = CGRect(origin: .zero, size: newSize)
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    self.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage ?? UIImage()
  }
  
  final private func fixOrientation() -> UIImage {
    if imageOrientation == .up {
      return self
    }
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    var transform: CGAffineTransform = CGAffineTransform.identity
    
    if imageOrientation == .down || imageOrientation == .downMirrored {
      transform = transform.translatedBy(x: size.width, y: size.height)
      transform = transform.rotated(by: CGFloat(Double.pi))
    }
    
    if imageOrientation == .left || imageOrientation == .leftMirrored {
      transform = transform.translatedBy(x: size.width, y: 0)
      transform = transform.rotated(by: CGFloat(Double.pi / 2.0))
    }
    
    if imageOrientation == .right || imageOrientation == .rightMirrored {
      transform = transform.translatedBy(x: 0, y: size.height);
      transform = transform.rotated(by: CGFloat(-Double.pi / 2.0));
    }
    
    if imageOrientation == .upMirrored || imageOrientation == .downMirrored {
      transform = transform.translatedBy(x: size.width, y: 0)
      transform = transform.scaledBy(x: -1, y: 1)
    }
    
    if imageOrientation == .leftMirrored || imageOrientation == .rightMirrored {
      transform = transform.translatedBy(x: size.height, y: 0);
      transform = transform.scaledBy(x: -1, y: 1);
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height),
                                   bitsPerComponent: cgImage!.bitsPerComponent, bytesPerRow: 0,
                                   space: cgImage!.colorSpace!,
                                   bitmapInfo: cgImage!.bitmapInfo.rawValue)!;
    
    ctx.concatenate(transform)
    
    if imageOrientation == .left || imageOrientation == .leftMirrored || imageOrientation == .right || imageOrientation == .rightMirrored {
      ctx.draw(cgImage!, in: CGRect(x: 0,y: 0,width: size.height,height: size.width))
    } else {
      ctx.draw(cgImage!, in: CGRect(x: 0,y: 0,width: size.width,height: size.height))
    }
    
    // And now we just create a new UIImage from the drawing context and return it
    return UIImage(cgImage: ctx.makeImage()!)
  }
}
