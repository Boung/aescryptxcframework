//
//  UIButton + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/1/23.
//

import UIKit

extension UIButton {
    
    func addImagePadding(_ padding: UIEdgeInsets) {
        contentEdgeInsets = padding
    }
}
