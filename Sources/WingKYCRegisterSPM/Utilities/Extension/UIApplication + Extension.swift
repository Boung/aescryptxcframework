//
//  UIApplication + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

extension UIApplication {
    func getTopViewController(baseViewController: UIViewController? = UIApplication.shared.windows.first?.rootViewController) -> UIViewController? {
        if let navigationController = baseViewController as? UINavigationController {
            return getTopViewController(baseViewController: navigationController.visibleViewController)
        }
        
        if let tabbarController = baseViewController as? UITabBarController, let selected = tabbarController.selectedViewController {
            return getTopViewController(baseViewController: selected)
        }
        
        if let presented = baseViewController?.presentedViewController {
            return getTopViewController(baseViewController: presented)
        }
        
        return baseViewController
    }
    
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        
        urls.forEach { urlString in
            guard let url = URL(string: urlString) else { return }
            
            if application.canOpenURL(url) {
                application.open(url)
                return
            }
        }
    }
    
    func openAppSettings(completion: @escaping () -> Void) {
        guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url) { _ in
                completion()
            }
        }
    }
    
    func callNumber(phone number: String) {
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
