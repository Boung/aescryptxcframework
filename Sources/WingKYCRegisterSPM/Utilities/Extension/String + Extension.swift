//
//  String + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

extension String {
    var localize: String {
        let languageKey = Constants.applicationLanguage.languageKey
        
        let bundle = Bundle(for: KYCRegisterSDKManager.self)
        
        guard let languagePath = bundle.path(forResource: "\(languageKey).lproj", ofType: nil) else {
            return "Failed to load Language Path"
        }
        guard let languageBundle = Bundle(path: languagePath) else {
            return "Failed to load Language Bundle"
        }
        
        return languageBundle.localizedString(forKey: self, value: nil, table: nil)
    }
    
    var isNotEmpty: Bool {
        return !self.isEmpty
    }
    
    func toDictionary() -> [String: Any]? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            
            return dictionary
        } catch {
            return nil
        }
    }
    
    func toDate(from string: String = "dd/MM/yyyy") -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = string
        formatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        
        let date = formatter.date(from: self)
        return date
    }
    
    func convertBase64StringToImage() -> UIImage? {
        guard let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) else { return nil }
        
        return UIImage(data: data)
    }
    
    func removeSpecialCharacter(additionCharacter: String = "") -> String {
        let allowCharacters : Set<Character> = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890" + additionCharacter)
        return String(self.filter { allowCharacters.contains($0) })
    }
    
    func endIndex<S: StringProtocol>(of strings: [S], options: String.CompareOptions = []) -> Index? {
        var upperIndex: Index?
        for string in strings {
            upperIndex = range(of: string, options: options)?.upperBound
            if let idx = upperIndex {
                return idx
            }
        }
        return upperIndex
    }
    
    func ranges<S: StringProtocol>(of strings: [S], options: String.CompareOptions = []) -> Range<Index>? {
        var result: [Range<Index>] = []
        var startIndex = self.startIndex
        for string in strings {
            while startIndex < endIndex, let range = self[startIndex...].range(of: string, options: options) {
                result.append(range)
                startIndex = range.lowerBound < range.upperBound ? range.upperBound :
                index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
            }
        }
        return result.first
    }
}

extension String {
    static let accessToken = "AccessToken"
    static let refreshToken = "RefreshToken"
    
    static let username = "Username"
    static let password = "Password"
}

extension String {

    var toCGFloat: CGFloat {
        guard let doubleValue = Double(self) else {
            return 0.0
        }

        return CGFloat(doubleValue)
    }
}

extension String {
    func toFloatValue() -> CGFloat? {
        return CGFloat(Double(self) ?? 0.0)
    }
}

extension String {
    func appendColon() -> String {
        return self + ":"
    }
}
