//
//  Int + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/2/23.
//

import Foundation

extension Int {
    func toPrecisionString(digit: Int) -> String {
        return String(format: "%0\(digit)d", self)
    }
}
