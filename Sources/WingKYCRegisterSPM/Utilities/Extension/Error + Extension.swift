//
//  Error + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import Foundation

extension Error {
    func asNSError() -> NSError {
        return self as NSError
    }
}
