//
//  CGFloat + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

extension CGFloat {
    static var padding: CGFloat = 16

    static var smallFontSize: CGFloat = 12
    static var normalFontSize: CGFloat = 15
    static var bigFontSize: CGFloat = 18
}
