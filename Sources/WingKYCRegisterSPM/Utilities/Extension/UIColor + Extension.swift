//
//  UIColor + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

extension UIColor {
    convenience init(hexString: String) {
        var hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (hexString.hasPrefix("#")) {
            hexString.removeFirst()
        }
        
        var color: UInt64 = 0
        Scanner(string: hexString).scanHexInt64(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue = CGFloat(b) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
}

extension UIColor {
    static let primaryColor: UIColor = .init(hexString: "#A9CB37")
    static let secondaryColor: UIColor = .init(hexString: "#2C7AF6")
    static let backgroundColor: UIColor = .init(hexString: "#F5F6F8")
    static let containerBackgroundColor: UIColor = .init(hexString: "#EDF1F5")
    static let textFieldBackgroundColor: UIColor = .init(hexString: "#F5F6F8")
    
    //    static let headerBackgroundColor: UIColor = .init(hexString: "#F0F1F5")
//    static let headerTitleColor: UIColor = .init(hexString: "#7B808C")
    static let headerTitleColor: UIColor = .white
    static let headerBackgroundColor: UIColor = .secondaryColor
    
    static let addressBackgroundColor: UIColor = .init(hexString: "#F5F6F8")
    
    static let titleColor: UIColor = .init(hexString: "#ACB8C3")
    static let subTitleColor: UIColor = .init(hexString: "#777C88")
    
    static let errorTextField: UIColor = UIColor.systemRed.withAlphaComponent(0.3)
    static let blurBackground: UIColor = UIColor.black.withAlphaComponent(0.8)
    static let disabledTextFieldColor = containerBackgroundColor.withAlphaComponent(0.8)
}
