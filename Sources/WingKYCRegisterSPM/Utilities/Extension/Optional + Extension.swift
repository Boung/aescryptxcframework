//
//  Optional + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/2/23.
//

import UIKit

extension Optional where Wrapped == String {
    var orEmptyString: String {
        return self ?? ""
    }
}
