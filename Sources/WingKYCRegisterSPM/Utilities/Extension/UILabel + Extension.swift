//
//  UILabel + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

extension UILabel {
    func scaleToWidth(to factor: CGFloat = 0.2) {
        adjustsFontSizeToFitWidth = true
        minimumScaleFactor = factor
    }
}

