//
//  UINavigationController + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 28/12/21.
//

import UIKit

extension UINavigationController {
    static public func navBarHeight() -> CGFloat {
        let navigationController = UINavigationController(rootViewController: UIViewController())
        let navBarHeight = navigationController.navigationBar.frame.size.height
        return navBarHeight
    }
    
    func popTo(viewController vc: UIViewController.Type, animated: Bool = true, completion: (() -> Void)? = nil) {
        if let viewController = viewControllers.first(where: { type(of: $0) == vc }) {
            popToViewController(viewController, animated: animated)
            completion?()
        } else {
            popToRootViewController(animated: animated)
        }
    }
}
