//
//  UIImageView + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 29/1/22.
//

import UIKit

extension UIImageView {
    func loadImageFromURL(urlString: String, placeholder: UIImage? = nil) {
        let placeholder = placeholder
        image = placeholder
        
        guard let url = URL(string: urlString) else { return }
        
        getData(from: url) { data, _, error in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                guard let data = data, error == nil else {
                    self.image = placeholder
                    return
                }
                
                UIView.transition(with: self, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                    guard let self = self else { return }
                    self.image = UIImage(data: data)
                }, completion: nil)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}
