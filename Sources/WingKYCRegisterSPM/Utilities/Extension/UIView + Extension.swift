//
//  UIView + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

enum CornerEdge {
    case topLeft
    case topRight
    case bottomLeft
    case bottomRight
    
    var cornerMask: CACornerMask {
        switch self {
        case .topLeft:
            return .layerMinXMinYCorner
        case .topRight:
            return .layerMaxXMinYCorner
        case .bottomLeft:
            return .layerMinXMaxYCorner
        case .bottomRight:
            return .layerMaxXMaxYCorner
        }
    }
}

extension UIView {
    func setupCornerRadiusOnly(corners: [CornerEdge], radius: CGFloat) {
        clipsToBounds = true
        layer.cornerRadius = radius
        
        var cornerMasks: CACornerMask = []
        
        corners.forEach {
            cornerMasks.insert($0.cornerMask)
        }
        layer.maskedCorners = cornerMasks
    }
}

extension UIView {
    func setCornerRadius(radius: CGFloat = 8) {
        clipsToBounds = true
        layer.cornerRadius = radius
        layoutIfNeeded()
    }
    
    func setRoundedView() {
        layoutIfNeeded()
        clipsToBounds = true
        layer.cornerRadius = self.bounds.height / 2
        layoutIfNeeded()
    }
    
    func setBorder(width: CGFloat, color: UIColor) {
        layoutIfNeeded()
        layer.borderWidth = width
        layer.borderColor = color.cgColor
        layoutIfNeeded()
    }
    
    func setupShadowView(x: CGFloat, y: CGFloat, color: UIColor, shadowRadius: CGFloat = 5, radius: CGFloat = 0) {
        layoutIfNeeded()
        layer.cornerRadius = radius
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 0.6
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: x, height: y)
        layer.masksToBounds = false
    }
    
    func showErrorAlert(_ message: String = "error_please_fill_all_information".localize) {
        guard let topVC = UIApplication.shared.getTopViewController() else { return }
        let alert = UIAlertController(title: "error".localize, message: message, preferredStyle: .alert)
        alert.addAction(.init(title: "done".localize, style: .destructive, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        topVC.present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(_ error: Error?) {
        guard let error = error?.asNSError() else { return }
        guard let topVC = UIApplication.shared.getTopViewController() else { return }
        let alert = UIAlertController(title: "error".localize, message: error.domain, preferredStyle: .alert)
        alert.addAction(.init(title: "done".localize, style: .destructive, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        topVC.present(alert, animated: true, completion: nil)
    }
    
    func performFadeAnimate(completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .transitionCrossDissolve) {
            completion()
        }
    }
}

// MARK: Constraint
extension UIView {
    func fillInSuperView(padding: UIEdgeInsets = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let superviewTopAnchor = superview?.topAnchor {
            topAnchor.constraint(equalTo: superviewTopAnchor, constant: padding.top).isActive = true
        }
        
        if let superviewBottomAnchor = superview?.bottomAnchor {
            bottomAnchor.constraint(equalTo: superviewBottomAnchor, constant: -padding.bottom).isActive = true
        }
        
        if let superviewLeadingAnchor = superview?.leadingAnchor {
            leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: padding.left).isActive = true
        }
        
        if let superviewTrailingAnchor = superview?.trailingAnchor {
            trailingAnchor.constraint(equalTo: superviewTrailingAnchor, constant: -padding.right).isActive = true
        }
    }
    
    func anchor(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, padding: UIEdgeInsets = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom).isActive = true
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -padding.right).isActive = true
        }
    }
    
    func centerInSuperview(x: CGFloat = 0, y: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let superviewCenterXAnchor = superview?.centerXAnchor {
            centerXAnchor.constraint(equalTo: superviewCenterXAnchor, constant: x).isActive = true
        }
        
        if let superviewCenterYAnchor = superview?.centerYAnchor {
            centerYAnchor.constraint(equalTo: superviewCenterYAnchor, constant: y).isActive = true
        }
    }
    
    func centerXInSuperview() {
        translatesAutoresizingMaskIntoConstraints = false
        if let superViewCenterXAnchor = superview?.centerXAnchor {
            centerXAnchor.constraint(equalTo: superViewCenterXAnchor).isActive = true
        }
    }
    
    func centerYInSuperview() {
        translatesAutoresizingMaskIntoConstraints = false
        if let centerY = superview?.centerYAnchor {
            centerYAnchor.constraint(equalTo: centerY).isActive = true
        }
    }
    
    func centerXTo(view constraint: NSLayoutXAxisAnchor, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: constraint, constant: constant).isActive = true
    }
    
    func centerYTo(view constraint: NSLayoutYAxisAnchor, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        centerYAnchor.constraint(equalTo: constraint, constant: constant).isActive = true
    }
    
    func constrainWidth(constant: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: constant).isActive = true
    }
    
    func constrainHeight(constant: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: constant).isActive = true
    }
    
    func constraintHeightGreater(constant: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(greaterThanOrEqualToConstant: constant).isActive = true
    }
    
    func constraintHeightLessThan(constant: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(lessThanOrEqualToConstant: constant).isActive = true
    }
    
    func constraintWidthGreater(constant: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(greaterThanOrEqualToConstant: constant).isActive = true
    }
    
    func constraintWidthLessThan(constant: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(lessThanOrEqualToConstant: constant).isActive = true
    }
    
    // MARK: - Less Than Or Equal
    func lessThenOrEqualTo(top: NSLayoutYAxisAnchor, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(lessThanOrEqualTo: top, constant: constant).isActive = true
    }
    
    func lessThenOrEqualTo(leading: NSLayoutXAxisAnchor, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        leadingAnchor.constraint(lessThanOrEqualTo: leading, constant: constant).isActive = true
    }
    
    func lessThenOrEqualTo(bottom: NSLayoutYAxisAnchor, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        bottomAnchor.constraint(lessThanOrEqualTo: bottom, constant: constant).isActive = true
    }
    
    func lessThenOrEqualTo(trailing: NSLayoutXAxisAnchor, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        trailingAnchor.constraint(lessThanOrEqualTo: trailing, constant: -constant).isActive = true
    }
    
    // MARK - Greater Than Or Equal
    func greaterOrEqualTo(top: NSLayoutYAxisAnchor, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(greaterThanOrEqualTo: top, constant: constant).isActive = true
    }
    
    func greaterOrEqualTo(leading: NSLayoutXAxisAnchor, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        leadingAnchor.constraint(greaterThanOrEqualTo: leading, constant: constant).isActive = true
    }
    
    func greaterOrEqualTo(bottom: NSLayoutYAxisAnchor, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        bottomAnchor.constraint(greaterThanOrEqualTo: bottom, constant: constant).isActive = true
    }
    
    func greaterOrEqualTo(trailing: NSLayoutXAxisAnchor, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        trailingAnchor.constraint(greaterThanOrEqualTo: trailing, constant: constant).isActive = true
    }
    
    func horizonStackView(_ views: [UIView], alignment: UIStackView.Alignment = .fill) -> UIView {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = alignment
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        
        views.forEach {
            stackView.addArrangedSubview($0)
        }
        
        return stackView
    }
}
