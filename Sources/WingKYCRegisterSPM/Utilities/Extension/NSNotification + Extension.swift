//
//  NSNotification + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

extension NSNotification.Name {
    static let setReportMonthlyReport = Notification.Name.init("setReportMonthlyReport")
}
