//
//  UITableView + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/8/22.
//

import Foundation
import UIKit

extension UITableView {
    func reloadWithAnimation(_ animation: UIView.AnimationOptions = .transitionCrossDissolve) {
        UIView.transition(with: self, duration: 0.3, options: animation, animations: { [weak self] in
            guard let self = self else { return }
            
            self.reloadData()
        }, completion: nil)
    }
}
