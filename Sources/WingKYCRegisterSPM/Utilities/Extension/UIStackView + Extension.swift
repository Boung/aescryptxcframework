//
//  UIStackView + Extension.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 10/1/23.
//

import UIKit

extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
    
    func removeAllArrangedSubView() {
        arrangedSubviews.forEach {
            $0.removeFromSuperview()
        }
    }
    
    func setDefaultHorizonStackView(spacing: CGFloat = 8) {
        axis = .horizontal
        alignment = .fill
        distribution = .fill
        self.spacing = spacing
    }
    
    func setDefaultVerticalStackView(spacing: CGFloat = 8) {
        axis = .vertical
        alignment = .fill
        distribution = .fill
        self.spacing = spacing
    }
}
