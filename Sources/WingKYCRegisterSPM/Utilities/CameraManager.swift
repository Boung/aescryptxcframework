//
//  CameraManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/11/21.
//

import UIKit
import AVFoundation

class CameraManager {
    static let shared = CameraManager()
    private init() {}
    
    var grantedStatus: Bool = false
    
    func requestCameraPermission(completion: @escaping (Bool) -> Void) {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { [weak self] granted in
            guard let self = self else { return }
            self.grantedStatus = granted
            completion(granted)
        }
    }
    
}
