//
//  TimerManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 9/2/23.
//

import Foundation

class TimerManager {
    static let shared = TimerManager()
    
    private var timer: Timer?
    private var second = 0
    private init() { }
    
    func startCountDownTimer(second: Int, showHour: Bool = false, showMinute: Bool = true, handler: @escaping (String, Bool) -> Void) {
        timer?.invalidate()
        
        guard second > 0 else { return }
        self.second = second
        
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] timer in
            guard let self = self else { return }
            
            if self.second < 1 {
                handler("\(showHour ? "00".appendColon() : "")\(showMinute ? "00".appendColon() : "")00", true)
                return timer.invalidate()
            }
            
            self.second -= 1
            handler(self.secondToTimeString(second: self.second, showHour: showHour, showMinute: showMinute), false)
        })
    }
    
    func stopTimer() {
        timer?.invalidate()
    }
    
    func secondToTimeString(second: Int, showHour: Bool = false, showMinute: Bool = true) -> String {
        let hour = self.secondConverter(second).0
        let minute = self.secondConverter(second).1
        let second = self.secondConverter(second).2
        
        let hourString = hour.toPrecisionString(digit: 2)
        let minuteString = minute.toPrecisionString(digit: 2)
        let secondString = second.toPrecisionString(digit: 2)
        
        return "\(showHour ? hourString.appendColon() : "")\(showMinute ? minuteString.appendColon() : "")\(secondString)"
    }
    
    private func secondConverter(_ seconds: Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
