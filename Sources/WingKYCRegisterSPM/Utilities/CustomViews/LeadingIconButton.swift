//
//  LeadingIconButton.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 8/11/21.
//

import UIKit

class LeadingIconButton: UIView {
    
    let hStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        
        return imageView
    }()
    let label = BaseLabel()
    private let button: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        
        return button
    }()
    
    private var onTapHandler: (() -> Void)?
    
    init(iconSize: CGSize) {
        super.init(frame: .zero)
        
        addSubview(hStackView)
        hStackView.centerInSuperview()
        hStackView.greaterOrEqualTo(leading: leadingAnchor, constant: 8)
        hStackView.anchor(top: topAnchor,
                          leading: nil,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        
        iconImageView.constrainWidth(constant: iconSize.width)
        iconImageView.constrainHeight(constant: iconSize.height)
        hStackView.addArrangedSubview(iconImageView)
        hStackView.addArrangedSubview(label)
        
        addSubview(button)
        button.fillInSuperView()
        
        button.setAction { [weak self] in
            guard let self = self else { return }
            guard let handler = self.onTapHandler else { return }
            handler()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetails(_ icon: UIImage, title: String) {
        iconImageView.image = icon
        label.text = title
    }
    
    func setAction(handler: @escaping () -> Void) {
        self.onTapHandler = handler
    }
    
    func setIconColor(_ color: UIColor) {
        iconImageView.image?.withRenderingMode(.alwaysTemplate)
        iconImageView.tintColor = color
    }
    
    func setTextColor(_ color: UIColor) {
        label.textColor = color
    }
}
