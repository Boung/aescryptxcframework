//
//  BaseDatePickerTextField.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/11/21.
//

import UIKit

class BaseDatePickerTextField: BaseTextField {
    
    private let datePickerView: UIDatePicker = {
        let pickerView = UIDatePicker()
        pickerView.date = Date()
        pickerView.datePickerMode = .date
        if #available(iOS 13.4, *) {
            pickerView.preferredDatePickerStyle = .wheels
        }
        
        return pickerView
    }()
    private var displayFormat: String = "dd/MM/yyyy"
    var date: Date = Date() {
        didSet {
            textField.text = dateString
        }
    }
    var dateString: String {
        return date.toDisplayDate(format: displayFormat)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setRightViewImage(.localImage("ic_calendar"))
        
        textField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(onPickerValueChanged(_:)), for: .valueChanged)
        
        setOnTextFieldBeginEditing { [weak self] in
            guard let self = self else { return }
            
            self.textField.text = self.dateString
        }
        
        setOnTextFieldEndEditing { [weak self] in
            guard let self = self else { return }
                
            self.textField.text = self.dateString
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setDisplayFormat(_ string: String) {
        self.displayFormat = string
    }
    
    func setDateWith(string text: String, format: String = "ddMMyyyy") {
        if text.isEmpty {
            return
        }
        
        if let date = text.toDate(from: format) {
            self.date = date
            datePickerView.date = date
        }
    }
    
    @objc func onPickerValueChanged(_ sender: UIDatePicker) {
        date = sender.date
    }
}
