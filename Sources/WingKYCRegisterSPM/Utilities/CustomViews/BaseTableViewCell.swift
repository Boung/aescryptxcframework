//
//  BaseTableViewCell.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 5/11/21.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
