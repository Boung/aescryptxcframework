//
//  BaseButton.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/11/21.
//

import UIKit

class BaseButton: UIButton {
    var onButtonTapHandler: (() -> Void)!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .primaryColor
        tintColor = .white
        addTarget(self, action: #selector(onButtonTap), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setTitle(string: String) {
        setTitle(string, for: .normal)
    }
    
    func setAction(handler: @escaping () -> Void) {
        self.onButtonTapHandler = handler
    }
    
    func setDefaultHeight() {
        constrainHeight(constant: 50)
    }
    
    @objc private func onButtonTap() {
        guard let handler = onButtonTapHandler else { return }
        handler()
    }
    
    override var intrinsicContentSize: CGSize {
        var labelSize = titleLabel?.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: .greatestFiniteMagnitude)) ?? .zero
        if (labelSize.height == 0 || labelSize.width == 0) {
            labelSize = CGSize(width: 50, height: 20);
        }
        let fullWidth = labelSize.width + titleEdgeInsets.left + titleEdgeInsets.right
        let fullHeight = labelSize.height + titleEdgeInsets.top + titleEdgeInsets.bottom
        return CGSize(width: fullWidth, height: fullHeight)
    }
}
