//
//  KeyValueLabelView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/11/21.
//

import UIKit

class KeyValueLabelView: UIView {
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 4
        
        return stackView
    }()
    let keyLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .titleColor
        label.textAlignment = .left
        label.numberOfLines = 1
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        
        return label
    }()
    let valueLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .subTitleColor
        label.textAlignment = .left
        label.numberOfLines = 2
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        stackView.addArrangedSubview(keyLabel)
        stackView.addArrangedSubview(valueLabel)
        
        keyLabel.constraintWidthGreater(constant: 100)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDetails(_ key: String, value: String, hideOnEmpty: Bool = true) {
        keyLabel.text = key
        valueLabel.text = value
        
        isHidden = value.isEmpty && hideOnEmpty
    }
    
    func setTextAlign(_ key: NSTextAlignment, value: NSTextAlignment) {
        keyLabel.textAlignment = key
        valueLabel.textAlignment = value
    }
    
    func setColor(_ key: UIColor, value: UIColor) {
        keyLabel.textColor = key
        valueLabel.textColor = value
    }
    
    func setFont(_ key: UIFont, value: UIFont) {
        keyLabel.font = key
        valueLabel.font = value
    }
    
    func setNumberOfLine(_ key: Int, value: Int) {
        keyLabel.numberOfLines = key
        valueLabel.numberOfLines = value
    }
    
    func setLineBreakMode(_ key: NSLineBreakMode = .byWordWrapping, value: NSLineBreakMode = .byWordWrapping) {
        keyLabel.lineBreakMode = key
        valueLabel.lineBreakMode = value
    }
}
