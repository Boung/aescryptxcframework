//
//  ScrollViewContainerView.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 7/11/21.
//

import UIKit

class ScrollViewContainerView: UIView {
    
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        
        return scrollView
    }()
    let containerView = ContainerView(background: .clear)
    
    init(withScreenWidth: Bool = true) {
        super.init(frame: .zero)
        
        addSubview(scrollView)
        scrollView.fillInSuperView()
        
        scrollView.addSubview(containerView)
        containerView.fillInSuperView()
        
        if withScreenWidth {
            containerView.constrainWidth(constant: UIConstants.screenWidth)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
