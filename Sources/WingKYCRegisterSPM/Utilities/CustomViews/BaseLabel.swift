//
//  BaseLabel.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

class BaseLabel: UILabel {
    private static let isKhmerLanguage = Constants.applicationLanguage.languageKey == Constants.KHMER_LANGUAGE_KEY
    
    var textInsets = UIEdgeInsets(top: 0, left: 0, bottom: isKhmerLanguage ? 2 : 0, right: 0) {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        textColor = .subTitleColor
        textAlignment = .left
        font = .systemFont(ofSize: 15)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let textRect = super.textRect(forBounds: bounds, limitedToNumberOfLines: numberOfLines)
        let invertedInsets: UIEdgeInsets = .init(top: -textInsets.top,
                                                 left: -textInsets.left,
                                                 bottom: -textInsets.bottom,
                                                 right: -textInsets.right)
        return textRect.inset(by: invertedInsets)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textInsets))
    }
    
    func addStar() {
        guard var text = text else { return }
        
        if let last = text.last, last == "*" {
            text.removeLast()
        }
        
        let string = text.trimmingCharacters(in: .whitespacesAndNewlines)
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: string + " *")
        attributedString.addAttribute(.foregroundColor, value: UIColor.systemRed, range: .init(location: attributedString.length - 1, length: 1))
        
        self.attributedText = attributedString
    }
}
