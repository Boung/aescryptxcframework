//
//  BaseRadioButton.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 27/1/22.
//

import Foundation
import UIKit

class BaseRadioButton: UIView {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 8
        
        return stackView
    }()
    private let radioButtonImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        imageView.tintColor = .secondaryColor
        
        return imageView
    }()
    private let titleLabel = BaseLabel()
    
    private var onTapHandler: (() -> Void)?
    var isSelected: Bool = false {
        didSet {
            let selectedIcon: UIImage = .localImage("ic_radio_button_selected").withRenderingMode(.alwaysTemplate)
            let deSelectedIcon: UIImage = .localImage("ic_radio_button_deselected").withRenderingMode(.alwaysTemplate)
            
            radioButtonImage.image = isSelected ? selectedIcon : deSelectedIcon
        }
    }
        
    init(size: CGFloat = 30) {
        super.init(frame: .zero)
        
        radioButtonImage.constrainWidth(constant: size)
        radioButtonImage.constrainHeight(constant: size)
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        stackView.addArrangedSubview(radioButtonImage)
        stackView.addArrangedSubview(titleLabel)
        setDefaultSeleceted(bool: false)
        
        titleLabel.isHidden = true
        isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onRadioButtonTap))
        addGestureRecognizer(tapGesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func onRadioButtonTap() {
        guard let onTapHandler = onTapHandler else { return }
        onTapHandler()
    }
    
    func setTitle(_ string: String) {
        titleLabel.text = string
        titleLabel.isHidden = false
    }
    
    func setNumberOfLines(_ int: Int) {
        titleLabel.numberOfLines = int
        titleLabel.scaleToWidth(to: 0.5)
    }
    
    func setDefaultSeleceted(bool: Bool = true) {
        isSelected = bool
    }
    
    func setAction(handler: @escaping () -> Void) {
        self.onTapHandler = handler
    }
}
