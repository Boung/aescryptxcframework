//
//  AvailabilityRadioButton.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 27/1/22.
//

import Foundation
import UIKit

class AvailabilityRadioButton: UIView {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 16
        
        return stackView
    }()
    
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .titleColor
        
        return label
    }()
    private let yesRadioButton: BaseRadioButton = {
        let radioButton = BaseRadioButton()
        radioButton.setTitle("yes".localize)
        radioButton.setDefaultSeleceted()
        
        return radioButton
    }()
    private let noRadioButton: BaseRadioButton = {
        let radioButton = BaseRadioButton()
        radioButton.setTitle("no".localize)
        
        return radioButton
    }()
    
    private var onValueChangeHandler: ((Bool) -> Void)?
    var isYes: Bool = true {
        didSet {
            yesRadioButton.isSelected = isYes
            noRadioButton.isSelected = !isYes
            postOnValueChangeAction(bool: isYes)
        }
    }
    var isRequired: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(stackView)
        stackView.fillInSuperView()
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(yesRadioButton)
        stackView.addArrangedSubview(noRadioButton)
        
        yesRadioButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.isYes = true
        }
        
        noRadioButton.setAction { [weak self] in
            guard let self = self else { return }
            
            self.isYes = false
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setTitle(_ string: String, isRequired: Bool = false) {
        titleLabel.text = string
        
        if isRequired {
            titleLabel.addStar()
        }
    }
    
    func setOnValueChanged(handler: @escaping (Bool) -> Void) {
        onValueChangeHandler = handler
    }
    
    func setDefaultSelection(value bool: Bool) {
        yesRadioButton.setDefaultSeleceted(bool: bool)
        noRadioButton.setDefaultSeleceted(bool: !bool)
        postOnValueChangeAction(bool: bool)
    }
    
    func toggleEnable(_ bool: Bool) {
        [yesRadioButton, noRadioButton].forEach {
            $0.isUserInteractionEnabled = bool
            $0.alpha =  bool ? 1 : 0.5
        }
    }
    
    private func postOnValueChangeAction(bool: Bool) {
        guard let onValueChangeHandler = onValueChangeHandler else { return }
        onValueChangeHandler(bool)
    }
}
