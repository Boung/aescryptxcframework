//
//  BaseTextField.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 4/11/21.
//

import UIKit

class BaseTextField: UIView {
    
    var containerView = TitleComponentWrapperView()
    let textField: UITextField = {
        let textField = UITextField()
        textField.font = .systemFont(ofSize: 15)
        textField.backgroundColor = .textFieldBackgroundColor
        textField.borderStyle = .none
        textField.spellCheckingType = .no
        textField.autocorrectionType = .no
        textField.setupKeyboardToolbar()
        textField.constrainHeight(constant: 50)
        
        return textField
    }()
    
    let rightImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.constrainWidth(constant: 15)
        imageView.constrainHeight(constant: 15)
        
        return imageView
    }()
    private let leftPaddingView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.constrainWidth(constant: 8)
        view.constrainHeight(constant: 1)
        
        return view
    }()
    
    var maxInputLength: Int = 0
    var textFieldBackgroundColor: UIColor = .textFieldBackgroundColor {
        didSet {
            textField.backgroundColor = textFieldBackgroundColor
        }
    }
    private var onTextFieldBeginEditing: (() -> Void)?
    private var onTextFieldEndEditing: (() -> Void)?
    private var onValueChanged: ((String) -> Void)?
    private var onRightImageViewTapHandler: (() -> Void)?
    
    var isRequired: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        containerView.backgroundColor = .clear
        
        setTextFieldPlaceholder("please_enter".localize)
        
        textField.delegate = self
        textField.textColor = .subTitleColor
        textField.addLeftView(leftPaddingView, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        textField.setCornerRadius(radius: 10)
        
        addSubview(containerView)
        containerView.fillInSuperView()
        
        containerView.addCustomView(view: textField)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onRightImageViewTap))
        rightImageView.isUserInteractionEnabled = true
        rightImageView.addGestureRecognizer(tapGesture)
        
        textField.addTarget(self, action: #selector(onValueChangedHandler), for: .editingChanged)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setPlaceholder(_ string: String, isRequired: Bool = false) {
        self.isRequired = isRequired
        containerView.setPlaceholder(string, isRequired: isRequired)
    }
    
    func setPlaceholderNoTitle(_ string: String) {
        textField.placeholder = string
    }
    
    func setRightViewImage(_ image: UIImage, color: UIColor = .titleColor) {
        rightImageView.image = image.withRenderingMode(.alwaysTemplate)
        rightImageView.tintColor = color
        
        textField.addRightView(rightImageView, padding: .init(top: 0, left: 0, bottom: 0, right: 8))
    }
    
    func toggleEnable(_ bool: Bool) {
        isUserInteractionEnabled = bool
        textField.backgroundColor = bool ? textFieldBackgroundColor : .disabledTextFieldColor
    }
    
    func setText(_ string: String?) {
        textField.text = string
    }
    
    func setErrorTextField(_ bool: Bool) {
        textField.setBorder(width: 1, color: bool ? .errorTextField : textFieldBackgroundColor)
    }
    
    func setRightImageViewAction(handler: @escaping () -> Void) {
        onRightImageViewTapHandler = handler
    }
    
    func setOnTextFieldBeginEditing(handler: @escaping () -> Void) {
        onTextFieldBeginEditing = handler
    }
    
    func setOnTextFieldEndEditing(handler: @escaping () -> Void) {
        onTextFieldEndEditing = handler
    }
    
    func onValueChanged(handler: @escaping (String) -> Void) {
        onValueChanged = handler
    }
    
    @objc private func onRightImageViewTap() {
        guard let handler = onRightImageViewTapHandler else { return }
        handler()
    }
    
    @objc private func onValueChangedHandler(_ textField: UITextField) {
        guard let handler = onValueChanged else { return }
        
        let text = textField.text ?? ""
        handler(text)
    }
    
    func setTextFieldPlaceholder(_ string: String) {
        textField.attributedPlaceholder = NSAttributedString(
            string: string,
            attributes: [
                .foregroundColor: UIColor.titleColor.withAlphaComponent(0.5),
                .font: UIFont.systemFont(ofSize: 13)
            ]
        )
    }
}

extension BaseTextField: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let handler = onTextFieldBeginEditing else { return }
        handler()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let handler = onTextFieldEndEditing else { return }
        handler()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if maxInputLength == 0 {
            return true
        }
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= maxInputLength
    }
}
