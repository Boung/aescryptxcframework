//
//  DropdownTextFieldSelectionViewController.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 5/11/21.
//

import UIKit

protocol DropdownTextFieldDelegate {
    func dropdownTextField(did selected: KeyValueModel)
}

class DropdownTextFieldSelectionViewController: BaseViewController {
    
    private let containerView = ContainerView(background: .white)
    private let tableView = UITableView()
    private let headerContainerView = ContainerView(background: .clear)
    private let titleLabel: BaseLabel = {
        let label = BaseLabel()
        label.textColor = .subTitleColor
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 18)
        
        return label
    }()
    private let closeButton: BaseButton = {
        let button = BaseButton()
        button.backgroundColor = .clear
        
        return button
    }()
    
    var datasources: [KeyValueModel] = []
    var selectedDatasource: KeyValueModel?
    var delegate: DropdownTextFieldDelegate?
    var placeholder: String = ""
    private let cellIdentifier = "DropdownSelectionTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
//            guard let self = self else { return }
//
//            self.setupBlurBackground()
//        }
        
        if let selectedDatasource = selectedDatasource  {
            datasources.first { $0.key == selectedDatasource.key }?.isSelected = true
            tableView.reloadData()
        }
        
        titleLabel.text = placeholder
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.register(DropdownSelectionTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        
        closeButton.setAction { [weak self] in
            guard let self = self else { return }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func generateUI() {
        super.generateUI()
        
        view.addSubview(containerView)
        containerView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: view.bottomAnchor,
                             trailing: view.trailingAnchor)
        
        containerView.addSubview(headerContainerView)
        headerContainerView.constrainHeight(constant: 50)
        headerContainerView.anchor(top: containerView.topAnchor,
                                   leading: containerView.leadingAnchor,
                                   bottom: nil,
                                   trailing: containerView.trailingAnchor)
                
        headerContainerView.addSubview(titleLabel)
        titleLabel.centerInSuperview()
        titleLabel.anchor(top: nil,
                          leading: headerContainerView.leadingAnchor,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: 0, left: 8, bottom: 0, right: 0))
        
        containerView.addSubview(tableView)
        tableView.anchor(top: headerContainerView.bottomAnchor,
                         leading: containerView.leadingAnchor,
                         bottom: view.layoutMarginsGuide.bottomAnchor,
                         trailing: containerView.trailingAnchor,
                         padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        
        view.addSubview(closeButton)
        closeButton.anchor(top: view.topAnchor,
                           leading: view.leadingAnchor,
                           bottom: containerView.topAnchor,
                           trailing: view.trailingAnchor,
                           padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        
        let ROW_HEIGHT: CGFloat = 60
        let MAX_HEIGHT: CGFloat = UIConstants.screenHeight * 0.7
        let height: CGFloat = ROW_HEIGHT * CGFloat(datasources.count)
        
        if height > MAX_HEIGHT {
            tableView.constrainHeight(constant: MAX_HEIGHT)
        }else {
            tableView.constrainHeight(constant: height)
        }
            
        containerView.constraintHeightLessThan(constant: MAX_HEIGHT)
        containerView.setupCornerRadiusOnly(corners: [.topLeft, .topRight], radius: 16)
        setupBlurBackground()
    }
}

extension DropdownTextFieldSelectionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DropdownSelectionTableViewCell
        cell.configCell(datasources[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        datasources.forEach { $0.isSelected = false }
        datasources[indexPath.row].isSelected = true
        tableView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                
                self.delegate?.dropdownTextField(did: self.datasources[indexPath.row])
            }
        }
    }
}
