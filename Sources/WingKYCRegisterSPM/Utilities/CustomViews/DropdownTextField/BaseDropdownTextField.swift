//
//  BaseDropdownTextField.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 5/11/21.
//

import UIKit

class KeyValueModel {
    var key: String
    var value: String
    
    var icon: UIImage? = nil
    var isSelected: Bool = false
    
    var additionalValue: Any?
    
    init(key: String, value: String, isSelected: Bool? = false) {
        self.key = key
        self.value = value
        self.isSelected = isSelected ?? false
    }
    
    init(icon: UIImage?, value: String) {
        self.key = ""
        self.value = value
        self.icon = icon
    }
    
    static func mockupDatas() -> [KeyValueModel] {
        return [
            .init(key: "1", value: "One"),
            .init(key: "2", value: "Two"),
            .init(key: "3", value: "Three"),
        ]
    }
}

class BaseDropdownTextField: BaseTextField {
    
    var datasources: [KeyValueModel] = []
    var selectedDatasource: KeyValueModel? {
        didSet {
            textField.text = selectedDatasource?.value
        }
    }
    var placeholder: String = ""
    private var onValueSelected: ((KeyValueModel) -> Void)?
    private var onNoDatasourceHandler: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setTextFieldPlaceholder("please_select".localize)
        
        textField.inputView = UIView()
        setRightViewImage(.localImage("ic_dropdown"))
        setRightImageViewAction { [weak self] in
            guard let self = self else { return }
            
            self.textField.becomeFirstResponder()
        }
        
        textField.isEnabled = false
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
        isUserInteractionEnabled = true
        addGestureRecognizer(tapGesture)
    }
    
    @objc func onTap() {
        if datasources.isEmpty {
            onNoDatasourceHandler?()
        }else {
            presentSelectionView()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setPlaceholder(_ string: String, isRequired: Bool = false) {
        placeholder = string
        
        super.setPlaceholder(string, isRequired: isRequired)
    }
    
    func setDatasource(_ datasources: [KeyValueModel]) {
        self.datasources = datasources
        
        if let defaultSelect = datasources.first(where: { $0.isSelected }) {
            setDefault(value: .init(key: defaultSelect.key, value: defaultSelect.value))
        }
    }
    
    func presentSelectionView() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            guard let topVC = UIApplication.shared.getTopViewController() else { return }
            topVC.view.endEditing(true)
            
            let vc = DropdownTextFieldSelectionViewController()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.delegate = self
            vc.datasources = self.datasources
            vc.placeholder = self.placeholder
            vc.selectedDatasource = self.selectedDatasource
            
            topVC.present(vc, animated: true, completion: nil)
        }
    }
    
    func setOnValueSelected(handler: @escaping (KeyValueModel) -> Void) {
        onValueSelected = handler
    }
    
    func setOnNoDatasourceAction(_ handler: @escaping () -> Void) {
        self.onNoDatasourceHandler = handler
    }
    
    func setDefault(value model: KeyValueModel) {
        selectedDatasource = model
    }
    
    func setValueWhere(index int: Int) {
        if datasources.isEmpty {
            return
        }
        
        if int < 0 || int >= datasources.count {
            return
        }
        
        selectedDatasource = datasources[int]
        onValueSelected?(datasources[int])
    }
    
    func setValueWhere(key string: String) {
        guard string.isNotEmpty else { return }
        guard let selected = datasources.filter({ $0.key == string }).first else { return }
        selectedDatasource = selected
        
        guard let handler = onValueSelected else { return }
        handler(selected)
    }
    
    func reset() {
        selectedDatasource = nil
    }
}

extension BaseDropdownTextField: DropdownTextFieldDelegate {
    func dropdownTextField(did selected: KeyValueModel) {
        selectedDatasource = selected
        
        guard let handler = onValueSelected else { return }
        handler(selected)
    }
}
