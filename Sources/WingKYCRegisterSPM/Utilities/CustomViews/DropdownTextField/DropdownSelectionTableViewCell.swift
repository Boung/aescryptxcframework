//
//  DropdownSelectionTableViewCell.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 5/11/21.
//

import UIKit

class DropdownSelectionTableViewCell: BaseTableViewCell {
    
    private let valueLabel: BaseLabel = {
        let label = BaseLabel()
        label.font = .systemFont(ofSize: 18)
        label.numberOfLines = 2
        label.scaleToWidth(to: 0.7)
        
        return label
    }()
    private let radioImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .localImage("ic_radio_button_deselected").withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .secondaryColor
        imageView.constrainWidth(constant: 30)
        imageView.constrainHeight(constant: 30)
        return imageView
    }()
    
    private let selectedImage: UIImage = .localImage("ic_radio_button_selected").withRenderingMode(.alwaysTemplate)
    private let deSelectedImage: UIImage = .localImage("ic_radio_button_deselected").withRenderingMode(.alwaysTemplate)

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        
        addSubview(radioImageView)
        radioImageView.centerYInSuperview()
        radioImageView.anchor(top: nil,
                              leading: nil,
                              bottom: nil,
                              trailing: trailingAnchor,
                              padding: .init(top: 0, left: 0, bottom: 0, right: 16))
        
        addSubview(valueLabel)
        valueLabel.anchor(top: topAnchor,
                          leading: leadingAnchor,
                          bottom: bottomAnchor,
                          trailing: radioImageView.leadingAnchor,
                          padding: .init(top: 16, left: 16, bottom: 16, right: 8))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configCell(_ datasource: KeyValueModel) {
        self.valueLabel.text = datasource.value
        self.radioImageView.image = datasource.isSelected ? selectedImage : deSelectedImage
    }
}
