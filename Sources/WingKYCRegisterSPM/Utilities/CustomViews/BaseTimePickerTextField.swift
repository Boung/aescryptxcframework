//
//  BaseTimePickerTextField.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/1/23.
//

import UIKit

class BaseTimePickerTextField: BaseTextField {
    
    let timePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        picker.timeZone = TimeZone(abbreviation: "GMT+0:00")
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }
        
        return picker
    }()
    
    var time: Date? {
        return timePicker.date
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        textField.inputView = timePicker
        
        setOnTextFieldEndEditing { [weak self] in
            guard let self = self else { return }
            
            self.textField.text = self.timePicker.date.toDisplayDate(format: "HH:mm")
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setDefaultTime(_ time: String) {
        guard let time = time.toDate(from: "HH:mm") else { return }
        
        timePicker.date = time
        textField.text = time.toDisplayDate(format: "HH:mm")
    }
}
