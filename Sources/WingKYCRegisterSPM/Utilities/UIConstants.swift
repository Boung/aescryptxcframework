//
//  UIConstants.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import UIKit

class UIConstants {
    static var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    static var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    static var navigationHeight: CGFloat {
        return UINavigationController.navBarHeight()
    }
    
    static var topSafeArea: CGFloat {
        return UIApplication.shared.windows.first?.safeAreaInsets.top ?? 0
    }
    
    static var bottomSafeArea: CGFloat {
        return UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0
    }
    
    static let markerSize: CGFloat = 40
}
