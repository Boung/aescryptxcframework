//
//  EncodingManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 16/11/21.
//

import Foundation
import WingKYCRegisterFormSDK

class EncodingManager {
  static let shared = EncodingManager()

  private init() {
  }

  func encode(string: String) -> String? {
    return AESEncodingManager.encode(string: string, password: Constants.encodingPassword)
  }

  func decode(encoded string: String) -> String? {
    return AESEncodingManager.decode(encoded: string, password: Constants.encodingPassword)
  }
}
