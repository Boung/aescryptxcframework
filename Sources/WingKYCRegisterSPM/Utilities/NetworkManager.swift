//
//  NetworkManager.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 3/11/21.
//

import Foundation

class NetworkManager {
    static let shared = NetworkManager()
    
    private var reachability = Reachability()
    private init() {}
    
    func isConnected() -> Bool {
        guard let isReachable = reachability?.isReachable else {
            return false
        }
        
        return isReachable
    }
}
