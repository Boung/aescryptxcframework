//
//  KYCHelper.swift
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 25/11/21.
//

import Foundation

class KYCHelper {
    static let shared = KYCHelper()
    
    private init() {}
    
    func nameSeperator(_ name: String) -> (String, String) {
        let splitedName = name.components(separatedBy: " ")
        
        if splitedName.count > 1 {
            return (splitedName[0], splitedName[1])
        }else {
            return (splitedName[0], "")
        }
    }
    
    func khmerNumberToNumeric(khmer text: String) -> String {
        let khmerNumber = ["១", "២", "៣", "៤", "៥", "៦", "៧", "៨", "៩", "០"]
        let numeric = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
        
        var output = ""
        
        text.enumerated().forEach { original in
            if khmerNumber.contains("\(original.element)") {
                khmerNumber.enumerated().forEach { value in
                    if "\(original.element)" == value.element {
                        output.append("\(original.element)".replacingOccurrences(of: value.element, with: numeric[value.offset]))
                        return
                    }
                }
            }else {
                output.append("\(original.element)")
            }
        }
        
        return output
    }
    
    func convertGenderKey(gender string: String?) -> String {
        guard let string = string else { return "" }
        
        let maleOptions = ["ប្រុស", "male", "m"]
        let femaleOptions = ["ស្រី", "female", "f"]
        
        if maleOptions.contains(string.lowercased()) {
            return "M"
        }
        
        if femaleOptions.contains(string.lowercased()) {
            return "F"
        }
        
        return ""
    }
    
    
    func isValid(phoneNumber: String) -> Bool {
        let PHONE_REGEX = "^(\\+855|0)[1-9]{1}[0-9]{7,8}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: phoneNumber)
        
        return result
    }
    
    func isValid(age fromDate: Date) -> Bool{
        // MARK: - Confirmation from B Soung to validate Year only. (Ignore Day and Month)
        let yearString = fromDate.toDisplayDate(format: "yyyy")
        let yearDate = yearString.toDate(from: "yyyy") ?? Date()
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year], from: yearDate, to: Date())
        
        // MARK: - Valid Year is >= 16
        let result = components.year! > 15
        
        return result
    }
    
    func isValid(expiry date: Date) -> Bool {
        return date.millisecondsSince1970 >= Date().millisecondsSince1970
    }
    
    private let splitCommune = ["ឃុំ", "សង្កាត់"]
    private let splitDistict = ["ស្រុក", "ក្រុង", "ខណ្ឌ"]
    
    func mapPlaceOfBirth(from birthPlace: String) -> Address {
        let address = Address()
        
        let communeIndex = birthPlace.endIndex(of: splitCommune)
        let districtIndex = birthPlace.ranges(of: splitDistict)
        
        // find commnue
        if let firstIdx = communeIndex, let endIdx = districtIndex, firstIdx <= endIdx.lowerBound {
            let commune = birthPlace[firstIdx..<endIdx.lowerBound]
            address.communeID = .init(key: "", value: String(commune).trimmingCharacters(in: .whitespacesAndNewlines))
        }
        // find district and province
        if let start = districtIndex, start.upperBound <= birthPlace.endIndex {
            let addr = birthPlace[start.upperBound..<birthPlace.endIndex]
            let split = addr.components(separatedBy: " ")
            if split.count > 0 {
                address.districtID = .init(key: "", value: String(split[0]))
                
                if split.count >= 2 {
                    address.districtID = .init(key: "", value: String(split[0]))
                    address.provinceID = .init(key: "", value: String(split[1]))
                }
            }
        }
        
        // In case above map failed because of format place of birth from NID are different format
        if address.toParam().isEmpty || address.toParam().count == 1  { // MARK: - "address.toParam().count == 1" for only CBCCode
            let arrAddress = birthPlace.components(separatedBy: " ").reversed()
            arrAddress.enumerated().forEach { index, item in
                switch index {
                case 0:
                    address.provinceID = .init(key: "", value: item)
                case 1:
                    address.districtID = .init(key: "", value: item)
                default:
                    if let commune =  address.communeID {
                        address.communeID = .init(key: "", value: "\(item) \(commune.value)")
                    } else {
                        address.communeID = .init(key: "", value: item)
                    }
                }
            }
        }
        
        return address
    }
    
    func mapCurrentAddress(from currentAddress: String) -> Address {
        let address = Address()
        
        let villIndex = currentAddress.range(of: "ភូមិ")
        let communeIndex = currentAddress.ranges(of: splitCommune)
        let districtIndex = currentAddress.ranges(of: splitDistict)
        
        if let start = villIndex, let end = communeIndex, start.upperBound <= end.lowerBound {
            let village = currentAddress[start.upperBound..<end.lowerBound]
            address.villageID = .init(key: "", value: String(village).trimmingCharacters(in: .whitespacesAndNewlines))
        }
        
        if let start = communeIndex, let end = districtIndex, start.upperBound <= end.lowerBound {
            let commune = currentAddress[start.upperBound..<end.lowerBound]
            address.communeID = .init(key: "", value: String(commune).trimmingCharacters(in: .whitespacesAndNewlines))
        }
        
        if let start = districtIndex, start.upperBound <= currentAddress.endIndex {
            let addr = currentAddress[start.upperBound..<currentAddress.endIndex]
            let split = addr.components(separatedBy: " ")
            if split.count > 0 {
                address.districtID = .init(key: "", value: String(split[0]).trimmingCharacters(in: .whitespacesAndNewlines))
                
                if split.count >= 2 {
                    address.districtID = .init(key: "", value: String(split[0]).trimmingCharacters(in: .whitespacesAndNewlines))
                    address.provinceID = .init(key: "", value: String(split[1]).trimmingCharacters(in: .whitespacesAndNewlines))
                }
            }
        }
        
        return address
    }
}
