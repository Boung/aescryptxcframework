//
//  WingKYCRegisterFormSDK.h
//  WingKYCRegisterFormSDK
//
//  Created by Wing Specialized Bank on 2/11/21.
//

#import <Foundation/Foundation.h>
#import "AESCrypt.h"
#import "Jwt.h"

//! Project version number for WingKYCRegisterFormSDK.
FOUNDATION_EXPORT double WingKYCRegisterFormSDKVersionNumber;

//! Project version string for WingKYCRegisterFormSDK.
FOUNDATION_EXPORT const unsigned char WingKYCRegisterFormSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WingKYCRegisterFormSDK/PublicHeader.h>


